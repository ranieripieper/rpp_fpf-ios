//
//  Team.swift
//  FPF
//
//  Created by Gilson Gil on 1/8/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Parse

final class Team: PFObject {
  var city: String? {
    get {
      return self["Cidade"] as? String
    }
  }
  var fantasy: String? {
    get {
      return self["EquipeFantasia"] as? String
    }
  }
  var state: String? {
    get {
      return self["Estado"] as? String
    }
  }
  var id: Int? {
    get {
      return self["Id"] as? Int
    }
  }
  var name: String? {
    get {
      return self["Nome"] as? String
    }
  }
  var country: String? {
    get {
      return self["Pais"] as? String
    }
  }
  var shortName: String? {
    get {
      return self["Sigla"] as? String
    }
  }
  var hasCompleteStats: Bool? {
    get {
      return self["TemEstatisticaCompleta"] as? Bool
    }
  }
  var logoURLString: String? {
    get {
      return self["URLLogo"] as? String
    }
  }
  var hasPush: Bool? {
    get {
      return self["Push"] as? Bool
    }
  }
}

extension Team {
  static func logoURL(teamId: Int, completion: (inner: () throws -> String) -> ()) {
    guard let query = Team.query() else {
      completion {throw ModelError.Unknown}
      return
    }
    query.fromLocalDatastore()
    query.whereKey("Id", equalTo: teamId)
    query.limit = 1000
    query.getFirstObjectInBackground().continueWithBlock { task in
      guard task.error == nil else {
        completion {throw ModelError.Unknown}
        return task
      }
      guard let team = task.result as? Team else {
        completion {throw ModelError.Unknown}
        return nil
      }
      guard let logoURLString = team.logoURLString else {
        completion {throw ModelError.Unknown}
        return nil
      }
      completion {return logoURLString}
      return team
    }
  }
  
  static func allLogosAndHasStats(leagueId: Int?, completion: ([Int: (String, Bool)]?) -> ()) {
    guard let query = Team.query() else {
      completion(nil)
      return
    }
    query.fromLocalDatastore()
    query.limit = 1000
    if let leagueId = leagueId {
      query.whereKey("IdCampeonato", equalTo: leagueId)
    }
    query.findObjectsInBackgroundWithBlock { teams, error in
      guard error == nil else {
        completion(nil)
        return
      }
      guard let teams = teams as? [Team] else {
        completion(nil)
        return
      }
      completion(teams.logoURLHasStatsDictionary)
    }
  }
}

extension Team {
  static func shouldRefreshTeams(completion: Bool -> ()) {
    if let currentVersion = NSBundle.mainBundle().infoDictionary?["CFBundleVersion"] as? String {
      guard let persistedVersion = NSUserDefaults.standardUserDefaults().objectForKey("CFBundleVersion") as? String where persistedVersion == currentVersion else {
        NSUserDefaults.standardUserDefaults().setObject(currentVersion, forKey: "CFBundleVersion")
        NSUserDefaults.standardUserDefaults().synchronize()
        completion(true)
        return
      }
    }
    let query = Team.query()!
    query.fromLocalDatastore()
    query.getFirstObjectInBackgroundWithBlock {
      if let _ = $0.0 {
        let lastRefreshDate = NSUserDefaults.standardUserDefaults().floatForKey("LastTeamRefreshDate")
        if NSDate(timeIntervalSince1970: NSTimeInterval(lastRefreshDate + 60 * 60 * 24)).compare(NSDate()) == .OrderedAscending {
          completion(true)
        } else {
          completion(false)
        }
      } else {
        completion(true)
      }
    }
  }
  
  static func teams(completion: () -> ()) {
    if Reachability.isConnectedToNetwork() {
      fetchTeams(completion)
    } else {
      getTeams(completion)
    }
  }
  
  static func getTeams(completion: () -> ()) {
    guard let query = Team.query() else {
      completion()
      return
    }
    query.fromLocalDatastore()
    query.limit = 1000
    
    query.findObjectsInBackground().continueWithBlock { task in
      guard task.error == nil else {
        completion()
        return task
      }
      guard let teams = task.result as? [Team] else {
        completion()
        return nil
      }
      completion()
      return teams
    }
  }
  
  static func fetchTeams(completion: () -> ()) {
    guard let query = Team.query() else {
      completion()
      return
    }
    query.limit = 1000
    
    // Query for new results from the network
    query.findObjectsInBackground().continueWithSuccessBlock { task in
      return PFObject.unpinAllObjectsInBackgroundWithName("Teams").continueWithSuccessBlock { ignored in
        
        // Cache new results
        guard let teams = task.result as? [Team] else {
          completion()
          return nil
        }
        NSUserDefaults.standardUserDefaults().setFloat(Float(NSDate().timeIntervalSince1970), forKey: "LastTeamRefreshDate")
        NSUserDefaults.standardUserDefaults().synchronize()
        completion()
        return PFObject.pinAllInBackground(teams, withName: "Teams")
      }
    }
  }
  
  static func allTeams(completion: [Team] -> ()) {
    guard let query = Team.query() else {
      completion([])
      return
    }
    query.fromLocalDatastore()
    query.limit = 1000
    query.findObjectsInBackgroundWithBlock { teams, error in
      guard let teams = teams as? [Team] else {
        completion([])
        return
      }
      completion(teams)
      return
    }
  }
  
  static func pushTeams(completion: [Team] -> ()) {
    guard let query = Team.query() else {
      completion([])
      return
    }
    query.fromLocalDatastore()
    query.whereKey("Push", equalTo: true)
    query.limit = 1000
    query.findObjectsInBackgroundWithBlock { teams, error in
      guard let teams = teams as? [Team] else {
        completion([])
        return
      }
      completion(teams.unique)
      return
    }
  }
}

extension Team: PFSubclassing {
  static func parseClassName() -> String {
    return "Equipe"
  }
}

extension Array where Element: Team {
  var logoURLHasStatsDictionary: [Int:(String, Bool)] {
    var result: [Int:(String, Bool)] = [:]
    for element in enumerate() {
      result[element.element.id!] = (element.element.logoURLString!, element.element.hasCompleteStats!)
    }
    return result
  }
}

extension Array where Element : Team {
  var unique: [Element] {
    var uniqueValues: [Element] = []
    var uniqueNames: [String] = []
    forEach { item in
      guard let name = item.name else {
        return
      }
      if !uniqueNames.contains(name) {
        uniqueValues.append(item)
        uniqueNames.append(name)
      }
    }
    return uniqueValues.sort { $0.name < $1.name }
  }
}
