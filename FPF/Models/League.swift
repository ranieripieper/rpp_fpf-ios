//
//  League.swift
//  FPF
//
//  Created by Gilson Gil on 1/8/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Parse

final class League: PFObject {
  var name: String? {
    get {
      return self["Nome"] as? String
    }
  }
  var leagueName: String? {
    get {
      return self["NomeCampeonatoApp"] as? String
    }
  }
  var id: Int? {
    get {
      return self["Id"] as? Int
    }
  }
  var category: String? {
    get {
      return self["Categoria"] as? String
    }
  }
  var groupStandings: String? {
    get {
      return self["ClassificacaoGrupo"] as? String
    }
  }
  var currentPhase: String? {
    get {
      return self["FaseAtual"] as? String
    }
  }
  var location: String? {
    get {
      return self["Local"] as? String
    }
  }
  var totalRounds: Int? {
    get {
      return self["QtdRodadas"] as? Int
    }
  }
  var currentRound: Int? {
    get {
      return self["RodadaAtual"] as? Int
    }
  }
  var hasStandings: Bool? {
    get {
      return self["TemClassificacao"] as? Bool
    }
  }
  var season: String? {
    get {
      return self["Temporada"] as? String
    }
  }
  var colectType: String? {
    get {
      return self["TipoColeta"] as? String
    }
  }
  var order: Int? {
    get {
      return self["Ordem"] as? Int
    }
  }
  var newsLeagueId: Int? {
    get {
      return self["IdCampeonatoNoticias"] as? Int
    }
  }
  @NSManaged var lastUpdated: NSDate?
}

extension League {
  static func leagueWithId(id: Int?, completion: (inner: () throws -> League) -> ()) {
    needsUpdate(id) {
      if $0 && Reachability.isConnectedToNetwork() {
        getLeagueWithId(id, completion: completion)
        fetchLeagueWithId(id, completion: completion)
      } else {
        getLeagueWithId(id, completion: completion)
      }
    }
  }
  
  static func getLeagueWithId(id: Int?, completion: (inner: () throws -> League) -> ()) {
    let query = PFQuery(className:"Campeonato")
    query.fromLocalDatastore()
    query.limit = 1000
    if let id = id {
      query.whereKey("Id", equalTo: id)
    }
    query.orderByAscending("Ordem")
    
    query.getFirstObjectInBackground().continueWithBlock { task in
      if task.error != nil {
        // There was an error.
        completion {throw ModelError.Unknown}
        return task
      }
      
      guard let league = task.result as? League else {
        completion {throw ModelError.Unknown}
        return nil
      }
      completion {return league}
      return league
    }
  }
  
  static func fetchLeagueWithId(id: Int?, completion: (inner: () throws -> League) -> ()) {
    guard let query = League.query() else {
      completion {throw ModelError.Unknown}
      return
    }
    if let id = id {
      query.whereKey("Id", equalTo: id)
    }
    query.orderByAscending("Ordem")
    query.limit = 1000

    query.getFirstObjectInBackground().continueWithSuccessBlock { task in
      guard let league = task.result as? League else {
        completion {throw ModelError.Unknown}
        return nil
      }
      league.lastUpdated = NSDate()
      completion {return league}
      if let id = id {
        league.pinInBackgroundWithName("League \(id)")
      } else {
        league.pinInBackgroundWithName("League \(league.id ?? 0)")
      }
      return task
    }
  }
  
  static func leagues(completion: ((inner: () throws -> [League]) -> ())?) {
    if Reachability.isConnectedToNetwork() {
      get(completion)
      fetch(completion)
    } else {
      get(completion)
    }
  }
  
  static func get(completion: ((inner: () throws -> [League]) -> ())?) {
    let query = PFQuery(className:"Campeonato")
    query.fromLocalDatastore()
    query.limit = 1000
    query.orderByAscending("Ordem")
    query.findObjectsInBackground().continueWithBlock { task in
      if task.error != nil {
        // There was an error.
        completion? {throw ModelError.Unknown}
        return task
      }
      
      guard let leagues = task.result as? [League] else {
        completion? {throw ModelError.Unknown}
        return nil
      }
      completion? {return leagues}
      return leagues
    }
  }
  
  static func fetch(completion: ((inner: () throws -> [League]) -> ())?) {
    guard let query = League.query() else {
      completion? {throw ModelError.Unknown}
      return
    }
    query.limit = 1000
    query.whereKey("Visible", equalTo: true)
    
    // Query for new results from the network
    query.findObjectsInBackground().continueWithSuccessBlock { task in
      return PFObject.unpinAllObjectsInBackgroundWithName("Leagues").continueWithSuccessBlock { ignored in
        
        // Cache new results
        guard var leagues = task.result as? [League] else {
          completion? {throw ModelError.Unknown}
          return nil
        }
        leagues.sortInPlace {
          $0.order < $1.order
        }
        completion? {return leagues}
        NSNotificationCenter.defaultCenter().postNotificationName(Constants.updatedLeaguesNotificationString, object: nil, userInfo: ["leagues": leagues])
        return PFObject.pinAllInBackground(leagues, withName: "Leagues")
      }
    }
  }
  
  static func needsUpdate(leagueId: Int?, completion: Bool -> ()) {
    guard let leagueId = leagueId else {
      completion(true)
      return
    }
    guard let query = League.query() else {
      completion(true)
      return
    }
    query.fromLocalDatastore()
    query.limit = 1
    query.whereKey("Id", equalTo: leagueId)
    query.getFirstObjectInBackgroundWithBlock { league, error in
      guard error == nil else {
        completion(true)
        return
      }
      guard let league = league as? League else {
        completion(true)
        return
      }
      if let lastUpdated = league.lastUpdated {
        let needsUpdate = lastUpdated.timeIntervalSinceNow < -60 * 60 * 4
        completion(needsUpdate)
      } else {
        league.lastUpdated = NSDate()
        completion(true)
      }
    }
  }
}

extension League: PFSubclassing {
  static func parseClassName() -> String {
    return "Campeonato"
  }
}

