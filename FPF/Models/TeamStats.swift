//
//  TeamStats.swift
//  FPF
//
//  Created by Gilson Gil on 3/3/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Parse

final class TeamStats: PFObject {
//  [{"Equipe":"São Bento","EquipeId":1058,"Gols":1},{"Equipe":"Ferroviária","EquipeId":2043,"Gols":2},{"Equipe":"Capivariano","EquipeId":2409,"Gols":1}]
  var goalsAgainst: [[String: AnyObject]]? {
    get {
      return self["GolsContra"] as? [[String: AnyObject]]
    }
  }
  var teamId: Int? {
    get {
      return self["IdEquipe"] as? Int
    }
  }
  var awayPoints: Int? {
    get {
      return self["PontosComoVisitante"] as? Int
    }
  }
  var awayMatches: [[String: AnyObject]]? {
    get {
      return self["JogosComoVisitante"] as? [[String: AnyObject]]
    }
  }
  var awayMatchesCount: Int? {
    get {
      return self["TotalJogosComoVisitante"] as? Int
    }
  }
  var teamName: String? {
    get {
      return self["Equipe"] as? String
    }
  }
  var homeMatches: [[String: AnyObject]]? {
    get {
      return self["JogosComoMandante"] as? [[String: AnyObject]]
    }
  }
  var homeMatchesCount: Int? {
    get {
      return self["TotalJogosComoMandante"] as? Int
    }
  }
  var homePoints: Int? {
    get {
      return self["PontosComoMandante"] as? Int
    }
  }
  var goals: [[String: AnyObject]]? {
    get {
      return self["GolsPro"] as? [[String: AnyObject]]
    }
  }
  @NSManaged var logoURL: String
  @NSManaged var staff: [[String: String]]
}

extension TeamStats {
  static func hasStatsForTeamId(teamId: Int, completion: Bool -> ()) {
    let query = TeamStats.query()!
    query.fromLocalDatastore()
    query.whereKey("IdEquipe", equalTo: teamId)
    query.limit = 1
    query.countObjectsInBackgroundWithBlock { count, error in
      if count > 0 {
        completion(true)
      } else {
        completion(false)
      }
    }
  }
  
  static func statsForTeamId(teamId: Int, completion: (inner: () throws -> TeamStats) -> ()) {
    if Reachability.isConnectedToNetwork() {
//      hasStatsForTeamId(teamId) {
//        if $0 {
//          getStatsForTeamId(teamId, completion: completion)
//        } else {
          fetchStatsForTeamId(teamId, completion: completion)
//        }
//      }
    } else {
      getStatsForTeamId(teamId, completion: completion)
    }
  }
  
  static func getStatsForTeamId(teamId: Int, completion: (inner: () throws -> TeamStats) -> ()) {
    hasStatsForTeamId(teamId) {
      if $0 {
        guard let query = TeamStats.query() else {
          completion {throw ModelError.Unknown}
          return
        }
        query.fromLocalDatastore()
        query.whereKey("IdEquipe", equalTo: teamId)
        query.limit = 1
        
        query.getFirstObjectInBackgroundWithBlock { teamStats, error in
          guard error == nil else {
            completion {throw ModelError.Unknown}
            return
          }
          guard let teamStats = teamStats as? TeamStats else {
            completion {throw ModelError.Unknown}
            return
          }
          completion {return teamStats}
        }
      } else {
        fetchStatsForTeamId(teamId, completion: completion)
      }
    }
  }
  
  static func fetchStatsForTeamId(teamId: Int, completion: (inner: () throws -> TeamStats) -> ()) {
    guard let query = TeamStats.query() else {
      completion {throw ModelError.Unknown}
      return
    }
    query.whereKey("IdEquipe", equalTo: teamId)
    query.limit = 1
    
    query.getFirstObjectInBackgroundWithBlock { teamStats, error in
      guard error == nil else {
        completion {throw ModelError.Unknown}
        return
      }
      guard let teamStats = teamStats as? TeamStats else {
        completion {throw ModelError.Unknown}
        return
      }
      Guide.fetchGuideWithTeamId(teamId) {
        if let guide = try? $0() {
          teamStats.staff = guide["Elenco"] as? [[String: String]] ?? []
        }
        Team.logoURL(teamId) {
          if let logoURL = try? $0() {
            teamStats.logoURL = logoURL
          }
          teamStats.pinInBackground()
          completion {return teamStats}
        }
      }
    }
  }
}

extension TeamStats: PFSubclassing {
  static func parseClassName() -> String {
    return "EstatisticasDoTime"
  }
}
