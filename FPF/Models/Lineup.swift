//
//  Lineup.swift
//  FPF
//
//  Created by Gilson Gil on 1/27/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Parse

struct LineupPlayer {
  let id: String
  let name: String
  let position: String
  let substituted: Bool
  var substitutee: Bool?
  let substituteId: String
  let goals: Int
  let yellowCard: Bool
  let redCard: Bool
  
  init?(dictionary: [String: String]) {
    guard let id = dictionary["Id"], let name = dictionary["Nome"], let position = dictionary["Posicao"], let substituted = dictionary["Substituido"], let substituteId = dictionary["IdJogadorSubstituto"] else {
      return nil
    }
    self.id = id
    self.name = name
    self.position = position
    self.substituted = substituted == "1"
    self.substituteId = substituteId
    self.goals = 0
    self.yellowCard = false
    self.redCard = false
  }
  
  init(lineupPlayer: LineupPlayer, goals: Int, yellowCard: Bool, redCard: Bool) {
    self.id = lineupPlayer.id
    self.name = lineupPlayer.name
    self.position = lineupPlayer.position
    self.substituted = lineupPlayer.substituted
    self.substitutee = lineupPlayer.substitutee
    self.substituteId = lineupPlayer.substituteId
    self.goals = goals
    self.yellowCard = yellowCard
    self.redCard = redCard
  }
  
  init(managerName: String) {
    self.id = ""
    self.name = managerName
    self.position = "TEC"
    self.substituted = false
    self.substitutee = false
    self.substituteId = ""
    self.goals = 0
    self.yellowCard = false
    self.redCard = false
  }
}

final class Lineup: PFObject {
  var homeId: Int? {
    get {
      return self["IdMandante"] as? Int
    }
  }
  var homeLineup: [[String: String]]? {
    get {
      return self["Mandante"] as? [[String: String]]
    }
  }
  var awayId: Int? {
    get {
      return self["IdVisitante"] as? Int
    }
  }
  var awayLineup: [[String: String]]? {
    get {
      return self["Visitante"] as? [[String: String]]
    }
  }
  var matchId: Int? {
    get {
      return self["IdPartida"] as? Int
    }
  }
  var lineupPlayers: ([LineupPlayer], [LineupPlayer]) = ([], [])
}

extension Lineup {
  static func lineupsForMatchId(matchId: Int, forcedCache: Bool, completion: (inner: () throws -> Lineup) -> ()) {
    if forcedCache || !Reachability.isConnectedToNetwork() {
      getForMatchId(matchId, completion: completion)
    } else {
      getForMatchId(matchId, completion: completion)
      fetchForMatchId(matchId, completion: completion)
    }
  }
  
  static func getForMatchId(matchId: Int, completion: (inner: () throws -> Lineup) -> ()) {
    guard let query = Lineup.query() else {
      completion {throw ModelError.Unknown}
      return
    }
    query.fromLocalDatastore()
    query.whereKey("IdPartida", equalTo: matchId)
    query.limit = 1000
    
    query.getFirstObjectInBackground().continueWithBlock { task in
      guard task.error == nil else {
        completion {throw ModelError.Unknown}
        return task
      }
      guard let lineup = task.result as? Lineup else {
        completion {throw ModelError.Unknown}
        return nil
      }
      lineup.setLineupPlayers()
      completion {return lineup}
      return lineup
    }
  }
  
  static func fetchForMatchId(matchId: Int, completion: (inner: () throws -> Lineup) -> ()) {
    guard let query = Lineup.query() else {
      completion {throw ModelError.Unknown}
      return
    }
    query.whereKey("IdPartida", equalTo: matchId)
    query.limit = 1000
    
    // Query for new results from the network
    query.getFirstObjectInBackground().continueWithSuccessBlock { task in
      return PFObject.unpinAllObjectsInBackgroundWithName("Lineup \(matchId)").continueWithSuccessBlock { ignored in
        
        // Cache new results
        guard let lineup = task.result as? Lineup else {
          completion {throw ModelError.Unknown}
          return nil
        }
        lineup.setLineupPlayers()
        completion {return lineup}
        return lineup.pinInBackgroundWithName("Lineup \(matchId)")
      }
    }
  }
  
  func setLineupPlayers() {
    if let homeLineup = self["Mandante"] as? [[String: String]] {
      if lineupPlayers.0.count == 0 {
        var substitutesIds = [(String, String)]()
        var lineups = homeLineup.flatMap { LineupPlayer(dictionary: $0) }
        lineups.forEach {
          if $0.substituted {
            substitutesIds.append(($0.id, $0.substituteId))
          }
        }
        substitutesIds.forEach { substitutesId in
          let substitute = lineups.filter {$0.id == substitutesId.1}.first
          if var substitute = substitute {
            let substituted = lineups.filter {$0.id == substitutesId.0}.first
            if let substituted = substituted {
              let substituteIndex = lineups.indexOf{$0.id == substitute.id}!
              lineups.removeAtIndex(substituteIndex)
              let substitutedIndex = lineups.indexOf{$0.id == substituted.id}!
              substitute.substitutee = true
              lineups.insert(substitute, atIndex: substitutedIndex + 1)
            }
          }
        }
        lineupPlayers.0 = lineups
      }
    }
    if let awayLineup = self["Visitante"] as? [[String: String]] {
      if lineupPlayers.1.count == 0 {
        var substitutesIds = [(String, String)]()
        var lineups = awayLineup.flatMap { LineupPlayer(dictionary: $0) }
        lineups.forEach {
          if $0.substituted {
            substitutesIds.append(($0.id, $0.substituteId))
          }
        }
        substitutesIds.forEach { substitutesId in
          let substitute = lineups.filter {$0.id == substitutesId.1}.first
          if var substitute = substitute {
            let substituted = lineups.filter {$0.id == substitutesId.0}.first
            if let substituted = substituted {
              let substituteIndex = lineups.indexOf{$0.id == substitute.id}!
              lineups.removeAtIndex(substituteIndex)
              let substitutedIndex = lineups.indexOf{$0.id == substituted.id}!
              substitute.substitutee = true
              lineups.insert(substitute, atIndex: substitutedIndex + 1)
            }
          }
        }
        lineupPlayers.1 = lineups
      }
    }
  }
}

extension Lineup: PFSubclassing {
  static func parseClassName() -> String {
    return "Escalacao"
  }
}
