//
//  Live.swift
//  FPF
//
//  Created by Gilson Gil on 1/26/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Parse

final class Live: PFObject {
  var importantAction: String? {
    get {
      return self["AcaoImportante"] as? String
    }
  }
  var liveDescription: String? {
    get {
      return self["Descricao"] as? String
    }
  }
  var id: Int? {
    get {
      return self["Id"] as? Int
    }
  }
  var teamId: Int? {
    get {
      return self["IdEquipe"] as? Int
    }
  }
  var playerId: Int? {
    get {
      return self["IdJogador"] as? Int
    }
  }
  var substituteId: Int? {
    get {
      return self["IdSubstituto"] as? Int
    }
  }
  var matchId: Int? {
    get {
      return self["IdPartida"] as? Int
    }
  }
  var time: String? {
    get {
      return self["Momento"] as? String
    }
  }
  var teamName: String? {
    get {
      return self["NomeEquipe"] as? String
    }
  }
  var playerName: String? {
    get {
      return self["NomeJogador"] as? String
    }
  }
  var substituteName: String? {
    get {
      return self["NomeSubstituto"] as? String
    }
  }
  var period: String? {
    get {
      return self["Periodo"] as? String
    }
  }
}

extension Live {
  static func livesForMatchId(matchId: Int, forceCached: Bool, completion: (inner: () throws -> [Live]) -> ()) {
    if forceCached || !Reachability.isConnectedToNetwork() {
      getForMatchId(matchId, completion: completion)
    } else {
      getForMatchId(matchId, completion: completion)
      fetchForMatchId(matchId, completion: completion)
    }
  }
  
  static func getForMatchId(matchId: Int, completion: (inner: () throws -> [Live]) -> ()) {
    guard let query = Live.query() else {
      completion {throw ModelError.Unknown}
      return
    }
    query.fromLocalDatastore()
    query.whereKey("IdPartida", equalTo: matchId)
    query.limit = 1000
    
    query.findObjectsInBackground().continueWithBlock { task in
      guard task.error == nil else {
        completion {throw ModelError.Unknown}
        return task
      }
      guard let lives = task.result as? [Live] else {
        completion {throw ModelError.Unknown}
        return nil
      }
      completion {return lives}
      return lives
    }
  }
  
  static func fetchForMatchId(matchId: Int, completion: (inner: () throws -> [Live]) -> ()) {
    guard let query = Live.query() else {
      completion {throw ModelError.Unknown}
      return
    }
    query.whereKey("IdPartida", equalTo: matchId)
    query.limit = 1000
    
    // Query for new results from the network
    query.findObjectsInBackground().continueWithSuccessBlock { task in
      return PFObject.unpinAllObjectsInBackgroundWithName("Lives \(matchId)").continueWithSuccessBlock { ignored in
        
        // Cache new results
        guard let lives = task.result as? [Live] else {
          completion {throw ModelError.Unknown}
          return nil
        }
        
        completion {return lives}
        return PFObject.pinAllInBackground(lives, withName: "Lives \(matchId)")
      }
    }
  }
}

extension Live: PFSubclassing {
  static func parseClassName() -> String {
    return "Narracao"
  }
}

extension Live: Groupable {
  func sameGroupAs(otherLive: Live) -> Bool {
    return period == otherLive.period
  }
}

extension Live: Comparable {
  
}

func <(lhs: Live, rhs: Live) -> Bool {
  return lhs.period < rhs.period
}
