//
//  Scorer.swift
//  FPF
//
//  Created by Gilson Gil on 1/26/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Parse

final class Scorer: PFObject {
  var teamName: String? {
    get {
      return self["Equipe"] as? String
    }
  }
  var leagueId: Int? {
    get {
      return self["IdCampeonato"] as? Int
    }
  }
  var teamId: Int? {
    get {
      return self["IdEquipe"] as? Int
    }
  }
  var playerId: Int? {
    get {
      return self["IdJogador"] as? Int
    }
  }
  var playerName: String? {
    get {
      return self["Jogador"] as? String
    }
  }
  var total: Int? {
    get {
      return self["Total"] as? Int
    }
  }
  @NSManaged var teamURLString: String
  @NSManaged var position: Int
}

extension Scorer {
  static func scorersForLeagueId(leagueId: Int, completion: (inner: () throws -> [Scorer]) -> ()) {
    if Reachability.isConnectedToNetwork() {
      getForLeagueId(leagueId, completion: completion)
      fetchForLeagueId(leagueId, completion: completion)
    } else {
      getForLeagueId(leagueId, completion: completion)
    }
  }
  
  static func getForLeagueId(leagueId: Int, completion: (inner: () throws -> [Scorer]) -> ()) {
    guard let query = Scorer.query() else {
      completion {throw ModelError.Unknown}
      return
    }
    query.fromLocalDatastore()
    query.whereKey("IdCampeonato", equalTo: leagueId)
    query.orderByDescending("Total")
    query.limit = 1000
    
    query.findObjectsInBackground().continueWithBlock { task in
      if task.error != nil {
        // There was an error.
        completion {throw ModelError.Unknown}
        return task
      }
      
      guard let scorers = task.result as? [Scorer] else {
        completion {throw ModelError.Unknown}
        return nil
      }
      guard scorers.count > 0 else {
        completion {throw ModelError.NoConnection}
        return nil
      }
      completion {return scorers.sort { $0.position == $1.position ? ($0.playerName < $1.playerName) : ($0.position < $1.position)}}
      return scorers
    }
  }
  
  static func fetchForLeagueId(leagueId: Int, completion: (inner: () throws -> [Scorer]) -> ()) {
    guard let query = Scorer.query() else {
      completion {throw ModelError.Unknown}
      return
    }
    query.whereKey("IdCampeonato", equalTo: leagueId)
    query.orderByDescending("Total")
    query.limit = 1000
    
    // Query for new results from the network
    query.findObjectsInBackground().continueWithSuccessBlock { task in
      return PFObject.unpinAllObjectsInBackgroundWithName("Scorers \(leagueId)").continueWithSuccessBlock { ignored in
        
        // Cache new results
        guard let scorers = task.result as? [Scorer] else {
          completion {throw ModelError.Unknown}
          return nil
        }
        Team.allLogosAndHasStats(leagueId) { dict in
          scorers.forEach { scorer in
            if let dict = dict, tuple = dict[scorer.teamId!] {
              scorer.teamURLString = tuple.0
            }
          }
          setPositions(scorers)
          completion {return scorers.sort { $0.position == $1.position ? ($0.playerName < $1.playerName) : ($0.position < $1.position)}}
          PFObject.pinAllInBackground(scorers, withName: "Scorers \(leagueId)")
        }
        return nil
      }
    }
  }
  
  static func setPositions(scorers: [Scorer]) {
    let positions = Set(scorers.flatMap { $0.total }).sort { $0 > $1 }
    scorers.forEach {
      if let total = $0.total, index = positions.indexOf(total) {
        $0.position = index + 1
      }
    }
  }
}

extension Scorer: PFSubclassing {
  static func parseClassName() -> String {
    return "Artilharia"
  }
}
