//
//  Guide.swift
//  FPF
//
//  Created by Gilson Gil on 2/27/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Parse

struct Staff {
  let name: String
  let position: String
  let birthDate: String
  let birthPlace: String
  
  init?(_ json: [String: String]) {
    if let name = json["NOME"] {
      self.name = name
    } else {
      return nil
    }
    if let position = json["POS"] {
      self.position = position
    } else {
      return nil
    }
    if let birthDate = json["NASC"] {
      self.birthDate = birthDate
    } else {
      return nil
    }
    if let birthPlace = json["LOCAL"] {
      self.birthPlace = birthPlace
    } else {
      return nil
    }
  }
}

final class Guide: PFObject {
  var coordinate: CLLocation? {
    get {
      if let coordinate = self["Coordenada"] as? PFGeoPoint {
        return CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)
      } else {
        return nil
      }
    }
  }
  var stadiumURL: String? {
    get {
      return self["URLEstadio"] as? String
    }
  }
  var idhm: String? {
    get {
      return self["IDHM"] as? String
    }
  }
  var badgeURL: String? {
    get {
      return self["URLDistintivo"] as? String
    }
  }
  var teamDescription: String? {
    get {
      return self["Descricao"] as? String
    }
  }
  var mascotURL: String? {
    get {
      return self["URLMascote"] as? String
    }
  }
  var cityDescription: String? {
    get {
      return self["DescricaoCidade"] as? String
    }
  }
  var cityArea: String? {
    get {
      return self["CidadeArea"] as? String
    }
  }
  var uniformURLs: [String]? {
    get {
      return self["URLUniformes"] as? [String]
    }
  }
  var demographicDensity: String? {
    get {
      return self["DensidadeDemografica"] as? String
    }
  }
  var cityURL: String? {
    get {
      return self["URLCidade"] as? String
    }
  }
  var uniformMaker: String? {
    get {
      return self["UniformeFabricante"] as? String
    }
  }
  var stadiumEstablishment: String? {
    get {
      return self["InauguracaoEstadio"] as? String
    }
  }
  var stadiumName: String? {
    get {
      return self["NomeEstadio"] as? String
    }
  }
  var cityPopulation: String? {
    get {
      return self["Populacao"] as? String
    }
  }
  var name: String? {
    get {
      return self["Nome"] as? String
    }
  }
  var staff: [Staff]? {
    get {
      if let json = self["Elenco"] as? [[String: String]] {
        return json.flatMap { Staff($0) }
      } else {
        return nil
      }
    }
  }
  var stadiumCapacity: Int? {
    get {
      return self["CapacidadeEstadio"] as? Int
    }
  }
  var mascot: String? {
    get {
      return self["Mascote"] as? String
    }
  }
  var teamId: Int? {
    get {
      return self["IdEquipe"] as? Int
    }
  }
  var city: String? {
    get {
      return self["Cidade"] as? String
    }
  }
  var cityStadium: String? {
    get {
      return self["CidadeEstadio"] as? String
    }
  }
  var serie: String? {
    get {
      return self["Serie"] as? String
    }
  }
  var leagueId: Int? {
    get {
      return self["IdCampeonato"] as? Int
    }
  }
  @NSManaged var team: Team?
}

extension Guide {
  static func guideWithTeamId(teamId: Int, completion: (inner: () throws -> Guide) -> ()) {
    if Reachability.isConnectedToNetwork() {
      getGuideWithTeamId(teamId, completion: completion)
      fetchGuideWithTeamId(teamId, completion: completion)
    } else {
      getGuideWithTeamId(teamId, completion: completion)
    }
  }
  
  static func getGuideWithTeamId(teamId: Int, completion: (inner: () throws -> Guide) -> ()) {
    let query = Guide.query()!
    query.fromLocalDatastore()
    query.limit = 1
    query.whereKey("IdEquipe", equalTo: teamId)
    
    query.getFirstObjectInBackground().continueWithBlock { task in
      if task.error != nil {
        // There was an error.
        completion {throw ModelError.Unknown}
        return task
      }
      
      guard let guide = task.result as? Guide else {
        completion {throw ModelError.Unknown}
        return nil
      }
      completion {return guide}
      return guide
    }
  }
  
  static func fetchGuideWithTeamId(teamId: Int, completion: (inner: () throws -> Guide) -> ()) {
    guard let query = Guide.query() else {
      completion {throw ModelError.Unknown}
      return
    }
    query.whereKey("IdEquipe", equalTo: teamId)
    query.limit = 1
    
    // Query for new results from the network
    query.getFirstObjectInBackgroundWithBlock { guide, error in
      return PFObject.unpinAllObjectsInBackgroundWithName("Guide \(teamId)").continueWithSuccessBlock { ignored in
        
        // Cache new results
        guard let guide = guide as? Guide else {
          completion {throw ModelError.Unknown}
          return nil
        }
        completion {return guide}
        return guide.pinInBackgroundWithName("Guide \(teamId)")
      }
    }
  }
  
  static func guides(completion: (inner: () throws -> [(String, [Guide])]) -> ()) {
    if Reachability.isConnectedToNetwork() {
      fetchGuides(completion)
    } else {
      getGuides(completion)
    }
  }
  
  static func getGuides(completion: (inner: () throws -> [(String, [Guide])]) -> ()) {
    let query = Guide.query()!
    query.fromLocalDatastore()
    query.limit = 1000
    
    query.getFirstObjectInBackground().continueWithBlock { task in
      if task.error != nil {
        // There was an error.
        completion {throw ModelError.Unknown}
        return task
      }
      
      guard let guides = task.result as? [Guide] else {
        completion {throw ModelError.Unknown}
        return nil
      }
      let leaguedGuides = leagued(guides)
      completion {return leaguedGuides}
      return guides
    }
  }
  
  static func fetchGuides(completion: (inner: () throws -> [(String, [Guide])]) -> ()) {
    Team.allTeams {
      let teams = $0
      guard let query = Guide.query() else {
        completion {throw ModelError.Unknown}
        return
      }
      query.limit = 1000
      
      // Query for new results from the network
      query.findObjectsInBackground().continueWithSuccessBlock { task in
        return PFObject.unpinAllObjectsInBackgroundWithName("Guides").continueWithSuccessBlock { ignored in
          
          // Cache new results
          guard let guides = task.result as? [Guide] else {
            completion {throw ModelError.Unknown}
            return nil
          }
          guides.forEach { guide in
            guide.team = teams.filter {$0.id == guide.teamId}.first
          }
          let leaguedGuides = leagued(guides)
          completion {return leaguedGuides}
          return PFObject.pinAllInBackground(guides, withName: "Guides")
        }
      }
    }
  }
  
  static func leagued(guides: [Guide]) -> [(String, [Guide])] {
    let leagueIds = Set(guides.flatMap { $0.leagueId }).sort()
    let leagueIded = leagueIds.flatMap { leagueId -> (String, [Guide])? in
      let filteredGuides = guides.filter { guide in
        guide.leagueId == leagueId
        }.sort { $0.team?.name < $1.team?.name }
      let leagueString: String
      if let guide = filteredGuides.first, leagueName = guide.serie {
        leagueString = leagueName
      } else {
        return nil
      }
      return (leagueString, filteredGuides)
    }
    return leagueIded
  }
}

extension Guide: PFSubclassing {
  static func parseClassName() -> String {
    return "Guia2Divisao"
  }
}
