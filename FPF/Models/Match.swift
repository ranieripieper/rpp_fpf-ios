//
//  Match.swift
//  FPF
//
//  Created by Gilson Gil on 1/8/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Parse

final class Match: PFObject {
  typealias Goal = (teamName: String, playerName: String, time: String, period: String, type: String)
  typealias Card = (teamName: String, playerName: String, type: GraphicType)
  
  var referee: String? {
    get {
      return self["Arbitro"] as? String
    }
  }
  var city: String? {
    get {
      return self["Cidade"] as? String
    }
  }
  var date: NSDate? {
    get {
      if let date = self["Data"] as? NSDate {
        let timezone = NSTimeZone.localTimeZone()
        let timeInterval = NSTimeInterval(timezone.secondsFromGMTForDate(date))
        let newDate = NSDate(timeInterval: -timeInterval, sinceDate: date)
        return newDate
      }
      return nil
    }
  }
  var dateToDefine: Bool? {
    get {
      return self["DataDefinir"] as? Bool
    }
  }
  var phase: String? {
    get {
      return self["Fase"] as? String
    }
  }
  var goals: [Goal]? {
    get {
      guard let array = self["Gols"] as? [[String: String]] else {
        return nil
      }
      return array.map {(teamName: $0["Equipe"]!, playerName: $0["Jogador"]!, time: $0["Momento"]!, period: $0["Periodo"]!, type: $0["Tipo"]!)}
    }
  }
  var group: String? {
    get {
      return self["Grupo"] as? String
    }
  }
  var timeToDefine: Bool? {
    get {
      return self["HoraDefinir"] as? Bool
    }
  }
  var id: Int? {
    get {
      return self["Id"] as? Int
    }
  }
  var leagueId: Int? {
    get {
      return self["IdCampeonato"] as? Int
    }
  }
  var homeTeamId: Int? {
    get {
      return self["IdEquipeMandante"] as? Int
    }
  }
  var awayTeamId: Int? {
    get {
      return self["IdEquipeVisitante"] as? Int
    }
  }
  var nonneededMatch: Bool? {
    get {
      return self["JogoNaoNecessario"] as? Bool
    }
  }
  var leagueName: String? {
    get {
      return self["NomeCampeonato"] as? String
    }
  }
  var stadiumName: String? {
    get {
      return self["NomeEstadio"] as? String
    }
  }
  var homeTeamName: String? {
    get {
      return self["NomeMandante"] as? String
    }
  }
  var awayTeamName: String? {
    get {
      return self["NomeVisitante"] as? String
    }
  }
  var keyNumberMatch: Int? {
    get {
      return self["NumeroJogoChave"] as? Int
    }
  }
  var currentPeriod: String? {
    get {
      return self["PeriodoAtual"] as? String
    }
  }
  var homeScore: Int? {
    get {
      return self["PlacarMandante"] as? Int
    }
  }
  var homePenaltyScore: Int? {
    get {
      return self["PlacarPenaltisMandante"] as? Int
    }
  }
  var awayScore: Int? {
    get {
      return self["PlacarVisitante"] as? Int
    }
  }
  var awayPenaltyScore: Int? {
    get {
      return self["PlacarPenaltisVisitante"] as? Int
    }
  }
  var attendancy: Int? {
    get {
      return self["Publico"] as? Int
    }
  }
  var revenue: Int? {
    get {
      return self["Renda"] as? Int
    }
  }
  var round: Int? {
    get {
      return self["Rodada"] as? Int
    }
  }
  var cup: String? {
    get {
      return self["Taca"] as? String
    }
  }
  var homeManager: String? {
    get {
      return self["TecnicoMandante"] as? String
    }
  }
  var awayManager: String? {
    get {
      return self["TecnicoVisitante"] as? String
    }
  }
  var hasStats: Bool? {
    get {
      return self["TemEstatistica"] as? Bool
    }
  }
  var hasLive: Bool? {
    get {
      return self["TemNarracao"] as? Bool
    }
  }
  var season: String? {
    get {
      return self["Temporada"] as? String
    }
  }
  var cards = [Card]()
  @NSManaged var homeURLString: String
  @NSManaged var awayURLString: String
  @NSManaged var liveCompleted: Bool
  @NSManaged var lineupCompleted: Bool
  @NSManaged var matchStatsCompleted: Bool
}

extension Match {
  static func matchesForLeagueId(leagueId: Int, forced: Bool, completion: (inner: () throws -> [(String, [Match])]) -> ()) {
    if Reachability.isConnectedToNetwork() {
      if forced {
        getForLeagueId(leagueId, completion: completion)
      }
      fetchForLeagueId(leagueId, completion: completion)
    } else {
      getForLeagueId(leagueId, completion: completion)
    }
  }
  
  static func getForLeagueId(leagueId: Int, completion: (inner: () throws -> [(String, [Match])]) -> ()) {
    guard let query = Match.query() else {
      completion {throw ModelError.Unknown}
      return
    }
    query.fromLocalDatastore()
    query.whereKey("IdCampeonato", equalTo: leagueId)
    query.limit = 1000
    
    query.findObjectsInBackground().continueWithBlock { task in
      if task.error != nil {
        // There was an error.
        completion {throw ModelError.Unknown}
        return task
      }
      
      guard let matches = task.result as? [Match] else {
        completion {throw ModelError.Unknown}
        return nil
      }
      completion {return rounded(matches)}
      return matches
    }
  }
  
  static func fetchForLeagueId(leagueId: Int, completion: (inner: () throws -> [(String, [Match])]) -> ()) {
    guard let query = Match.query() else {
      completion {throw ModelError.Unknown}
      return
    }
    query.whereKey("IdCampeonato", equalTo: leagueId)
    query.limit = 1000
    
    // Query for new results from the network
    query.findObjectsInBackground().continueWithSuccessBlock { task in
      return PFObject.unpinAllObjectsInBackgroundWithName("Matches \(leagueId)").continueWithSuccessBlock { ignored in
        
        // Cache new results
        guard let matches = task.result as? [Match] else {
          completion {throw ModelError.Unknown}
          return nil
        }
        Team.allLogosAndHasStats(leagueId) { dict in
          matches.forEach { match in
            if let dict = dict {
              if let tuple = dict[match.homeTeamId!] {
                match.homeURLString = tuple.0
              }
              if let tuple = dict[match.awayTeamId!] {
                match.awayURLString = tuple.0
              }
            }
          }
          completion {return rounded(matches)}
          PFObject.pinAllInBackground(matches, withName: "Matches \(leagueId)")
        }
        return nil
      }
    }
  }
  
  static func rounded(matches: [Match]) -> [(String, [Match])] {
    let rounds = Set(matches.flatMap { $0.round }).sort()
    let rounded = rounds.map { round -> (String, [Match]) in
      var filteredMatches = matches.filter { match in
        match.round == round
        }.sort {
          if let ldate = $0.date, rdate = $1.date {
            return ldate.compare(rdate) == .OrderedSame ? ($0.homeTeamName < $1.homeTeamName) : (ldate.compare(rdate) == .OrderedAscending)
          } else if $0.date != nil {
            return true
          } else {
            return false
          }
      }
      let roundString: String
      if let match = filteredMatches.first, phase = match.phase where !phase.containsString("Fase") {
        roundString = phase
        var tempMatches = filteredMatches
        var finalMatches: [Match] = []
        while tempMatches.count > 0 {
          let firstMatch = tempMatches[0]
          if let firstMatchIndex = tempMatches.indexOf(firstMatch) {
            finalMatches.append(firstMatch)
            tempMatches.removeAtIndex(firstMatchIndex)
            let secondMatch = filteredMatches.filter { $0.homeTeamId == firstMatch.awayTeamId }.first
            if let secondMatch = secondMatch, secondMatchIndex = tempMatches.indexOf(secondMatch) {
              finalMatches.append(secondMatch)
              tempMatches.removeAtIndex(secondMatchIndex)
            }
          }
        }
        filteredMatches = finalMatches
      } else {
        roundString = "Rodada \(round)"
      }
      return (roundString, filteredMatches)
    }
    return rounded
  }
  
  static func fetchMatchWithId(matchId: Int, completion: Match? -> ()) {
    let query = Match.query()!
    query.whereKey("Id", equalTo: matchId)
    
    query.getFirstObjectInBackground().continueWithBlock { task in
      guard let match = task.result as? Match else {
        completion(nil)
        return nil
      }
      Team.allLogosAndHasStats(nil) { dict in
        if let dict = dict {
          if let tuple = dict[match.homeTeamId ?? 0] {
            match.homeURLString = tuple.0
          }
          if let tuple = dict[match.awayTeamId ?? 0] {
            match.awayURLString = tuple.0
          }
        }
        completion(match)
        match.pinInBackground()
      }
      return nil
    }
  }
}

extension Match: PFSubclassing {
  static func parseClassName() -> String {
    return "Partida"
  }
}
