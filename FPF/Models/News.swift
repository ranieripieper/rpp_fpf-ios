//
//  News.swift
//  FPF
//
//  Created by Gilson Gil on 5/5/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation

struct News {
  let title: String
  let date: String
  let imageURL: String?
  let url: String
  var content: String?
  
  init?(json: [String: AnyObject]) {
    guard let title = json["titulo"] as? String, date = json["data"] as? String, url = json["url"] as? String else {
      return nil
    }
    self.title = title
    self.date = date
    self.url = url
    self.imageURL = json["imagem"] as? String
  }
}
