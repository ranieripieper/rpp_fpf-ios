//
//  Standing.swift
//  FPF
//
//  Created by Gilson Gil on 1/8/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Parse

struct Group {
  let name: String
  let standings: [Standing]
  
  init(name: String, standings: [Standing]) {
    self.name = name
    self.standings = standings
  }
}

struct Phase {
  let name: String
  let groups: [Group]
  
  init(name: String, standings: [Standing]) {
    self.name = name
    self.groups = Standing.grouped(standings)
  }
}

final class Standing: PFObject {
  var leagueId: Int? {
    get {
      return self["IdCampeonato"] as? Int
    }
  }
  var leagueName: String? {
    get {
      return self["NomeCampeonato"] as? String
    }
  }
  var successRate: String? {
    get {
      return self["Aproveitamento"] as? String
    }
  }
  var losses: Int? {
    get {
      return self["Derrotas"] as? Int
    }
  }
  var homeLosses: Int? {
    get {
      return self["DerrotasCasa"] as? Int
    }
  }
  var awayLosses: Int? {
    get {
      return self["DerrotasFora"] as? Int
    }
  }
  var draws: Int? {
    get {
      return self["Empates"] as? Int
    }
  }
  var homeDraws: Int? {
    get {
      return self["EmpatesCasa"] as? Int
    }
  }
  var awayDraws: Int? {
    get {
      return self["EmpatesFora"] as? Int
    }
  }
  var phase: String? {
    get {
      return self["Fase"] as? String
    }
  }
  var goals: Int? {
    get {
      return self["GolsPro"] as? Int
    }
  }
  var goalsAgainst: Int? {
    get {
      return self["GolsSofridos"] as? Int
    }
  }
  var group: String? {
    get {
      return self["Grupo"] as? String
    }
  }
  var teamId: Int? {
    get {
      return self["IdEquipe"] as? Int
    }
  }
  var played: Int? {
    get {
      return self["Jogos"] as? Int
    }
  }
  var teamName: String? {
    get {
      return self["NomeEquipe"] as? String
    }
  }
  var maxPoints: Int? {
    get {
      return self["PontoMaximo"] as? Int
    }
  }
  var points: Int? {
    get {
      return self["Pontos"] as? Int
    }
  }
  var position: Int? {
    get {
      return self["Posicao"] as? Int
    }
  }
  var round: Int? {
    get {
      return self["Rodada"] as? Int
    }
  }
  var goalDifference: Int? {
    get {
      return self["SaldoGols"] as? Int
    }
  }
  var cup: String? {
    get {
      return self["Taca"] as? String
    }
  }
  var wins: Int? {
    get {
      return self["Vitorias"] as? Int
    }
  }
  var homeWins: Int? {
    get {
      return self["VitoriasCasa"] as? Int
    }
  }
  var awayWins: Int? {
    get {
      return self["VitoriasFora"] as? Int
    }
  }
  @NSManaged var logoURLString: String
  @NSManaged var teamHasStats: Bool
}

extension Standing {
  static func standingsForLeagueId(leagueId: Int, completion: (inner: () throws -> [Phase]) -> ()) {
    if Reachability.isConnectedToNetwork() {
      getForLeagueId(leagueId, completion: completion)
      fetchForLeagueId(leagueId, completion: completion)
    } else {
      getForLeagueId(leagueId, completion: completion)
    }
  }
  
  static func getForLeagueId(leagueId: Int, completion: (inner: () throws -> [Phase]) -> ()) {
    guard let query = Standing.query() else {
      completion {throw ModelError.Unknown}
      return
    }
    query.fromLocalDatastore()
    query.whereKey("IdCampeonato", equalTo: leagueId)
    query.limit = 1000
    query.orderByDescending("Posicao")
    
    query.findObjectsInBackground().continueWithBlock { task in
      if task.error != nil {
        // There was an error.
        completion {throw ModelError.Unknown}
        return task
      }
      
      guard let standings = task.result as? [Standing] else {
        completion {throw ModelError.Unknown}
        return nil
      }
      completion {return phased(standings)}
      return standings
    }
  }
  
  static func fetchForLeagueId(leagueId: Int, completion: (inner: () throws -> [Phase]) -> ()) {
    guard let query = Standing.query() else {
      completion {throw ModelError.Unknown}
      return
    }
    query.whereKey("IdCampeonato", equalTo: leagueId)
    query.limit = 1000
    query.orderByDescending("Posicao")
    
    // Query for new results from the network
    query.findObjectsInBackground().continueWithSuccessBlock { task in
      return PFObject.unpinAllObjectsInBackgroundWithName("Standings \(leagueId)").continueWithSuccessBlock { ignored in
        
        // Cache new results
        guard let standings = task.result as? [Standing] else {
          completion {throw ModelError.Unknown}
          return nil
        }
        Team.allLogosAndHasStats(leagueId) { dict in
          standings.forEach { standing in
            if let dict = dict, tuple = dict[standing.teamId!] {
              standing.logoURLString = tuple.0
              standing.teamHasStats = tuple.1
            }
          }
          completion {return phased(standings)}
          PFObject.pinAllInBackground(standings, withName: "Standings \(leagueId)")
        }
        return nil
      }
    }
  }
  
  static func grouped(standings: [Standing]) -> [Group] {
    let groups = Set(standings.flatMap { $0.group }).sort()
    let grouped = groups.map { group in
      Group(name: group, standings: standings.filter { $0.group == group }.sort {$0.position < $1.position})
    }
    return grouped
  }
  
  static func phased(standings: [Standing]) -> [Phase] {
    let phases = Set(standings.flatMap { $0.phase }).sort()
    let phased = phases.map { phase in
      Phase(name: phase, standings: standings.filter { $0.phase == phase })
    }
    return phased
  }
}

extension Standing: PFSubclassing {
  static func parseClassName() -> String {
    return "Classificacao"
  }
}
