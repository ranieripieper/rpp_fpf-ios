//
//  MatchStats.swift
//  FPF
//
//  Created by Gilson Gil on 3/2/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Parse

final class MatchStats: PFObject {
  var awayPossession: Float? {
    get {
      return self["PosseBolaVisitante"] as? Float
    }
  }
  var awayCompletedCrosses: Int? {
    get {
      return self["CruzamentoCertoVisitante"] as? Int
    }
  }
  var awayCorners: Int? {
    get {
      return self["EscanteioProVisitante"] as? Int
    }
  }
  var awayRedCards: Int? {
    get {
      return self["CartaoVermelhoVisitante"] as? Int
    }
  }
  var awayShotsOnTarget: Int? {
    get {
      return self["FinalizacaoCertaVisitante"] as? Int
    }
  }
  var homeWrongPasses: Int? {
    get {
      return self["PasseErradoMandante"] as? Int
    }
  }
  var awayPenalties: Int? {
    get {
      return self["PenaltiRecebidoVisitante"] as? Int
    }
  }
  var homeYellowCards: Int? {
    get {
      return self["CartaoAmareloMandante"] as? Int
    }
  }
  var awayWrongTackles: Int? {
    get {
      return self["DesarmeErradoVisitante"] as? Int
    }
  }
  var awayWrongThroughBalls: Int? {
    get {
      return self["LancamentoErradoVisitante"] as? Int
    }
  }
  var homePenalties: Int? {
    get {
      return self["PenaltiRecebidoMandante"] as? Int
    }
  }
  var homeWrongThroughBalls: Int? {
    get {
      return self["LancamentoErradoMandante"] as? Int
    }
  }
  var homeWrongTackles: Int? {
    get {
      return self["DesarmeErradoMandante"] as? Int
    }
  }
  var awayYellowCards: Int? {
    get {
      return self["CartaoAmareloVisitante"] as? Int
    }
  }
  var homeRedCards: Int? {
    get {
      return self["CartaoVermelhoMandante"] as? Int
    }
  }
  var awayWrongPasses: Int? {
    get {
      return self["PasseErradoVisitante"] as? Int
    }
  }
  var homeShotsOnTarget: Int? {
    get {
      return self["FinalizacaoCertaMandante"] as? Int
    }
  }
  var homeCompletedCrosses: Int? {
    get {
      return self["CruzamentoCertoMandante"] as? Int
    }
  }
  var homePossession: Float? {
    get {
      return self["PosseBolaMandante"] as? Float
    }
  }
  var awayCompletedPasses: Int? {
    get {
      return self["PasseCertoVisitante"] as? Int
    }
  }
  var awayFouls: Int? {
    get {
      return self["FaltaCometidaVisitante"] as? Int
    }
  }
  var homeWrongCrosses: Int? {
    get {
      return self["CruzamentoErradoMandante"] as? Int
    }
  }
  var awayCompletedThroughBalls: Int? {
    get {
      return self["LancamentoCertoVisitante"] as? Int
    }
  }
  var homeShotsOffTarget: Int? {
    get {
      return self["FinalizacaoErradaMandante"] as? Int
    }
  }
  var homeCompletedTackles: Int? {
    get {
      return self["DesarmeCertoMandante"] as? Int
    }
  }
  var homeCompletedThroughBalls: Int? {
    get {
      return self["LancamentoCertoMandante"] as? Int
    }
  }
  var awayCompletedTackles: Int? {
    get {
      return self["DesarmeCertoVisitante"] as? Int
    }
  }
  var awayShotsOffTarget: Int? {
    get {
      return self["FinalizacaoErradaVisitante"] as? Int
    }
  }
  var homeCorners: Int? {
    get {
      return self["EscanteioProMandante"] as? Int
    }
  }
  var homeFouls: Int? {
    get {
      return self["FaltaCometidaMandante"] as? Int
    }
  }
  var awayWrongCrosses: Int? {
    get {
      return self["CruzamentoErradoVisitante"] as? Int
    }
  }
  var homeCompletedPasses: Int? {
    get {
      return self["PasseCertoMandante"] as? Int
    }
  }
  var matchId: Int? {
    get {
      return self["IdPartida"] as? Int
    }
  }
}

extension MatchStats {
  static func statsForMatchId(matchId: Int, completion: (inner: () throws -> MatchStats) -> ()) {
    if Reachability.isConnectedToNetwork() {
      getStatsForMatchId(matchId, completion: completion)
      fetchStatsForMatchId(matchId, completion: completion)
    } else {
      getStatsForMatchId(matchId, completion: completion)
    }
  }
  
  static func getStatsForMatchId(matchId: Int, completion: (inner: () throws -> MatchStats) -> ()) {
    guard let query = Team.query() else {
      completion {throw ModelError.Unknown}
      return
    }
    query.fromLocalDatastore()
    query.whereKey("IdPartida", equalTo: matchId)
    query.limit = 1000
    
    query.getFirstObjectInBackgroundWithBlock { matchStats, error in
      guard error == nil else {
        completion {throw ModelError.Unknown}
        return
      }
      guard let matchStats = matchStats as? MatchStats else {
        completion {throw ModelError.Unknown}
        return
      }
      completion {return matchStats}
    }
  }
  
  static func fetchStatsForMatchId(matchId: Int, completion: (inner: () throws -> MatchStats) -> ()) {
    guard let query = MatchStats.query() else {
      completion {throw ModelError.Unknown}
      return
    }
    query.whereKey("IdPartida", equalTo: matchId)
    query.limit = 1000
    
    query.getFirstObjectInBackgroundWithBlock { matchStats, error in
      guard error == nil else {
        completion {throw ModelError.Unknown}
        return
      }
      guard let matchStats = matchStats as? MatchStats else {
        completion {throw ModelError.Unknown}
        return
      }
      matchStats.pinInBackground()
      completion {return matchStats}
    }
  }
}

extension MatchStats: PFSubclassing {
  static func parseClassName() -> String {
    return "EstatisticasDaPartida"
  }
}

