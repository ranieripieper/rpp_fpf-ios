//
//  Constants.swift
//  FPF
//
//  Created by Gilson Gil on 1/11/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

struct Constants {
  static let defaultAnimationDuration: NSTimeInterval = 0.3
  static let defaultMargin: CGFloat = 8
  static let maxTeamSelection = 4
  static let scoreLabelWidth: CGFloat = 50
  static let smallLogoWidth: CGFloat = 40
  static let mediumLogoWidth: CGFloat = 50
  static let largeLogoWidth: CGFloat = 60
  static let tabbarHeight: CGFloat = 60
  static let tabbarAnimationDamping: CGFloat = 0.6
  static let hasPresentedNotificationsSettingsString = "HasPresentedNotificationsSettings"
  static let hasPresentedTutorialString = "HasPresentedTutorial"
  static let hasRegisteredForRemoteNotificationsString = "HasRegisteredForRemoteNotificationsString"
  static let updatedLeaguesNotificationString = "UpdatedLeaguesNotificationString"
  static let channelString = "ChannelString"
  static let liveTimerInterval: NSTimeInterval = 15
  
  static let a1Id = 444
  static let a2Id = 456
  static let a3Id = 461
  static let segundaId = 478
  static let libertadoresId = 449
}
