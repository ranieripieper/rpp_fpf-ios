//
//  AppDelegate.swift
//  FPF
//
//  Created by Gilson Gil on 1/7/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit
import Fabric
import Crashlytics

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
  var window: UIWindow?
  var modalNavigationController: UINavigationController?

  func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
    ParseHelper.setApplication()
    ParseHelper.trackPushAppOpen(launchOptions)
    
    Fabric.with([Crashlytics()])
    
    window = UIWindow(frame: UIScreen.mainScreen().bounds)
    window?.makeKeyAndVisible()
    window?.rootViewController = MainViewController()
    
    setUpAppearance()
    
    if hasNotPresentedNotificationsViewController() {
      presentNotificationsViewController()
    }
    
    if let notificationPayload = launchOptions?[UIApplicationLaunchOptionsRemoteNotificationKey] as? NSDictionary {
      if let matchId = notificationPayload["idPartida"] as? Int, let mainViewController = window?.rootViewController as? MainViewController {
        mainViewController.presentMatchWithId(matchId)
      }
    }
    
    return true
  }
  
  func hasNotPresentedNotificationsViewController() -> Bool {
    return !NSUserDefaults.standardUserDefaults().boolForKey(Constants.hasPresentedNotificationsSettingsString)
  }
  
  func presentNotificationsViewController() {
    if let window = window {
      let notificationsViewController = NotificationsViewController()
      modalNavigationController = UINavigationController(rootViewController: notificationsViewController)
      modalNavigationController!.view.frame = window.bounds
      window.addSubview(modalNavigationController!.view)
    }
  }
  
  func dismissNotificationsViewController() {
    modalNavigationController?.removeFromParentViewController()
    UIView.animateWithDuration(0.5, delay: 0.5, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .CurveEaseInOut, animations: { [weak self] in
      if let weakSelf = self {
        weakSelf.modalNavigationController?.view.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, 0, weakSelf.window!.bounds.height)
      }
      }, completion: { [weak self] finished in
        self?.modalNavigationController?.view.removeFromSuperview()
        NSUserDefaults.standardUserDefaults().setBool(true, forKey: Constants.hasPresentedNotificationsSettingsString)
    })
  }
  
  func setUpAppearance() {
    UINavigationBar.appearance().tintColor = UIColor.whiteColor()
    UINavigationBar.appearance().barTintColor = UIColor.fpfBlackColor()
    UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName: UIColor.fpfSunflowerColor()]
    
    ProgressHUDHelper.setUp()
  }
}

extension AppDelegate {
  func application(application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: NSError) {
    ParseHelper.didFailToRegisterForRemoteNotifications(error)
  }
  
  func application(application: UIApplication, didReceiveRemoteNotification userInfo: [NSObject : AnyObject]) {
    ParseHelper.didReceiveRemoteNotification(userInfo)
  }
  
  func application(application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
    ParseHelper.didRegisterForRemoteNotifications(deviceToken)
  }
}
