//
//  NewsListView.swift
//  FPF
//
//  Created by Gilson Gil on 5/5/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

protocol NewsListViewDelegate: class {
  func didSelectNews(news: News)
}

final class NewsListView: UIView {
  private let tableView = UITableView(frame: .zero, style: .Plain)
  private let imageCache = NSCache()
  private let estimatedImageRowHeight: CGFloat = 330
  private let estimatedTextRowHeight: CGFloat = 90
  
  weak var delegate: NewsListViewDelegate?
  weak var scrollDelegate: LeagueScrollDelegate?
  var newsListViewModel: NewsListViewModel? {
    didSet {
      dispatch_async(dispatch_get_main_queue()) { [weak self] in
        guard let weakSelf = self else {
          return
        }
        weakSelf.newsListViewModel?.configurators.forEach {
          $0.register(weakSelf.tableView)
        }
        weakSelf.tableView.reloadData()
      }
    }
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setUp()
  }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    setUp()
  }
  
  private func setUp() {
    translatesAutoresizingMaskIntoConstraints = false
    
    tableView.translatesAutoresizingMaskIntoConstraints = false
    tableView.dataSource = self
    tableView.delegate = self
    tableView.backgroundColor = UIColor.clearColor()
    addSubview(tableView)
    
    let views = ["tableView": tableView]
    let horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[tableView]|", options: [], metrics: nil, views: views)
    addConstraints(horizontalConstraints)
    let verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[tableView]|", options: [], metrics: nil, views: views)
    addConstraints(verticalConstraints)
  }
}

extension NewsListView: UITableViewDataSource {
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return newsListViewModel?.news.count ?? 0
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    guard let configurator = newsListViewModel?.configurators[indexPath.row] else {
      return UITableViewCell()
    }
    let cell = tableView.dequeueReusableCellWithIdentifier(configurator.reuseIdentifier, forIndexPath: indexPath) as! NewsCell
    cell.imageCache = imageCache
    configurator.update(cell)
    cell.delegate = self
    return cell
  }
}

extension NewsListView: UITableViewDelegate {
  func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    if let imageURL = newsListViewModel?.news[indexPath.row].imageURL where imageURL.characters.count > 0 {
      return estimatedImageRowHeight
    } else {
      return estimatedTextRowHeight
    }
  }
  
  func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    if let viewModel = newsListViewModel?.configurators[indexPath.row].currentViewModel() as? NewsCellViewModel, imageURL = viewModel.imageURL where imageURL.characters.count > 0 {
      if imageCache.objectForKey(imageURL) != nil {
        return UITableViewAutomaticDimension
      } else {
        return estimatedImageRowHeight
      }
    } else {
      return UITableViewAutomaticDimension
    }
  }
  
  func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    guard indexPath.row < newsListViewModel?.news.count, let news = newsListViewModel?.news[indexPath.row] else {
      return
    }
    delegate?.didSelectNews(news)
    tableView.deselectRowAtIndexPath(indexPath, animated: true)
  }
  
  func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
    return UIView()
  }
  
  func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
    return 1
  }
}

extension NewsListView: UIScrollViewDelegate {
  func scrollViewDidScroll(scroll: UIScrollView) {
    scrollDelegate?.scrollViewDidScroll(scroll)
  }
  
  func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
    scrollDelegate?.scrollViewDidStop(scrollView)
  }
  
  func scrollViewDidEndDragging(scrollView: UIScrollView, willDecelerate decelerate: Bool) {
    if !decelerate {
      scrollDelegate?.scrollViewDidStop(scrollView)
    }
  }
}

extension NewsListView: LeagueScrollProtocol {}

extension NewsListView: NewsCellDelegate {
  func didLoadImage(cell: NewsCell) {
    tableView.beginUpdates()
    tableView.endUpdates()
  }
}
