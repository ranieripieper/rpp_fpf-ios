//
//  NewsView.swift
//  FPF
//
//  Created by Gilson Gil on 5/5/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class NewsView: UIView {
  private let titleLabel = UILabel()
  private let dateLabel = UILabel()
  private let newsImageView = UIImageView()
  private let webView = UIWebView()
  private var webViewHeightConstraint: NSLayoutConstraint!
  
  private var newsViewModel: NewsViewModel
  
  required init?(coder aDecoder: NSCoder) {
    return nil
  }
  
  init(news: News) {
    newsViewModel = NewsViewModel(title: news.title, date: news.date, imageURL: news.imageURL, url: news.url, content: nil)
    super.init(frame: .zero)
    setUp()
    update(newsViewModel)
    newsViewModel.getContent { [weak self] inner in
      do {
        let newsViewModel = try inner()
        self?.update(newsViewModel)
        self?.newsViewModel = newsViewModel
      } catch {
        
      }
    }
  }
  
  private func setUp() {
    backgroundColor = UIColor.whiteColor()
    
    let scrollView = UIScrollView()
    scrollView.translatesAutoresizingMaskIntoConstraints = false
    addSubview(scrollView)
    
    let scrollViewContentView = UIView()
    scrollViewContentView.translatesAutoresizingMaskIntoConstraints = false
    scrollView.addSubview(scrollViewContentView)
    
    titleLabel.translatesAutoresizingMaskIntoConstraints = false
    titleLabel.font = UIFont.fpfMedium(16)
    titleLabel.textColor = UIColor.fpfBlackColor()
    titleLabel.numberOfLines = 0
    titleLabel.setContentHuggingPriority(UILayoutPriorityRequired, forAxis: .Vertical)
    scrollViewContentView.addSubview(titleLabel)
    
    dateLabel.translatesAutoresizingMaskIntoConstraints = false
    dateLabel.font = UIFont.fpfMedium(14)
    dateLabel.textColor = UIColor.fpfGreyishColor()
    dateLabel.setContentHuggingPriority(UILayoutPriorityRequired, forAxis: .Vertical)
    scrollViewContentView.addSubview(dateLabel)
    
    newsImageView.translatesAutoresizingMaskIntoConstraints = false
    newsImageView.contentMode = .ScaleAspectFit
    newsImageView.clipsToBounds = true
    newsImageView.setContentHuggingPriority(UILayoutPriorityRequired, forAxis: .Vertical)
    scrollViewContentView.addSubview(newsImageView)
    
    webView.translatesAutoresizingMaskIntoConstraints = false
    webView.delegate = self
    webView.scrollView.scrollEnabled = false
    scrollViewContentView.addSubview(webView)
    
    let metrics = ["hMargin": 25, "vMargin": 12]
    let views = ["scrollView": scrollView, "scrollViewContentView": scrollViewContentView, "titleLabel": titleLabel, "dateLabel": dateLabel, "newsImageView": newsImageView, "webView": webView]
    var horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-hMargin-[titleLabel]-hMargin-|", options: [], metrics: metrics, views: views)
    scrollViewContentView.addConstraints(horizontalConstraints)
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-hMargin-[dateLabel]-hMargin-|", options: [], metrics: metrics, views: views)
    scrollViewContentView.addConstraints(horizontalConstraints)
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-hMargin-[newsImageView]-hMargin-|", options: [], metrics: metrics, views: views)
    scrollViewContentView.addConstraints(horizontalConstraints)
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-hMargin-[webView]-hMargin-|", options: [], metrics: metrics, views: views)
    scrollViewContentView.addConstraints(horizontalConstraints)
    
    var verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|-hMargin-[titleLabel]-vMargin-[dateLabel]-vMargin-[newsImageView]-vMargin-[webView]-vMargin-|", options: [], metrics: metrics, views: views)
    scrollViewContentView.addConstraints(verticalConstraints)
    
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[scrollView]|", options: [], metrics: metrics, views: views)
    addConstraints(horizontalConstraints)
    verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[scrollView]|", options: [], metrics: metrics, views: views)
    addConstraints(verticalConstraints)
    
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[scrollViewContentView]|", options: [], metrics: metrics, views: views)
    scrollView.addConstraints(horizontalConstraints)
    verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[scrollViewContentView]|", options: [], metrics: metrics, views: views)
    scrollView.addConstraints(verticalConstraints)
    
    horizontalConstraints = [
      NSLayoutConstraint(item: scrollViewContentView, attribute: .Width, relatedBy: .Equal, toItem: self, attribute: .Width, multiplier: 1, constant: 0)
    ]
    addConstraints(horizontalConstraints)
    verticalConstraints = [
      NSLayoutConstraint(item: scrollViewContentView, attribute: .Height, relatedBy: .GreaterThanOrEqual, toItem: self, attribute: .Height, multiplier: 1, constant: -64)
    ]
    addConstraints(verticalConstraints)
    
    webViewHeightConstraint = NSLayoutConstraint(item: webView, attribute: .Height, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1, constant: 0)
    webViewHeightConstraint.priority = 999
    webView.addConstraint(webViewHeightConstraint)
  }
  
  func update(viewModel: NewsViewModel) {
    dispatch_async(dispatch_get_main_queue()) { [weak self] in
      self?.titleLabel.text = viewModel.title
      self?.dateLabel.text = viewModel.date
      guard let urlString = viewModel.imageURL, url = NSURL(string: urlString) else {
        self?.newsImageView.image = nil
        return
      }
      self?.newsImageView.kf_setImageWithURL(url)
      guard let content = viewModel.content else {
        return
      }
      if let regex = try? NSRegularExpression(pattern: "<script\\b[^>]*>([\\s\\S]*?)<\\/script>", options: .CaseInsensitive) {
        if let regex2 = try? NSRegularExpression(pattern: "<a[\\s]+([^>]+)>((?:.(?!\\<\\/a\\>))*.)<\\/a>", options: .CaseInsensitive) {
          let regexContent = regex.stringByReplacingMatchesInString(content, options: .WithTransparentBounds, range: NSMakeRange(0, content.characters.count), withTemplate: "")
          let regex2Content = regex2.stringByReplacingMatchesInString(regexContent, options: .WithTransparentBounds, range: NSMakeRange(0, regexContent.characters.count), withTemplate: "")
          let strippedContent = regex2Content.stringByReplacingOccurrencesOfString("width=\"640\"", withString: "width=\"100%\"")
          self?.webView.loadHTMLString(strippedContent, baseURL: nil)
        }
      }
    }
  }
}

extension NewsView: UIWebViewDelegate {
  func webViewDidFinishLoad(webView: UIWebView) {
    guard let string = webView.stringByEvaluatingJavaScriptFromString("document.body.scrollHeight;"), height = Int(string) else {
      return
    }
    webViewHeightConstraint.constant = CGFloat(height + 1)
  }
}
