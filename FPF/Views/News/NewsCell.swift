//
//  NewsCell.swift
//  FPF
//
//  Created by Gilson Gil on 5/5/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit
import Kingfisher

protocol NewsCellDelegate: class {
  func didLoadImage(cell: NewsCell)
}

final class NewsCell: UITableViewCell {
  private let titleLabel = UILabel()
  private let dateLabel = UILabel()
  private let newsImageView = UIImageView()
  weak var imageCache: NSCache?
  
  weak var delegate: NewsCellDelegate?
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }
  
  override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    setUp()
  }
  
  private func setUp() {
    backgroundColor = UIColor.whiteColor()
    
    titleLabel.translatesAutoresizingMaskIntoConstraints = false
    titleLabel.font = UIFont.fpfMedium(16)
    titleLabel.textColor = UIColor.fpfBlackColor()
    titleLabel.numberOfLines = 0
    contentView.addSubview(titleLabel)
    
    dateLabel.translatesAutoresizingMaskIntoConstraints = false
    dateLabel.font = UIFont.fpfMedium(14)
    dateLabel.textColor = UIColor.fpfGreyishColor()
    contentView.addSubview(dateLabel)
    
    newsImageView.translatesAutoresizingMaskIntoConstraints = false
    newsImageView.contentMode = .ScaleAspectFit
    newsImageView.clipsToBounds = true
    newsImageView.setContentHuggingPriority(UILayoutPriorityRequired, forAxis: .Vertical)
    contentView.addSubview(newsImageView)
    
    let metrics = ["hMargin": 25, "vMargin": 12]
    let views = ["titleLabel": titleLabel, "dateLabel": dateLabel, "newsImageView": newsImageView]
    var horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-hMargin-[titleLabel]-hMargin-|", options: [], metrics: metrics, views: views)
    contentView.addConstraints(horizontalConstraints)
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-hMargin-[dateLabel]-hMargin-|", options: [], metrics: metrics, views: views)
    contentView.addConstraints(horizontalConstraints)
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-hMargin-[newsImageView]-hMargin-|", options: [], metrics: metrics, views: views)
    contentView.addConstraints(horizontalConstraints)
    
    let verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|-vMargin-[titleLabel]-vMargin-[dateLabel]-vMargin-[newsImageView]-vMargin-|", options: [], metrics: metrics, views: views)
    contentView.addConstraints(verticalConstraints)
  }
  
  func update(viewModel: NewsCellViewModel) {
    titleLabel.text = viewModel.title
    dateLabel.text = viewModel.date
    newsImageView.image = nil
    guard let urlString = viewModel.imageURL, url = NSURL(string: urlString) else {
      return
    }
    if let image = imageCache?.objectForKey(urlString) {
      newsImageView.image = image as? UIImage
      return
    }
    newsImageView.kf_setImageWithURL(url, placeholderImage: nil, optionsInfo: nil) { [weak self] image, error, cacheType, imageURL in
      guard imageURL == url else {
        self?.newsImageView.image = nil
        return
      }
      guard let image = image, weakSelf = self else {
        return
      }
      weakSelf.imageCache?.setObject(image, forKey: urlString)
      weakSelf.delegate?.didLoadImage(weakSelf)
    }
  }
}

extension NewsCell: Updatable {
  typealias ViewModel = NewsCellViewModel
}
