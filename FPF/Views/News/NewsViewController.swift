//
//  NewsViewController.swift
//  FPF
//
//  Created by Gilson Gil on 5/5/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class NewsViewController: UIViewController {
  private let newsView: NewsView
  
  required init?(coder aDecoder: NSCoder) {
    return nil
  }
  
  init(news: News) {
    newsView = NewsView(news: news)
    super.init(nibName: nil, bundle: nil)
  }
  
  override func loadView() {
    view = newsView
  }
}
