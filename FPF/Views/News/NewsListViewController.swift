//
//  NewsListViewController.swift
//  FPF
//
//  Created by Gilson Gil on 5/5/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class NewsListViewController: UIViewController, LeagueViewControllerProtocol {
  private let newsListView = NewsListView()
  
  weak var leagueViewControllerDelegate: LeagueViewControllerDelegate?
  
  override func loadView() {
    view = newsListView
    newsListView.delegate = self
  }
  
  func setViewModel(viewModel: NewsListViewModel) {
    newsListView.newsListViewModel = viewModel
  }
  
  #if DEBUG
  deinit {
    print("deinit", self)
  }
  #endif
}

extension NewsListViewController: NewsListViewDelegate {
  func didSelectNews(news: News) {
    let newsViewController = NewsViewController(news: news)
    leagueViewControllerDelegate?.pushViewController(newsViewController)
  }
}
