//
//  MainViewController.swift
//  FPF
//
//  Created by Gilson Gil on 1/12/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class MainViewController: UINavigationController {
  private var menuAnimator: MenuAnimator!
  private var leagues: [League]?
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }
  
  override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: NSBundle?) {
    super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
  }
  
  init() {
    let leagueId = LeagueViewModel.lastLeagueOpened()
    
    let leagueViewController = LeagueViewController(leagueId: leagueId)
    
    super.init(rootViewController: leagueViewController)
    
    menuAnimator = MenuAnimator(navigationController: self)
    let pan = UIPanGestureRecognizer(target: self, action: #selector(MainViewController.openMenuWithPan(_:)))
    view.addGestureRecognizer(pan)
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    League.leagues { inner in
      self.leagues = try? inner()
    }
  }
  
  func openMenuWithPan(pan: UIPanGestureRecognizer) {
    guard let view = view where viewControllers.count == 1 else {
      return
    }
    
    menuAnimator.openMenuWithPan(pan)
    
    if pan.state == .Began && pan.locationInView(view).x > view.bounds.width * 0.9 {
      presentMenu()
    }
  }
  
  func presentMenu() {
    MenuViewModel.viewModel(leagues, viewType: selectedViewType()) { [weak self] inner in
      do {
        let viewModel = try inner()
        guard let weakSelf = self else {
          return
        }
        weakSelf.leagues = viewModel.leagues
        let menuViewController = MenuViewController(menuViewModel: viewModel)
        menuViewController.delegate = self
        menuViewController.transitioningDelegate = weakSelf.menuAnimator
        menuViewController.modalPresentationStyle = .OverFullScreen
        weakSelf.presentViewController(menuViewController, animated: true, completion: nil)
      } catch let error as NSError {
        print(error.localizedDescription)
      }
    }
  }
  
  func presentMatchWithId(matchId: Int) {
    ProgressHUDHelper.show()
    Match.fetchMatchWithId(matchId) { match in
      if let match = match {
        dispatch_async(dispatch_get_main_queue()) { [weak self] in
          let matchViewController = MatchViewController(matchViewModel: MatchViewModel(match: match))
          self?.viewControllers.last!.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
          self?.pushViewController(matchViewController, animated: true)
          ProgressHUDHelper.dismiss()
        }
      }
    }
  }
  
  private func selectedViewType() -> ViewType {
    switch viewControllers.first {
    case is NotificationsViewController:
      return .Notifications
    case is GuideHomeViewController:
      return .Guide
    case is AboutViewController:
      return .About
    case is LeagueViewController:
      let leagueViewController = viewControllers.first as! LeagueViewController
      return .League(leagueViewController.currentLeagueId)
    default:
      return .League(nil)
    }
  }
}

extension MainViewController: MenuViewControllerDelegate {
  func showLeague(league: League) {
    if let leagueViewController = viewControllers.first as? LeagueViewController where leagueViewController.currentLeagueId == league.id {
      menuAnimator.dismiss()
      return
    }
    let leagueViewModel = LeagueViewModel(league: league)
    let leagueViewController = LeagueViewController(leagueViewModel: leagueViewModel)
    viewControllers.forEach {
      $0.removeFromParentViewController()
    }
    viewControllers = [leagueViewController]
    LeagueViewModel.setLastLeagueOpened(league.id ?? 0, name: league.leagueName ?? league.name ?? "")
    menuAnimator.dismiss()
  }
  
  func showGuide() {
    if !(viewControllers.first is GuideHomeViewController) {
      viewControllers.forEach {
        $0.removeFromParentViewController()
      }
      let guideHomeViewController = GuideHomeViewController()
      viewControllers = [guideHomeViewController]
      addChildViewController(guideHomeViewController)
    }
    menuAnimator.dismiss()
  }
  
  func showNotifications() {
    if !(viewControllers.first is NotificationsViewController) {
      viewControllers.forEach {
        $0.removeFromParentViewController()
      }
      let notificationsViewController = NotificationsViewController()
      viewControllers = [notificationsViewController]
    }
    menuAnimator.dismiss()
  }
  
  func showAbout() {
    if !(viewControllers.first is AboutViewController) {
      viewControllers.forEach {
        $0.removeFromParentViewController()
      }
      let aboutViewController = AboutViewController()
      viewControllers = [aboutViewController]
    }
    menuAnimator.dismiss()
  }
}
