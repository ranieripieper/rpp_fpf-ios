//
//  TabbarView.swift
//  FPF
//
//  Created by Gilson Gil on 1/11/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

protocol TabbarViewDatasource: class {
  func tabbarViewNumberOfButtons() -> Int
  func tabbarViewTitleAtIndex(index: Int) -> String
}

protocol TabbarViewDelegate: class {
  func didSelectButtonAtIndex(index: Int)
}

final class TabbarView: UIView {
  private var tabbarButtons = [TabbarButton]()
  
  weak var dataSource: TabbarViewDatasource? {
    didSet {
      setUp()
    }
  }
  weak var delegate: TabbarViewDelegate?
  
  override init(frame: CGRect) {
    super.init(frame: frame)
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }
  
  convenience init() {
    self.init(frame: .zero)
  }
  
  private func setUp() {
    dispatch_async(dispatch_get_main_queue()) { [weak self] in
      self?.backgroundColor = UIColor.clearColor()
      
      var tabbarButtons: [TabbarButton] = []
      guard let dataSource = self?.dataSource else {
        return
      }
      
      for i in 0..<dataSource.tabbarViewNumberOfButtons() {
        guard let button = self?.createButtonWithTitle(dataSource.tabbarViewTitleAtIndex(i)) else {
          continue
        }
        button.selected = i == 0
        self?.addSubview(button)
        tabbarButtons.append(button)
      }
      
      self?.tabbarButtons = tabbarButtons
      self?.addConstraintsForViews(tabbarButtons)
    }
  }
  
  private func createButtonWithTitle(title: String) -> TabbarButton {
    let button = TabbarButton()
    button.translatesAutoresizingMaskIntoConstraints = false
    button.setTitle(title, forState: .Normal)
    button.addTarget(self, action: #selector(TabbarView.buttonPressed(_:)), forControlEvents: .TouchUpInside)
    
    return button
  }
  
  func buttonPressed(button: TabbarButton) {
    guard let index = tabbarButtons.indexOf(button) else {
      return
    }
    
    delegate?.didSelectButtonAtIndex(index)
    tabbarButtons.forEach { tabbarButton in
      tabbarButton.selected = tabbarButton == button
    }
  }
  
  private func addConstraintsForViews(views: [UIView]) {
    guard views.count > 0 else {
      return
    }
    
    if let first = views.first {
      let leftConstant = NSLayoutConstraint(item: first, attribute: .Left, relatedBy: .Equal, toItem: self, attribute: .Left, multiplier: 1, constant: 0)
      addConstraint(leftConstant)
    }
    if let last = views.last {
      let rightConstraint = NSLayoutConstraint(item: last, attribute: .Right, relatedBy: .Equal, toItem: self, attribute: .Right, multiplier: 1, constant: 0)
      addConstraint(rightConstraint)
    }
    
    for i in 0 ..< views.count - 1 {
      let left = views[i]
      let right = views[i + 1]
      let separator = UIView()
      separator.translatesAutoresizingMaskIntoConstraints = false
      separator.backgroundColor = UIColor.blackColor()
      addSubview(separator)
      let spaceLeftConstraint = NSLayoutConstraint(item: left, attribute: .Right, relatedBy: .Equal, toItem: separator, attribute: .Left, multiplier: 1, constant: 0)
      addConstraint(spaceLeftConstraint)
      let separatorConstraint = NSLayoutConstraint(item: separator, attribute: .Width, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1, constant: 4)
      addConstraint(separatorConstraint)
      let spaceRightConstraint = NSLayoutConstraint(item: separator, attribute: .Right, relatedBy: .Equal, toItem: right, attribute: .Left, multiplier: 1, constant: 0)
      addConstraint(spaceRightConstraint)
      let widthConstraint = NSLayoutConstraint(item: left, attribute: .Width, relatedBy: .Equal, toItem: right, attribute: .Width, multiplier: 1, constant: 0)
      addConstraint(widthConstraint)
      let separatorVerticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[view]|", options: [], metrics: nil, views: ["view": separator])
      addConstraints(separatorVerticalConstraints)
    }
    
    views.forEach { view in
      let constraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[view]|", options: [], metrics: nil, views: ["view": view])
      self.addConstraints(constraints)
    }
  }
  
  #if DEBUG
  deinit {
    print("deinit", self)
  }
  #endif
}
