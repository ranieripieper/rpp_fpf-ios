//
//  LeagueViewController.swift
//  FPF
//
//  Created by Gilson Gil on 1/7/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

protocol LeagueViewControllerProtocol: class {
  weak var leagueViewControllerDelegate: LeagueViewControllerDelegate? {get set}
}

protocol LeagueViewControllerDelegate: class {
  func pushViewController(viewController: UIViewController)
  func presentViewController(viewController: UIViewController)
}

final class LeagueViewController: UIViewController {
  private var leagueView: LeagueView
  private var leagueViewModel: LeagueViewModel?
  private var currentViewController: UIViewController?
  private var standingsViewController: StandingsViewController?
  private var scheduleViewController: ScheduleViewController?
  private var scorersViewController: ScorersViewController?
  private var newsListViewController: NewsListViewController?
  var currentLeagueId: Int {
    return leagueViewModel?.leagueId ?? 0
  }
  
  required init?(coder aDecoder: NSCoder) {
    leagueViewModel = nil
    leagueView = LeagueView(leagueViewModel: nil)
    super.init(coder: aDecoder)
  }
  
  init(leagueId: Int?) {
    leagueViewModel = nil
    leagueView = LeagueView(leagueViewModel: nil)
    super.init(nibName: nil, bundle: nil)
    LeagueViewModel.viewModel(leagueId) { [weak self] inner in
      dispatch_async(dispatch_get_main_queue()) {
        do {
          let leagueViewModel = try inner()
          self?.leagueViewModel = leagueViewModel
          self?.leagueView.leagueViewModel = leagueViewModel
          if leagueViewModel.hasStandings {
            self?.presentStandings()
          } else {
            self?.presentSchedules()
          }
          self?.navigationItem.title = leagueViewModel.leagueName
          
          let menuBarButton = UIBarButtonItem(image: UIImage.icnMenu(), style: .Plain, target: self?.navigationController, action: #selector(MainViewController.presentMenu))
          self?.navigationItem.rightBarButtonItems = [menuBarButton]
        } catch {
          
        }
      }
    }
  }
  
  init(leagueViewModel: LeagueViewModel) {
    self.leagueViewModel = leagueViewModel
    leagueView = LeagueView(leagueViewModel: leagueViewModel)
    super.init(nibName: nil, bundle: nil)
    if leagueViewModel.needsUpdateLeague {
      League.leagueWithId(leagueViewModel.leagueId) { [weak self] inner in
        dispatch_async(dispatch_get_main_queue()) { [weak self] in
          do {
            let league = try inner()
            let updatedLeagueViewModel = LeagueViewModel(league: league)
            self?.leagueViewModel = updatedLeagueViewModel
          } catch {
            
          }
        }
      }
    }
    navigationItem.title = leagueViewModel.leagueName
    
    let menuBarButton = UIBarButtonItem(image: UIImage.icnMenu(), style: .Plain, target: navigationController, action: #selector(MainViewController.presentMenu))
    navigationItem.rightBarButtonItems = [menuBarButton]
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    guard let leagueViewModel = leagueViewModel else {
      return
    }
    if leagueViewModel.hasStandings {
      presentStandings()
    } else {
      presentSchedules()
    }
  }
  
  override func loadView() {
    view = leagueView
    leagueView.delegate = self
  }
  
  private func presentStandings() {
    if standingsViewController == nil {
      if let appDelegate = UIApplication.sharedApplication().delegate as? AppDelegate, window = appDelegate.window where window.subviews.count == 1 {
        ProgressHUDHelper.show()
      }
      standingsViewController = StandingsViewController()
      standingsViewController!.leagueViewControllerDelegate = self
    }
    present(standingsViewController!)
    
    leagueViewModel?.refreshTeams() { [weak self] in
      self?.getStandings()
    }
  }
  
  private func getStandings() {
    StandingsViewModel.standingsForLeagueId(currentLeagueId) { standingsViewModel in
      self.standingsViewController!.standingsViewModel = standingsViewModel
      ProgressHUDHelper.dismiss()
    }
  }

  private func presentSchedules() {
    let shouldReload: Bool
    if scheduleViewController == nil {
      ProgressHUDHelper.show()
      scheduleViewController = ScheduleViewController()
      scheduleViewController!.leagueViewControllerDelegate = self
      shouldReload = true
    } else {
      shouldReload = false
    }
    present(scheduleViewController!)
    
    ScheduleViewModel.matchesForLeagueId(currentLeagueId, currentRound: leagueViewModel?.currentRound ?? 1, forced: shouldReload) { scheduleViewModel in
      self.scheduleViewController!.scheduleViewModel = scheduleViewModel
      ProgressHUDHelper.dismiss()
    }
  }
  
  private func presentScorers() {
    if scorersViewController == nil {
      ProgressHUDHelper.show()
      scorersViewController = ScorersViewController()
      scorersViewController!.leagueViewControllerDelegate = self
    }
    present(scorersViewController!)
    
    ScorersViewModel.scorersForLeagueId(currentLeagueId) { scorersViewModel in
      self.scorersViewController!.scorersViewModel = scorersViewModel
      ProgressHUDHelper.dismiss()
    }
  }
  
  private func presentNews() {
    if newsListViewController == nil {
      ProgressHUDHelper.show()
      newsListViewController = NewsListViewController()
      newsListViewController!.leagueViewControllerDelegate = self
    }
    present(newsListViewController!)
    
    guard let leagueNewsId = leagueViewModel?.leagueNewsId else {
      return
    }
    
    NewsListViewModel.viewModel(leagueNewsId) { inner in
      guard let viewModel = try? inner() else {
        return
      }
      self.newsListViewController!.setViewModel(viewModel)
      ProgressHUDHelper.dismiss()
    }
  }
  
  private func present<T: UIViewController where T: LeagueViewControllerProtocol>(viewController: T) {
    dispatch_async(dispatch_get_main_queue()) { [weak self] in
      if let currentViewController = self?.currentViewController {
        currentViewController.view.removeFromSuperview()
        currentViewController.removeFromParentViewController()
        self?.currentViewController = nil
      }
      
      viewController.willMoveToParentViewController(self)
      self?.leagueView.presentView(viewController.view)
      self?.addChildViewController(viewController)
      viewController.didMoveToParentViewController(self)
      self?.currentViewController = viewController
    }
  }
  
  override func removeFromParentViewController() {
    leagueView.delegate = nil
    standingsViewController?.leagueViewControllerDelegate = nil
    scheduleViewController?.leagueViewControllerDelegate = nil
    scorersViewController?.leagueViewControllerDelegate = nil
    super.removeFromParentViewController()
  }
  
  #if DEBUG
  deinit {
    print("deinit", self)
  }
  #endif
}

extension LeagueViewController: LeagueViewDelegate {
  func didSelectStandings() {
    guard currentViewController != standingsViewController else {
      return
    }
    presentStandings()
  }
  
  func didSelectSchedule() {
    guard currentViewController != scheduleViewController else {
      return
    }
    presentSchedules()
  }
  
  func didSelectScorers() {
    guard currentViewController != scorersViewController else {
      return
    }
    presentScorers()
  }
  
  func didSelectNews() {
    guard currentViewController != newsListViewController else {
      return
    }
    presentNews()
  }
}

extension LeagueViewController: LeagueViewControllerDelegate {
  func pushViewController(viewController: UIViewController) {
    navigationController?.pushViewController(viewController, animated: true)
    navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
  }
  
  func presentViewController(viewController: UIViewController) {
    presentViewController(viewController, animated: false, completion: nil)
  }
}
