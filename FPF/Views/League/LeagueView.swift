//
//  LeagueView.swift
//  FPF
//
//  Created by Gilson Gil on 1/7/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

protocol LeagueViewDelegate: class {
  func didSelectStandings()
  func didSelectSchedule()
  func didSelectScorers()
  func didSelectNews()
}

protocol LeagueScrollDelegate: class {
  func scrollViewDidScroll(scrollView: UIScrollView)
  func scrollViewDidStop(scrollView: UIScrollView)
}

protocol LeagueScrollProtocol: class {
  weak var scrollDelegate: LeagueScrollDelegate? {get set}
}

final class LeagueView: UIView {
  private let tabbarView = TabbarView()
  
  var leagueViewModel: LeagueViewModel? {
    didSet {
      tabbarView.dataSource = self
    }
  }
  weak var delegate: LeagueViewDelegate?
  private var lastYOffset: CGFloat = 0
  
  override init(frame: CGRect) {
    super.init(frame: frame)
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }
  
  convenience init(leagueViewModel: LeagueViewModel?) {
    self.init(frame: .zero)
    self.leagueViewModel = leagueViewModel
    setUp()
  }
  
  private func setUp() {
    backgroundColor = UIColor.fpfBlackColor()
    
    let background = RadialGradientView()
    background.translatesAutoresizingMaskIntoConstraints = false
    background.gradient = Gradient(startColor: UIColor.fpfCharcoalColor(), endColor: UIColor.fpfBlackColor())
    addSubview(background)
    
    let logoImage = UIImage.imgLogoMenu()
    let logoImageView = UIImageView(image: logoImage)
    logoImageView.translatesAutoresizingMaskIntoConstraints = false
    logoImageView.contentMode = .Center
    addSubview(logoImageView)
    
    tabbarView.dataSource = self
    tabbarView.delegate = self
    tabbarView.translatesAutoresizingMaskIntoConstraints = false
    addSubview(tabbarView)
    
    let horizontalConstraint = NSLayoutConstraint(item: logoImageView, attribute: .CenterX, relatedBy: .Equal, toItem: self, attribute: .CenterX, multiplier: 1, constant: 0)
    addConstraint(horizontalConstraint)
    let verticalConstraint = NSLayoutConstraint(item: logoImageView, attribute: .Top, relatedBy: .Equal, toItem: self, attribute: .Top, multiplier: 1, constant: 84)
    addConstraint(verticalConstraint)
    
    let metrics = ["tabbarHeight": Constants.tabbarHeight]
    let views = ["tabbarView": tabbarView, "background": background]
    var horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[tabbarView]|", options: [], metrics: nil, views: views)
    let verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:[tabbarView(tabbarHeight)]|", options: [], metrics: metrics, views: views)
    addConstraints(horizontalConstraints)
    addConstraints(verticalConstraints)
    
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[background]|", options: [], metrics: metrics, views: views)
    addConstraints(horizontalConstraints)
    
    let ratioConstraint = NSLayoutConstraint(item: background, attribute: .Height, relatedBy: .Equal, toItem: background, attribute: .Width, multiplier: 1, constant: 0)
    background.addConstraint(ratioConstraint)
    let centerConstraint = NSLayoutConstraint(item: background, attribute: .CenterY, relatedBy: .Equal, toItem: logoImageView, attribute: .CenterY, multiplier: 1, constant: 0)
    addConstraint(centerConstraint)
  }
  
  func presentView(view: UIView) {
    dispatch_async(dispatch_get_main_queue()) { [weak self] in
      if let weakSelf = self {
        weakSelf.insertSubview(view, belowSubview: weakSelf.tabbarView)
        let views = ["view": view]
        let horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[view]|", options: [], metrics: nil, views: views)
        let verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|-64-[view]|", options: [], metrics: nil, views: views)
        weakSelf.addConstraints(horizontalConstraints)
        weakSelf.addConstraints(verticalConstraints)
        
        if let scrollView = view as? LeagueScrollProtocol {
          scrollView.scrollDelegate = weakSelf
        }
        weakSelf.tabbarView.transform = CGAffineTransformIdentity
      }
    }
  }
  
  #if DEBUG
  deinit {
    print("deinit", self)
  }
  #endif
}

extension LeagueView: LeagueScrollDelegate {
  func scrollViewDidScroll(scrollView: UIScrollView) {
    let aboveBottom = scrollView.contentOffset.y + scrollView.bounds.height <= scrollView.contentSize.height
    guard aboveBottom else {
      return
    }
    let deltaY = (scrollView.contentOffset.y - lastYOffset) / 2
    if scrollView.contentOffset.y <= -scrollView.contentInset.top {
      UIView.animateWithDuration(Constants.defaultAnimationDuration, delay: 0, usingSpringWithDamping: Constants.tabbarAnimationDamping, initialSpringVelocity: 0, options: [], animations: {
        self.tabbarView.transform = CGAffineTransformIdentity
        }, completion: nil)
    } else {
      tabbarView.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, 0, max(min(tabbarView.transform.ty + deltaY, Constants.tabbarHeight), 0))
    }
    lastYOffset = scrollView.contentOffset.y
  }
  
  func scrollViewDidStop(scrollView: UIScrollView) {
    UIView.animateWithDuration(Constants.defaultAnimationDuration, delay: 0, usingSpringWithDamping: Constants.tabbarAnimationDamping, initialSpringVelocity: 0, options: [], animations: {
      if self.tabbarView.transform.ty <= Constants.tabbarHeight / 2 {
        self.tabbarView.transform = CGAffineTransformIdentity
      } else {
        self.tabbarView.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, 0, Constants.tabbarHeight)
      }
      }, completion: nil)
  }
}

extension LeagueView: TabbarViewDatasource {
  func tabbarViewNumberOfButtons() -> Int {
    if let leagueViewModel = leagueViewModel where !leagueViewModel.hasStandings {
      return leagueViewModel.tabbarTitles.count - 1
    }
    return leagueViewModel?.tabbarTitles.count ?? 0
  }
  
  func tabbarViewTitleAtIndex(index: Int) -> String {
    if let leagueViewModel = leagueViewModel where !leagueViewModel.hasStandings {
      return leagueViewModel.tabbarTitles[index + 1]
    }
    return leagueViewModel?.tabbarTitles[index] ?? ""
  }
}

extension LeagueView: TabbarViewDelegate {
  func didSelectButtonAtIndex(index: Int) {
    let modIndex: Int
    if let leagueViewModel = leagueViewModel where !leagueViewModel.hasStandings {
      modIndex = index + 1
    } else {
      modIndex = index
    }
    switch modIndex {
    case 0:
      delegate?.didSelectStandings()
    case 1:
      delegate?.didSelectSchedule()
    case 2:
      delegate?.didSelectScorers()
    case 3:
      delegate?.didSelectNews()
    default:
      break
    }
  }
}
