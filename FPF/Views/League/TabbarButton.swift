//
//  TabbarButton.swift
//  FPF
//
//  Created by Gilson Gil on 1/14/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class TabbarButton: UIButton {
  private let indicatorHeight: CGFloat = 7
  private let indicatorMaxWidth: CGFloat = 123
  
  private var selectionIndicator: UIView? = nil
  
  override var selected: Bool {
    willSet(newValue) {
      if newValue {
        if selectionIndicator == nil {
          selectionIndicator = addIndicator()
        }
      } else {
        selectionIndicator?.removeFromSuperview()
        selectionIndicator = nil
      }
    }
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }
  
  init() {
    super.init(frame: .zero)
    backgroundColor = UIColor.blackColor()
    alpha = 0.85
    setTitleColor(UIColor.whiteColor(), forState: .Normal)
    setTitleColor(UIColor.fpfSunflowerColor(), forState: .Selected)
    titleLabel?.font = UIFont.fpfMedium(14)
    titleLabel?.adjustsFontSizeToFitWidth = true
    titleLabel?.minimumScaleFactor = 0.5
    titleEdgeInsets = UIEdgeInsets(top: 0, left: 4, bottom: 0, right: 4)
  }
  
  private func addIndicator() -> UIView {
    let indicator = UIView()
    indicator.translatesAutoresizingMaskIntoConstraints = false
    indicator.backgroundColor = UIColor.fpfSunflowerColor()
    addSubview(indicator)
    
    let metrics = ["height": indicatorHeight]
    let views = ["view": indicator]
    
    let horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|->=0-[view]->=0-|", options: [], metrics: nil, views: views)
    addConstraints(horizontalConstraints)
    
    let widthConstraint = NSLayoutConstraint(item: indicator, attribute: .Width, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1, constant: indicatorMaxWidth)
    widthConstraint.priority = UILayoutPriorityDefaultHigh
    addConstraint(widthConstraint)
    
    let centerConstraint = NSLayoutConstraint(item: indicator, attribute: .CenterX, relatedBy: .Equal, toItem: self, attribute: .CenterX, multiplier: 1, constant: 0)
    addConstraint(centerConstraint)
    
    let verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:[view(height)]|", options: [], metrics: metrics, views: views)
    addConstraints(verticalConstraints)
    
    return indicator
  }
  
  #if DEBUG
  deinit {
    print("deinit", self)
  }
  #endif
}
