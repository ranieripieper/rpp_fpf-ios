//
//  ScheduleRoundCell.swift
//  FPF
//
//  Created by Gilson Gil on 1/11/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

protocol ScheduleRoundCellDelegate: class {
  func didSelectMatchAtIndex(index: Int)
  func didScroll(scrollView: UIScrollView)
}

final class ScheduleRoundCell: UICollectionViewCell {
  private let tableView = UITableView()
  
  var roundCellViewModel: RoundCellViewModel? {
    didSet {
      tableView.reloadData()
    }
  }
  
  weak var delegate: ScheduleRoundCellDelegate?
  weak var scrollDelegate: LeagueScrollDelegate?
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setUp()
  }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    setUp()
  }
  
  private func setUp() {
    translatesAutoresizingMaskIntoConstraints = false
    
    tableView.translatesAutoresizingMaskIntoConstraints = false
    tableView.separatorColor = UIColor.fpfPinkishGreyColor()
    tableView.layoutMargins = UIEdgeInsetsZero
    if #available(iOS 9.0, *) {
        tableView.cellLayoutMarginsFollowReadableWidth = false
    }
    tableView.estimatedRowHeight = 110
    tableView.dataSource = self
    tableView.delegate = self
    tableView.backgroundColor = UIColor.clearColor()
    tableView.registerClass(ScheduleMatchCell.classForCoder(), forCellReuseIdentifier: ScheduleMatchCell.reuseIdentifier())
    addSubview(tableView)
    
    let views = ["tableView": tableView]
    let horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[tableView]|", options: [], metrics: nil, views: views)
    let verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[tableView]|", options: [], metrics: nil, views: views)
    addConstraints(horizontalConstraints)
    addConstraints(verticalConstraints)
  }
  
  func configureWithRoundCellViewModel(viewModel: RoundCellViewModel) {
    roundCellViewModel = viewModel
  }
  
  #if DEBUG
  deinit {
    print("deinit", self)
  }
  #endif
}

extension ScheduleRoundCell: UITableViewDataSource {
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return roundCellViewModel?.matches.count ?? 0
  }
  
  func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
    let footer = UIView()
    footer.backgroundColor = UIColor.clearColor()
    return footer
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCellWithIdentifier(ScheduleMatchCell.reuseIdentifier(), forIndexPath: indexPath) as! ScheduleMatchCell
    cell.configureWithMatchCellViewModel(MatchCellViewModel(match: roundCellViewModel!.matches[indexPath.row]))
    return cell
  }
}

extension ScheduleRoundCell: UITableViewDelegate {
  func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    return UITableViewAutomaticDimension
  }
  
  func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    delegate?.didSelectMatchAtIndex(indexPath.row)
    tableView.deselectRowAtIndexPath(indexPath, animated: true)
  }
  
  func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
    return 1
  }
}

extension ScheduleRoundCell: ReuseIdentifier {
  static func reuseIdentifier() -> String {
    return "ScheduleRoundCell"
  }
}

extension ScheduleRoundCell: UIScrollViewDelegate {
  func scrollViewDidScroll(scroll: UIScrollView) {
    scrollDelegate?.scrollViewDidScroll(scroll)
    delegate?.didScroll(scroll)
  }
  
  func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
    scrollDelegate?.scrollViewDidStop(scrollView)
  }
  
  func scrollViewDidEndDragging(scrollView: UIScrollView, willDecelerate decelerate: Bool) {
    if !decelerate {
      scrollDelegate?.scrollViewDidStop(scrollView)
    }
  }
}

extension ScheduleRoundCell: LeagueScrollProtocol {
  
}
