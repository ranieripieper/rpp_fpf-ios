//
//  ScheduleViewController.swift
//  FPF
//
//  Created by Gilson Gil on 1/8/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class ScheduleViewController: UIViewController, LeagueViewControllerProtocol {
  private let scheduleView = ScheduleView()
  
  var scheduleViewModel: ScheduleViewModel? {
    didSet {
      scheduleView.scheduleViewModel = scheduleViewModel
    }
  }
  
  weak var leagueViewControllerDelegate: LeagueViewControllerDelegate?
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }
  
  init() {
    super.init(nibName: nil, bundle: nil)
  }
  
  override func loadView() {
    scheduleView.delegate = self
    view = scheduleView
  }
  
  #if DEBUG
  deinit {
    print("deinit", self)
  }
  #endif
}

extension ScheduleViewController: ScheduleViewDelegate {
  func didSelectMatchAtRound(round: Int, index: Int) {
    guard let match = scheduleViewModel?.rounds[round].matches[index] where match.hasLive ?? false, let date = match.date where date.compare(NSDate()) == .OrderedAscending else {
      return
    }
    let matchViewController = MatchViewController(matchViewModel: MatchViewModel(match: match))
    leagueViewControllerDelegate?.pushViewController(matchViewController)
  }
}
