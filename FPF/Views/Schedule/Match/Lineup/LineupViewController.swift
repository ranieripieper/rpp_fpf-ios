//
//  LineupViewController.swift
//  FPF
//
//  Created by Gilson Gil on 1/8/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class LineupViewController: UIViewController {
  private let lineupView = LineupView()
  
  var lineupViewModel: LineupViewModel? {
    didSet {
      lineupView.lineupViewModel = lineupViewModel
    }
  }
  
  override func loadView() {
    view = lineupView
  }
  
  #if DEBUG
  deinit {
    print("deinit", self)
  }
  #endif
}
