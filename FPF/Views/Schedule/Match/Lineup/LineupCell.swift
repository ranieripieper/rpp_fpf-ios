//
//  LineupCell.swift
//  FPF
//
//  Created by Gilson Gil on 1/18/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class LineupCell: UITableViewCell {
  private let stripeWidth: CGFloat = 1
  
  private let player1View = LineupPlayerView()
  private let player2View = LineupPlayerView()
  
  var viewModel: LineupCellViewModel? {
    didSet {
      if let viewModel = viewModel {
        if viewModel.hasHomePlayer {
          player1View.subviews.forEach {
            $0.hidden = false
          }
          player1View.nameLabel.text = viewModel.homeName
          player1View.posLabel.text = viewModel.homePosition!.substringToIndex(viewModel.homePosition!.startIndex.advancedBy(3)).uppercaseString
          if let homeGoals = viewModel.homeGoals where homeGoals > 0 {
            player1View.goalImageView.image = UIImage.icnLineupGoal()
            player1View.goalLabel.text = String(homeGoals)
          } else {
            player1View.goalImageView.image = nil
            player1View.goalLabel.text = nil
          }
          if let subs = viewModel.homeSubstitute where subs {
            player1View.subsImageView.image = UIImage.icnLineupSubsOut()
          } else if let subs = viewModel.homeSubstitutee where subs {
            player1View.subsImageView.image = UIImage.icnLineupSubsIn()
          } else {
            player1View.subsImageView.image = nil
          }
          if let red = viewModel.homeRedCard where red {
            player1View.redCardImageView.image = UIImage.icnLineupRedCard()
          } else {
            player1View.redCardImageView.image = nil
          }
          if let yellow = viewModel.homeYellowCard where yellow {
            player1View.yellowCardImageView.image = UIImage.icnLineupYellowCard()
          } else {
            player1View.yellowCardImageView.image = nil
          }
        } else {
          player1View.subviews.forEach {
            $0.hidden = true
          }
        }
        if viewModel.hasAwayPlayer {
          player2View.subviews.forEach {
            $0.hidden = false
          }
          player2View.nameLabel.text = viewModel.awayName
          player2View.posLabel.text = viewModel.awayPosition!.substringToIndex(viewModel.awayPosition!.startIndex.advancedBy(3)).uppercaseString
          if let awayGoals = viewModel.awayGoals where awayGoals > 0 {
            player2View.goalImageView.image = UIImage.icnLineupGoal()
            player2View.goalLabel.text = String(awayGoals)
          } else {
            player2View.goalImageView.image = nil
            player2View.goalLabel.text = nil
          }
          if let subs = viewModel.awaySubstitute where subs {
            player2View.subsImageView.image = UIImage.icnLineupSubsOut()
          } else if let subs = viewModel.awaySubstitutee where subs {
            player2View.subsImageView.image = UIImage.icnLineupSubsIn()
          } else {
            player2View.subsImageView.image = nil
          }
          if let red = viewModel.awayRedCard where red {
            player2View.redCardImageView.image = UIImage.icnLineupRedCard()
          } else {
            player2View.redCardImageView.image = nil
          }
          if let yellow = viewModel.awayYellowCard where yellow {
            player2View.yellowCardImageView.image = UIImage.icnLineupYellowCard()
          } else {
            player2View.yellowCardImageView.image = nil
          }
        } else {
          player2View.subviews.forEach {
            $0.hidden = true
          }
        }
      }
    }
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setUp()
  }
  
  override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    setUp()
  }
  
  private func setUp() {
    contentView.translatesAutoresizingMaskIntoConstraints = false
    selectionStyle = .None
    
    player1View.translatesAutoresizingMaskIntoConstraints = false
    contentView.addSubview(player1View)
    
    let stripe = UIView()
    stripe.translatesAutoresizingMaskIntoConstraints = false
    stripe.backgroundColor = UIColor.fpfStripeGreyColor()
    contentView.addSubview(stripe)
    
    player2View.translatesAutoresizingMaskIntoConstraints = false
    contentView.addSubview(player2View)
    
    let metrics = ["stripeWidth": stripeWidth]
    let views = ["player1View": player1View, "stripe": stripe, "player2View": player2View]
    
    var horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[player1View][stripe(stripeWidth)][player2View]|", options: [], metrics: metrics, views: views)
    contentView.addConstraints(horizontalConstraints)
    
    horizontalConstraints = [NSLayoutConstraint(item: player1View, attribute: .Width, relatedBy: .Equal, toItem: player2View, attribute: .Width, multiplier: 1, constant: 0)]
    contentView.addConstraints(horizontalConstraints)
    
    var verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[player1View]|", options: [], metrics: nil, views: views)
    contentView.addConstraints(verticalConstraints)
    
    verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[stripe]|", options: [], metrics: nil, views: views)
    contentView.addConstraints(verticalConstraints)
    
    verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[player2View]|", options: [], metrics: nil, views: views)
    contentView.addConstraints(verticalConstraints)
    
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[contentView]|", options: [], metrics: nil, views: ["contentView": contentView])
    addConstraints(horizontalConstraints)
    verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[contentView]|", options: [], metrics: nil, views: ["contentView": contentView])
    addConstraints(verticalConstraints)
  }
  
  func configureWithLineupCellViewModel(viewModel: LineupCellViewModel, index: Int) {
    self.viewModel = viewModel
    if index % 2 == 0 {
      player1View.backgroundColor = UIColor.whiteColor()
      player2View.backgroundColor = UIColor.whiteColor()
    } else {
      player1View.backgroundColor = UIColor.fpfLightGrey30Color()
      player2View.backgroundColor = UIColor.fpfLightGrey30Color()
    }
  }
    
  #if DEBUG
  deinit {
    print("deinit", self)
  }
  #endif
}

extension LineupCell: ReuseIdentifier {
  static func reuseIdentifier() -> String {
    return "LineupCell"
  }
}
