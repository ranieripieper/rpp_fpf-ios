//
//  LineupPlayerView.swift
//  FPF
//
//  Created by Gilson Gil on 1/18/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class LineupPlayerView: UIView {
  private let margin: CGFloat = Constants.defaultMargin
  private let largeMargin: CGFloat = 12
  
  let nameLabel = UILabel()
  let posLabel = UILabel()
  let goalImageView = UIImageView()
  let goalLabel = UILabel()
  let subsImageView = UIImageView()
  let redCardImageView = UIImageView()
  let yellowCardImageView = UIImageView()
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setUp()
  }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    setUp()
  }
  
  convenience init() {
    self.init(frame: .zero)
  }
  
  private func setUp() {
    translatesAutoresizingMaskIntoConstraints = false
    backgroundColor = UIColor.clearColor()
    
    nameLabel.translatesAutoresizingMaskIntoConstraints = false
    nameLabel.font = UIFont.fpfMedium(16)
    nameLabel.textColor = UIColor.fpfBlackColor()
    addSubview(nameLabel)
    
    posLabel.translatesAutoresizingMaskIntoConstraints = false
    posLabel.font = UIFont.fpfMedium(12)
    posLabel.textColor = UIColor.fpfGreyishColor()
    addSubview(posLabel)
    
    goalImageView.translatesAutoresizingMaskIntoConstraints = false
    addSubview(goalImageView)
    
    goalLabel.translatesAutoresizingMaskIntoConstraints = false
    goalLabel.font = UIFont.fpfMedium(12)
    goalLabel.textColor = UIColor.fpfGreyishColor()
    addSubview(goalLabel)
    
    subsImageView.translatesAutoresizingMaskIntoConstraints = false
    addSubview(subsImageView)
    
    redCardImageView.translatesAutoresizingMaskIntoConstraints = false
    addSubview(redCardImageView)
    
    yellowCardImageView.translatesAutoresizingMaskIntoConstraints = false
    addSubview(yellowCardImageView)
    
    let metrics = ["margin": margin, "largeMargin": largeMargin]
    let views = ["nameLabel": nameLabel, "posLabel": posLabel, "goalImageView": goalImageView, "goalLabel": goalLabel, "subsImageView": subsImageView, "redCardImageView": redCardImageView, "yellowCardImageView": yellowCardImageView]
    
    var horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-largeMargin-[nameLabel]-largeMargin-|", options: [], metrics: metrics, views: views)
    addConstraints(horizontalConstraints)
    
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-largeMargin-[posLabel]", options: [], metrics: metrics, views: views)
    addConstraints(horizontalConstraints)
    
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:[goalLabel][goalImageView]-margin-|", options: [], metrics: metrics, views: views)
    addConstraints(horizontalConstraints)
    
    var verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|-margin-[nameLabel]-margin-[posLabel]-margin-|", options: [], metrics: metrics, views: views)
    addConstraints(verticalConstraints)
    
    [yellowCardImageView, redCardImageView, subsImageView, goalLabel, goalImageView].forEach { view in
      verticalConstraints = [NSLayoutConstraint(item: view, attribute: .CenterY, relatedBy: .Equal, toItem: posLabel, attribute: .CenterY, multiplier: 1, constant: 0)]
      addConstraints(verticalConstraints)
      
      view.setContentCompressionResistancePriority(UILayoutPriorityRequired, forAxis: .Horizontal)
      view.setContentHuggingPriority(UILayoutPriorityRequired, forAxis: .Horizontal)
    }
    
    var horizontalConstraint = NSLayoutConstraint(item: yellowCardImageView, attribute: .Right, relatedBy: .Equal, toItem: self, attribute: .Right, multiplier: 1, constant: -Constants.defaultMargin)
    horizontalConstraint.priority = 940
    addConstraint(horizontalConstraint)
    horizontalConstraint = NSLayoutConstraint(item: yellowCardImageView, attribute: .Right, relatedBy: .Equal, toItem: goalLabel, attribute: .Left, multiplier: 1, constant: -Constants.defaultMargin / 2)
    horizontalConstraint.priority = 960
    addConstraint(horizontalConstraint)
    horizontalConstraint = NSLayoutConstraint(item: yellowCardImageView, attribute: .Right, relatedBy: .Equal, toItem: subsImageView, attribute: .Left, multiplier: 1, constant: -Constants.defaultMargin / 2)
    horizontalConstraint.priority = 980
    addConstraint(horizontalConstraint)
    horizontalConstraint = NSLayoutConstraint(item: yellowCardImageView, attribute: .Right, relatedBy: .Equal, toItem: redCardImageView, attribute: .Left, multiplier: 1, constant: -Constants.defaultMargin / 2)
    horizontalConstraint.priority = 1000
    addConstraint(horizontalConstraint)
    
    horizontalConstraint = NSLayoutConstraint(item: redCardImageView, attribute: .Right, relatedBy: .Equal, toItem: self, attribute: .Right, multiplier: 1, constant: -Constants.defaultMargin)
    horizontalConstraint.priority = 960
    addConstraint(horizontalConstraint)
    horizontalConstraint = NSLayoutConstraint(item: redCardImageView, attribute: .Right, relatedBy: .Equal, toItem: goalLabel, attribute: .Left, multiplier: 1, constant: -Constants.defaultMargin / 2)
    horizontalConstraint.priority = 980
    addConstraint(horizontalConstraint)
    horizontalConstraint = NSLayoutConstraint(item: redCardImageView, attribute: .Right, relatedBy: .Equal, toItem: subsImageView, attribute: .Left, multiplier: 1, constant: -Constants.defaultMargin / 2)
    horizontalConstraint.priority = 1000
    addConstraint(horizontalConstraint)
    
    horizontalConstraint = NSLayoutConstraint(item: subsImageView, attribute: .Right, relatedBy: .Equal, toItem: self, attribute: .Right, multiplier: 1, constant: -Constants.defaultMargin)
    horizontalConstraint.priority = 980
    addConstraint(horizontalConstraint)
    horizontalConstraint = NSLayoutConstraint(item: subsImageView, attribute: .Right, relatedBy: .Equal, toItem: goalLabel, attribute: .Left, multiplier: 1, constant: -Constants.defaultMargin / 2)
    horizontalConstraint.priority = 1000
    addConstraint(horizontalConstraint)
  }
  
  #if DEBUG
  deinit {
    print("deinit", self)
  }
  #endif
}
