//
//  LineupView.swift
//  FPF
//
//  Created by Gilson Gil on 1/8/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class LineupView: UIView {
  internal let tableView = UITableView()
  
  var lineupViewModel: LineupViewModel? {
    didSet {
      dispatch_async(dispatch_get_main_queue()) { [weak self] in
        self?.tableView.reloadData()
      }
    }
  }
  
  weak var scrollDelegate: MatchViewScrollDelegate?
  var scrollTopContentInset: CGFloat? {
    didSet {
      if let scrollTopContentInset = scrollTopContentInset where !didChangeContentOffset {
        didChangeContentOffset = true
        tableView.contentInset = UIEdgeInsets(top: scrollTopContentInset + Constants.defaultMargin, left: 0, bottom: 8, right: 0)
        tableView.scrollIndicatorInsets = UIEdgeInsets(top: scrollTopContentInset, left: 0, bottom: 0, right: 0)
        tableView.contentOffset = CGPoint(x: 0, y: -scrollTopContentInset - Constants.defaultMargin)
      }
    }
  }
  private var didChangeContentOffset = false
  private var lastYOffset: CGFloat = 0
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    setUp()
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setUp()
  }
  
  private func setUp() {
    translatesAutoresizingMaskIntoConstraints = false
    backgroundColor = UIColor.clearColor()
    
    tableView.backgroundColor = UIColor.clearColor()
    tableView.translatesAutoresizingMaskIntoConstraints = false
    tableView.dataSource = self
    tableView.delegate = self
    tableView.estimatedRowHeight = 60
    tableView.separatorStyle = .None
    tableView.allowsSelection = false
    tableView.registerClass(LineupCell.classForCoder(), forCellReuseIdentifier: LineupCell.reuseIdentifier())
    addSubview(tableView)
    
    let metrics = ["margin": Constants.defaultMargin]
    let views = ["tableView": tableView]
    
    let horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-margin-[tableView]-margin-|", options: [], metrics: metrics, views: views)
    let verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[tableView]|", options: [], metrics: nil, views: views)
    addConstraints(horizontalConstraints)
    addConstraints(verticalConstraints)
  }
  
  #if DEBUG
  deinit {
    print("deinit", self)
  }
  #endif
}

extension LineupView: UITableViewDataSource {
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return lineupViewModel?.lineups.count ?? 0
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCellWithIdentifier(LineupCell.reuseIdentifier(), forIndexPath: indexPath) as! LineupCell
    if let viewModel = lineupViewModel {
      cell.configureWithLineupCellViewModel(viewModel.lineups[indexPath.row], index: indexPath.row)
    }
    return cell
  }
}

extension LineupView: UITableViewDelegate {
  func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    return UITableViewAutomaticDimension
  }
}

extension LineupView: UIScrollViewDelegate {
  func scrollViewDidScroll(scroll: UIScrollView) {
    scrollDelegate?.scrollViewDidScroll(scroll)
  }
  
  func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
    scrollDelegate?.scrollViewDidStop(scrollView)
  }
  
  func scrollViewDidEndDragging(scrollView: UIScrollView, willDecelerate decelerate: Bool) {
    if !decelerate {
      scrollDelegate?.scrollViewDidStop(scrollView)
    }
  }
}

extension LineupView: MatchViewScrollProtocol {
  
}
