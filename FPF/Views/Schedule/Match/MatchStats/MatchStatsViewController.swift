//
//  MatchStatsViewController.swift
//  FPF
//
//  Created by Gilson Gil on 3/2/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class MatchStatsViewController: UIViewController {
  private let matchStatsView = MatchStatsView()
  
  var matchStatsViewModel: MatchStatsViewModel? {
    didSet {
      matchStatsView.matchStatsViewModel = matchStatsViewModel
    }
  }
  
  override func loadView() {
    view = matchStatsView
  }
  
  #if DEBUG
  deinit {
    print("deinit", self)
  }
  #endif
}
