//
//  MatchStatsView.swift
//  FPF
//
//  Created by Gilson Gil on 3/2/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class MatchStatsView: UIView {
  internal let tableView = UITableView()
  
  var matchStatsViewModel: MatchStatsViewModel? {
    didSet {
      dispatch_async(dispatch_get_main_queue()) { [weak self] in
        if let weakSelf = self {
          weakSelf.matchStatsViewModel?.items.forEach {$0.register(weakSelf.tableView)}
          weakSelf.tableView.reloadData()
        }
      }
    }
  }
  
  weak var scrollDelegate: MatchViewScrollDelegate?
  var scrollTopContentInset: CGFloat? {
    didSet {
      if let scrollTopContentInset = scrollTopContentInset where !didChangeContentOffset {
        didChangeContentOffset = true
        tableView.contentInset = UIEdgeInsets(top: scrollTopContentInset + Constants.defaultMargin, left: 0, bottom: 28, right: 0)
        tableView.scrollIndicatorInsets = UIEdgeInsets(top: scrollTopContentInset, left: 0, bottom: 0, right: 0)
        tableView.contentOffset = CGPoint(x: 0, y: -scrollTopContentInset - Constants.defaultMargin)
      }
    }
  }
  private var didChangeContentOffset = false
  private var lastYOffset: CGFloat = 0
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    setUp()
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setUp()
  }
  
  private func setUp() {
    translatesAutoresizingMaskIntoConstraints = false
    backgroundColor = UIColor.clearColor()
    
    tableView.backgroundColor = UIColor.clearColor()
    tableView.translatesAutoresizingMaskIntoConstraints = false
    tableView.dataSource = self
    tableView.delegate = self
    tableView.estimatedRowHeight = 100
    tableView.separatorStyle = .None
    tableView.allowsSelection = false
    addSubview(tableView)
    
    let views = ["tableView": tableView]
    
    let horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[tableView]|", options: [], metrics: nil, views: views)
    let verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[tableView]|", options: [], metrics: nil, views: views)
    addConstraints(horizontalConstraints)
    addConstraints(verticalConstraints)
  }
  
  #if DEBUG
  deinit {
    print("deinit", self)
  }
  #endif
}

extension MatchStatsView: UITableViewDataSource {
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return matchStatsViewModel?.items.count ?? 0
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let configurator = matchStatsViewModel!.items[indexPath.row]
    let cell = tableView.dequeueReusableCellWithIdentifier(configurator.reuseIdentifier, forIndexPath: indexPath)
    configurator.update(cell)
    return cell
  }
}

extension MatchStatsView: UITableViewDelegate {
  func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    return UITableViewAutomaticDimension
  }
}

extension MatchStatsView: UIScrollViewDelegate {
  func scrollViewDidScroll(scroll: UIScrollView) {
    scrollDelegate?.scrollViewDidScroll(scroll)
  }
  
  func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
    scrollDelegate?.scrollViewDidStop(scrollView)
  }
  
  func scrollViewDidEndDragging(scrollView: UIScrollView, willDecelerate decelerate: Bool) {
    if !decelerate {
      scrollDelegate?.scrollViewDidStop(scrollView)
    }
  }
}

extension MatchStatsView: MatchViewScrollProtocol {}
