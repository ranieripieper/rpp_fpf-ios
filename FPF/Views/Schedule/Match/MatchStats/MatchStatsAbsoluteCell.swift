//
//  MatchStatsAbsoluteCell.swift
//  FPF
//
//  Created by Gilson Gil on 3/2/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class MatchStatsAbsoluteCell: UITableViewCell {
  private let card = UIView()
  private let titleLabel = UILabel()
  private let homeLabel = UILabel()
  private let awayLabel = UILabel()
  private let typeImageView = UIImageView()
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setUp()
  }
  
  override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    setUp()
  }
  
  private func setUp() {
    contentView.backgroundColor = UIColor.fpfLightGreyColor()
    selectionStyle = .None
    
    let connector1 = UIView()
    connector1.translatesAutoresizingMaskIntoConstraints = false
    connector1.backgroundColor = UIColor.fpfStripeGreyColor()
    contentView.addSubview(connector1)
    
    let connector2 = UIView()
    connector2.translatesAutoresizingMaskIntoConstraints = false
    connector2.backgroundColor = UIColor.fpfStripeGreyColor()
    contentView.addSubview(connector2)
    
    card.translatesAutoresizingMaskIntoConstraints = false
    card.backgroundColor = UIColor.whiteColor()
    contentView.addSubview(card)
    
    typeImageView.translatesAutoresizingMaskIntoConstraints = false
    typeImageView.contentMode = .ScaleAspectFit
    contentView.addSubview(typeImageView)
    
    titleLabel.translatesAutoresizingMaskIntoConstraints = false
    titleLabel.font = UIFont.fpfMedium(14)
    titleLabel.textColor = UIColor.fpfPinkishGreyColor()
    titleLabel.textAlignment = .Center
    card.addSubview(titleLabel)
    
    homeLabel.translatesAutoresizingMaskIntoConstraints = false
    homeLabel.font = UIFont.fpfMedium(38)
    homeLabel.textColor = UIColor.fpfGreyishColor()
    homeLabel.textAlignment = .Center
    card.addSubview(homeLabel)
    
    awayLabel.translatesAutoresizingMaskIntoConstraints = false
    awayLabel.font = UIFont.fpfMedium(38)
    awayLabel.textColor = UIColor.fpfGreyishColor()
    awayLabel.textAlignment = .Center
    card.addSubview(awayLabel)
    
    let metrics = ["margin": 14, "margin2": 50, "margin3": 34]
    let views = ["connector1": connector1, "connector2": connector2, "card": card, "typeImageView": typeImageView, "titleLabel": titleLabel, "homeLabel": homeLabel, "awayLabel": awayLabel]
    var horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-margin-[card]-margin-|", options: [], metrics: metrics, views: views)
    contentView.addConstraints(horizontalConstraints)
    var verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[connector1(margin3)][card]|", options: [], metrics: metrics, views: views)
    contentView.addConstraints(verticalConstraints)
    verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[connector2(margin3)][card]", options: [], metrics: metrics, views: views)
    contentView.addConstraints(verticalConstraints)
    horizontalConstraints = [
      NSLayoutConstraint(item: connector1, attribute: .Width, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1, constant: 4),
      NSLayoutConstraint(item: connector1, attribute: .CenterX, relatedBy: .Equal, toItem: contentView, attribute: .CenterX, multiplier: 0.5, constant: 0),
      NSLayoutConstraint(item: connector2, attribute: .Width, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1, constant: 4),
      NSLayoutConstraint(item: connector2, attribute: .CenterX, relatedBy: .Equal, toItem: contentView, attribute: .CenterX, multiplier: 1.5, constant: 0)
    ]
    contentView.addConstraints(horizontalConstraints)
    horizontalConstraints = [
      NSLayoutConstraint(item: typeImageView, attribute: .CenterX, relatedBy: .Equal, toItem: card, attribute: .CenterX, multiplier: 1, constant: 0)
    ]
    contentView.addConstraints(horizontalConstraints)
    verticalConstraints = [
      NSLayoutConstraint(item: typeImageView, attribute: .CenterY, relatedBy: .Equal, toItem: card, attribute: .Top, multiplier: 1, constant: 0)
    ]
    contentView.addConstraints(verticalConstraints)
    
    horizontalConstraints = [NSLayoutConstraint(item: titleLabel, attribute: .CenterX, relatedBy: .Equal, toItem: card, attribute: .CenterX, multiplier: 1, constant: 0)]
    card.addConstraints(horizontalConstraints)
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-margin-[homeLabel]-margin-[titleLabel]-margin-[awayLabel]-margin-|", options: [], metrics: metrics, views: views)
    card.addConstraints(horizontalConstraints)
    verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|-margin2-[titleLabel]-margin3-|", options: [], metrics: metrics, views: views)
    card.addConstraints(verticalConstraints)
    verticalConstraints = [
      NSLayoutConstraint(item: homeLabel, attribute: .CenterY, relatedBy: .Equal, toItem: titleLabel, attribute: .CenterY, multiplier: 1, constant: 0),
      NSLayoutConstraint(item: titleLabel, attribute: .CenterY, relatedBy: .Equal, toItem: awayLabel, attribute: .CenterY, multiplier: 1, constant: 0)
    ]
    card.addConstraints(verticalConstraints)
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    card.applyShadow()
  }
  
  func update(viewModel: MatchStatsAbsoluteViewModel) {
    titleLabel.text = viewModel.title.uppercaseString
    homeLabel.text = viewModel.home
    awayLabel.text = viewModel.away
    typeImageView.image = viewModel.image
  }
}

extension MatchStatsAbsoluteCell: Updatable {
  typealias ViewModel = MatchStatsAbsoluteViewModel
}
