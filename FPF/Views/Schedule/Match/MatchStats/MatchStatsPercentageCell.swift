//
//  MatchStatsPercentageCell.swift
//  FPF
//
//  Created by Gilson Gil on 3/2/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class MatchStatsPercentageCell: UITableViewCell {
  private let card = UIView()
  private let titleLabel = UILabel()
  private let homeLabel = UILabel()
  private let awayLabel = UILabel()
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setUp()
  }
  
  override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    setUp()
  }
  
  private func setUp() {
    contentView.backgroundColor = UIColor.fpfLightGreyColor()
    selectionStyle = .None
    
    card.translatesAutoresizingMaskIntoConstraints = false
    card.backgroundColor = UIColor.whiteColor()
    contentView.addSubview(card)
    
    titleLabel.translatesAutoresizingMaskIntoConstraints = false
    titleLabel.font = UIFont.fpfMedium(14)
    titleLabel.textColor = UIColor.fpfPinkishGreyColor()
    titleLabel.textAlignment = .Center
    card.addSubview(titleLabel)
    
    homeLabel.translatesAutoresizingMaskIntoConstraints = false
    homeLabel.font = UIFont.fpfMedium(38)
    homeLabel.textColor = UIColor.fpfGreyishColor()
    homeLabel.textAlignment = .Left
    card.addSubview(homeLabel)
    
    awayLabel.translatesAutoresizingMaskIntoConstraints = false
    awayLabel.font = UIFont.fpfMedium(38)
    awayLabel.textColor = UIColor.fpfGreyishColor()
    awayLabel.textAlignment = .Right
    card.addSubview(awayLabel)
    
    let ovalImage = UIImage.icnOval()
    let ovalImageView = UIImageView(image: ovalImage)
    ovalImageView.translatesAutoresizingMaskIntoConstraints = false
    ovalImageView.contentMode = .ScaleAspectFit
    card.addSubview(ovalImageView)
    
    let metrics = ["margin": 14, "margin2": 18]
    let views = ["card": card, "titleLabel": titleLabel, "homeLabel": homeLabel, "awayLabel": awayLabel, "ovalImageView": ovalImageView]
    var horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-margin-[card]-margin-|", options: [], metrics: metrics, views: views)
    contentView.addConstraints(horizontalConstraints)
    var verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|-margin-[card]|", options: [], metrics: metrics, views: views)
    contentView.addConstraints(verticalConstraints)
    
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|->=margin-[titleLabel]->=margin-|", options: [], metrics: metrics, views: views)
    card.addConstraints(horizontalConstraints)
    horizontalConstraints = [NSLayoutConstraint(item: titleLabel, attribute: .CenterX, relatedBy: .Equal, toItem: card, attribute: .CenterX, multiplier: 1, constant: 0)]
    card.addConstraints(horizontalConstraints)
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-margin-[homeLabel]-margin-[ovalImageView]-margin-[awayLabel]-margin-|", options: [], metrics: metrics, views: views)
    card.addConstraints(horizontalConstraints)
    horizontalConstraints = [NSLayoutConstraint(item: ovalImageView, attribute: .CenterX, relatedBy: .Equal, toItem: card, attribute: .CenterX, multiplier: 1, constant: 0)]
    card.addConstraints(horizontalConstraints)
    verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|-margin-[titleLabel]-margin-[homeLabel]-margin2-|", options: [], metrics: metrics, views: views)
    card.addConstraints(verticalConstraints)
    verticalConstraints = [
      NSLayoutConstraint(item: homeLabel, attribute: .CenterY, relatedBy: .Equal, toItem: ovalImageView, attribute: .CenterY, multiplier: 1, constant: 0),
      NSLayoutConstraint(item: ovalImageView, attribute: .CenterY, relatedBy: .Equal, toItem: awayLabel, attribute: .CenterY, multiplier: 1, constant: 0)
    ]
    card.addConstraints(verticalConstraints)
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    card.applyShadow()
  }
  
  func update(viewModel: MatchStatsPercentageViewModel) {
    titleLabel.text = viewModel.title.uppercaseString
    homeLabel.text = viewModel.home
    awayLabel.text = viewModel.away
  }
}

extension MatchStatsPercentageCell: Updatable {
  typealias ViewModel = MatchStatsPercentageViewModel
}
