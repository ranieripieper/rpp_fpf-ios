//
//  MatchStatsCompletedCell.swift
//  FPF
//
//  Created by Gilson Gil on 3/2/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class MatchStatsCompletedCell: UITableViewCell {
  private let card = UIView()
  private let titleLabel = UILabel()
  private let homeCompletedLabel = UILabel()
  private let homeWrongLabel = UILabel()
  private let awayCompletedLabel = UILabel()
  private let awayWrongLabel = UILabel()
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setUp()
  }
  
  override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    setUp()
  }
  
  private func setUp() {
    contentView.backgroundColor = UIColor.fpfLightGreyColor()
    selectionStyle = .None
    
    let connector = UIView()
    connector.translatesAutoresizingMaskIntoConstraints = false
    connector.backgroundColor = UIColor.fpfStripeGreyColor()
    contentView.addSubview(connector)
    
    card.translatesAutoresizingMaskIntoConstraints = false
    card.backgroundColor = UIColor.whiteColor()
    contentView.addSubview(card)
    
    titleLabel.translatesAutoresizingMaskIntoConstraints = false
    titleLabel.font = UIFont.fpfMedium(18)
    titleLabel.textColor = UIColor.fpfCementColor()
    titleLabel.textAlignment = .Center
    card.addSubview(titleLabel)
    
    homeCompletedLabel.translatesAutoresizingMaskIntoConstraints = false
    homeCompletedLabel.font = UIFont.fpfMedium(38)
    homeCompletedLabel.textColor = UIColor.fpfGreenColor()
    homeCompletedLabel.textAlignment = .Center
    card.addSubview(homeCompletedLabel)
    
    homeWrongLabel.translatesAutoresizingMaskIntoConstraints = false
    homeWrongLabel.font = UIFont.fpfMedium(38)
    homeWrongLabel.textColor = UIColor.fpfRustyRedColor()
    homeWrongLabel.textAlignment = .Center
    card.addSubview(homeWrongLabel)
    
    awayCompletedLabel.translatesAutoresizingMaskIntoConstraints = false
    awayCompletedLabel.font = UIFont.fpfMedium(38)
    awayCompletedLabel.textColor = UIColor.fpfGreenColor()
    awayCompletedLabel.textAlignment = .Center
    card.addSubview(awayCompletedLabel)
    
    awayWrongLabel.translatesAutoresizingMaskIntoConstraints = false
    awayWrongLabel.font = UIFont.fpfMedium(38)
    awayWrongLabel.textColor = UIColor.fpfRustyRedColor()
    awayWrongLabel.textAlignment = .Center
    card.addSubview(awayWrongLabel)
    
    let separator = UIView()
    separator.translatesAutoresizingMaskIntoConstraints = false
    separator.backgroundColor = UIColor.fpfMediumGreyColor()
    card.addSubview(separator)
    
    let metrics = ["margin": 14, "margin2": 10, "margin3": 16]
    let views = ["connector": connector, "card": card, "titleLabel": titleLabel, "homeCompletedLabel": homeCompletedLabel, "homeWrongLabel": homeWrongLabel, "awayCompletedLabel": awayCompletedLabel, "awayWrongLabel": awayWrongLabel, "separator": separator]
    var horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-margin-[card]-margin-|", options: [], metrics: metrics, views: views)
    contentView.addConstraints(horizontalConstraints)
    var verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[connector(margin)][card]|", options: [], metrics: metrics, views: views)
    contentView.addConstraints(verticalConstraints)
    horizontalConstraints = [
      NSLayoutConstraint(item: connector, attribute: .Width, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1, constant: 4),
      NSLayoutConstraint(item: connector, attribute: .CenterX, relatedBy: .Equal, toItem: contentView, attribute: .CenterX, multiplier: 1, constant: 0)
    ]
    contentView.addConstraints(horizontalConstraints)
    
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|->=margin-[titleLabel]->=margin-|", options: [], metrics: metrics, views: views)
    card.addConstraints(horizontalConstraints)
    horizontalConstraints = [NSLayoutConstraint(item: titleLabel, attribute: .CenterX, relatedBy: .Equal, toItem: card, attribute: .CenterX, multiplier: 1, constant: 0)]
    card.addConstraints(horizontalConstraints)
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-margin-[homeCompletedLabel]-margin-[separator(1)]-margin-[awayCompletedLabel]-margin-|", options: [], metrics: metrics, views: views)
    card.addConstraints(horizontalConstraints)
    horizontalConstraints = [
      NSLayoutConstraint(item: homeCompletedLabel, attribute: .CenterX, relatedBy: .Equal, toItem: homeWrongLabel, attribute: .CenterX, multiplier: 1, constant: 0),
      NSLayoutConstraint(item: awayCompletedLabel, attribute: .CenterX, relatedBy: .Equal, toItem: awayWrongLabel, attribute: .CenterX, multiplier: 1, constant: 0)
    ]
    card.addConstraints(horizontalConstraints)
    horizontalConstraints = [NSLayoutConstraint(item: separator, attribute: .CenterX, relatedBy: .Equal, toItem: card, attribute: .CenterX, multiplier: 1, constant: 0)]
    card.addConstraints(horizontalConstraints)
    verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|-margin-[titleLabel]-margin2-[homeCompletedLabel]-margin2-[homeWrongLabel]-margin3-|", options: [], metrics: metrics, views: views)
    card.addConstraints(verticalConstraints)
    verticalConstraints = [
      NSLayoutConstraint(item: homeCompletedLabel, attribute: .CenterY, relatedBy: .Equal, toItem: awayCompletedLabel, attribute: .CenterY, multiplier: 1, constant: 0),
      NSLayoutConstraint(item: homeWrongLabel, attribute: .CenterY, relatedBy: .Equal, toItem: awayWrongLabel, attribute: .CenterY, multiplier: 1, constant: 0),
      NSLayoutConstraint(item: separator, attribute: .Top, relatedBy: .Equal, toItem: homeCompletedLabel, attribute: .Top, multiplier: 1, constant: 0),
      NSLayoutConstraint(item: separator, attribute: .Bottom, relatedBy: .Equal, toItem: homeWrongLabel, attribute: .Bottom, multiplier: 1, constant: 0)
    ]
    card.addConstraints(verticalConstraints)
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    card.applyShadow()
  }
  
  func update(viewModel: MatchStatsCompletedViewModel) {
    titleLabel.text = viewModel.title.uppercaseString
    homeCompletedLabel.text = viewModel.homeCompleted
    homeWrongLabel.text = viewModel.homeWrong
    awayCompletedLabel.text = viewModel.awayCompleted
    awayWrongLabel.text = viewModel.awayWrong
  }
}

extension MatchStatsCompletedCell: Updatable {
  typealias ViewModel = MatchStatsCompletedViewModel
}
