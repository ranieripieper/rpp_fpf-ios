//
//  MatchViewController.swift
//  FPF
//
//  Created by Gilson Gil on 1/8/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class MatchViewController: UIViewController {
  private let matchViewModel: MatchViewModel!
  private let matchView: MatchView!
  
  private var currentViewController: UIViewController?
  private lazy var liveViewController: LiveViewController = LiveViewController()
  private lazy var lineupViewController: LineupViewController = LineupViewController()
  private lazy var matchStatsViewController: MatchStatsViewController = MatchStatsViewController()
  private var liveTimer: NSTimer?
  private var lineupTimer: NSTimer?
  private var statsTimer: NSTimer?
  
  required init?(coder aDecoder: NSCoder) {
    matchViewModel = nil
    matchView = nil
    super.init(coder: aDecoder)
  }
  
  init(matchViewModel: MatchViewModel) {
    self.matchViewModel = matchViewModel
    self.matchView = MatchView(matchViewModel: matchViewModel)
    super.init(nibName: nil, bundle: nil)
  }
  
  override func loadView() {
    matchView.delegate = self
    view = matchView
    presentLiveViewController()
  }
  
  override func viewWillDisappear(animated: Bool) {
    super.viewWillDisappear(animated)
    dispatch_async(dispatch_get_main_queue()) { [weak self] in
      self?.liveTimer?.invalidate()
      self?.liveTimer = nil
      self?.lineupTimer?.invalidate()
      self?.lineupTimer = nil
      self?.statsTimer?.invalidate()
      self?.statsTimer = nil
    }
  }
  
  private func presentLiveViewController() {
    guard !(currentViewController is LiveViewController) else {
      return
    }
    reloadLives()
    self.presentViewController(self.liveViewController)
  }
  
  func reloadLives() {
    matchViewModel.lives() { [weak self] liveViewModel in
      self?.liveViewController.liveViewModel = liveViewModel
      if let weakSelf = self where !weakSelf.matchViewModel.hasFinished() {
        dispatch_async(dispatch_get_main_queue()) { [weak self] in
          if let weakSelf = self {
            weakSelf.liveTimer?.invalidate()
            weakSelf.liveTimer = nil
            weakSelf.liveTimer = NSTimer.scheduledTimerWithTimeInterval(Constants.liveTimerInterval, target: weakSelf, selector: #selector(MatchViewController.reloadLives), userInfo: nil, repeats: false)
          }
        }
      }
    }
    reloadMatch()
  }
  
  func reloadMatch() {
    Match.fetchMatchWithId(matchViewModel.matchId) { [weak self] match in
      if let match = match {
        self?.matchViewModel.team1Score.value = match.homeScore ?? 0
        self?.matchViewModel.team2Score.value = match.awayScore ?? 0
      }
    }
  }
  
  private func presentLineupViewController() {
    guard !(currentViewController is LineupViewController), let _ = matchViewModel else {
      return
    }
    matchViewModel.lineups() { [weak self] lineupViewModel in
      self?.lineupViewController.lineupViewModel = lineupViewModel
    }
    presentViewController(lineupViewController)
  }
  
  func reloadLineups() {
    matchViewModel.lineups() { [weak self] lineupViewModel in
      self?.lineupViewController.lineupViewModel = lineupViewModel
      if let weakSelf = self where !weakSelf.matchViewModel.hasFinished() {
        dispatch_async(dispatch_get_main_queue()) { [weak self] in
          if let weakSelf = self {
            weakSelf.lineupTimer?.invalidate()
            weakSelf.lineupTimer = nil
            weakSelf.lineupTimer = NSTimer.scheduledTimerWithTimeInterval(Constants.liveTimerInterval, target: weakSelf, selector: #selector(MatchViewController.reloadLineups), userInfo: nil, repeats: false)
          }
        }
      }
    }
    reloadMatch()
  }
  
  private func presentMatchStatsViewController() {
    guard !(currentViewController is MatchStatsViewController), let _ = matchViewModel else {
      return
    }
    matchViewModel.stats() { [weak self] matchStatsViewModel in
      if let matchStatsViewModel = matchStatsViewModel {
        self?.matchStatsViewController.matchStatsViewModel = matchStatsViewModel
      }
    }
    presentViewController(matchStatsViewController)
  }
  
  func reloadStats() {
    matchViewModel.stats() { [weak self] matchStatsViewModel in
      self?.matchStatsViewController.matchStatsViewModel = matchStatsViewModel
      if let weakSelf = self where !weakSelf.matchViewModel.hasFinished() {
        dispatch_async(dispatch_get_main_queue()) { [weak self] in
          if let weakSelf = self {
            weakSelf.statsTimer?.invalidate()
            weakSelf.statsTimer = nil
            weakSelf.statsTimer = NSTimer.scheduledTimerWithTimeInterval(Constants.liveTimerInterval, target: weakSelf, selector: #selector(MatchViewController.reloadStats), userInfo: nil, repeats: false)
          }
        }
      }
    }
    reloadMatch()
  }
  
  private func presentViewController(viewController: UIViewController) {
    if let currentViewController = currentViewController {
      currentViewController.view.removeFromSuperview()
      currentViewController.removeFromParentViewController()
    }
    
    viewController.willMoveToParentViewController(self)
    matchView.addViewController(viewController)
    viewController.didMoveToParentViewController(self)
    currentViewController = viewController
  }
  
  #if DEBUG
  deinit {
    print("deinit", self)
  }
  #endif
}

extension MatchViewController: MatchViewDelegate {
  func didSelectLive() {
    presentLiveViewController()
  }
  
  func didSelectLineup() {
    presentLineupViewController()
  }
  
  func didSelectStats() {
    presentMatchStatsViewController()
  }
}
