//
//  LiveView.swift
//  FPF
//
//  Created by Gilson Gil on 1/8/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class LiveView: UIView {
  internal let tableView = UITableView()
  
  var liveViewModel: LiveViewModel? {
    didSet {
      dispatch_async(dispatch_get_main_queue()) { [weak self] in
        self?.tableView.reloadData()
      }
    }
  }
  
  weak var scrollDelegate: MatchViewScrollDelegate?
  var scrollTopContentInset: CGFloat? {
    didSet {
      if let scrollTopContentInset = scrollTopContentInset where !didChangeContentOffset {
        didChangeContentOffset = true
        tableView.contentInset = UIEdgeInsets(top: scrollTopContentInset, left: 0, bottom: 20, right: 0)
        tableView.scrollIndicatorInsets = UIEdgeInsets(top: scrollTopContentInset, left: 0, bottom: 0, right: 0)
        tableView.contentOffset = CGPoint(x: 0, y: -scrollTopContentInset)
      }
    }
  }
  private var didChangeContentOffset = false
  private var lastYOffset: CGFloat = 0
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setUp()
  }
  
  init() {
    super.init(frame: .zero)
    setUp()
  }
  
  private func setUp() {
    translatesAutoresizingMaskIntoConstraints = false
    backgroundColor = UIColor.clearColor()
    
    tableView.backgroundColor = UIColor.clearColor()
    tableView.translatesAutoresizingMaskIntoConstraints = false
    tableView.dataSource = self
    tableView.delegate = self
    tableView.clipsToBounds = false
    tableView.estimatedRowHeight = 70
    tableView.separatorStyle = .None
    tableView.allowsSelection = false
    tableView.registerClass(LiveEmptyCell.classForCoder(), forCellReuseIdentifier: LiveEmptyCell.reuseIdentifier())
    tableView.registerClass(LiveCell.classForCoder(), forCellReuseIdentifier: LiveCell.reuseIdentifier())
    addSubview(tableView)
    
    let views = ["tableView": tableView]
    let horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[tableView]|", options: [], metrics: nil, views: views)
    let verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[tableView]|", options: [], metrics: nil, views: views)
    addConstraints(horizontalConstraints)
    addConstraints(verticalConstraints)
  }
  
  #if DEBUG
  deinit {
    print("deinit", self)
  }
  #endif
}

extension LiveView: UITableViewDataSource {
  func numberOfSectionsInTableView(tableView: UITableView) -> Int {
    return liveViewModel?.lives.count ?? 0
  }
  
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    if let count = liveViewModel?.lives[section].count where count > 0 {
      if section == 0 {
        return count
      } else {
        return count + 1
      }
    }
    return 0
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell: UITableViewCell
    if tableView.numberOfRowsInSection(indexPath.section) > liveViewModel!.lives[indexPath.section].count {
      if indexPath.row == 0 {
        cell = tableView.dequeueReusableCellWithIdentifier(LiveEmptyCell.reuseIdentifier(), forIndexPath: indexPath)
      } else {
        cell = tableView.dequeueReusableCellWithIdentifier(LiveCell.reuseIdentifier(), forIndexPath: indexPath)
        if let cell = cell as? LiveCell, liveViewModel = liveViewModel where indexPath.section < liveViewModel.lives.count && indexPath.row - 1 < liveViewModel.lives[indexPath.section].count {
          cell.configureWithLiveCellViewModel(liveViewModel.lives[indexPath.section][indexPath.row - 1])
        }
      }
    } else {
      cell = tableView.dequeueReusableCellWithIdentifier(LiveCell.reuseIdentifier(), forIndexPath: indexPath)
      if let cell = cell as? LiveCell, liveViewModel = liveViewModel where indexPath.section < liveViewModel.lives.count && indexPath.row < liveViewModel.lives[indexPath.section].count {
        cell.configureWithLiveCellViewModel(liveViewModel.lives[indexPath.section][indexPath.row])
      }
    }
    return cell
  }
}

extension LiveView: UITableViewDelegate {
  func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    if tableView.numberOfRowsInSection(indexPath.section) > liveViewModel!.lives[indexPath.section].count {
      if indexPath.row == 0 {
        return 90
      } else {
        return UITableViewAutomaticDimension
      }
    } else {
      return UITableViewAutomaticDimension
    }
  }
}

extension LiveView: UIScrollViewDelegate {
  func scrollViewDidScroll(scroll: UIScrollView) {
    scrollDelegate?.scrollViewDidScroll(scroll)
  }
  
  func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
    scrollDelegate?.scrollViewDidStop(scrollView)
  }
  
  func scrollViewDidEndDragging(scrollView: UIScrollView, willDecelerate decelerate: Bool) {
    if !decelerate {
      scrollDelegate?.scrollViewDidStop(scrollView)
    }
  }
}

extension LiveView: MatchViewScrollProtocol {
  
}
