//
//  LiveEmptyCell.swift
//  FPF
//
//  Created by Gilson Gil on 1/27/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class LiveEmptyCell: UITableViewCell {
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setUp()
  }
  
  override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    setUp()
  }
  
  private func setUp() {
    contentView.translatesAutoresizingMaskIntoConstraints = false
    contentView.backgroundColor = UIColor.fpfLightGreyColor()
    selectionStyle = .None
    
    let verticalStripe = UIView()
    verticalStripe.translatesAutoresizingMaskIntoConstraints = false
    verticalStripe.backgroundColor = UIColor.fpfStripeGreyColor()
    contentView.addSubview(verticalStripe)
    
    let horizontalStripe = UIView()
    horizontalStripe.translatesAutoresizingMaskIntoConstraints = false
    horizontalStripe.backgroundColor = UIColor.fpfPinkishGreyColor()
    contentView.addSubview(horizontalStripe)
    
    let metrics = ["topMargin": 50, "leftMargin": 40, "verticalStripeWidth": 5, "horizontalStripeHeight": 3]
    let views = ["verticalStripe": verticalStripe, "horizontalStripe": horizontalStripe]
    
    var horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-leftMargin-[verticalStripe(verticalStripeWidth)]", options: [], metrics: metrics, views: views)
    contentView.addConstraints(horizontalConstraints)
    
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[horizontalStripe]|", options: [], metrics: metrics, views: views)
    contentView.addConstraints(horizontalConstraints)
    
    var verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|-topMargin-[horizontalStripe(horizontalStripeHeight)][verticalStripe]|", options: [], metrics: metrics, views: views)
    contentView.addConstraints(verticalConstraints)
    
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[contentView]|", options: [], metrics: nil, views: ["contentView": contentView])
    addConstraints(horizontalConstraints)
    verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[contentView]|", options: [], metrics: nil, views: ["contentView": contentView])
    addConstraints(verticalConstraints)
  }
  
  #if DEBUG
  deinit {
    print("deinit", self)
  }
  #endif
}

extension LiveEmptyCell: ReuseIdentifier {
  static func reuseIdentifier() -> String {
    return "LiveEmptyCell"
  }
}
