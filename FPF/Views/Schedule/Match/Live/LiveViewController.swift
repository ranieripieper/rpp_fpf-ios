//
//  LiveViewController.swift
//  FPF
//
//  Created by Gilson Gil on 1/8/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class LiveViewController: UIViewController {
  private let liveView = LiveView()

  var liveViewModel: LiveViewModel! {
    didSet {
      self.liveView.liveViewModel = liveViewModel
    }
  }
  
  override func loadView() {
    view = liveView
  }
  
  #if DEBUG
  deinit {
    print("deinit", self)
  }
  #endif
}
