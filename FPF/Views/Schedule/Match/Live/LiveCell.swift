//
//  LiveCell.swift
//  FPF
//
//  Created by Gilson Gil on 1/18/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class LiveCell: UITableViewCell {
  private let graphicImageView = UIImageView()
  private let contentLabel = UILabel()
  private let timeLabel = UILabel()
  
  var liveCellViewModel: LiveCellViewModel? {
    didSet {
      switch liveCellViewModel?.graphicType {
      case .None:
        graphicImageView.image = nil
      case .Some(.Goal):
        graphicImageView.image = UIImage.icnGoal()
      case .Some(.RedCard):
        graphicImageView.image = UIImage.icnRedCard()
      case .Some(.Substitute):
        graphicImageView.image = UIImage.icnSubs()
      case .Some(.YellowCard):
        graphicImageView.image = UIImage.icnYellowCard()
      }
      contentLabel.text = liveCellViewModel?.content
      timeLabel.text = liveCellViewModel?.time
    }
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setUp()
  }
  
  override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    setUp()
  }
  
  private func setUp() {
    contentView.translatesAutoresizingMaskIntoConstraints = false
    contentView.backgroundColor = UIColor.fpfLightGreyColor()
    selectionStyle = .None
    
    let stripe = UIView()
    stripe.translatesAutoresizingMaskIntoConstraints = false
    stripe.backgroundColor = UIColor.fpfStripeGreyColor()
    contentView.addSubview(stripe)
    
    let contentSubView = UIView()
    contentSubView.translatesAutoresizingMaskIntoConstraints = false
    contentSubView.backgroundColor = UIColor.whiteColor()
    contentView.addSubview(contentSubView)
    
    contentLabel.translatesAutoresizingMaskIntoConstraints = false
    contentLabel.font = UIFont.fpfRegular(12)
    contentLabel.textColor = UIColor.fpfGreyishColor()
    contentLabel.numberOfLines = 0
    contentSubView.addSubview(contentLabel)
    
    timeLabel.translatesAutoresizingMaskIntoConstraints = false
    timeLabel.font = UIFont.fpfMedium(32)
    timeLabel.textColor = UIColor.fpfGreyishColor()
    timeLabel.textAlignment = .Center
    contentView.addSubview(timeLabel)
    
    graphicImageView.translatesAutoresizingMaskIntoConstraints = false
    contentView.addSubview(graphicImageView)
    
    let metrics = ["margin": Constants.defaultMargin, "doubleMargin": Constants.defaultMargin * 2, "tripleMargin": Constants.defaultMargin * 3, "contentSubViewShortMargin": 20, "contentSubViewLongMargin": 95]
    let views = ["contentSubView": contentSubView, "contentLabel": contentLabel, "timeLabel": timeLabel, "stripe": stripe]
    
    var horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-contentSubViewShortMargin-[contentSubView]-contentSubViewLongMargin-|", options: [], metrics: metrics, views: views)
    contentView.addConstraints(horizontalConstraints)
    
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:[contentSubView]-margin-[timeLabel]-margin-|", options: [], metrics: metrics, views: views)
    contentView.addConstraints(horizontalConstraints)
    
    horizontalConstraints = [NSLayoutConstraint(item: graphicImageView, attribute: .Left, relatedBy: .Equal, toItem: contentSubView, attribute: .Left, multiplier: 1, constant: 2)]
    contentView.addConstraints(horizontalConstraints)
    
    var verticalConstraints = [NSLayoutConstraint(item: graphicImageView, attribute: .Top, relatedBy: .Equal, toItem: contentView, attribute: .Top, multiplier: 1, constant: 20)]
    contentView.addConstraints(verticalConstraints)
    
    verticalConstraints = [NSLayoutConstraint(item: contentSubView, attribute: .Top, relatedBy: .Equal, toItem: graphicImageView, attribute: .Bottom, multiplier: 0.8, constant: 0)]
    contentView.addConstraints(verticalConstraints)
    
    verticalConstraints = [NSLayoutConstraint(item: contentSubView, attribute: .Top, relatedBy: .GreaterThanOrEqual, toItem: contentView, attribute: .Top, multiplier: 1, constant: 20)]
    contentView.addConstraints(verticalConstraints)
    
    verticalConstraints = [NSLayoutConstraint(item: contentSubView, attribute: .Bottom, relatedBy: .Equal, toItem: contentView, attribute: .Bottom, multiplier: 1, constant: 0)]
    contentView.addConstraints(verticalConstraints)

    verticalConstraints = [NSLayoutConstraint(item: timeLabel, attribute: .CenterY, relatedBy: .Equal, toItem: contentSubView, attribute: .CenterY, multiplier: 1, constant: 0)]
    contentView.addConstraints(verticalConstraints)
    
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-contentSubViewShortMargin-[contentLabel]-contentSubViewShortMargin-|", options: [], metrics: metrics, views: views)
    contentSubView.addConstraints(horizontalConstraints)
    
    verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|-contentSubViewShortMargin-[contentLabel]-contentSubViewShortMargin-|", options: [], metrics: metrics, views: views)
    contentSubView.addConstraints(verticalConstraints)
    
    horizontalConstraints = [NSLayoutConstraint(item: stripe, attribute: .Left, relatedBy: .Equal, toItem: contentSubView, attribute: .Left, multiplier: 2, constant: 0), NSLayoutConstraint(item: stripe, attribute: .Width, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1, constant: 5)]
    contentView.addConstraints(horizontalConstraints)
    
    verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[stripe]|", options: [], metrics: metrics, views: views)
    contentView.addConstraints(verticalConstraints)
    
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[contentView]|", options: [], metrics: nil, views: ["contentView": contentView])
    addConstraints(horizontalConstraints)
    verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[contentView]|", options: [], metrics: nil, views: ["contentView": contentView])
    addConstraints(verticalConstraints)
  }
  
  func configureWithLiveCellViewModel(viewModel: LiveCellViewModel) {
    self.liveCellViewModel = viewModel
  }
  
  #if DEBUG
  deinit {
    print("deinit", self)
  }
  #endif
}

extension LiveCell: ReuseIdentifier {
  static func reuseIdentifier() -> String {
    return "LiveCell"
  }
}
