//
//  MatchView.swift
//  FPF
//
//  Created by Gilson Gil on 1/8/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit
import Kingfisher

protocol MatchViewDelegate: class {
  func didSelectLive()
  func didSelectLineup()
  func didSelectStats()
}

protocol MatchViewScrollDelegate: class {
  func scrollViewDidScroll(scrollView: UIScrollView)
  func scrollViewDidStop(scrollView: UIScrollView)
}

protocol MatchViewScrollProtocol: class {
  weak var scrollDelegate: MatchViewScrollDelegate? {get set}
  var scrollTopContentInset: CGFloat? {get set}
  var tableView: UITableView {get}
}

final class MatchView: UIView {
  private let separatorViewHeight: CGFloat = 2
  
  private let containerView = UIView()
  private let team1ImageView = UIImageView()
  private let team1NameLabel = UILabel()
  private let team1ScoreLabel = UILabel()
  private let team2ImageView = UIImageView()
  private let team2NameLabel = UILabel()
  private let team2ScoreLabel = UILabel()
  private let dateLabel = UILabel()
  private let locationLabel = UILabel()
  private let refereeLabel = UILabel()
  private let cityLabel = UILabel()
  private let bottomView = UIView()
  private let tabbarView = TabbarView()
  
  private let matchViewModel: MatchViewModel!
  private var lastYOffset: CGFloat = 0
  weak var delegate: MatchViewDelegate?
  
  required init?(coder aDecoder: NSCoder) {
    matchViewModel = nil
    super.init(coder: aDecoder)
    setUp()
  }
  
  init(matchViewModel: MatchViewModel) {
    self.matchViewModel = matchViewModel
    super.init(frame: .zero)
    setUp()
  }
  
  private func setUp() {
    backgroundColor = UIColor.whiteColor()
    
    let topView = UIView()
    topView.translatesAutoresizingMaskIntoConstraints = false
    topView.backgroundColor = UIColor.whiteColor()
    addSubview(topView)
    
    let team1View = UIView()
    team1View.translatesAutoresizingMaskIntoConstraints = false
    topView.addSubview(team1View)
    
    team1ImageView.translatesAutoresizingMaskIntoConstraints = false
    team1ImageView.contentMode = .ScaleAspectFit
    team1View.addSubview(team1ImageView)
    
    team1NameLabel.translatesAutoresizingMaskIntoConstraints = false
    team1NameLabel.font = UIFont.fpfRegular(14)
    team1NameLabel.textColor = UIColor.fpfBlackColor()
    team1NameLabel.textAlignment = .Center
    team1NameLabel.adjustsFontSizeToFitWidth = true
    team1NameLabel.minimumScaleFactor = 0.5
    team1View.addSubview(team1NameLabel)
    
    team1ScoreLabel.translatesAutoresizingMaskIntoConstraints = false
    team1ScoreLabel.font = UIFont.fpfCondensedBold(48)
    team1ScoreLabel.textColor = UIColor.blackColor()
    team1ScoreLabel.textAlignment = .Center
    topView.addSubview(team1ScoreLabel)
    
    let xLabel = UILabel()
    xLabel.translatesAutoresizingMaskIntoConstraints = false
    xLabel.textColor = UIColor.fpfSunflowerColor()
    xLabel.font = UIFont.fpfMedium(48)
    xLabel.textAlignment = .Center
    xLabel.text = "X"
    xLabel.setContentCompressionResistancePriority(UILayoutPriorityRequired, forAxis: .Horizontal)
    topView.addSubview(xLabel)
    
    let team2View = UIView()
    team2View.translatesAutoresizingMaskIntoConstraints = false
    topView.addSubview(team2View)
    
    team2ImageView.translatesAutoresizingMaskIntoConstraints = false
    team2ImageView.contentMode = .ScaleAspectFit
    team2View.addSubview(team2ImageView)
    
    team2NameLabel.translatesAutoresizingMaskIntoConstraints = false
    team2NameLabel.font = UIFont.fpfRegular(14)
    team2NameLabel.textColor = UIColor.fpfBlackColor()
    team2NameLabel.textAlignment = .Center
    team2NameLabel.adjustsFontSizeToFitWidth = true
    team2NameLabel.minimumScaleFactor = 0.5
    team2View.addSubview(team2NameLabel)
    
    team2ScoreLabel.translatesAutoresizingMaskIntoConstraints = false
    team2ScoreLabel.font = UIFont.fpfCondensedBold(48)
    team2ScoreLabel.textColor = UIColor.blackColor()
    team2ScoreLabel.textAlignment = .Center
    topView.addSubview(team2ScoreLabel)
    
    bottomView.translatesAutoresizingMaskIntoConstraints = false
    bottomView.backgroundColor = UIColor.fpfLightestGreyColor()
    bottomView.clipsToBounds = false
    insertSubview(bottomView, belowSubview: topView)
    
    let dateDescLabel = UILabel()
    dateDescLabel.translatesAutoresizingMaskIntoConstraints = false
    dateDescLabel.font = UIFont.fpfMedium(10)
    dateDescLabel.textColor = UIColor.fpfPinkishGreyColor()
    dateDescLabel.text = Strings.MatchDate
    bottomView.addSubview(dateDescLabel)
    
    dateLabel.translatesAutoresizingMaskIntoConstraints = false
    dateLabel.font = UIFont.fpfMedium(14)
    dateLabel.textColor = UIColor.fpfCementColor()
    bottomView.addSubview(dateLabel)
    
    let locationDescLabel = UILabel()
    locationDescLabel.translatesAutoresizingMaskIntoConstraints = false
    locationDescLabel.font = UIFont.fpfMedium(10)
    locationDescLabel.textColor = UIColor.fpfPinkishGreyColor()
    locationDescLabel.textAlignment = .Right
    locationDescLabel.text = Strings.MatchPlace
    bottomView.addSubview(locationDescLabel)
    
    locationLabel.translatesAutoresizingMaskIntoConstraints = false
    locationLabel.font = UIFont.fpfMedium(14)
    locationLabel.textColor = UIColor.fpfCementColor()
    locationLabel.textAlignment = .Right
    bottomView.addSubview(locationLabel)
    
    let refereeDescLabel = UILabel()
    refereeDescLabel.translatesAutoresizingMaskIntoConstraints = false
    refereeDescLabel.font = UIFont.fpfMedium(10)
    refereeDescLabel.textColor = UIColor.fpfPinkishGreyColor()
    refereeDescLabel.text = Strings.MatchReferee
    bottomView.addSubview(refereeDescLabel)
    
    refereeLabel.translatesAutoresizingMaskIntoConstraints = false
    refereeLabel.font = UIFont.fpfMedium(14)
    refereeLabel.textColor = UIColor.fpfCementColor()
    bottomView.addSubview(refereeLabel)
    
    let cityDescLabel = UILabel()
    cityDescLabel.translatesAutoresizingMaskIntoConstraints = false
    cityDescLabel.font = UIFont.fpfMedium(10)
    cityDescLabel.textColor = UIColor.fpfPinkishGreyColor()
    cityDescLabel.textAlignment = .Right
    cityDescLabel.text = Strings.MatchCity
    bottomView.addSubview(cityDescLabel)
    
    cityLabel.translatesAutoresizingMaskIntoConstraints = false
    cityLabel.font = UIFont.fpfMedium(14)
    cityLabel.textColor = UIColor.fpfCementColor()
    cityLabel.textAlignment = .Right
    bottomView.addSubview(cityLabel)
    
    let separatorView = UIView()
    separatorView.translatesAutoresizingMaskIntoConstraints = false
    separatorView.backgroundColor = UIColor.fpfMediumGreyColor()
    bottomView.addSubview(separatorView)
    
    containerView.translatesAutoresizingMaskIntoConstraints = false
    containerView.backgroundColor = UIColor.fpfLightGreyColor()
    insertSubview(containerView, belowSubview: bottomView)
    
    tabbarView.dataSource = self
    tabbarView.delegate = self
    tabbarView.translatesAutoresizingMaskIntoConstraints = false
    addSubview(tabbarView)
    
    let metrics = ["margin": Constants.defaultMargin, "doubleMargin": Constants.defaultMargin * 2, "halfMargin": Constants.defaultMargin / 2, "scoreLabelWidth": Constants.scoreLabelWidth, "tabbarHeight": Constants.tabbarHeight]
    let views = ["topView": topView, "team1View": team1View, "team1ImageView": team1ImageView, "team1NameLabel": team1NameLabel, "team1ScoreLabel": team1ScoreLabel, "xLabel": xLabel, "team2View": team2View, "team2ImageView": team2ImageView, "team2NameLabel": team2NameLabel, "team2ScoreLabel": team2ScoreLabel, "bottomView": bottomView, "dateDescLabel": dateDescLabel, "dateLabel": dateLabel, "locationDescLabel": locationDescLabel, "locationLabel": locationLabel, "refereeDescLabel": refereeDescLabel, "refereeLabel": refereeLabel, "cityDescLabel": cityDescLabel, "cityLabel": cityLabel, "separatorView": separatorView, "containerView": containerView, "tabbarView": tabbarView]
    
    let widthConstraint = NSLayoutConstraint(item: self, attribute: .Width, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1, constant: UIScreen.mainScreen().bounds.width)
    addConstraint(widthConstraint)
    
    let heightConstraint = NSLayoutConstraint(item: self, attribute: .Height, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1, constant: UIScreen.mainScreen().bounds.height)
    addConstraint(heightConstraint)
    
    var horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[topView]|", options: [], metrics: metrics, views: views)
    addConstraints(horizontalConstraints)
    
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[bottomView]|", options: [], metrics: metrics, views: views)
    addConstraints(horizontalConstraints)
    
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[containerView]|", options: [], metrics: metrics, views: views)
    addConstraints(horizontalConstraints)
    
    var verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|-64-[topView][bottomView]", options: [], metrics: metrics, views: views)
    addConstraints(verticalConstraints)
    
    verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:[topView][containerView]|", options: [], metrics: metrics, views: views)
    addConstraints(verticalConstraints)
    
    verticalConstraints = [NSLayoutConstraint(item: topView, attribute: .Height, relatedBy: .Equal, toItem: bottomView, attribute: .Height, multiplier: 1.2, constant: 0)]
    addConstraints(verticalConstraints)
    
    horizontalConstraints = [NSLayoutConstraint(item: xLabel, attribute: .CenterX, relatedBy: .Equal, toItem: topView, attribute: .CenterX, multiplier: 1, constant: 0)]
    topView.addConstraints(horizontalConstraints)
    
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:[team1View]-margin-[team1ScoreLabel(scoreLabelWidth)]-margin-[xLabel]-margin-[team2ScoreLabel(scoreLabelWidth)]-margin-[team2View]", options: [], metrics: metrics, views: views)
    topView.addConstraints(horizontalConstraints)
    
    [team1View, team1ScoreLabel, xLabel, team2ScoreLabel, team2View].forEach { view in
      verticalConstraints = [NSLayoutConstraint(item: view, attribute: .CenterY, relatedBy: .Equal, toItem: topView, attribute: .CenterY, multiplier: 1, constant: 0)]
      topView.addConstraints(verticalConstraints)
    }
    
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[team1ImageView(60)]|", options: [], metrics: metrics, views: views)
    team1View.addConstraints(horizontalConstraints)
    
    horizontalConstraints = [NSLayoutConstraint(item: team1ImageView, attribute: .CenterX, relatedBy: .Equal, toItem: team1View, attribute: .CenterX, multiplier: 1, constant: 0)]
    team1View.addConstraints(horizontalConstraints)
    
    horizontalConstraints = [NSLayoutConstraint(item: team1ImageView, attribute: .Width, relatedBy: .Equal, toItem: team1ImageView, attribute: .Height, multiplier: 1, constant: 0)]
    team1ImageView.addConstraints(horizontalConstraints)
    
    horizontalConstraints = [NSLayoutConstraint(item: team1NameLabel, attribute: .CenterX, relatedBy: .Equal, toItem: team1View, attribute: .CenterX, multiplier: 1, constant: 0)]
    team1View.addConstraints(horizontalConstraints)
    
    horizontalConstraints = [NSLayoutConstraint(item: team1NameLabel, attribute: .Left, relatedBy: .GreaterThanOrEqual, toItem: topView, attribute: .LeftMargin, multiplier: 1, constant: 0)]
    topView.addConstraints(horizontalConstraints)
    
    verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[team1ImageView]-margin-[team1NameLabel]|", options: [], metrics: metrics, views: views)
    team1View.addConstraints(verticalConstraints)
    
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[team2ImageView(60)]|", options: [], metrics: metrics, views: views)
    team2View.addConstraints(horizontalConstraints)
    
    horizontalConstraints = [NSLayoutConstraint(item: team2ImageView, attribute: .CenterX, relatedBy: .Equal, toItem: team2View, attribute: .CenterX, multiplier: 1, constant: 0)]
    team2View.addConstraints(horizontalConstraints)
    
    horizontalConstraints = [NSLayoutConstraint(item: team2ImageView, attribute: .Width, relatedBy: .Equal, toItem: team2ImageView, attribute: .Height, multiplier: 1, constant: 0)]
    team2ImageView.addConstraints(horizontalConstraints)
    
    horizontalConstraints = [NSLayoutConstraint(item: team2NameLabel, attribute: .CenterX, relatedBy: .Equal, toItem: team2View, attribute: .CenterX, multiplier: 1, constant: 0)]
    team2View.addConstraints(horizontalConstraints)
    
    horizontalConstraints = [NSLayoutConstraint(item: team2NameLabel, attribute: .Right, relatedBy: .LessThanOrEqual, toItem: topView, attribute: .RightMargin, multiplier: 1, constant: 0)]
    topView.addConstraints(horizontalConstraints)
    
    verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[team2ImageView]-margin-[team2NameLabel]|", options: [], metrics: metrics, views: views)
    team2View.addConstraints(verticalConstraints)
    
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-margin-[dateDescLabel]->=margin-[locationDescLabel]-margin-|", options: [], metrics: metrics, views: views)
    bottomView.addConstraints(horizontalConstraints)
    
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-margin-[dateLabel]->=margin-[locationLabel]-margin-|", options: [], metrics: metrics, views: views)
    bottomView.addConstraints(horizontalConstraints)
    
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-margin-[refereeDescLabel]->=margin-[cityDescLabel]-margin-|", options: [], metrics: metrics, views: views)
    bottomView.addConstraints(horizontalConstraints)
    
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-margin-[refereeLabel]->=margin-[cityLabel]-margin-|", options: [], metrics: metrics, views: views)
    bottomView.addConstraints(horizontalConstraints)
    
    verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|-margin-[dateDescLabel]-halfMargin-[dateLabel]-doubleMargin-[refereeDescLabel]-halfMargin-[refereeLabel]-margin-|", options: [], metrics: metrics, views: views)
    bottomView.addConstraints(verticalConstraints)
    
    verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|-margin-[locationDescLabel]-halfMargin-[locationLabel]-doubleMargin-[cityDescLabel]-halfMargin-[cityLabel]-margin-|", options: [], metrics: metrics, views: views)
    bottomView.addConstraints(verticalConstraints)
    
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[separatorView]|", options: [], metrics: metrics, views: views)
    bottomView.addConstraints(horizontalConstraints)
    
    verticalConstraints = [NSLayoutConstraint(item: separatorView, attribute: .Height, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1, constant: separatorViewHeight), NSLayoutConstraint(item: separatorView, attribute: .Top, relatedBy: .Equal, toItem: bottomView, attribute: .Bottom, multiplier: 1, constant: 0)]
    bottomView.addConstraints(verticalConstraints)
    
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[tabbarView]|", options: [], metrics: nil, views: views)
    verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:[tabbarView(tabbarHeight)]|", options: [], metrics: metrics, views: views)
    addConstraints(horizontalConstraints)
    addConstraints(verticalConstraints)
    
    if let URL = NSURL(string: matchViewModel.team1LogoURLString ?? "") {
      team1ImageView.kf_setImageWithURL(URL, placeholderImage: UIImage.icnPlaceholder())
    }
    team1NameLabel.text = matchViewModel.team1Name.uppercaseString
    matchViewModel.team1Score.bind { value in
      self.team1ScoreLabel.text = String(value)
    }
    if let URL = NSURL(string: matchViewModel.team2LogoURLString ?? "") {
      team2ImageView.kf_setImageWithURL(URL, placeholderImage: UIImage.icnPlaceholder())
    }
    team2NameLabel.text = matchViewModel.team2Name.uppercaseString
    matchViewModel.team2Score.bind { value in
      self.team2ScoreLabel.text = String(value)
    }
    dateLabel.text = matchViewModel.date
    locationLabel.text = matchViewModel.location
    refereeLabel.text = matchViewModel.referee
    cityLabel.text = matchViewModel.city
  }
  
  func addViewController(viewController: UIViewController) {
    viewController.view.frame = containerView.bounds
    containerView.insertSubview(viewController.view, atIndex: 0)
    
    if let scrollView = viewController.view as? MatchViewScrollProtocol {
      scrollView.scrollDelegate = self
      scrollViewDidScroll(scrollView.tableView)
    }
    
    let horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[view]|", options: [], metrics: nil, views: ["view": viewController.view])
    let verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[view]|", options: [], metrics: nil, views: ["view": viewController.view])
    containerView.addConstraints(horizontalConstraints)
    containerView.addConstraints(verticalConstraints)
  }
  
  private func setScrollViewTopContentInset() {
    containerView.subviews.forEach {
      if let scrollView = $0 as? MatchViewScrollProtocol {
        scrollView.scrollTopContentInset = bottomView.bounds.height
      }
    }
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    setScrollViewTopContentInset()
  }
  
  #if DEBUG
  deinit {
    print("deinit", self)
  }
  #endif
}

extension MatchView: TabbarViewDatasource {
  func tabbarViewNumberOfButtons() -> Int {
    if matchViewModel.hasStats {
      return 3
    } else {
      return 2
    }
  }
  
  func tabbarViewTitleAtIndex(index: Int) -> String {
    switch index {
    case 0:
      return Strings.MatchLive
    case 1:
      return Strings.MatchLineup
    case 2:
      return Strings.MatchStats
    default:
      return ""
    }
  }
}

extension MatchView: TabbarViewDelegate {
  func didSelectButtonAtIndex(index: Int) {
    switch index {
    case 0:
      delegate?.didSelectLive()
    case 1:
      delegate?.didSelectLineup()
    case 2:
      delegate?.didSelectStats()
    default:
      break
    }
  }
}

extension MatchView: MatchViewScrollDelegate {
  func scrollViewDidScroll(scrollView: UIScrollView) {
    bottomView.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, 0, -max(bottomView.bounds.height + min(scrollView.contentOffset.y, 0), 0))
    
    let aboveBottom = scrollView.contentOffset.y + scrollView.bounds.height <= scrollView.contentSize.height
    guard aboveBottom else {
      return
    }
    let deltaY = (scrollView.contentOffset.y - lastYOffset) / 2
    if scrollView.contentOffset.y <= -scrollView.contentInset.top {
      UIView.animateWithDuration(Constants.defaultAnimationDuration, delay: 0, usingSpringWithDamping: Constants.tabbarAnimationDamping, initialSpringVelocity: 0, options: [], animations: {
        self.tabbarView.transform = CGAffineTransformIdentity
        }, completion: nil)
    } else {
      tabbarView.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, 0, max(min(tabbarView.transform.ty + deltaY, Constants.tabbarHeight), 0))
    }
    lastYOffset = scrollView.contentOffset.y
  }
  
  func scrollViewDidStop(scrollView: UIScrollView) {
    UIView.animateWithDuration(Constants.defaultAnimationDuration, delay: 0, usingSpringWithDamping: Constants.tabbarAnimationDamping, initialSpringVelocity: 0, options: [], animations: {
      if self.tabbarView.transform.ty <= Constants.tabbarHeight / 2 {
        self.tabbarView.transform = CGAffineTransformIdentity
      } else {
        self.tabbarView.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, 0, Constants.tabbarHeight)
      }
      }, completion: nil)
  }
}
