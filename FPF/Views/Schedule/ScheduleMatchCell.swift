//
//  ScheduleMatchCell.swift
//  FPF
//
//  Created by Gilson Gil on 1/8/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit
import Kingfisher

final class ScheduleMatchCell: UITableViewCell {
  private let team1ImageView = UIImageView()
  private let team1NameLabel = UILabel()
  private let team1ScoreLabel = UILabel()
  private let team2ImageView = UIImageView()
  private let team2NameLabel = UILabel()
  private let team2ScoreLabel = UILabel()
  private let dateLabel = UILabel()
  private let locationLabel = UILabel()
  
  var matchCellViewModel: MatchCellViewModel? {
    didSet {
      if let URL = NSURL(string: matchCellViewModel?.team1LogoURLString ?? "") {
        team1ImageView.kf_setImageWithURL(URL, placeholderImage: UIImage.icnPlaceholder())
      }
      team1NameLabel.text = matchCellViewModel!.team1Name.uppercaseString
      team1ScoreLabel.text = String(matchCellViewModel!.team1Score)
      if let URL = NSURL(string: matchCellViewModel?.team2LogoURLString ?? "") {
        team2ImageView.kf_setImageWithURL(URL, placeholderImage: UIImage.icnPlaceholder())
      }
      team2NameLabel.text = matchCellViewModel!.team2Name.uppercaseString
      team2ScoreLabel.text = String(matchCellViewModel!.team2Score)
      dateLabel.text = matchCellViewModel!.date
      locationLabel.text = matchCellViewModel!.location
      if matchCellViewModel!.pastDate && matchCellViewModel?.match.hasLive ?? false {
        accessoryView = ScheduleMatchCellAccessoryView()
      } else {
        accessoryView = nil
      }
    }
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setUp()
  }
  
  override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    setUp()
  }
  
  private func setUp() {
    contentView.translatesAutoresizingMaskIntoConstraints = false
    selectionStyle = .None
    separatorInset = UIEdgeInsetsZero
    preservesSuperviewLayoutMargins = false
    layoutMargins = UIEdgeInsetsZero
    
    let topView = UIView()
    topView.translatesAutoresizingMaskIntoConstraints = false
    contentView.addSubview(topView)
    
    let team1View = UIView()
    team1View.translatesAutoresizingMaskIntoConstraints = false
    topView.addSubview(team1View)
    
    team1ImageView.translatesAutoresizingMaskIntoConstraints = false
    team1ImageView.contentMode = .ScaleAspectFit
    team1View.addSubview(team1ImageView)
    
    team1NameLabel.translatesAutoresizingMaskIntoConstraints = false
    team1NameLabel.font = UIFont.fpfRegular(14)
    team1NameLabel.textColor = UIColor.fpfBlackColor()
    team1NameLabel.textAlignment = .Center
    team1NameLabel.minimumScaleFactor = 0.5
    team1NameLabel.adjustsFontSizeToFitWidth = true
    team1View.addSubview(team1NameLabel)
    
    team1ScoreLabel.translatesAutoresizingMaskIntoConstraints = false
    team1ScoreLabel.font = UIFont.fpfCondensedBold(48)
    team1ScoreLabel.textColor = UIColor.blackColor()
    team1ScoreLabel.textAlignment = .Center
    topView.addSubview(team1ScoreLabel)
    
    let xLabel = UILabel()
    xLabel.translatesAutoresizingMaskIntoConstraints = false
    xLabel.textColor = UIColor.fpfSunflowerColor()
    xLabel.font = UIFont.fpfMedium(48)
    xLabel.textAlignment = .Center
    xLabel.text = "X"
    xLabel.setContentCompressionResistancePriority(UILayoutPriorityRequired, forAxis: .Horizontal)
    topView.addSubview(xLabel)
    
    let team2View = UIView()
    team2View.translatesAutoresizingMaskIntoConstraints = false
    topView.addSubview(team2View)
    
    team2ImageView.translatesAutoresizingMaskIntoConstraints = false
    team2ImageView.contentMode = .ScaleAspectFit
    team2View.addSubview(team2ImageView)
    
    team2NameLabel.translatesAutoresizingMaskIntoConstraints = false
    team2NameLabel.font = UIFont.fpfRegular(14)
    team2NameLabel.textColor = UIColor.fpfBlackColor()
    team2NameLabel.textAlignment = .Center
    team2NameLabel.minimumScaleFactor = 0.5
    team2NameLabel.adjustsFontSizeToFitWidth = true
    team2View.addSubview(team2NameLabel)
    
    team2ScoreLabel.translatesAutoresizingMaskIntoConstraints = false
    team2ScoreLabel.font = UIFont.fpfCondensedBold(48)
    team2ScoreLabel.textColor = UIColor.blackColor()
    team2ScoreLabel.textAlignment = .Center
    topView.addSubview(team2ScoreLabel)
    
    let bottomView = UIView()
    bottomView.translatesAutoresizingMaskIntoConstraints = false
    bottomView.backgroundColor = UIColor.fpfLightGreyColor()
    contentView.addSubview(bottomView)
    
    let dateDescLabel = UILabel()
    dateDescLabel.translatesAutoresizingMaskIntoConstraints = false
    dateDescLabel.font = UIFont.fpfMedium(10)
    dateDescLabel.textColor = UIColor.fpfPinkishGreyColor()
    dateDescLabel.text = "Data e hora"
    bottomView.addSubview(dateDescLabel)
    
    dateLabel.translatesAutoresizingMaskIntoConstraints = false
    dateLabel.font = UIFont.fpfMedium(14)
    dateLabel.textColor = UIColor.fpfCementColor()
    bottomView.addSubview(dateLabel)
    
    let locationDescLabel = UILabel()
    locationDescLabel.translatesAutoresizingMaskIntoConstraints = false
    locationDescLabel.font = UIFont.fpfMedium(10)
    locationDescLabel.textColor = UIColor.fpfPinkishGreyColor()
    locationDescLabel.textAlignment = .Right
    locationDescLabel.text = "Local"
    bottomView.addSubview(locationDescLabel)
    
    locationLabel.translatesAutoresizingMaskIntoConstraints = false
    locationLabel.font = UIFont.fpfMedium(14)
    locationLabel.textColor = UIColor.fpfCementColor()
    locationLabel.textAlignment = .Right
    bottomView.addSubview(locationLabel)
    
    let metrics = ["margin": Constants.defaultMargin, "doubleMargin": Constants.defaultMargin * 2, "halfMargin": Constants.defaultMargin / 2, "scoreLabelWidth": Constants.scoreLabelWidth, "logoWidth": Constants.largeLogoWidth]
    let views = ["topView": topView, "team1View": team1View, "team1ImageView": team1ImageView, "team1NameLabel": team1NameLabel, "team1ScoreLabel": team1ScoreLabel, "xLabel": xLabel, "team2View": team2View, "team2ImageView": team2ImageView, "team2NameLabel": team2NameLabel, "team2ScoreLabel": team2ScoreLabel, "bottomView": bottomView, "dateDescLabel": dateDescLabel, "dateLabel": dateLabel, "locationDescLabel": locationDescLabel, "locationLabel": locationLabel]
    var horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[topView]|", options: [], metrics: metrics, views: views)
    contentView.addConstraints(horizontalConstraints)
    
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[bottomView]|", options: [], metrics: metrics, views: views)
    contentView.addConstraints(horizontalConstraints)
    
    var verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[topView][bottomView]|", options: [], metrics: metrics, views: views)
    contentView.addConstraints(verticalConstraints)
    
    verticalConstraints = [NSLayoutConstraint(item: topView, attribute: .Height, relatedBy: .Equal, toItem: bottomView, attribute: .Height, multiplier: 2.3, constant: 0)]
    contentView.addConstraints(verticalConstraints)
    
    horizontalConstraints = [NSLayoutConstraint(item: xLabel, attribute: .CenterX, relatedBy: .Equal, toItem: topView, attribute: .CenterX, multiplier: 1, constant: 0)]
    topView.addConstraints(horizontalConstraints)
    
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:[team1View]-margin-[team1ScoreLabel(scoreLabelWidth)]-margin-[xLabel]-margin-[team2ScoreLabel(scoreLabelWidth)]-margin-[team2View]", options: [], metrics: metrics, views: views)
    topView.addConstraints(horizontalConstraints)
    
    [team1View, team1ScoreLabel, xLabel, team2ScoreLabel, team2View].forEach { view in
      verticalConstraints = [NSLayoutConstraint(item: view, attribute: .CenterY, relatedBy: .Equal, toItem: topView, attribute: .CenterY, multiplier: 1, constant: 0)]
      topView.addConstraints(verticalConstraints)
    }
    
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[team1ImageView(logoWidth)]|", options: [], metrics: metrics, views: views)
    team1View.addConstraints(horizontalConstraints)
    
    horizontalConstraints = [NSLayoutConstraint(item: team1ImageView, attribute: .CenterX, relatedBy: .Equal, toItem: team1View, attribute: .CenterX, multiplier: 1, constant: 0)]
    team1View.addConstraints(horizontalConstraints)
    
    horizontalConstraints = [NSLayoutConstraint(item: team1ImageView, attribute: .Width, relatedBy: .Equal, toItem: team1ImageView, attribute: .Height, multiplier: 1, constant: 0)]
    team1ImageView.addConstraints(horizontalConstraints)

    horizontalConstraints = [NSLayoutConstraint(item: team1NameLabel, attribute: .CenterX, relatedBy: .Equal, toItem: team1View, attribute: .CenterX, multiplier: 1, constant: 0)]
    team1View.addConstraints(horizontalConstraints)
    
    horizontalConstraints = [NSLayoutConstraint(item: team1NameLabel, attribute: .Left, relatedBy: .GreaterThanOrEqual, toItem: contentView, attribute: .LeftMargin, multiplier: 1, constant: 0)]
    contentView.addConstraints(horizontalConstraints)
    
    verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[team1ImageView]-margin-[team1NameLabel]|", options: [], metrics: metrics, views: views)
    team1View.addConstraints(verticalConstraints)
    
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[team2ImageView(logoWidth)]|", options: [], metrics: metrics, views: views)
    team2View.addConstraints(horizontalConstraints)
    
    horizontalConstraints = [NSLayoutConstraint(item: team2ImageView, attribute: .CenterX, relatedBy: .Equal, toItem: team2View, attribute: .CenterX, multiplier: 1, constant: 0)]
    team2View.addConstraints(horizontalConstraints)
    
    horizontalConstraints = [NSLayoutConstraint(item: team2ImageView, attribute: .Width, relatedBy: .Equal, toItem: team2ImageView, attribute: .Height, multiplier: 1, constant: 0)]
    team2ImageView.addConstraints(horizontalConstraints)

    horizontalConstraints = [NSLayoutConstraint(item: team2NameLabel, attribute: .CenterX, relatedBy: .Equal, toItem: team2View, attribute: .CenterX, multiplier: 1, constant: 0)]
    team2View.addConstraints(horizontalConstraints)
    
    horizontalConstraints = [NSLayoutConstraint(item: team2NameLabel, attribute: .Right, relatedBy: .LessThanOrEqual, toItem: contentView, attribute: .RightMargin, multiplier: 1, constant: 0)]
    contentView.addConstraints(horizontalConstraints)
    
    verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[team2ImageView]-margin-[team2NameLabel]|", options: [], metrics: metrics, views: views)
    team2View.addConstraints(verticalConstraints)
    
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-margin-[dateDescLabel]->=margin-[locationDescLabel]-margin-|", options: [], metrics: metrics, views: views)
    bottomView.addConstraints(horizontalConstraints)
    
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-margin-[dateLabel]->=margin-[locationLabel]-margin-|", options: [], metrics: metrics, views: views)
    bottomView.addConstraints(horizontalConstraints)
    
    verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|-margin-[dateDescLabel]-halfMargin-[dateLabel]-margin-|", options: [], metrics: metrics, views: views)
    bottomView.addConstraints(verticalConstraints)
    
    verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|-margin-[locationDescLabel]-halfMargin-[locationLabel]-margin-|", options: [], metrics: metrics, views: views)
    bottomView.addConstraints(verticalConstraints)
    
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[contentView]|", options: [], metrics: nil, views: ["contentView": contentView])
    addConstraints(horizontalConstraints)
    verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[contentView]|", options: [], metrics: nil, views: ["contentView": contentView])
    addConstraints(verticalConstraints)
  }
  
  func configureWithMatchCellViewModel(viewModel: MatchCellViewModel) {
    matchCellViewModel = viewModel
  }
  
  #if DEBUG
  deinit {
    print("deinit", self)
  }
  #endif
}

extension ScheduleMatchCell: ReuseIdentifier {
  static func reuseIdentifier() -> String {
    return "ScheduleMatchCell"
  }
}
