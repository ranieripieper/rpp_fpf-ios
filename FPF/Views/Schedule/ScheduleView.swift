//
//  ScheduleView.swift
//  FPF
//
//  Created by Gilson Gil on 1/8/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit
import AKPickerView_Swift

protocol ScheduleViewDelegate: class {
  func didSelectMatchAtRound(round: Int, index: Int)
}

final class ScheduleView: UIView {
  private let pickerViewHeight: CGFloat = 50
  private let pickerView = AKPickerView(frame: .zero)
  private let collectionView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewLayout())
  private var hasDragged = false
  
  var scheduleViewModel: ScheduleViewModel? {
    didSet {
      dispatch_async(dispatch_get_main_queue()) { [weak self] in
        self?.collectionView.reloadData()
        self?.pickerView.reloadData()
        if let weakSelf = self {
          if !weakSelf.hasDragged, let currentRound = weakSelf.scheduleViewModel?.currentRound where currentRound > 1 {
            if weakSelf.collectionView.numberOfItemsInSection(0) >= currentRound {
              weakSelf.hasDragged = true
              weakSelf.collectionView.scrollToItemAtIndexPath(NSIndexPath(forItem: currentRound - 1, inSection: 0), atScrollPosition: .CenteredHorizontally, animated: false)
              weakSelf.pickerView.selectItem(currentRound - 1, animated: false)
            }
          }
        }
      }
    }
  }
  
  weak var delegate: ScheduleViewDelegate?
  weak var scrollDelegate: LeagueScrollDelegate?
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setUp()
  }
  
  init() {
    super.init(frame: .zero)
    setUp()
  }
  
  private func setUp() {
    translatesAutoresizingMaskIntoConstraints = false
    backgroundColor = UIColor.clearColor()
    
    pickerView.translatesAutoresizingMaskIntoConstraints = false
    pickerView.backgroundColor = UIColor.fpfLightGreyColor()
    pickerView.font = UIFont.fpfBold(18)!
    pickerView.textColor = UIColor.fpfCementColor()
    pickerView.highlightedTextColor = UIColor.fpfCementColor()
    pickerView.dataSource = self
    pickerView.delegate = self
    addSubview(pickerView)
    
    let collectionViewLayout = UICollectionViewFlowLayout()
    collectionViewLayout.scrollDirection = .Horizontal
    collectionViewLayout.minimumInteritemSpacing = 0
    collectionView.collectionViewLayout = collectionViewLayout
    collectionView.translatesAutoresizingMaskIntoConstraints = false
    collectionView.dataSource = self
    collectionView.delegate = self
    collectionView.registerClass(ScheduleRoundCell.classForCoder(), forCellWithReuseIdentifier: ScheduleRoundCell.reuseIdentifier())
    collectionView.pagingEnabled = true
    collectionView.backgroundColor = UIColor.clearColor()
    collectionView.allowsSelection = false
    collectionView.scrollsToTop = false
    addSubview(collectionView)
    
    let metrics = ["pickerViewHeight": pickerViewHeight]
    let views = ["pickerView": pickerView, "collectionView": collectionView]
    let horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[pickerView]|", options: [], metrics: metrics, views: views)
    let horizontalConstraints2 = NSLayoutConstraint.constraintsWithVisualFormat("H:|[collectionView]|", options: [], metrics: metrics, views: views)
    let verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[pickerView(pickerViewHeight)][collectionView]|", options: [], metrics: metrics, views: views)
    addConstraints(horizontalConstraints)
    addConstraints(horizontalConstraints2)
    addConstraints(verticalConstraints)
  }
  
  #if DEBUG
  deinit {
    print("deinit", self)
  }
  #endif
}

extension ScheduleView: AKPickerViewDataSource {
  func numberOfItemsInPickerView(pickerView: AKPickerView) -> Int {
    if let count = scheduleViewModel?.rounds.count where count > 0 {
      pickerView.hidden = false
      return count
    }
    pickerView.hidden = true
    return 0
  }
  
  func pickerView(pickerView: AKPickerView, titleForItem item: Int) -> String {
    if item < scheduleViewModel?.rounds.count {
      return scheduleViewModel?.rounds[item].roundName ?? ""
    }
    return ""
  }
}

extension ScheduleView: AKPickerViewDelegate {
  func pickerView(pickerView: AKPickerView, didSelectItem item: Int) {
    collectionView.scrollToItemAtIndexPath(NSIndexPath(forItem: item, inSection: 0), atScrollPosition: .CenteredHorizontally, animated: true)
  }
  
  func pickerView(pickerView: AKPickerView, marginForItem item: Int) -> CGSize {
    return CGSize(width: 40, height: 0)
  }
}

extension ScheduleView: UICollectionViewDataSource {
  func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return scheduleViewModel?.rounds.count ?? 0
  }
  
  func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableCellWithReuseIdentifier(ScheduleRoundCell.reuseIdentifier(), forIndexPath: indexPath) as! ScheduleRoundCell
    cell.delegate = self
    cell.scrollDelegate = scrollDelegate
    cell.configureWithRoundCellViewModel(scheduleViewModel!.rounds[indexPath.item])
    return cell
  }
}

extension ScheduleView: UICollectionViewDelegateFlowLayout {
  func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
    return 0
  }
  
  func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
    return collectionView.bounds.size
  }
}

extension ScheduleView: UIScrollViewDelegate {
  func scrollViewDidScroll(scrollView: UIScrollView) {
    guard scrollView == collectionView && scrollView.dragging else {
      return
    }
    let index = Int(scrollView.contentOffset.x / scrollView.bounds.width + 0.5)
    if index != pickerView.selectedItem {
      pickerView.selectItem(index, animated: true)
    }
  }
}

extension ScheduleView: ScheduleRoundCellDelegate {
  func didSelectMatchAtIndex(index: Int) {
    let round = pickerView.selectedItem
    delegate?.didSelectMatchAtRound(round, index: index)
  }
  
  func didScroll(scrollView: UIScrollView) {
    pickerView.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, 0, max(0, -scrollView.contentOffset.y))
  }
}

extension ScheduleView: LeagueScrollProtocol {
  
}
