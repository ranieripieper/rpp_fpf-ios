//
//  ScheduleMatchCellAccessoryView.swift
//  FPF
//
//  Created by Gilson Gil on 1/26/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class ScheduleMatchCellAccessoryView: UIView {
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }
  
  init() {
    super.init(frame: .zero)
    setUp()
  }
  
  private func setUp() {
    let image = UIImage.icnArrow()
    let imageView = UIImageView(image: image)
    imageView.contentMode = .Center
    imageView.frame = CGRect(x: imageView.frame.origin.x, y: imageView.frame.origin.y - 30, width: imageView.bounds.width, height: imageView.bounds.height)
    addSubview(imageView)
  }
}
