//
//  StandingsViewController.swift
//  FPF
//
//  Created by Gilson Gil on 1/8/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

public final class StandingsViewController: UIViewController, LeagueViewControllerProtocol {
  private let standingsView = StandingsView()
  var standingsViewModel: StandingsViewModel? {
    didSet {
      standingsView.standingsViewModel = standingsViewModel
    }
  }
  
  weak var leagueViewControllerDelegate: LeagueViewControllerDelegate?
  
  override public func loadView() {
    standingsView.delegate = self
    view = standingsView
  }
  
  public override func viewDidLoad() {
    super.viewDidLoad()
    if !NSUserDefaults.standardUserDefaults().boolForKey(Constants.hasPresentedTutorialString), let hasTeamStats = standingsViewModel?.phases.first?.groups.first?.standings.first?.hasTeamStats where hasTeamStats {
      view.userInteractionEnabled = false
    }
  }
  
  func presentTutorial(frame: CGRect) {
    guard let hasTeamStats = standingsViewModel?.phases.first?.groups.first?.standings.first?.hasTeamStats where hasTeamStats else {
      return
    }
    NSUserDefaults.standardUserDefaults().setBool(true, forKey: Constants.hasPresentedTutorialString)
    NSUserDefaults.standardUserDefaults().synchronize()
    dispatch_async(dispatch_get_main_queue()) { [weak self] in
      let tutorialViewController = TutorialViewController(circleFrame: frame)
      self?.leagueViewControllerDelegate?.presentViewController(tutorialViewController)
      self?.view.userInteractionEnabled = true
    }
  }
  
  #if DEBUG
  deinit {
    print("deinit", self)
  }
  #endif
}

extension StandingsViewController: StandingsViewDelegate {
  func goToTeamStats(teamId: Int) {
    ProgressHUDHelper.show()
    TeamStatsViewModel.create(teamId) { [weak self] teamStatsViewModel in
      dispatch_async(dispatch_get_main_queue()) { [weak self] in
        ProgressHUDHelper.dismiss()
        if let teamStatsViewModel = teamStatsViewModel {
          let teamStatsViewController = TeamStatsViewController(teamStatsViewModel: teamStatsViewModel)
          self?.leagueViewControllerDelegate?.pushViewController(teamStatsViewController)
        }
      }
    }
  }
}
