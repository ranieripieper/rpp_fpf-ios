//
//  StandingPhaseCell.swift
//  FPF
//
//  Created by Gilson Gil on 1/28/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

protocol StandingPhaseCellDelegate: class {
  func didScroll(scrollView: UIScrollView)
  func goToTeamStats(teamId: Int)
}

final class StandingPhaseCell: UICollectionViewCell {
  private let tableView = UITableView()
  private let rowHeight: CGFloat = 66
  private let headerHeight: CGFloat = 60
  
  var viewModel: StandingPhaseCellViewModel? {
    didSet {
      tableView.reloadData()
    }
  }
  var logoFrame: CGRect? {
    if !NSUserDefaults.standardUserDefaults().boolForKey(Constants.hasPresentedTutorialString) {
      if let standingsCell = tableView.cellForRowAtIndexPath(NSIndexPath(forRow: 0, inSection: 0)) as? StandingsCell, frame = standingsCell.window?.convertRect(standingsCell.logoFrame, fromView: standingsCell) {
        return frame
      }
    }
    return nil
  }
  
  weak var delegate: StandingPhaseCellDelegate?
  weak var scrollDelegate: LeagueScrollDelegate?
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setUp()
  }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    setUp()
  }
  
  private func setUp() {
    translatesAutoresizingMaskIntoConstraints = false
    
    tableView.translatesAutoresizingMaskIntoConstraints = false
    tableView.separatorStyle = .None
    tableView.rowHeight = rowHeight
    tableView.dataSource = self
    tableView.delegate = self
    tableView.backgroundColor = UIColor.clearColor()
    tableView.registerClass(StandingsCell.classForCoder(), forCellReuseIdentifier: StandingsCell.reuseIdentifier())
    tableView.registerClass(StandingsHeader.classForCoder(), forHeaderFooterViewReuseIdentifier: StandingsHeader.reuseIdentifier())
    addSubview(tableView)
    
    let views = ["tableView": tableView]
    let horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[tableView]|", options: [], metrics: nil, views: views)
    let verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[tableView]|", options: [], metrics: nil, views: views)
    addConstraints(horizontalConstraints)
    addConstraints(verticalConstraints)
  }
  
  func configureWithStandingPhaseCellViewModel(viewModel: StandingPhaseCellViewModel) {
    self.viewModel = viewModel
  }
  
  #if DEBUG
  deinit {
    print("deinit", self)
  }
  #endif
}

extension StandingPhaseCell: UITableViewDataSource {
  func numberOfSectionsInTableView(tableView: UITableView) -> Int {
    return viewModel?.groups.count ?? 0
  }
  
  func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    let header = tableView.dequeueReusableHeaderFooterViewWithIdentifier(StandingsHeader.reuseIdentifier()) as! StandingsHeader
    if let group = viewModel?.groups[section].name where group.characters.count > 0 {
      header.configureWithText("Grupo \(group)")
    } else {
      header.configureWithText("")
    }
    
    return header
  }
  
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return viewModel?.groups[section].standings.count ?? 0
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCellWithIdentifier(StandingsCell.reuseIdentifier(), forIndexPath: indexPath) as! StandingsCell
    cell.configureWithStandingCellViewModel(viewModel!.groups[indexPath.section].standings[indexPath.row], index: indexPath.row)
    cell.delegate = self
    return cell
  }
}

extension StandingPhaseCell: UITableViewDelegate {
  func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    guard let group = viewModel?.groups[section].name where group.characters.count > 0 else {
      return headerHeight / 2
    }
    return headerHeight
  }
  
  func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    guard let standing = viewModel?.groups[indexPath.section].standings[indexPath.row] where standing.hasTeamStats else {
      return
    }
    delegate?.goToTeamStats(standing.teamId)
  }
}

extension StandingPhaseCell: ReuseIdentifier {
  static func reuseIdentifier() -> String {
    return "StandingsPhaseCell"
  }
}

extension StandingPhaseCell: UIScrollViewDelegate {
  func scrollViewDidScroll(scroll: UIScrollView) {
    scrollDelegate?.scrollViewDidScroll(scroll)
    delegate?.didScroll(scroll)
  }
  
  func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
    scrollDelegate?.scrollViewDidStop(scrollView)
  }
  
  func scrollViewDidEndDragging(scrollView: UIScrollView, willDecelerate decelerate: Bool) {
    if !decelerate {
      scrollDelegate?.scrollViewDidStop(scrollView)
    }
  }
}

extension StandingPhaseCell: StandingsCellDelegate {
  func goToTeamStats(teamId: Int) {
    delegate?.goToTeamStats(teamId)
  }
}

extension StandingPhaseCell: LeagueScrollProtocol {}
