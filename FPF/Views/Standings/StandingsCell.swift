//
//  StandingsCell.swift
//  FPF
//
//  Created by Gilson Gil on 1/14/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit
import Kingfisher

protocol StandingsCellDelegate {
  func goToTeamStats(teamId: Int)
}

final class StandingsCell: UITableViewCell {
  private let positionLabel = UILabel()
  private let logoButton = UIButton(type: .Custom)
  private let nameLabel = UILabel()
  private let pointsLabel = UILabel()
  private let matchesLabel = UILabel()
  private let winsLabel = UILabel()
  private let drawsLabel = UILabel()
  private let lossesLabel = UILabel()
  private let goalsLabel = UILabel()
  var logoFrame: CGRect {
    return logoButton.frame
  }
  var viewModel: StandingCellViewModel? {
    didSet {
      if let position = viewModel?.position {
        positionLabel.text = position + "º"
      } else {
        positionLabel.text = ""
      }
      if let URL = NSURL(string: viewModel?.imageURL ?? "") {
        KingfisherManager.sharedManager.retrieveImageWithURL(URL, optionsInfo: nil, progressBlock: nil) { image, error, cacheType, imageURL in
          self.logoButton.setImage(image, forState: .Normal)
        }
      }
      logoButton.userInteractionEnabled = viewModel?.hasTeamStats ?? false
      nameLabel.text = (viewModel?.name ?? "").uppercaseString
      pointsLabel.text = String(viewModel?.points ?? "")
      matchesLabel.text = String(viewModel?.played ?? "")
      winsLabel.text = String(viewModel?.wins ?? "")
      drawsLabel.text = String(viewModel?.draws ?? "")
      lossesLabel.text = String(viewModel?.losses ?? "")
      goalsLabel.text = String(viewModel?.goalDifference ?? "")
    }
  }
  var delegate: StandingsCellDelegate?
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setUp()
  }
  
  override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    setUp()
  }
  
  private func setUp() {
    contentView.translatesAutoresizingMaskIntoConstraints = false
    selectionStyle = .None
    
    positionLabel.translatesAutoresizingMaskIntoConstraints = false
    positionLabel.font = UIFont.fpfCondensedBold(14)
    positionLabel.textColor = UIColor.fpfBlackColor()
    positionLabel.textAlignment = .Center
    contentView.addSubview(positionLabel)
    
    logoButton.translatesAutoresizingMaskIntoConstraints = false
    logoButton.imageView?.contentMode = .ScaleAspectFit
    logoButton.addTarget(self, action: #selector(StandingsCell.goToTeamStats), forControlEvents: .TouchUpInside)
    contentView.addSubview(logoButton)
    
    nameLabel.translatesAutoresizingMaskIntoConstraints = false
    nameLabel.font = UIFont.fpfMedium(12)
    nameLabel.textColor = UIColor.fpfBlackColor()
    nameLabel.adjustsFontSizeToFitWidth = true
    nameLabel.minimumScaleFactor = 0.5
    contentView.addSubview(nameLabel)
    
    pointsLabel.translatesAutoresizingMaskIntoConstraints = false
    pointsLabel.font = UIFont.fpfMedium(14)
    pointsLabel.textColor = UIColor.fpfCementColor()
    pointsLabel.textAlignment = .Center
    contentView.addSubview(pointsLabel)
    
    matchesLabel.translatesAutoresizingMaskIntoConstraints = false
    matchesLabel.font = UIFont.fpfMedium(14)
    matchesLabel.textColor = UIColor.fpfPinkishGreyColor()
    matchesLabel.textAlignment = .Center
    contentView.addSubview(matchesLabel)
    
    winsLabel.translatesAutoresizingMaskIntoConstraints = false
    winsLabel.font = UIFont.fpfMedium(14)
    winsLabel.textColor = UIColor.fpfPinkishGreyColor()
    winsLabel.textAlignment = .Center
    contentView.addSubview(winsLabel)
    
    drawsLabel.translatesAutoresizingMaskIntoConstraints = false
    drawsLabel.font = UIFont.fpfMedium(14)
    drawsLabel.textColor = UIColor.fpfPinkishGreyColor()
    drawsLabel.textAlignment = .Center
    contentView.addSubview(drawsLabel)
    
    lossesLabel.translatesAutoresizingMaskIntoConstraints = false
    lossesLabel.font = UIFont.fpfMedium(14)
    lossesLabel.textColor = UIColor.fpfPinkishGreyColor()
    lossesLabel.textAlignment = .Center
    contentView.addSubview(lossesLabel)
    
    goalsLabel.translatesAutoresizingMaskIntoConstraints = false
    goalsLabel.font = UIFont.fpfMedium(14)
    goalsLabel.textColor = UIColor.fpfPinkishGreyColor()
    goalsLabel.textAlignment = .Center
    contentView.addSubview(goalsLabel)
    
    let metrics = ["margin": Constants.defaultMargin, "doubleMargin": Constants.defaultMargin * 2, "width": 30]
    let views = ["positionLabel": positionLabel, "logoButton": logoButton, "nameLabel": nameLabel, "matchesLabel": matchesLabel, "pointsLabel": pointsLabel, "winsLabel": winsLabel, "drawsLabel": drawsLabel, "lossesLabel": lossesLabel, "goalsLabel": goalsLabel]
    var horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-margin-[positionLabel]-margin-[logoButton(width)]-margin-[nameLabel]->=margin-[pointsLabel(width)][matchesLabel(width)][winsLabel(width)][drawsLabel(width)][lossesLabel(width)][goalsLabel(width)]|", options: [], metrics: metrics, views: views)
    contentView.addConstraints(horizontalConstraints)
    
    [positionLabel, logoButton, nameLabel, pointsLabel, matchesLabel, winsLabel, drawsLabel, lossesLabel, goalsLabel].forEach { view in
      let verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|-margin-[view]-margin-|", options: [], metrics: metrics, views: ["view": view])
      contentView.addConstraints(verticalConstraints)
    }
    
    [pointsLabel, matchesLabel, winsLabel, drawsLabel, lossesLabel, goalsLabel].forEach { view in
      let separator = UIView()
      separator.translatesAutoresizingMaskIntoConstraints = false
      separator.backgroundColor = UIColor.fpfMediumGreyColor()
      contentView.addSubview(separator)
      
      let hConstraints = [
        NSLayoutConstraint(item: separator, attribute: .Left, relatedBy: .Equal, toItem: view, attribute: .Left, multiplier: 1, constant: 0),
        NSLayoutConstraint(item: separator, attribute: .Width, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1, constant: 1)
      ]
      contentView.addConstraints(hConstraints)
      let vConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[separator]|", options: [], metrics: nil, views: ["separator": separator])
      contentView.addConstraints(vConstraints)
    }
    
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[contentView]|", options: [], metrics: nil, views: ["contentView": contentView])
    addConstraints(horizontalConstraints)
    let verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[contentView]|", options: [], metrics: nil, views: ["contentView": contentView])
    addConstraints(verticalConstraints)
  }
  
  func configureWithStandingCellViewModel(viewModel: StandingCellViewModel, index: Int) {
    self.viewModel = viewModel
    if index % 2 == 0 {
      contentView.backgroundColor = UIColor.whiteColor()
    } else {
      contentView.backgroundColor = UIColor.fpfLightGreyColor()
    }
  }
  
  func goToTeamStats() {
    if let viewModel = viewModel where viewModel.hasTeamStats {
      delegate?.goToTeamStats(viewModel.teamId ?? 0)
    }
  }
  
  #if DEBUG
  deinit {
    print("deinit", self)
  }
  #endif
}

extension StandingsCell: ReuseIdentifier {
  static func reuseIdentifier() -> String {
    return "StandingsCell"
  }
}
