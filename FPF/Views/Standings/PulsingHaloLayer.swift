//
//  PulsingHaloLayer.swift
//  FPF
//
//  Created by Gilson Gil on 3/7/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class PulsingHaloLayer: CAReplicatorLayer {
  private let effect = CALayer()
  private var animationGroup = CAAnimationGroup()
  private let fromValueForRadius: CGFloat = 0
  private let fromValueForAlpha: CGFloat = 0.8
  private let toValueForAlpha: CGFloat = 0.2
  private let keyTimeForHalfOpacity: CGFloat = 0.6
  private let useTimingFunction = true
  private let color = UIColor.fpfSunflowerColor().CGColor
  
  override var frame: CGRect {
    didSet {
      position = CGPoint(x: frame.origin.x + frame.width, y: frame.origin.y + frame.height)
      radius = max(frame.width, frame.height)
    }
  }
  override var backgroundColor: CGColor? {
    didSet {
      effect.backgroundColor = backgroundColor
    }
  }
  private var radius: CGFloat = 60 {
    didSet {
      let diameter = radius * 2
      effect.bounds = CGRect(x: 0, y: 0, width: diameter, height: diameter)
      effect.cornerRadius = radius
    }
  }
  private var pulseInterval: NSTimeInterval = 0 {
    didSet {
      if pulseInterval == NSTimeInterval(Float.infinity) {
        effect.removeAnimationForKey("pulse")
      }
    }
  }
  private var haloLayerNumber: Int = 2 {
    didSet {
      instanceCount = haloLayerNumber
      instanceDelay = (animationDuration + pulseInterval) / NSTimeInterval(haloLayerNumber)
    }
  }
  private var startInterval: NSTimeInterval = 1 {
    didSet {
      instanceDelay = startInterval
    }
  }
  private var animationDuration: NSTimeInterval = 2 {
    didSet {
      animationGroup.duration = animationDuration + pulseInterval
      for anAnimation in animationGroup.animations ?? [] {
        anAnimation.duration = animationDuration
      }
      effect.removeAllAnimations()
      effect.addAnimation(animationGroup, forKey: "pulse")
      instanceDelay = (animationDuration + pulseInterval) / NSTimeInterval(haloLayerNumber)
    }
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }
  
  init(repeatCount: Float) {
    super.init()
    self.repeatCount = repeatCount
    effect.backgroundColor = color
    effect.contentsScale = UIScreen.mainScreen().scale
    effect.opacity = 0
    addSublayer(effect)
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) { [weak self] in
      if let weakSelf = self {
        weakSelf.setupAnimationGroup()
      }
      if let weakSelf = self where weakSelf.pulseInterval != NSTimeInterval(Float.infinity) {
        dispatch_async(dispatch_get_main_queue()) {
          weakSelf.effect.addAnimation(weakSelf.animationGroup, forKey: "pulse")
        }
      }
    }
  }
  
  convenience init(layerNumber: Int) {
    self.init(repeatCount: Float.infinity)
  }
  
  private func setupAnimationGroup() {
    let animationGroup = CAAnimationGroup()
    animationGroup.duration = animationDuration + pulseInterval
    animationGroup.repeatCount = repeatCount
    animationGroup.removedOnCompletion = false
    if useTimingFunction {
      let easeOut = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseOut)
      animationGroup.timingFunction = easeOut
    }
    
    let scaleAnimation = CABasicAnimation(keyPath: "transform.scale.xy")
    scaleAnimation.fromValue = fromValueForRadius
    scaleAnimation.toValue = 1
    scaleAnimation.duration = animationDuration
    
    let opacityAnimation = CAKeyframeAnimation(keyPath: "opacity")
    opacityAnimation.duration = animationDuration
    opacityAnimation.values = [fromValueForAlpha, 0.45, toValueForAlpha, 0]
    opacityAnimation.keyTimes = [0, keyTimeForHalfOpacity, 0.9, 1]
    opacityAnimation.removedOnCompletion = false
    
    let animations = [scaleAnimation, opacityAnimation]
    
    animationGroup.animations = animations
    
    self.animationGroup = animationGroup
  }
}
