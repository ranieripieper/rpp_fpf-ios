//
//  TeamStatsStaffCell.swift
//  FPF
//
//  Created by Gilson Gil on 3/2/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class TeamStatsStaffCell: UICollectionViewCell {
  private let tableView = UITableView(frame: .zero, style: .Plain)
  private var teamStatsStaffViewModel: TeamStatsStaffViewModel!
  
  required init?(coder aDecoder: NSCoder) {
    teamStatsStaffViewModel = nil
    super.init(coder: aDecoder)
    setUp()
  }
  
  override init(frame: CGRect) {
    teamStatsStaffViewModel = nil
    super.init(frame: frame)
    setUp()
  }
  
  private func setUp() {
    contentView.backgroundColor = UIColor.whiteColor()
    
    tableView.translatesAutoresizingMaskIntoConstraints = false
    tableView.backgroundColor = UIColor.whiteColor()
    tableView.dataSource = self
    tableView.delegate = self
    tableView.estimatedRowHeight = 50
    tableView.separatorStyle = .None
    addSubview(tableView)
    
    let views = ["tableView": tableView]
    
    let horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[tableView]|", options: [], metrics: nil, views: views)
    addConstraints(horizontalConstraints)
    let verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[tableView]|", options: [], metrics: nil, views: views)
    addConstraints(verticalConstraints)
  }
  
  func update(viewModel: TeamStatsStaffViewModel) {
    teamStatsStaffViewModel = viewModel
    viewModel.items.forEach {
      $0.register(tableView)
    }
    tableView.reloadData()
  }
  
  #if DEBUG
  deinit {
    print("deinit", self)
  }
  #endif
}

extension TeamStatsStaffCell: Updatable {
  typealias ViewModel = TeamStatsStaffViewModel
}

extension TeamStatsStaffCell: UITableViewDataSource {
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return teamStatsStaffViewModel?.items.count ?? 0
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let configurator = teamStatsStaffViewModel!.items[indexPath.row]
    let cell = tableView.dequeueReusableCellWithIdentifier(configurator.reuseIdentifier, forIndexPath: indexPath)
    configurator.update(cell)
    if indexPath.row % 2 == 0 {
      cell.contentView.backgroundColor = UIColor.whiteColor()
    } else {
      cell.contentView.backgroundColor = UIColor.fpfLightGrey30Color()
    }
    return cell
  }
}

extension TeamStatsStaffCell: UITableViewDelegate {
  func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    return UITableViewAutomaticDimension
  }
}
