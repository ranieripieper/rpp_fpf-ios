//
//  TeamStatsMatchCell.swift
//  FPF
//
//  Created by Gilson Gil on 3/4/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit
import Kingfisher

final class TeamStatsMatchCell: UITableViewCell {
  private let logoSize: CGFloat = 24
  
  private let homeLogoImageView = UIImageView()
  private let homeScoreLabel = UILabel()
  private let awayLogoImageView = UIImageView()
  private let awayScoreLabel = UILabel()
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setUp()
  }
  
  override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    setUp()
  }
  
  private func setUp() {
    homeLogoImageView.translatesAutoresizingMaskIntoConstraints = false
    homeLogoImageView.contentMode = .ScaleAspectFit
    contentView.addSubview(homeLogoImageView)
    
    homeScoreLabel.translatesAutoresizingMaskIntoConstraints = false
    homeScoreLabel.font = UIFont.fpfMedium(20)
    homeScoreLabel.textColor = UIColor.fpfGreyishColor()
    homeScoreLabel.textAlignment = .Center
    contentView.addSubview(homeScoreLabel)
    
    let xLabel = UILabel()
    xLabel.translatesAutoresizingMaskIntoConstraints = false
    xLabel.font = UIFont.fpfMedium(20)
    xLabel.textColor = UIColor.fpfSunflowerColor()
    xLabel.textAlignment = .Center
    xLabel.text = "X"
    contentView.addSubview(xLabel)
    
    awayLogoImageView.translatesAutoresizingMaskIntoConstraints = false
    awayLogoImageView.contentMode = .ScaleAspectFit
    contentView.addSubview(awayLogoImageView)
    
    awayScoreLabel.translatesAutoresizingMaskIntoConstraints = false
    awayScoreLabel.font = UIFont.fpfMedium(20)
    awayScoreLabel.textColor = UIColor.fpfGreyishColor()
    awayScoreLabel.textAlignment = .Center
    contentView.addSubview(awayScoreLabel)
    
    let metrics = ["logoSize": logoSize]
    let views = ["homeLogoImageView": homeLogoImageView, "homeScoreLabel": homeScoreLabel, "xLabel": xLabel, "awayLogoImageView": awayLogoImageView, "awayScoreLabel": awayScoreLabel]
    var horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[homeLogoImageView(logoSize)]-[homeScoreLabel]-[xLabel]-[awayScoreLabel]-[awayLogoImageView(logoSize)]|", options: [], metrics: metrics, views: views)
    contentView.addConstraints(horizontalConstraints)
    horizontalConstraints = [
      NSLayoutConstraint(item: xLabel, attribute: .CenterX, relatedBy: .Equal, toItem: contentView, attribute: .CenterX, multiplier: 1, constant: 0)
    ]
    contentView.addConstraints(horizontalConstraints)
    var verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|-[homeLogoImageView(logoSize)]-|", options: [], metrics: metrics, views: views)
    contentView.addConstraints(verticalConstraints)
    verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|-[homeScoreLabel]-|", options: [], metrics: nil, views: views)
    contentView.addConstraints(verticalConstraints)
    verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|-[xLabel]-|", options: [], metrics: nil, views: views)
    contentView.addConstraints(verticalConstraints)
    verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|-[awayScoreLabel]-|", options: [], metrics: nil, views: views)
    contentView.addConstraints(verticalConstraints)
    verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|-[awayLogoImageView(logoSize)]-|", options: [], metrics: metrics, views: views)
    contentView.addConstraints(verticalConstraints)
  }
  
  func update(viewModel: TeamStatsMatchViewModel) {
    if let homeURL = viewModel.homeURL {
      homeLogoImageView.kf_setImageWithURL(NSURL(string: homeURL)!)
    }
    homeScoreLabel.text = viewModel.homeScore
    if let awayURL = viewModel.awayURL {
      awayLogoImageView.kf_setImageWithURL(NSURL(string: awayURL)!)
    }
    awayScoreLabel.text = viewModel.awayScore
  }
}

extension TeamStatsMatchCell: Updatable {
  typealias ViewModel = TeamStatsMatchViewModel
}
