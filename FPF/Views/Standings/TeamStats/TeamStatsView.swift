//
//  TeamStatsView.swift
//  FPF
//
//  Created by Gilson Gil on 3/2/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit
import Kingfisher

final class TeamStatsView: UIView {
  private let logoImageView = UIImageView()
  private let nameLabel = UILabel()
  private let pointsCard = UIView()
  private let pointsLabel = UILabel()
  private let homePointsLabel = UILabel()
  private let awayPointsLabel = UILabel()
  private let sectionLabel = UILabel()
  private let collectionViewCard = UIView()
  private let collectionView: UICollectionView!
  private let leftButton = UIButton(type: .Custom)
  private let rightButton = UIButton(type: .Custom)
  
  let teamStatsViewModel: TeamStatsViewModel!
  var delegate: GuideViewDelegate?
  
  required init?(coder aDecoder: NSCoder) {
    let layout = UICollectionViewFlowLayout()
    layout.scrollDirection = .Horizontal
    collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
    teamStatsViewModel = nil
    super.init(coder: aDecoder)
    setUp()
  }
  
  init(teamStatsViewModel: TeamStatsViewModel) {
    let layout = UICollectionViewFlowLayout()
    layout.scrollDirection = .Horizontal
    collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
    self.teamStatsViewModel = teamStatsViewModel
    super.init(frame: .zero)
    setUp()
    logoImageView.kf_setImageWithURL(NSURL(string: teamStatsViewModel.logoURL)!)
    nameLabel.text = teamStatsViewModel.teamName.uppercaseString
    pointsLabel.text = teamStatsViewModel.points
    awayPointsLabel.text = teamStatsViewModel.awayPoints
    homePointsLabel.text = teamStatsViewModel.homePoints
    teamStatsViewModel.items.forEach {
      $0.1.register(collectionView)
    }
    setSectionLabel(0)
    enableButtons(0)
  }
  
  private func setUp() {
    backgroundColor = UIColor.fpfLightestGreyColor()
    
    let headerView = UIView()
    headerView.translatesAutoresizingMaskIntoConstraints = false
    headerView.backgroundColor = UIColor.whiteColor()
    addSubview(headerView)
    
    let headerShadow = UIView()
    headerShadow.translatesAutoresizingMaskIntoConstraints = false
    headerShadow.backgroundColor = UIColor.fpfMediumGreyColor()
    headerView.addSubview(headerShadow)
    
    logoImageView.translatesAutoresizingMaskIntoConstraints = false
    logoImageView.contentMode = .ScaleAspectFit
    headerView.addSubview(logoImageView)
    
    nameLabel.translatesAutoresizingMaskIntoConstraints = false
    nameLabel.font = UIFont.fpfBold(24)
    nameLabel.textColor = UIColor.fpfPinkishGreyColor()
    nameLabel.textAlignment = .Right
    headerView.addSubview(nameLabel)
    
    let connector1 = UIView()
    connector1.translatesAutoresizingMaskIntoConstraints = false
    connector1.backgroundColor = UIColor.fpfStripeGreyColor()
    addSubview(connector1)
    
    pointsCard.translatesAutoresizingMaskIntoConstraints = false
    pointsCard.backgroundColor = UIColor.whiteColor()
    addSubview(pointsCard)
    
    let pointsDescriptionLabel = UILabel()
    pointsDescriptionLabel.translatesAutoresizingMaskIntoConstraints = false
    pointsDescriptionLabel.font = UIFont.fpfBold(18)
    pointsDescriptionLabel.textColor = UIColor.fpfGreyishColor()
    pointsDescriptionLabel.text = Strings.TeamStatsPoints.uppercaseString
    pointsCard.addSubview(pointsDescriptionLabel)
    
    pointsLabel.translatesAutoresizingMaskIntoConstraints = false
    pointsLabel.font = UIFont.fpfMedium(64)
    pointsLabel.textColor = UIColor.fpfSunflowerColor()
    pointsLabel.textAlignment = .Center
    pointsCard.addSubview(pointsLabel)
    
    let separator = UIView()
    separator.translatesAutoresizingMaskIntoConstraints = false
    separator.backgroundColor = UIColor.fpfMediumGreyColor()
    pointsCard.addSubview(separator)
    
    let awayPointsDescriptionLabel = UILabel()
    awayPointsDescriptionLabel.translatesAutoresizingMaskIntoConstraints = false
    awayPointsDescriptionLabel.font = UIFont.fpfRegular(14)
    awayPointsDescriptionLabel.textAlignment = .Center
    awayPointsDescriptionLabel.textColor = UIColor.fpfGreyishColor()
    awayPointsDescriptionLabel.text = Strings.TeamStatsAway
    pointsCard.addSubview(awayPointsDescriptionLabel)
    
    let homePointsDescriptionLabel = UILabel()
    homePointsDescriptionLabel.translatesAutoresizingMaskIntoConstraints = false
    homePointsDescriptionLabel.font = UIFont.fpfRegular(14)
    homePointsDescriptionLabel.textAlignment = .Center
    homePointsDescriptionLabel.textColor = UIColor.fpfGreyishColor()
    homePointsDescriptionLabel.text = Strings.TeamStatsHome
    pointsCard.addSubview(homePointsDescriptionLabel)
    
    awayPointsLabel.translatesAutoresizingMaskIntoConstraints = false
    awayPointsLabel.font = UIFont.fpfMedium(24)
    awayPointsLabel.textColor = UIColor.fpfBlackColor()
    awayPointsLabel.textAlignment = .Center
    pointsCard.addSubview(awayPointsLabel)
    
    homePointsLabel.translatesAutoresizingMaskIntoConstraints = false
    homePointsLabel.font = UIFont.fpfMedium(24)
    homePointsLabel.textColor = UIColor.fpfBlackColor()
    homePointsLabel.textAlignment = .Center
    pointsCard.addSubview(homePointsLabel)
    
    let connector2 = UIView()
    connector2.translatesAutoresizingMaskIntoConstraints = false
    connector2.backgroundColor = UIColor.fpfStripeGreyColor()
    addSubview(connector2)
    
    let connector3 = UIView()
    connector3.translatesAutoresizingMaskIntoConstraints = false
    connector3.backgroundColor = UIColor.fpfStripeGreyColor()
    addSubview(connector3)
    
    collectionViewCard.translatesAutoresizingMaskIntoConstraints = false
    collectionViewCard.backgroundColor = UIColor.whiteColor()
    addSubview(collectionViewCard)
    
    let headerSection = UIView()
    headerSection.translatesAutoresizingMaskIntoConstraints = false
    headerSection.backgroundColor = UIColor.whiteColor()
    collectionViewCard.addSubview(headerSection)
    
    leftButton.translatesAutoresizingMaskIntoConstraints = false
    leftButton.setImage(UIImage.icnLeftArrow(), forState: .Normal)
    leftButton.addTarget(self, action: #selector(TeamStatsView.previousTapped), forControlEvents: .TouchUpInside)
    headerSection.addSubview(leftButton)
    
    sectionLabel.translatesAutoresizingMaskIntoConstraints = false
    sectionLabel.font = UIFont.fpfBold(18)
    sectionLabel.textColor = UIColor.fpfGreyishColor()
    sectionLabel.textAlignment = .Center
    headerSection.addSubview(sectionLabel)
    
    rightButton.translatesAutoresizingMaskIntoConstraints = false
    rightButton.setImage(UIImage.icnRightArrow(), forState: .Normal)
    rightButton.addTarget(self, action: #selector(TeamStatsView.nextTapped), forControlEvents: .TouchUpInside)
    headerSection.addSubview(rightButton)
    
    collectionView.translatesAutoresizingMaskIntoConstraints = false
    collectionView.backgroundColor = UIColor.whiteColor()
    collectionView.dataSource = self
    collectionView.delegate = self
    collectionView.pagingEnabled = true
    collectionView.allowsSelection = false
    collectionView.showsHorizontalScrollIndicator = false
    collectionViewCard.addSubview(collectionView)
    
    let metrics = ["margin": 14, "margin2": 20, "headerSectionHeight": 40, "headerShadowHeight": 2, "logoImageViewSize": 80]
    let views = ["headerView": headerView, "headerShadow": headerShadow, "logoImageView": logoImageView, "nameLabel": nameLabel, "connector1": connector1, "pointsCard": pointsCard, "pointsDescriptionLabel": pointsDescriptionLabel, "pointsLabel": pointsLabel, "separator": separator, "awayPointsDescriptionLabel": awayPointsDescriptionLabel, "awayPointsLabel": awayPointsLabel, "homePointsDescriptionLabel": homePointsDescriptionLabel, "homePointsLabel": homePointsLabel, "connector2": connector2, "connector3": connector3, "headerSection": headerSection, "leftButton": leftButton, "sectionLabel": sectionLabel, "rightButton": rightButton, "collectionViewCard": collectionViewCard, "collectionView": collectionView]
    var horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[headerView]|", options: [], metrics: metrics, views: views)
    addConstraints(horizontalConstraints)
    horizontalConstraints = [
      NSLayoutConstraint(item: connector1, attribute: .CenterX, relatedBy: .Equal, toItem: self, attribute: .CenterX, multiplier: 1, constant: 0),
      NSLayoutConstraint(item: connector1, attribute: .Width, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1, constant: 4)
    ]
    addConstraints(horizontalConstraints)
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-margin-[pointsCard]-margin-|", options: [], metrics: metrics, views: views)
    addConstraints(horizontalConstraints)
    horizontalConstraints = [
      NSLayoutConstraint(item: connector2, attribute: .CenterX, relatedBy: .Equal, toItem: self, attribute: .CenterX, multiplier: 0.5, constant: 0),
      NSLayoutConstraint(item: connector2, attribute: .Width, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1, constant: 4),
      NSLayoutConstraint(item: connector3, attribute: .CenterX, relatedBy: .Equal, toItem: self, attribute: .CenterX, multiplier: 1.5, constant: 0),
      NSLayoutConstraint(item: connector3, attribute: .Width, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1, constant: 4)
    ]
    addConstraints(horizontalConstraints)
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-margin-[collectionViewCard]-margin-|", options: [], metrics: metrics, views: views)
    addConstraints(horizontalConstraints)
    var verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|-64-[headerView][connector1(margin)][pointsCard][connector2(margin)][collectionViewCard]-margin-|", options: [], metrics: metrics, views: views)
    addConstraints(verticalConstraints)
    verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:[pointsCard][connector3(margin)][collectionViewCard]", options: [], metrics: metrics, views: views)
    addConstraints(verticalConstraints)
    
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-margin-[logoImageView(logoImageViewSize)]-margin-[nameLabel]-margin2-|", options: [], metrics: metrics, views: views)
    headerView.addConstraints(horizontalConstraints)
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[headerShadow]|", options: [], metrics: metrics, views: views)
    headerView.addConstraints(horizontalConstraints)
    verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|-margin-[logoImageView(logoImageViewSize)]-margin-[headerShadow(headerShadowHeight)]|", options: [], metrics: metrics, views: views)
    headerView.addConstraints(verticalConstraints)
    verticalConstraints = [NSLayoutConstraint(item: nameLabel, attribute: .CenterY, relatedBy: .Equal, toItem: headerView, attribute: .CenterY, multiplier: 1, constant: 0)]
    headerView.addConstraints(verticalConstraints)
    
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-margin2-[pointsDescriptionLabel]", options: [], metrics: metrics, views: views)
    pointsCard.addConstraints(horizontalConstraints)
    horizontalConstraints = [
      NSLayoutConstraint(item: pointsDescriptionLabel, attribute: .Left, relatedBy: .Equal, toItem: pointsLabel, attribute: .Left, multiplier: 1, constant: 0),
      NSLayoutConstraint(item: pointsDescriptionLabel, attribute: .Right, relatedBy: .Equal, toItem: pointsLabel, attribute: .Right, multiplier: 1, constant: 0)
    ]
    pointsCard.addConstraints(horizontalConstraints)
    horizontalConstraints = [
      NSLayoutConstraint(item: separator, attribute: .CenterX, relatedBy: .Equal, toItem: pointsCard, attribute: .CenterX, multiplier: 3/4, constant: 0),
      NSLayoutConstraint(item: separator, attribute: .Width, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1, constant: 1)
    ]
    pointsCard.addConstraints(horizontalConstraints)
    horizontalConstraints = [
      NSLayoutConstraint(item: homePointsDescriptionLabel, attribute: .Left, relatedBy: .Equal, toItem: pointsCard, attribute: .CenterX, multiplier: 1, constant: 0),
      NSLayoutConstraint(item: homePointsDescriptionLabel, attribute: .Right, relatedBy: .Equal, toItem: pointsCard, attribute: .CenterX, multiplier: 1.5, constant: -20),
      NSLayoutConstraint(item: awayPointsDescriptionLabel, attribute: .Left, relatedBy: .Equal, toItem: pointsCard, attribute: .CenterX, multiplier: 1.5, constant: 0),
      NSLayoutConstraint(item: awayPointsDescriptionLabel, attribute: .Right, relatedBy: .Equal, toItem: pointsCard, attribute: .Right, multiplier: 1, constant: -20),
      NSLayoutConstraint(item: homePointsDescriptionLabel, attribute: .CenterX, relatedBy: .Equal, toItem: homePointsLabel, attribute: .CenterX, multiplier: 1, constant: 0),
      NSLayoutConstraint(item: awayPointsDescriptionLabel, attribute: .CenterX, relatedBy: .Equal, toItem: awayPointsLabel, attribute: .CenterX, multiplier: 1, constant: 0)
    ]
    pointsCard.addConstraints(horizontalConstraints)
    verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|-margin-[pointsDescriptionLabel]-margin-[pointsLabel]-margin-|", options: [], metrics: metrics, views: views)
    pointsCard.addConstraints(verticalConstraints)
    verticalConstraints = [
      NSLayoutConstraint(item: pointsLabel, attribute: .Top, relatedBy: .Equal, toItem: separator, attribute: .Top, multiplier: 1, constant: 0),
      NSLayoutConstraint(item: pointsLabel, attribute: .Bottom, relatedBy: .Equal, toItem: separator, attribute: .Bottom, multiplier: 1, constant: 0)
    ]
    pointsCard.addConstraints(verticalConstraints)
    verticalConstraints = [
      NSLayoutConstraint(item: homePointsDescriptionLabel, attribute: .Top, relatedBy: .Equal, toItem: separator, attribute: .Top, multiplier: 1, constant: 0),
      NSLayoutConstraint(item: awayPointsDescriptionLabel, attribute: .Top, relatedBy: .Equal, toItem: homePointsDescriptionLabel, attribute: .Top, multiplier: 1, constant: 0),
      NSLayoutConstraint(item: homePointsLabel, attribute: .Top, relatedBy: .Equal, toItem: homePointsDescriptionLabel, attribute: .Bottom, multiplier: 1, constant: 14),
      NSLayoutConstraint(item: awayPointsLabel, attribute: .Top, relatedBy: .Equal, toItem: awayPointsDescriptionLabel, attribute: .Bottom, multiplier: 1, constant: 14)
    ]
    pointsCard.addConstraints(verticalConstraints)
    
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-[leftButton(30)]-[sectionLabel]-[rightButton(30)]-|", options: [], metrics: metrics, views: views)
    headerSection.addConstraints(horizontalConstraints)
    verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[leftButton]|", options: [], metrics: metrics, views: views)
    headerSection.addConstraints(verticalConstraints)
    verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[sectionLabel]|", options: [], metrics: metrics, views: views)
    headerSection.addConstraints(verticalConstraints)
    verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[rightButton]|", options: [], metrics: metrics, views: views)
    headerSection.addConstraints(verticalConstraints)
    
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[headerSection]|", options: [], metrics: metrics, views: views)
    collectionViewCard.addConstraints(horizontalConstraints)
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[collectionView]|", options: [], metrics: metrics, views: views)
    collectionViewCard.addConstraints(horizontalConstraints)
    verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[headerSection(headerSectionHeight)][collectionView]|", options: [], metrics: metrics, views: views)
    collectionViewCard.addConstraints(verticalConstraints)
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    pointsCard.applyShadow()
    collectionViewCard.applyShadow()
  }
  
  func previousTapped() {
    let currentPage = Int(collectionView.contentOffset.x / collectionView.bounds.width + 0.5)
    let nextPage = max(0, currentPage - 1)
    collectionView.setContentOffset(CGPoint(x: CGFloat(nextPage) * collectionView.bounds.width, y: 0), animated: true)
  }
  
  func nextTapped() {
    let currentPage = Int(collectionView.contentOffset.x / collectionView.bounds.width + 0.5)
    let nextPage = min(teamStatsViewModel.items.count - 1, currentPage + 1)
    collectionView.setContentOffset(CGPoint(x: CGFloat(nextPage) * collectionView.bounds.width, y: 0), animated: true)
  }
  
  func enableButtons(page: Int) {
    if page == 0 {
      leftButton.enabled = false
      leftButton.alpha = 0.5
    } else {
      leftButton.enabled = true
      leftButton.alpha = 1
    }
    if page == teamStatsViewModel.items.count - 1 {
      rightButton.enabled = false
      rightButton.alpha = 0.5
    } else {
      rightButton.enabled = true
      rightButton.alpha = 1
    }
  }
  
  func setSectionLabel(page: Int?) {
    let currentPage: Int
    if let page = page {
      currentPage = page
    } else {
      currentPage = Int(collectionView.contentOffset.x / collectionView.bounds.width + 0.5)
    }
    sectionLabel.text = teamStatsViewModel.items[currentPage].0.uppercaseString
    enableButtons(currentPage)
  }
}

extension TeamStatsView: UICollectionViewDataSource {
  func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return teamStatsViewModel.items.count ?? 0
  }
  
  func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
    let configurator = teamStatsViewModel.items[indexPath.row]
    let cell = collectionView.dequeueReusableCellWithReuseIdentifier(configurator.1.reuseIdentifier, forIndexPath: indexPath)
    configurator.1.update(cell)
    return cell
  }
}

extension TeamStatsView: UICollectionViewDelegateFlowLayout {
  func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
    return collectionView.bounds.size
  }
  
  func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
    return UIEdgeInsetsZero
  }
  
  func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
    return 0
  }
  
  func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
    return 0
  }
}

extension TeamStatsView: UIScrollViewDelegate {
  func scrollViewDidScroll(scrollView: UIScrollView) {
    if collectionView == scrollView {
      setSectionLabel(nil)
    }
  }
}
