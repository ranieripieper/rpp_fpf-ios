//
//  TeamStatsGoalsAgainstCell.swift
//  FPF
//
//  Created by Gilson Gil on 3/2/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

class TeamStatsGoalsAgainstCell: UICollectionViewCell {
  private let tableView = UITableView(frame: .zero, style: .Plain)
  private let goalsLabel = UILabel()
  
  private var viewModel: TeamStatsGoalsAgainstViewModel?
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setUp()
  }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    setUp()
  }
  
  private func setUp() {
    contentView.backgroundColor = UIColor.whiteColor()
    
    tableView.translatesAutoresizingMaskIntoConstraints = false
    tableView.backgroundColor = UIColor.whiteColor()
    tableView.showsVerticalScrollIndicator = false
    tableView.estimatedRowHeight = 40
    tableView.separatorStyle = .None
    tableView.allowsSelection = false
    tableView.dataSource = self
    tableView.delegate = self
    contentView.addSubview(tableView)
    
    let goalsDescriptionLabel = UILabel()
    goalsDescriptionLabel.translatesAutoresizingMaskIntoConstraints = false
    goalsDescriptionLabel.font = UIFont.fpfRegular(14)
    goalsDescriptionLabel.textColor = UIColor.fpfGreyishColor()
    goalsDescriptionLabel.text = Strings.TeamStatsTotalGoals
    contentView.addSubview(goalsDescriptionLabel)
    
    goalsLabel.translatesAutoresizingMaskIntoConstraints = false
    goalsLabel.font = UIFont.fpfMedium(48)
    goalsLabel.textColor = UIColor.fpfSunflowerColor()
    goalsLabel.text = "0"
    goalsLabel.setContentHuggingPriority(UILayoutPriorityRequired, forAxis: .Vertical)
    contentView.addSubview(goalsLabel)
    
    let metrics = ["margin": 14]
    let views = ["tableView": tableView, "goalsDescriptionLabel": goalsDescriptionLabel, "goalsLabel": goalsLabel]
    var horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[tableView]|", options: [], metrics: metrics, views: views)
    contentView.addConstraints(horizontalConstraints)
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:[goalsDescriptionLabel]-margin-[goalsLabel]-margin-|", options: [], metrics: metrics, views: views)
    contentView.addConstraints(horizontalConstraints)
    var verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[tableView]-[goalsLabel]-|", options: [], metrics: metrics, views: views)
    contentView.addConstraints(verticalConstraints)
    verticalConstraints = [
      NSLayoutConstraint(item: goalsLabel, attribute: .CenterY, relatedBy: .Equal, toItem: goalsDescriptionLabel, attribute: .CenterY, multiplier: 1, constant: 0)
    ]
    contentView.addConstraints(verticalConstraints)
  }
  
  func update(viewModel: TeamStatsGoalsAgainstViewModel) {
    goalsLabel.text = viewModel.total
    self.viewModel = viewModel
    viewModel.items.forEach {
      $0.register(tableView)
    }
    tableView.reloadData()
  }
}

extension TeamStatsGoalsAgainstCell: UITableViewDataSource {
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return viewModel?.items.count ?? 0
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    guard let configurator = viewModel?.items[indexPath.row] else {
      return UITableViewCell()
    }
    let cell = tableView.dequeueReusableCellWithIdentifier(configurator.reuseIdentifier, forIndexPath: indexPath)
    configurator.update(cell)
    if indexPath.row % 2 == 0 {
      cell.contentView.backgroundColor = UIColor.whiteColor()
    } else {
      cell.contentView.backgroundColor = UIColor.fpfLightGrey30Color()
    }
    return cell
  }
}

extension TeamStatsGoalsAgainstCell: UITableViewDelegate {
  func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    return UITableViewAutomaticDimension
  }
}

extension TeamStatsGoalsAgainstCell: Updatable {
  typealias ViewModel = TeamStatsGoalsAgainstViewModel
}
