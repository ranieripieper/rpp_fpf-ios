//
//  TeamStatsViewController.swift
//  FPF
//
//  Created by Gilson Gil on 3/2/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class TeamStatsViewController: UIViewController {
  private let teamStatsView: TeamStatsView!
  
  required init?(coder aDecoder: NSCoder) {
    teamStatsView = nil
    super.init(coder: aDecoder)
  }
  
  init(teamStatsViewModel: TeamStatsViewModel) {
    teamStatsView = TeamStatsView(teamStatsViewModel: teamStatsViewModel)
    super.init(nibName: nil, bundle: nil)
  }
  
  override func loadView() {
    view = teamStatsView
  }
  
  #if DEBUG
  deinit {
    print("deinit", self)
  }
  #endif
}
