//
//  TeamStatsMatchesCell.swift
//  FPF
//
//  Created by Gilson Gil on 3/2/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class TeamStatsMatchesCell: UICollectionViewCell {
  private let underlineHeight: CGFloat = 1
  
  private let homeMatchesLabel = UILabel()
  private let awayMatchesLabel = UILabel()
  private let homeTableView = UITableView(frame: .zero, style: .Plain)
  private let awayTableView = UITableView(frame: .zero, style: .Plain)
  
  private var teamStatsMatchesViewModel: TeamStatsMatchesViewModel?
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setUp()
  }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    setUp()
  }
  
  private func setUp() {
    contentView.backgroundColor = UIColor.whiteColor()
    
    let homeMatchesDescriptionLabel = UILabel()
    homeMatchesDescriptionLabel.translatesAutoresizingMaskIntoConstraints = false
    homeMatchesDescriptionLabel.font = UIFont.fpfRegular(14)
    homeMatchesDescriptionLabel.textColor = UIColor.fpfGreyishColor()
    homeMatchesDescriptionLabel.text = Strings.TeamStatsHome
    contentView.addSubview(homeMatchesDescriptionLabel)
    
    homeMatchesLabel.translatesAutoresizingMaskIntoConstraints = false
    homeMatchesLabel.font = UIFont.fpfMedium(24)
    homeMatchesLabel.textColor = UIColor.fpfBlackColor()
    contentView.addSubview(homeMatchesLabel)
    
    let homeUnderline = UIView()
    homeUnderline.translatesAutoresizingMaskIntoConstraints = false
    homeUnderline.backgroundColor = UIColor.fpfMediumGreyColor()
    contentView.addSubview(homeUnderline)
    
    let awayMatchesDescriptionLabel = UILabel()
    awayMatchesDescriptionLabel.translatesAutoresizingMaskIntoConstraints = false
    awayMatchesDescriptionLabel.font = UIFont.fpfRegular(14)
    awayMatchesDescriptionLabel.textColor = UIColor.fpfGreyishColor()
    awayMatchesDescriptionLabel.text = Strings.TeamStatsAway
    contentView.addSubview(awayMatchesDescriptionLabel)
    
    awayMatchesLabel.translatesAutoresizingMaskIntoConstraints = false
    awayMatchesLabel.font = UIFont.fpfMedium(24)
    awayMatchesLabel.textColor = UIColor.fpfBlackColor()
    contentView.addSubview(awayMatchesLabel)
    
    let awayUnderline = UIView()
    awayUnderline.translatesAutoresizingMaskIntoConstraints = false
    awayUnderline.backgroundColor = UIColor.fpfMediumGreyColor()
    contentView.addSubview(awayUnderline)
    
    homeTableView.translatesAutoresizingMaskIntoConstraints = false
    homeTableView.backgroundColor = UIColor.whiteColor()
    homeTableView.showsVerticalScrollIndicator = false
    homeTableView.estimatedRowHeight = 40
    homeTableView.dataSource = self
    homeTableView.delegate = self
    homeTableView.separatorStyle = .None
    homeTableView.allowsSelection = false
    contentView.addSubview(homeTableView)
    
    awayTableView.translatesAutoresizingMaskIntoConstraints = false
    awayTableView.backgroundColor = UIColor.whiteColor()
    awayTableView.showsVerticalScrollIndicator = false
    awayTableView.estimatedRowHeight = 40
    awayTableView.dataSource = self
    awayTableView.delegate = self
    awayTableView.separatorStyle = .None
    awayTableView.allowsSelection = false
    contentView.addSubview(awayTableView)
    
    let metrics = ["margin": 14, "margin2": 4, "underlineHeight": underlineHeight]
    let views = ["homeMatchesDescriptionLabel": homeMatchesDescriptionLabel, "homeMatchesLabel": homeMatchesLabel, "homeUnderline": homeUnderline, "homeTableView": homeTableView, "awayMatchesDescriptionLabel": awayMatchesDescriptionLabel, "awayMatchesLabel": awayMatchesLabel, "awayUnderline": awayUnderline, "awayTableView": awayTableView]
    let horizontalConstraints = [
      NSLayoutConstraint(item: homeUnderline, attribute: .Width, relatedBy: .Equal, toItem: contentView, attribute: .Width, multiplier: 0.25, constant: 0),
      NSLayoutConstraint(item: homeUnderline, attribute: .CenterX, relatedBy: .Equal, toItem: contentView, attribute: .CenterX, multiplier: 0.5, constant: 0),
      NSLayoutConstraint(item: awayUnderline, attribute: .Width, relatedBy: .Equal, toItem: contentView, attribute: .Width, multiplier: 0.25, constant: 0),
      NSLayoutConstraint(item: awayUnderline, attribute: .CenterX, relatedBy: .Equal, toItem: contentView, attribute: .CenterX, multiplier: 1.5, constant: 0),
      NSLayoutConstraint(item: homeUnderline, attribute: .CenterX, relatedBy: .Equal, toItem: homeMatchesLabel, attribute: .CenterX, multiplier: 1, constant: 0),
      NSLayoutConstraint(item: homeUnderline, attribute: .CenterX, relatedBy: .Equal, toItem: homeMatchesDescriptionLabel, attribute: .CenterX, multiplier: 1, constant: 0),
      NSLayoutConstraint(item: awayUnderline, attribute: .CenterX, relatedBy: .Equal, toItem: awayMatchesLabel, attribute: .CenterX, multiplier: 1, constant: 0),
      NSLayoutConstraint(item: awayUnderline, attribute: .CenterX, relatedBy: .Equal, toItem: awayMatchesDescriptionLabel, attribute: .CenterX, multiplier: 1, constant: 0),
      NSLayoutConstraint(item: homeTableView, attribute: .CenterX, relatedBy: .Equal, toItem: homeMatchesLabel, attribute: .CenterX, multiplier: 1, constant: 0),
      NSLayoutConstraint(item: awayTableView, attribute: .CenterX, relatedBy: .Equal, toItem: awayMatchesLabel, attribute: .CenterX, multiplier: 1, constant: 0),
      NSLayoutConstraint(item: homeTableView, attribute: .Width, relatedBy: .Equal, toItem: contentView, attribute: .Width, multiplier: 0.4, constant: 0),
      NSLayoutConstraint(item: awayTableView, attribute: .Width, relatedBy: .Equal, toItem: contentView, attribute: .Width, multiplier: 0.4, constant: 0)
    ]
    contentView.addConstraints(horizontalConstraints)
    var verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|-margin-[homeMatchesDescriptionLabel]-margin2-[homeMatchesLabel]-margin2-[homeUnderline(underlineHeight)][homeTableView]|", options: [], metrics: metrics, views: views)
    contentView.addConstraints(verticalConstraints)
    verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|-margin-[awayMatchesDescriptionLabel]-margin2-[awayMatchesLabel]-margin2-[awayUnderline(underlineHeight)][awayTableView]|", options: [], metrics: metrics, views: views)
    contentView.addConstraints(verticalConstraints)
  }
  
  func update(viewModel: TeamStatsMatchesViewModel) {
    self.teamStatsMatchesViewModel = viewModel
    homeMatchesLabel.text = viewModel.homeMatchesCount
    awayMatchesLabel.text = viewModel.awayMatchesCount
    viewModel.homeMatches.forEach {
      $0.register(homeTableView)
    }
    homeTableView.reloadData()
    viewModel.awayMatches.forEach {
      $0.register(awayTableView)
    }
    awayTableView.reloadData()
  }
}

extension TeamStatsMatchesCell: UITableViewDataSource {
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    switch tableView {
    case homeTableView:
      return teamStatsMatchesViewModel?.homeMatches.count ?? 0
    case awayTableView:
      return teamStatsMatchesViewModel?.awayMatches.count ?? 0
    default:
      return 0
    }
  }

  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let configurator: CellConfiguratorType?
    switch tableView {
    case homeTableView:
      configurator = teamStatsMatchesViewModel?.homeMatches[indexPath.row]
    case awayTableView:
      configurator = teamStatsMatchesViewModel?.awayMatches[indexPath.row]
    default:
      configurator = nil
    }
    guard let cellConfigurator = configurator else {
      return UITableViewCell()
    }
    let cell = tableView.dequeueReusableCellWithIdentifier(cellConfigurator.reuseIdentifier, forIndexPath: indexPath)
    cellConfigurator.update(cell)
    return cell
  }
}

extension TeamStatsMatchesCell: UITableViewDelegate {
  func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    return UITableViewAutomaticDimension
  }
}

extension TeamStatsMatchesCell: Updatable {
  typealias ViewModel = TeamStatsMatchesViewModel
}
