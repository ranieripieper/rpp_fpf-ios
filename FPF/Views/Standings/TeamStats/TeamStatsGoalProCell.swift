//
//  TeamStatsGoalProCell.swift
//  FPF
//
//  Created by Gilson Gil on 3/2/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class TeamStatsGoalProCell: UITableViewCell {
  private let nameLabel = UILabel()
  private let totalLabel = UILabel()
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setUp()
  }
  
  override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    setUp()
  }
  
  private func setUp() {
    nameLabel.translatesAutoresizingMaskIntoConstraints = false
    nameLabel.font = UIFont.fpfRegular(18)
    nameLabel.textColor = UIColor.fpfGreyishColor()
    contentView.addSubview(nameLabel)
    
    totalLabel.translatesAutoresizingMaskIntoConstraints = false
    totalLabel.font = UIFont.fpfMedium(16)
    totalLabel.textColor = UIColor.fpfGreyishColor()
    contentView.addSubview(totalLabel)
    
    let ballImage = UIImage.icnSoccerBall()
    let ballImageView = UIImageView(image: ballImage)
    ballImageView.translatesAutoresizingMaskIntoConstraints = false
    ballImageView.contentMode = .ScaleAspectFit
    contentView.addSubview(ballImageView)
    
    let metrics = ["margin": 26]
    let views = ["nameLabel": nameLabel, "totalLabel": totalLabel, "ballImageView": ballImageView]
    let horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-margin-[nameLabel]-[totalLabel]-[ballImageView]-|", options: [], metrics: metrics, views: views)
    contentView.addConstraints(horizontalConstraints)
    var verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|-[nameLabel]-|", options: [], metrics: metrics, views: views)
    contentView.addConstraints(verticalConstraints)
    verticalConstraints = [
      NSLayoutConstraint(item: nameLabel, attribute: .CenterY, relatedBy: .Equal, toItem: totalLabel, attribute: .CenterY, multiplier: 1, constant: 0),
      NSLayoutConstraint(item: totalLabel, attribute: .CenterY, relatedBy: .Equal, toItem: ballImageView, attribute: .CenterY, multiplier: 1, constant: 0)
    ]
    contentView.addConstraints(verticalConstraints)
  }
  
  func update(viewModel: TeamStatsGoalsProCellViewModel) {
    nameLabel.text = viewModel.name
    totalLabel.text = viewModel.goals
  }
}

extension TeamStatsGoalProCell: Updatable {
  typealias ViewModel = TeamStatsGoalsProCellViewModel
}
