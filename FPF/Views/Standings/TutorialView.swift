//
//  TutorialView.swift
//  FPF
//
//  Created by Gilson Gil on 3/4/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

protocol TutorialViewDelegate {
  func dismiss()
}

final class TutorialView: UIView {
  private let circleFrame: CGRect!
  
  var delegate: TutorialViewDelegate?
  
  required init?(coder aDecoder: NSCoder) {
    circleFrame = nil
    super.init(coder: aDecoder)
    setUp()
  }
  
  init(circleFrame: CGRect) {
    self.circleFrame = circleFrame
    super.init(frame: .zero)
    setUp()
  }
  
  private func setUp() {
    backgroundColor = UIColor.clearColor()
    
    let textBox = UIView()
    textBox.translatesAutoresizingMaskIntoConstraints = false
    textBox.backgroundColor = UIColor.clearColor()
    addSubview(textBox)
    
    let textBackgroundView = UIView()
    textBackgroundView.translatesAutoresizingMaskIntoConstraints = false
    textBackgroundView.backgroundColor = UIColor(white: 0, alpha: 0.8)
    textBox.addSubview(textBackgroundView)
    
    let textLabel = UILabel()
    textLabel.translatesAutoresizingMaskIntoConstraints = false
    textLabel.font = UIFont.fpfMedium(12)
    textLabel.text = Strings.TutorialText
    textLabel.textAlignment = .Center
    textLabel.textColor = UIColor.whiteColor()
    textLabel.numberOfLines = 0
    textBox.addSubview(textLabel)
    
    let dismissButton = UIButton(type: .Custom)
    dismissButton.translatesAutoresizingMaskIntoConstraints = false
    dismissButton.backgroundColor = UIColor.clearColor()
    dismissButton.addTarget(self, action: #selector(TutorialView.dismissTapped), forControlEvents: .TouchUpInside)
    addSubview(dismissButton)
    
    let metrics = ["margin": 14, "margin2": 44]
    let views = ["textBox": textBox, "textBackgroundView": textBackgroundView, "textLabel": textLabel]
    var horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-margin-[textBox]-margin-|", options: [], metrics: metrics, views: views)
    addConstraints(horizontalConstraints)
    var verticalConstraints = [
      NSLayoutConstraint(item: textBox, attribute: .CenterY, relatedBy: .Equal, toItem: self, attribute: .CenterY, multiplier: 1, constant: 0)
    ]
    addConstraints(verticalConstraints)
    horizontalConstraints = [
      NSLayoutConstraint(item: dismissButton, attribute: .Left, relatedBy: .Equal, toItem: self, attribute: .Left, multiplier: 1, constant: circleFrame.origin.x),
      NSLayoutConstraint(item: dismissButton, attribute: .Width, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1, constant: circleFrame.width)
    ]
    addConstraints(horizontalConstraints)
    verticalConstraints = [
      NSLayoutConstraint(item: dismissButton, attribute: .Top, relatedBy: .Equal, toItem: self, attribute: .Top, multiplier: 1, constant: circleFrame.origin.y),
      NSLayoutConstraint(item: dismissButton, attribute: .Height, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1, constant: circleFrame.height)
    ]
    addConstraints(verticalConstraints)
    
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[textBackgroundView]|", options: [], metrics: metrics, views: views)
    textBox.addConstraints(horizontalConstraints)
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[textLabel]|", options: [], metrics: metrics, views: views)
    textBox.addConstraints(horizontalConstraints)
    verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[textBackgroundView]|", options: [], metrics: metrics, views: views)
    textBox.addConstraints(verticalConstraints)
    verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|-margin2-[textLabel]-margin2-|", options: [], metrics: metrics, views: views)
    textBox.addConstraints(verticalConstraints)
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    startAnimating()
  }
  
  func dismissTapped() {
    UIView.animateWithDuration(Constants.defaultAnimationDuration, animations: { [weak self] in
      self?.alpha = 0
      }, completion: { [weak self] _ in
        self?.delegate?.dismiss()
    })
  }
  
  func startAnimating() {
    let halo = PulsingHaloLayer(layerNumber: 5)
    halo.frame = circleFrame
    layer.addSublayer(halo)
  }
}
