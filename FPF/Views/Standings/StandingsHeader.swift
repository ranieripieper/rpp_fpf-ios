//
//  StandingsHeader.swift
//  FPF
//
//  Created by Gilson Gil on 1/14/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class StandingsHeader: UITableViewHeaderFooterView {
  private let rightLabel = UILabel()
  private var rightLabelHeightConstraint: NSLayoutConstraint!
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setUp()
  }
  
  override init(reuseIdentifier: String?) {
    super.init(reuseIdentifier: reuseIdentifier)
    setUp()
  }
  
  private func setUp() {
    contentView.translatesAutoresizingMaskIntoConstraints = false
    contentView.backgroundColor = UIColor.fpfDarkRedColor()
    
    rightLabel.translatesAutoresizingMaskIntoConstraints = false
    rightLabel.textColor = UIColor.whiteColor()
    rightLabel.font = UIFont.fpfBold(14)
    rightLabel.setContentCompressionResistancePriority(UILayoutPriorityDefaultLow, forAxis: .Vertical)
    contentView.addSubview(rightLabel)
    
    let legendView = UIView()
    legendView.translatesAutoresizingMaskIntoConstraints = false
    legendView.backgroundColor = UIColor.fpfLightGreyColor()
    contentView.addSubview(legendView)
    
    let sg = legendLabel("SG")
    legendView.addSubview(sg)
    let d = legendLabel("D")
    legendView.addSubview(d)
    let e = legendLabel("E")
    legendView.addSubview(e)
    let v = legendLabel("V")
    legendView.addSubview(v)
    let j = legendLabel("J")
    legendView.addSubview(j)
    let p = legendLabel("P")
    p.textColor = UIColor.fpfCementColor()
    legendView.addSubview(p)
    
    let metrics = ["margin": Constants.defaultMargin, "width": 30]
    let views = ["rightLabel": rightLabel, "legendView": legendView, "sg": sg, "d": d, "e": e, "v": v, "j": j, "p": p]
    var horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:[rightLabel]-margin-|", options: [], metrics: metrics, views: views)
    var verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[rightLabel][legendView]|", options: [], metrics: metrics, views: views)
    contentView.addConstraints(horizontalConstraints)
    contentView.addConstraints(verticalConstraints)
    
    rightLabelHeightConstraint = NSLayoutConstraint(item: rightLabel, attribute: .Height, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1, constant: 0)
    rightLabel.addConstraint(rightLabelHeightConstraint)
    
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[legendView]|", options: [], metrics: metrics, views: views)
    contentView.addConstraints(horizontalConstraints)
    
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:[p(width)][j(width)][v(width)][e(width)][d(width)][sg(width)]|", options: [], metrics: metrics, views: views)
    legendView.addConstraints(horizontalConstraints)
    [sg, d, e, v, j, p].forEach { label in
      verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[label]|", options: [], metrics: metrics, views: ["label": label])
      legendView.addConstraints(verticalConstraints)
    }
    
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[contentView]|", options: [], metrics: nil, views: ["contentView": contentView])
    addConstraints(horizontalConstraints)
    verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[contentView]|", options: [], metrics: nil, views: ["contentView": contentView])
    addConstraints(verticalConstraints)
  }
  
  private func legendLabel(text: String) -> UILabel {
    let label = UILabel()
    label.translatesAutoresizingMaskIntoConstraints = false
    label.textColor = UIColor.fpfPinkishGreyColor()
    label.textAlignment = .Center
    label.font = UIFont.fpfBold(14)
    label.text = text
    return label
  }
  
  func configureWithText(text: String) {
    rightLabel.text = text
    if text.characters.count > 0 {
      rightLabelHeightConstraint.constant = 30
    } else {
      rightLabelHeightConstraint.constant = 0
    }
  }
  
  #if DEBUG
  deinit {
    print("deinit", self)
  }
  #endif
}

extension StandingsHeader: ReuseIdentifier {
  static func reuseIdentifier() -> String {
    return "StandingsHeader"
  }
}
