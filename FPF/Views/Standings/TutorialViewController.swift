//
//  TutorialViewController.swift
//  FPF
//
//  Created by Gilson Gil on 3/4/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class TutorialViewController: UIViewController {
  private let tutorialView: TutorialView!
  
  required init?(coder aDecoder: NSCoder) {
    tutorialView = nil
    super.init(coder: aDecoder)
  }
  
  init(circleFrame: CGRect) {
    tutorialView = TutorialView(circleFrame: circleFrame)
    super.init(nibName: nil, bundle: nil)
    modalPresentationStyle = .OverFullScreen
    tutorialView.delegate = self
  }
  
  override func loadView() {
    view = tutorialView
  }
  
  #if DEBUG
  deinit {
    print("deinit", self)
  }
  #endif
}

extension TutorialViewController: TutorialViewDelegate {
  func dismiss() {
    dismissViewControllerAnimated(false, completion: nil)
  }
}
