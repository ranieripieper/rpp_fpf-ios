//
//  StandingsView.swift
//  FPF
//
//  Created by Gilson Gil on 1/8/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit
import AKPickerView_Swift

protocol StandingsViewDelegate {
  func goToTeamStats(teamId: Int)
  func presentTutorial(frame: CGRect)
}

final class StandingsView: UIView {
  private let pickerViewHeight: CGFloat = 50
  private let pickerView = AKPickerView(frame: .zero)
  private let collectionView = UICollectionView(frame: .zero, collectionViewLayout: UICollectionViewLayout())
  
  private var collectionViewTopConstraint: NSLayoutConstraint?
  
  var standingsViewModel: StandingsViewModel? {
    didSet {
      dispatch_async(dispatch_get_main_queue()) { [weak self] in
        self?.collectionView.reloadData()
        self?.pickerView.reloadData()
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, Int64(NSEC_PER_SEC)), dispatch_get_main_queue()) {
          if let cell = self?.collectionView.cellForItemAtIndexPath(NSIndexPath(forItem: 0, inSection: 0)) as? StandingPhaseCell, frame = cell.logoFrame {
            self?.delegate?.presentTutorial(frame)
          }
        }
      }
    }
  }
  var delegate: StandingsViewDelegate?
  
  weak var scrollDelegate: LeagueScrollDelegate?
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setUp()
  }
  
  init() {
    super.init(frame: .zero)
    setUp()
  }
  
  private func setUp() {
    translatesAutoresizingMaskIntoConstraints = false
    backgroundColor = UIColor.clearColor()
    
    pickerView.translatesAutoresizingMaskIntoConstraints = false
    pickerView.backgroundColor = UIColor.fpfLightGreyColor()
    pickerView.font = UIFont.fpfBold(18)!
    pickerView.textColor = UIColor.fpfCementColor()
    pickerView.highlightedTextColor = UIColor.fpfCementColor()
    pickerView.dataSource = self
    pickerView.delegate = self
    addSubview(pickerView)
    
    let collectionViewLayout = UICollectionViewFlowLayout()
    collectionViewLayout.scrollDirection = .Horizontal
    collectionViewLayout.minimumInteritemSpacing = 0
    collectionView.collectionViewLayout = collectionViewLayout
    collectionView.translatesAutoresizingMaskIntoConstraints = false
    collectionView.dataSource = self
    collectionView.delegate = self
    collectionView.registerClass(StandingPhaseCell.classForCoder(), forCellWithReuseIdentifier: StandingPhaseCell.reuseIdentifier())
    collectionView.pagingEnabled = true
    collectionView.backgroundColor = UIColor.clearColor()
    collectionView.allowsSelection = false
    collectionView.scrollsToTop = false
    addSubview(collectionView)
    
    let metrics = ["pickerViewHeight": pickerViewHeight]
    let views = ["pickerView": pickerView, "collectionView": collectionView]
    let horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[pickerView]|", options: [], metrics: metrics, views: views)
    let horizontalConstraints2 = NSLayoutConstraint.constraintsWithVisualFormat("H:|[collectionView]|", options: [], metrics: metrics, views: views)
    var verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[pickerView(pickerViewHeight)]", options: [], metrics: metrics, views: views)
    addConstraints(horizontalConstraints)
    addConstraints(horizontalConstraints2)
    addConstraints(verticalConstraints)
    verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:[collectionView]|", options: [], metrics: metrics, views: views)
    addConstraints(verticalConstraints)
    collectionViewTopConstraint = NSLayoutConstraint(item: collectionView, attribute: .Top, relatedBy: .Equal, toItem: self, attribute: .Top, multiplier: 1, constant: 0)
    addConstraint(collectionViewTopConstraint!)
  }
  
  #if DEBUG
  deinit {
    print("deinit", self)
  }
  #endif
}

extension StandingsView: AKPickerViewDataSource {
  func numberOfItemsInPickerView(pickerView: AKPickerView) -> Int {
    if let count = standingsViewModel?.phases.count where count > 1 {
      pickerView.hidden = false
      collectionViewTopConstraint?.constant = pickerViewHeight
      return count
    }
    pickerView.hidden = true
    collectionViewTopConstraint?.constant = 0
    return 0
  }
  
  func pickerView(pickerView: AKPickerView, titleForItem item: Int) -> String {
    return standingsViewModel?.phases[item].name ?? ""
  }
}

extension StandingsView: AKPickerViewDelegate {
  func pickerView(pickerView: AKPickerView, didSelectItem item: Int) {
    collectionView.scrollToItemAtIndexPath(NSIndexPath(forItem: item, inSection: 0), atScrollPosition: .CenteredHorizontally, animated: true)
  }
  
  func pickerView(pickerView: AKPickerView, marginForItem item: Int) -> CGSize {
    return CGSize(width: 40, height: 0)
  }
}

extension StandingsView: UICollectionViewDataSource {
  func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return standingsViewModel?.phases.count ?? 0
  }
  
  func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
    guard let viewModel = standingsViewModel?.phases[indexPath.item] else {
      return UICollectionViewCell()
    }
    let cell = collectionView.dequeueReusableCellWithReuseIdentifier(StandingPhaseCell.reuseIdentifier(), forIndexPath: indexPath) as! StandingPhaseCell
    cell.configureWithStandingPhaseCellViewModel(viewModel)
    cell.delegate = self
    cell.scrollDelegate = scrollDelegate
    return cell
  }
}

extension StandingsView: UICollectionViewDelegateFlowLayout {
  func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
    return 0
  }
  
  func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
    return collectionView.bounds.size
  }
}

extension StandingsView: UIScrollViewDelegate {
  func scrollViewDidScroll(scrollView: UIScrollView) {
    guard scrollView == collectionView && scrollView.dragging else {
      return
    }
    let index = Int(scrollView.contentOffset.x / scrollView.bounds.width + 0.5)
    if index != pickerView.selectedItem {
      pickerView.selectItem(index, animated: true)
    }
  }
}

extension StandingsView: StandingPhaseCellDelegate {
  func didScroll(scrollView: UIScrollView) {
    pickerView.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, 0, max(0, -scrollView.contentOffset.y))
  }
  
  func goToTeamStats(teamId: Int) {
    delegate?.goToTeamStats(teamId)
  }
}

extension StandingsView: LeagueScrollProtocol {}
