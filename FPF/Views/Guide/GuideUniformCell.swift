//
//  GuideUniformCell.swift
//  FPF
//
//  Created by Gilson Gil on 2/29/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit
import Kingfisher

final class GuideUniformCell: UICollectionViewCell {
  let uniformImageView = UIImageView()
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setUp()
  }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    setUp()
  }
  
  private func setUp() {
    contentView.backgroundColor = UIColor.clearColor()
    
    uniformImageView.translatesAutoresizingMaskIntoConstraints = false
    uniformImageView.contentMode = .ScaleAspectFill
    contentView.addSubview(uniformImageView)
    
    let metrics = ["margin": 8]
    let views = ["uniformImageView": uniformImageView]
    let horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-margin-[uniformImageView]-margin-|", options: [], metrics: metrics, views: views)
    contentView.addConstraints(horizontalConstraints)
    let verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|-margin-[uniformImageView]-margin-|", options: [], metrics: metrics, views: views)
    contentView.addConstraints(verticalConstraints)
  }
  
  func update(viewModel: GuideUniformCellViewModel) {
    uniformImageView.kf_setImageWithURL(NSURL(string: viewModel.uniformURL)!)
  }
}

extension GuideUniformCell: Updatable {
  typealias ViewModel = GuideUniformCellViewModel
}
