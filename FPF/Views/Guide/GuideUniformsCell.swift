//
//  GuideUniformsCell.swift
//  FPF
//
//  Created by Gilson Gil on 2/29/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class GuideUniformsCell: UITableViewCell {
  private let card = UIView()
  private let collectionView: UICollectionView
  private let pageControl = UIPageControl()
  private let uniformMakerLabel = UILabel()
  
  private var uniformURLs: [CellConfigurator<GuideUniformCell>] = []
  
  required init?(coder aDecoder: NSCoder) {
    let layout = UICollectionViewFlowLayout()
    layout.scrollDirection = .Horizontal
    collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
    super.init(coder: aDecoder)
    setUp()
  }
  
  override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
    let layout = UICollectionViewFlowLayout()
    layout.scrollDirection = .Horizontal
    collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    setUp()
  }
  
  private func setUp() {
    contentView.backgroundColor = UIColor.fpfLightestGreyColor()
    selectionStyle = .None
    
    card.translatesAutoresizingMaskIntoConstraints = false
    card.backgroundColor = UIColor.whiteColor()
    contentView.addSubview(card)
    
    collectionView.translatesAutoresizingMaskIntoConstraints = false
    collectionView.dataSource = self
    collectionView.delegate = self
    collectionView.backgroundColor = UIColor.clearColor()
    collectionView.pagingEnabled = true
    collectionView.showsHorizontalScrollIndicator = false
    collectionView.showsVerticalScrollIndicator = false
    card.addSubview(collectionView)
    
    pageControl.translatesAutoresizingMaskIntoConstraints = false
    pageControl.hidesForSinglePage = true
    pageControl.currentPageIndicatorTintColor = UIColor.fpfCementColor()
    pageControl.pageIndicatorTintColor = UIColor.fpfStripeGreyColor()
    card.addSubview(pageControl)
    
    let uniformMakerDescriptionLabel = UILabel()
    uniformMakerDescriptionLabel.translatesAutoresizingMaskIntoConstraints = false
    uniformMakerDescriptionLabel.font = UIFont.fpfBold(18)
    uniformMakerDescriptionLabel.textColor = UIColor.fpfGreyishColor()
    uniformMakerDescriptionLabel.textAlignment = .Right
    uniformMakerDescriptionLabel.text = Strings.GuideUniformMaker
    uniformMakerDescriptionLabel.setContentHuggingPriority(UILayoutPriorityRequired, forAxis: .Horizontal)
    uniformMakerDescriptionLabel.setContentCompressionResistancePriority(UILayoutPriorityRequired, forAxis: .Horizontal)
    card.addSubview(uniformMakerDescriptionLabel)
    
    uniformMakerLabel.translatesAutoresizingMaskIntoConstraints = false
    uniformMakerLabel.font = UIFont.fpfMedium(14)
    uniformMakerLabel.textColor = UIColor.fpfGreyishColor()
    uniformMakerLabel.textAlignment = .Right
    card.addSubview(uniformMakerLabel)
    
    let metrics = ["cardMargin": 14, "margin": 12, "rMargin": 22]
    let views = ["contentView": contentView, "card": card, "collectionView": collectionView, "pageControl": pageControl, "uniformMakerDescriptionLabel": uniformMakerDescriptionLabel, "uniformMakerLabel": uniformMakerLabel]
    var horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-cardMargin-[card]-cardMargin-|", options: [], metrics: metrics, views: views)
    contentView.addConstraints(horizontalConstraints)
    var verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[card]|", options: [], metrics: metrics, views: views)
    contentView.addConstraints(verticalConstraints)
    
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-margin-[collectionView]-margin-[uniformMakerDescriptionLabel]-rMargin-|", options: [], metrics: metrics, views: views)
    card.addConstraints(horizontalConstraints)
    horizontalConstraints = [
      NSLayoutConstraint(item: collectionView, attribute: .Left, relatedBy: .Equal, toItem: pageControl, attribute: .Left, multiplier: 1, constant: 0),
      NSLayoutConstraint(item: collectionView, attribute: .Right, relatedBy: .Equal, toItem: pageControl, attribute: .Right, multiplier: 1, constant: 0),
      NSLayoutConstraint(item: uniformMakerDescriptionLabel, attribute: .Left, relatedBy: .Equal, toItem: uniformMakerLabel, attribute: .Left, multiplier: 1, constant: 0),
      NSLayoutConstraint(item: uniformMakerDescriptionLabel, attribute: .Right, relatedBy: .Equal, toItem: uniformMakerLabel, attribute: .Right, multiplier: 1, constant: 0)
    ]
    card.addConstraints(horizontalConstraints)
    verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[collectionView]|", options: [], metrics: metrics, views: views)
    card.addConstraints(verticalConstraints)
    verticalConstraints = [NSLayoutConstraint(item: collectionView, attribute: .Width, relatedBy: .Equal, toItem: collectionView, attribute: .Height, multiplier: 1.1, constant: 0)]
    collectionView.addConstraints(verticalConstraints)
    verticalConstraints = [NSLayoutConstraint(item: collectionView, attribute: .Bottom, relatedBy: .Equal, toItem: pageControl, attribute: .Bottom, multiplier: 1, constant: 0)]
    card.addConstraints(verticalConstraints)
    verticalConstraints = [
      NSLayoutConstraint(item: uniformMakerDescriptionLabel, attribute: .Bottom, relatedBy: .Equal, toItem: card, attribute: .CenterY, multiplier: 1, constant: 0),
      NSLayoutConstraint(item: uniformMakerLabel, attribute: .Top, relatedBy: .Equal, toItem: card, attribute: .CenterY, multiplier: 1, constant: 0)
    ]
    card.addConstraints(verticalConstraints)
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    card.applyShadow()
  }
  
  func update(viewModel: GuideUniformViewModel) {
    uniformURLs = viewModel.uniformsURLs.flatMap {
      if let uniformViewModel = GuideUniformCellViewModel(uniformURL: $0) {
        let cellConfigurator = CellConfigurator<GuideUniformCell>(viewModel: uniformViewModel)
        self.collectionView.registerClass(cellConfigurator.cellClass, forCellWithReuseIdentifier: cellConfigurator.reuseIdentifier)
        return cellConfigurator
      } else {
        return nil
      }
    }
    collectionView.reloadData()
    pageControl.numberOfPages = uniformURLs.count
    uniformMakerLabel.text = viewModel.uniformMaker
  }
}

extension GuideUniformsCell: UICollectionViewDataSource {
  func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return uniformURLs.count
  }
  
  func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
    let configurator = uniformURLs[indexPath.item]
    let cell = collectionView.dequeueReusableCellWithReuseIdentifier(configurator.reuseIdentifier, forIndexPath: indexPath) as! GuideUniformCell
    configurator.update(cell)
    return cell
  }
}

extension GuideUniformsCell: UICollectionViewDelegateFlowLayout {
  func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAtIndexPath indexPath: NSIndexPath) -> CGSize {
    return collectionView.bounds.size
  }
  
  func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAtIndex section: Int) -> UIEdgeInsets {
    return UIEdgeInsetsZero
  }
  
  func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
    return 0
  }
  
  func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
    return 0
  }
}

extension GuideUniformsCell: UIScrollViewDelegate {
  func scrollViewDidScroll(scrollView: UIScrollView) {
    let page = Int(scrollView.contentOffset.x + 0.5)
    pageControl.currentPage = page
  }
}

extension GuideUniformsCell: Updatable {
  typealias ViewModel = GuideUniformViewModel
}

extension GuideUniformsCell: RealTimeUpdatable {
  func update(percentage: CGFloat) {
    let factor = -max(0, min(1, (percentage - 0.5) / 0.5))
    collectionView.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, factor * collectionView.bounds.width, 0)
  }
}
