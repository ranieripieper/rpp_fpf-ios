//
//  GuideStaffCell.swift
//  FPF
//
//  Created by Gilson Gil on 3/1/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class GuideStaffCell: UITableViewCell {
  private let card = UIView()
  private let descriptionLabel = UILabel()
  private let managerLabel = UILabel()
  private let managerBirthLabel = UILabel()
  private let disclosureIndicatorImageView = UIImageView()
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setUp()
  }
  
  override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    setUp()
  }
  
  private func setUp() {
    contentView.backgroundColor = UIColor.fpfLightestGreyColor()
    selectionStyle = .None
    
    let connector = UIView()
    connector.translatesAutoresizingMaskIntoConstraints = false
    connector.backgroundColor = UIColor.fpfStripeGreyColor()
    contentView.addSubview(connector)
    
    card.translatesAutoresizingMaskIntoConstraints = false
    card.backgroundColor = UIColor.whiteColor()
    contentView.addSubview(card)
    
    let container = UIView()
    container.translatesAutoresizingMaskIntoConstraints = false
    container.backgroundColor = UIColor.whiteColor()
    card.addSubview(container)
    
    descriptionLabel.translatesAutoresizingMaskIntoConstraints = false
    descriptionLabel.font = UIFont.fpfBold(18)
    descriptionLabel.textColor = UIColor.fpfGreyishColor()
    container.addSubview(descriptionLabel)
    
    managerLabel.translatesAutoresizingMaskIntoConstraints = false
    managerLabel.font = UIFont.fpfMedium(16)
    managerLabel.textColor = UIColor.fpfBlackColor()
    managerLabel.setContentHuggingPriority(UILayoutPriorityRequired, forAxis: .Vertical)
    container.addSubview(managerLabel)
    
    managerBirthLabel.translatesAutoresizingMaskIntoConstraints = false
    managerBirthLabel.font = UIFont.fpfMedium(12)
    managerBirthLabel.textColor = UIColor.fpfGreyishColor()
    container.addSubview(managerBirthLabel)
    
    disclosureIndicatorImageView.translatesAutoresizingMaskIntoConstraints = false
    card.addSubview(disclosureIndicatorImageView)
    
    let metrics = ["cardMargin": 14, "margin": 22, "vMargin": 30]
    let views = ["contentView": contentView, "connector": connector, "card": card, "container": container, "descriptionLabel": descriptionLabel, "managerLabel": managerLabel, "managerBirthLabel": managerBirthLabel, "disclosureIndicatorImageView": disclosureIndicatorImageView]
    var horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-cardMargin-[card]-cardMargin-|", options: [], metrics: metrics, views: views)
    contentView.addConstraints(horizontalConstraints)
    var verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[connector(cardMargin)][card]|", options: [], metrics: metrics, views: views)
    contentView.addConstraints(verticalConstraints)
    horizontalConstraints = [
      NSLayoutConstraint(item: connector, attribute: .CenterX, relatedBy: .Equal, toItem: contentView, attribute: .CenterX, multiplier: 1, constant: 0),
      NSLayoutConstraint(item: connector, attribute: .Width, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1, constant: 4)
    ]
    contentView.addConstraints(horizontalConstraints)
    
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-margin-[container]-margin-|", options: [], metrics: metrics, views: views)
    card.addConstraints(horizontalConstraints)
    verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|-vMargin-[container]-vMargin-|", options: [], metrics: metrics, views: views)
    card.addConstraints(verticalConstraints)
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[descriptionLabel]|", options: [], metrics: metrics, views: views)
    container.addConstraints(horizontalConstraints)
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[managerLabel]-margin-[managerBirthLabel]|", options: [], metrics: metrics, views: views)
    container.addConstraints(horizontalConstraints)
    verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[descriptionLabel][managerLabel]|", options: [], metrics: metrics, views: views)
    container.addConstraints(verticalConstraints)
    verticalConstraints = [
      NSLayoutConstraint(item: managerLabel, attribute: .Top, relatedBy: .Equal, toItem: managerBirthLabel, attribute: .Top, multiplier: 1, constant: 0),
      NSLayoutConstraint(item: managerLabel, attribute: .Bottom, relatedBy: .Equal, toItem: managerBirthLabel, attribute: .Bottom, multiplier: 1, constant: 0)
    ]
    container.addConstraints(verticalConstraints)
    
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:[disclosureIndicatorImageView]-margin-|", options: [], metrics: metrics, views: views)
    card.addConstraints(horizontalConstraints)
    verticalConstraints = [NSLayoutConstraint(item: disclosureIndicatorImageView, attribute: .CenterY, relatedBy: .Equal, toItem: card, attribute: .CenterY, multiplier: 1, constant: 0)]
    card.addConstraints(verticalConstraints)
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    card.applyShadow()
  }
  
  func update(viewModel: GuideStaffViewModel) {
    if viewModel.staff.count == 1 {
      descriptionLabel.text = Strings.GuideManager
      managerLabel.text = viewModel.staff.first?.name
      managerBirthLabel.text = viewModel.staff.first?.birthDate
      disclosureIndicatorImageView.image = nil
      disclosureIndicatorImageView.hidden = true
    } else {
      descriptionLabel.text = Strings.GuideStaff
      managerLabel.text = nil
      managerBirthLabel.text = nil
      disclosureIndicatorImageView.image = UIImage.icnArrow()
      disclosureIndicatorImageView.hidden = false
    }
  }
}

extension GuideStaffCell: Updatable {
  typealias ViewModel = GuideStaffViewModel
}
