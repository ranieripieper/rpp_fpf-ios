//
//  StaffCell.swift
//  FPF
//
//  Created by Gilson Gil on 3/2/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class StaffCell: UITableViewCell {
  private let nameLabel = UILabel()
  private let birthDateLabel = UILabel()
  private let positionLabel = UILabel()
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setUp()
  }
  
  override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    setUp()
  }
  
  private func setUp() {
    contentView.backgroundColor = UIColor.fpfLightestGreyColor()
    selectionStyle = .None
    
    nameLabel.translatesAutoresizingMaskIntoConstraints = false
    nameLabel.font = UIFont.fpfMedium(16)
    nameLabel.textColor = UIColor.fpfBlackColor()
    contentView.addSubview(nameLabel)
    
    birthDateLabel.translatesAutoresizingMaskIntoConstraints = false
    birthDateLabel.font = UIFont.fpfMedium(12)
    birthDateLabel.textColor = UIColor.fpfGreyishColor()
    contentView.addSubview(birthDateLabel)
    
    positionLabel.translatesAutoresizingMaskIntoConstraints = false
    positionLabel.font = UIFont.fpfMedium(18)
    positionLabel.textColor = UIColor.fpfGreyishColor()
    positionLabel.textAlignment = .Right
    positionLabel.setContentHuggingPriority(UILayoutPriorityRequired, forAxis: .Horizontal)
    positionLabel.setContentCompressionResistancePriority(UILayoutPriorityRequired, forAxis: .Horizontal)
    contentView.addSubview(positionLabel)
    
    let metrics = ["margin": 4, "largeMargin": 20]
    let views = ["nameLabel": nameLabel, "birthDateLabel": birthDateLabel, "positionLabel": positionLabel]
    var horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-[nameLabel]-[positionLabel]-largeMargin-|", options: [], metrics: metrics, views: views)
    contentView.addConstraints(horizontalConstraints)
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-[birthDateLabel]-[positionLabel]", options: [], metrics: metrics, views: views)
    contentView.addConstraints(horizontalConstraints)
    var verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|-[nameLabel]-margin-[birthDateLabel]-|", options: [], metrics: metrics, views: views)
    contentView.addConstraints(verticalConstraints)
    verticalConstraints = [NSLayoutConstraint(item: positionLabel, attribute: .CenterY, relatedBy: .Equal, toItem: contentView, attribute: .CenterY, multiplier: 1, constant: 0)]
    contentView.addConstraints(verticalConstraints)
  }
  
  func update(viewModel: StaffCellViewModel) {
    nameLabel.text = viewModel.name
    birthDateLabel.text = viewModel.birthDate
    positionLabel.text = viewModel.position
  }
}

extension StaffCell: Updatable {
  typealias ViewModel = StaffCellViewModel
}
