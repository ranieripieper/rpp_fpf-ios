//
//  StaffView.swift
//  FPF
//
//  Created by Gilson Gil on 3/2/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class StaffView: UIView {
  private let tableView = UITableView(frame: .zero, style: .Plain)
  private let staffViewModel: StaffViewModel!
  
  override init(frame: CGRect) {
    staffViewModel = nil
    super.init(frame: frame)
    setUp()
  }
  
  required init?(coder aDecoder: NSCoder) {
    staffViewModel = nil
    super.init(coder: aDecoder)
    setUp()
  }
  
  init(staffViewModel: StaffViewModel) {
    self.staffViewModel = staffViewModel
    super.init(frame: .zero)
    setUp()
    staffViewModel.staff.forEach { $0.register(self.tableView) }
  }
  
  private func setUp() {
    backgroundColor = UIColor.fpfLightGreyColor()

    let margin = Constants.defaultMargin
    
    tableView.translatesAutoresizingMaskIntoConstraints = false
    tableView.backgroundColor = UIColor.clearColor()
    tableView.dataSource = self
    tableView.delegate = self
    tableView.estimatedRowHeight = 50
    tableView.separatorStyle = .None
    tableView.clipsToBounds = false
    tableView.scrollIndicatorInsets = UIEdgeInsets(top: -margin, left: -margin, bottom: -margin, right: -margin)
    addSubview(tableView)
    
    let metrics = ["margin": margin]
    let views = ["tableView": tableView]
    
    let horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-margin-[tableView]-margin-|", options: [], metrics: metrics, views: views)
    addConstraints(horizontalConstraints)
    let verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|-margin-[tableView]-margin-|", options: [], metrics: metrics, views: views)
    addConstraints(verticalConstraints)
  }
  
  #if DEBUG
  deinit {
    print("deinit", self)
  }
  #endif
}

extension StaffView: UITableViewDataSource {
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return staffViewModel?.staff.count ?? 0
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let configurator = staffViewModel!.staff[indexPath.row]
    let cell = tableView.dequeueReusableCellWithIdentifier(configurator.reuseIdentifier, forIndexPath: indexPath)
    configurator.update(cell)
    if indexPath.row % 2 == 0 {
      cell.contentView.backgroundColor = UIColor.whiteColor()
    } else {
      cell.contentView.backgroundColor = UIColor.fpfLightGrey30Color()
    }
    return cell
  }
}

extension StaffView: UITableViewDelegate {
  func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    return UITableViewAutomaticDimension
  }
}
