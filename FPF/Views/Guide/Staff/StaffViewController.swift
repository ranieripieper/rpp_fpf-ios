//
//  StaffViewController.swift
//  FPF
//
//  Created by Gilson Gil on 3/2/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class StaffViewController: UIViewController {
  private let staffView: StaffView!
  
  required init?(coder aDecoder: NSCoder) {
    staffView = nil
    super.init(coder: aDecoder)
  }
  
  init(staffViewModel: StaffViewModel) {
    staffView = StaffView(staffViewModel: staffViewModel)
    super.init(nibName: nil, bundle: nil)
  }
  
  override func loadView() {
    view = staffView
  }
  
  #if DEBUG
  deinit {
    print("deinit", self)
  }
  #endif
}
