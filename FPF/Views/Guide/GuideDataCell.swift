//
//  GuideDataCell.swift
//  FPF
//
//  Created by Gilson Gil on 3/1/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class GuideDataCell: UITableViewCell {
  private let card = UIView()
  private let areaLabel = UILabel()
  private let populationLabel = UILabel()
  private let densityLabel = UILabel()
  private let idhmLabel = UILabel()
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setUp()
  }
  
  override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    setUp()
  }
  
  private func setUp() {
    contentView.backgroundColor = UIColor.fpfLightestGreyColor()
    selectionStyle = .None
    
    let connector = UIView()
    connector.translatesAutoresizingMaskIntoConstraints = false
    connector.backgroundColor = UIColor.fpfStripeGreyColor()
    contentView.addSubview(connector)
    
    card.translatesAutoresizingMaskIntoConstraints = false
    card.backgroundColor = UIColor.whiteColor()
    contentView.addSubview(card)
    
    let areaDescriptionLabel = UILabel()
    areaDescriptionLabel.translatesAutoresizingMaskIntoConstraints = false
    areaDescriptionLabel.font = UIFont.fpfBold(14)
    areaDescriptionLabel.textColor = UIColor.fpfGreyishColor()
    areaDescriptionLabel.text = Strings.GuideArea
    areaDescriptionLabel.setContentHuggingPriority(UILayoutPriorityRequired, forAxis: .Horizontal)
    areaDescriptionLabel.minimumScaleFactor = 0.7
    areaDescriptionLabel.adjustsFontSizeToFitWidth = true
    card.addSubview(areaDescriptionLabel)
    
    areaLabel.translatesAutoresizingMaskIntoConstraints = false
    areaLabel.font = UIFont.fpfMedium(18)
    areaLabel.textColor = UIColor.fpfPinkishGreyColor()
    areaLabel.minimumScaleFactor = 0.7
    areaLabel.adjustsFontSizeToFitWidth = true
    card.addSubview(areaLabel)
    
    let populationDescriptionLabel = UILabel()
    populationDescriptionLabel.translatesAutoresizingMaskIntoConstraints = false
    populationDescriptionLabel.font = UIFont.fpfBold(14)
    populationDescriptionLabel.textColor = UIColor.fpfGreyishColor()
    populationDescriptionLabel.text = Strings.GuidePopulation
    populationDescriptionLabel.setContentHuggingPriority(UILayoutPriorityRequired, forAxis: .Horizontal)
    populationDescriptionLabel.minimumScaleFactor = 0.7
    populationDescriptionLabel.adjustsFontSizeToFitWidth = true
    card.addSubview(populationDescriptionLabel)
    
    populationLabel.translatesAutoresizingMaskIntoConstraints = false
    populationLabel.font = UIFont.fpfMedium(18)
    populationLabel.textColor = UIColor.fpfPinkishGreyColor()
    populationLabel.minimumScaleFactor = 0.7
    populationLabel.adjustsFontSizeToFitWidth = true
    card.addSubview(populationLabel)
    
    let densityDescriptionLabel = UILabel()
    densityDescriptionLabel.translatesAutoresizingMaskIntoConstraints = false
    densityDescriptionLabel.font = UIFont.fpfBold(14)
    densityDescriptionLabel.textColor = UIColor.fpfGreyishColor()
    densityDescriptionLabel.text = Strings.GuideDensity
    densityDescriptionLabel.setContentHuggingPriority(UILayoutPriorityRequired, forAxis: .Horizontal)
    densityDescriptionLabel.minimumScaleFactor = 0.7
    densityDescriptionLabel.adjustsFontSizeToFitWidth = true
    card.addSubview(densityDescriptionLabel)
    
    densityLabel.translatesAutoresizingMaskIntoConstraints = false
    densityLabel.font = UIFont.fpfMedium(18)
    densityLabel.textColor = UIColor.fpfPinkishGreyColor()
    densityLabel.minimumScaleFactor = 0.5
    densityLabel.adjustsFontSizeToFitWidth = true
    card.addSubview(densityLabel)
    
    let idhmDescriptionLabel = UILabel()
    idhmDescriptionLabel.translatesAutoresizingMaskIntoConstraints = false
    idhmDescriptionLabel.font = UIFont.fpfBold(14)
    idhmDescriptionLabel.textColor = UIColor.fpfGreyishColor()
    idhmDescriptionLabel.text = Strings.GuideIDHM
    idhmDescriptionLabel.setContentHuggingPriority(UILayoutPriorityRequired, forAxis: .Horizontal)
    idhmDescriptionLabel.setContentCompressionResistancePriority(UILayoutPriorityRequired, forAxis: .Horizontal)
    card.addSubview(idhmDescriptionLabel)
    
    idhmLabel.translatesAutoresizingMaskIntoConstraints = false
    idhmLabel.font = UIFont.fpfMedium(18)
    idhmLabel.textColor = UIColor.fpfPinkishGreyColor()
    idhmLabel.numberOfLines = 0
    idhmLabel.minimumScaleFactor = 0.5
    idhmLabel.adjustsFontSizeToFitWidth = true
    card.addSubview(idhmLabel)
    
    let metrics = ["cardMargin": 14, "margin": 22, "margin2": 8]
    let views = ["connector": connector, "card": card, "areaDescriptionLabel": areaDescriptionLabel, "areaLabel": areaLabel, "populationDescriptionLabel": populationDescriptionLabel, "populationLabel": populationLabel, "densityDescriptionLabel": densityDescriptionLabel, "densityLabel": densityLabel, "idhmDescriptionLabel": idhmDescriptionLabel, "idhmLabel": idhmLabel]
    var horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-cardMargin-[card]-cardMargin-|", options: [], metrics: metrics, views: views)
    contentView.addConstraints(horizontalConstraints)
    var verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[connector(cardMargin)][card]-cardMargin-|", options: [], metrics: metrics, views: views)
    contentView.addConstraints(verticalConstraints)
    horizontalConstraints = [
      NSLayoutConstraint(item: connector, attribute: .CenterX, relatedBy: .Equal, toItem: contentView, attribute: .CenterX, multiplier: 1, constant: 0),
      NSLayoutConstraint(item: connector, attribute: .Width, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1, constant: 6)
    ]
    contentView.addConstraints(horizontalConstraints)
    
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-margin-[areaDescriptionLabel]-margin2-[areaLabel]-margin-|", options: [], metrics: metrics, views: views)
    card.addConstraints(horizontalConstraints)
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-margin-[populationDescriptionLabel]-margin2-[populationLabel]-margin-|", options: [], metrics: metrics, views: views)
    card.addConstraints(horizontalConstraints)
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-margin-[densityDescriptionLabel]-margin2-[densityLabel]-margin-|", options: [], metrics: metrics, views: views)
    card.addConstraints(horizontalConstraints)
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-margin-[idhmDescriptionLabel]-margin2-[idhmLabel]-margin-|", options: [], metrics: metrics, views: views)
    card.addConstraints(horizontalConstraints)
    verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|-margin-[areaLabel]-margin2-[populationLabel]-margin2-[densityLabel]-margin2-[idhmLabel]-margin-|", options: [], metrics: metrics, views: views)
    card.addConstraints(verticalConstraints)
    verticalConstraints = [
      NSLayoutConstraint(item: areaDescriptionLabel, attribute: .CenterY, relatedBy: .Equal, toItem: areaLabel, attribute: .CenterY, multiplier: 1, constant: 0),
      NSLayoutConstraint(item: populationDescriptionLabel, attribute: .CenterY, relatedBy: .Equal, toItem: populationLabel, attribute: .CenterY, multiplier: 1, constant: 0),
      NSLayoutConstraint(item: densityDescriptionLabel, attribute: .CenterY, relatedBy: .Equal, toItem: densityLabel, attribute: .CenterY, multiplier: 1, constant: 0),
      NSLayoutConstraint(item: idhmDescriptionLabel, attribute: .CenterY, relatedBy: .Equal, toItem: idhmLabel, attribute: .CenterY, multiplier: 1, constant: 0)
    ]
    card.addConstraints(verticalConstraints)
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    card.applyShadow()
  }
  
  func update(viewModel: GuideDataViewModel) {
    areaLabel.text = viewModel.area
    populationLabel.text = viewModel.population
    densityLabel.text = viewModel.density
    idhmLabel.text = viewModel.idhm
  }
}

extension GuideDataCell: Updatable {
  typealias ViewModel = GuideDataViewModel
}
