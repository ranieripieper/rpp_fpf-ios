//
//  GuideHomeView.swift
//  FPF
//
//  Created by Gilson Gil on 2/25/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

protocol GuideHomeViewDelegate {
  func guideHomeView(guideHomeView: GuideHomeView, didSelectGuide guide: Guide)
}

final class GuideHomeView: UIView {
  private let searchBarPadding: CGFloat = 20
  private let searchBarHeight: CGFloat = 40
  
  private let topView = UIView()
  private let searchBarContainer = UIView()
  private let searchBar = UISearchBar()
  private let tableView = UITableView(frame: .zero, style: .Plain)

  private var keyboardHeight: CGFloat = 0
  
  private var firstLoad = true
  
  var guideHomeViewModel: GuideHomeViewModel? {
    didSet {
      dispatch_async(dispatch_get_main_queue()) { [weak self] in
        self?.tableView.reloadData()
        self?.searchBar.hidden = self?.guideHomeViewModel?.teams.count == 0
        if let weakSelf = self where weakSelf.firstLoad {
          weakSelf.firstLoad = false
          weakSelf.tableView.contentOffset = CGPoint(x: 0, y: -weakSelf.tableView.contentInset.top)
        }
      }
    }
  }
  var delegate: GuideHomeViewDelegate?
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    setUp()
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setUp()
  }
  
  convenience init() {
    self.init(frame: .zero)
  }
  
  private func setUp() {
    backgroundColor = UIColor.whiteColor()
    
    let guideImage = UIImage.imgGuide()
    let guideImageView = UIImageView(image: guideImage)
    guideImageView.frame = CGRect(x: 0, y: 40, width: UIScreen.mainScreen().bounds.width, height: 60)
    guideImageView.contentMode = .Center
    topView.addSubview(guideImageView)
    
    let guideLabel = UILabel(frame: CGRect(x: 0, y: 140, width: UIScreen.mainScreen().bounds.width, height: 37))
    guideLabel.text = Strings.GuideAbout
    guideLabel.textColor = UIColor.fpfGreyishColor()
    guideLabel.textAlignment = .Center
    guideLabel.font = UIFont.fpfRegular(14)
    guideLabel.numberOfLines = 0
    topView.addSubview(guideLabel)
    
    searchBarContainer.frame = CGRect(x: 0, y: 0, width: UIScreen.mainScreen().bounds.width, height: searchBarHeight + searchBarPadding * 2 + 187)
    searchBarContainer.backgroundColor = UIColor.whiteColor()
    
    topView.translatesAutoresizingMaskIntoConstraints = false
    topView.frame = CGRect(x: 0, y: 0, width: UIScreen.mainScreen().bounds.width, height: 187)
    topView.backgroundColor = UIColor.whiteColor()
    searchBarContainer.addSubview(topView)
    
    searchBar.frame = CGRect(x: searchBarPadding, y: 187 + searchBarPadding, width: UIScreen.mainScreen().bounds.width - searchBarPadding * 2, height: searchBarHeight)
    searchBar.placeholder = Strings.GuideSearchBarPlaceholder
    searchBar.tintColor = UIColor.fpfGreyishColor()
    searchBar.barTintColor = UIColor.fpfLightGreyColor()
    searchBar.layer.cornerRadius = 8
    searchBar.layer.borderWidth = 1
    searchBar.layer.borderColor = UIColor.fpfMediumGreyColor().CGColor
    searchBar.clipsToBounds = true
    searchBar.delegate = self
    for subview in searchBar.subviews {
      for subSubview in subview.subviews {
        if let textField = subSubview as? UITextField {
          textField.backgroundColor = UIColor.clearColor()
          textField.textColor = UIColor.fpfPinkishGreyColor()
          textField.font = UIFont.fpfItalic(14)
          continue
        }
      }
    }
    searchBarContainer.addSubview(searchBar)
    
    tableView.translatesAutoresizingMaskIntoConstraints = false
    tableView.backgroundColor = UIColor.clearColor()
    tableView.dataSource = self
    tableView.delegate = self
    tableView.estimatedRowHeight = UITableViewAutomaticDimension
    tableView.rowHeight = 64
    tableView.separatorStyle = .None
    tableView.registerClass(GuideHomeCell.classForCoder(), forCellReuseIdentifier: GuideHomeCell.reuseIdentifier())
    tableView.tableHeaderView = searchBarContainer
    tableView.keyboardDismissMode = .Interactive
    addSubview(tableView)
    
    let metrics = ["margin": 40, "smallMargin": 10, "navBarHeight": 64]
    let views = ["topView": topView, "tableView": tableView]
    [tableView].forEach {
      let horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[view]|", options: [], metrics: nil, views: ["view": $0])
      addConstraints(horizontalConstraints)
    }
    let verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[tableView]|", options: [], metrics: metrics, views: views)
    addConstraints(verticalConstraints)
  }
  
  func keyboardWillShow(notification: NSNotification) {
    if let userInfo = notification.userInfo, rectValue = userInfo[UIKeyboardFrameEndUserInfoKey], rect = rectValue.CGRectValue {
      keyboardHeight = rect.height
      UIView.animateWithDuration(Constants.defaultAnimationDuration, animations: { [weak self] in
        if let weakSelf = self {
          weakSelf.topView.alpha = 0
          weakSelf.searchBarContainer.frame = CGRect(x: 0, y: 0, width: UIScreen.mainScreen().bounds.width, height: weakSelf.searchBarHeight + weakSelf.searchBarPadding * 2)
          weakSelf.searchBar.frame = CGRect(x: weakSelf.searchBarPadding, y: weakSelf.searchBarPadding, width: UIScreen.mainScreen().bounds.width - weakSelf.searchBarPadding * 2, height: weakSelf.searchBarHeight)
        }
        }, completion: { [weak self] _ in
          if let weakSelf = self {
            weakSelf.tableView.tableHeaderView = weakSelf.searchBarContainer
          }
      })
    }
    guideHomeViewModel?.filteredTeams = []
    tableView.reloadData()
  }
  
  func keyboardWillHide(notification: NSNotification) {
    searchBarContainer.frame = CGRect(x: 0, y: 0, width: UIScreen.mainScreen().bounds.width, height: searchBarHeight + searchBarPadding * 2 + 187)
    searchBar.frame = CGRect(x: searchBarPadding, y: 187 + searchBarPadding, width: UIScreen.mainScreen().bounds.width - searchBarPadding * 2, height: searchBarHeight)
    tableView.tableHeaderView = searchBarContainer
    UIView.animateWithDuration(Constants.defaultAnimationDuration) { [weak self] in
      if let weakSelf = self {
        weakSelf.topView.alpha = 1
      }
    }
    guideHomeViewModel?.filteredTeams = guideHomeViewModel!.teams
    searchBar.text = ""
    tableView.reloadData()
  }
  
  #if DEBUG
  deinit {
    print("deinit", self)
  }
  #endif
}

extension GuideHomeView: UITableViewDataSource {
  func numberOfSectionsInTableView(tableView: UITableView) -> Int {
    return guideHomeViewModel?.filteredTeams.count ?? 0
  }
  
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return guideHomeViewModel?.filteredTeams[section].1.count ?? 0
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCellWithIdentifier(GuideHomeCell.reuseIdentifier(), forIndexPath: indexPath) as! GuideHomeCell
    cell.configureWithGuideHomeCellViewModel(guideHomeViewModel!.filteredTeams[indexPath.section].1[indexPath.row])
    return cell
  }
}

extension GuideHomeView: UITableViewDelegate {
  func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    return 40
  }
  
  func tableView(tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
    guard let header = view as? UITableViewHeaderFooterView else {
      return
    }
    header.contentView.backgroundColor = UIColor.fpfLightestGreyColor()
    header.textLabel?.font = UIFont.fpfMedium(14)
    header.textLabel?.textColor = UIColor.fpfGreyishColor()
    header.textLabel?.frame = CGRect(x: 20, y: 0, width: tableView.bounds.width - 20 * 2, height: 40)
  }
  
  func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
    return guideHomeViewModel?.filteredTeams[section].0
  }
  
  func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    tableView.deselectRowAtIndexPath(indexPath, animated: true)
    guard let guide = guideHomeViewModel?.filteredTeams[indexPath.section].1[indexPath.row].guide else {
      return
    }
    delegate?.guideHomeView(self, didSelectGuide: guide)
  }
}

extension GuideHomeView: UISearchBarDelegate {
  func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
    guideHomeViewModel?.filteredTeams = guideHomeViewModel!.teams.flatMap { tuple -> (String, [GuideHomeCellViewModel])? in
      let leagueName = tuple.0
      let guides = tuple.1
      let filteredGuides = guides.filter {
        $0.name.rangeOfString(searchText, options: [.DiacriticInsensitiveSearch, .CaseInsensitiveSearch], range: nil, locale: nil) != nil
      }
      if filteredGuides.count > 0 {
        return (leagueName, filteredGuides)
      } else {
        return nil
      }
    }
    tableView.reloadData()
  }
  
  func searchBarShouldBeginEditing(searchBar: UISearchBar) -> Bool {
    searchBar.showsCancelButton = true
    return true
  }
  
  func searchBarShouldEndEditing(searchBar: UISearchBar) -> Bool {
    searchBar.showsCancelButton = false
    return true
  }
  
  func searchBarCancelButtonClicked(searchBar: UISearchBar) {
    searchBar.resignFirstResponder()
  }
}
