//
//  GuideHomeCell.swift
//  FPF
//
//  Created by Gilson Gil on 2/26/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit
import Kingfisher

final class GuideHomeCell: UITableViewCell {
  private let logoImageView = UIImageView()
  private let label = UILabel()
  
  var viewModel: GuideHomeCellViewModel? {
    didSet {
      dispatch_async(dispatch_get_main_queue()) { [weak self] in
        if let URL = NSURL(string: self?.viewModel?.logoURLString ?? "") {
          self?.logoImageView.kf_setImageWithURL(URL, placeholderImage: UIImage.icnPlaceholder())
        }
        self?.label.text = self?.viewModel?.name
      }
    }
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setUp()
  }
  
  override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    setUp()
  }
  
  private func setUp() {
    contentView.translatesAutoresizingMaskIntoConstraints = false
    selectionStyle = .None
    
    logoImageView.translatesAutoresizingMaskIntoConstraints = false
    logoImageView.contentMode = .ScaleAspectFit
    contentView.addSubview(logoImageView)
    
    label.translatesAutoresizingMaskIntoConstraints = false
    label.font = UIFont.fpfRegular(16)
    label.textColor = UIColor.fpfBlackColor()
    contentView.addSubview(label)
    
    let metrics = ["margin": Constants.defaultMargin, "doubleMargin": Constants.defaultMargin * 2, "logoWidth": Constants.mediumLogoWidth]
    let views = ["logoImageView": logoImageView, "label": label]
    var horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-margin-[logoImageView(logoWidth)]-margin-[label]->=margin-|", options: [], metrics: metrics, views: views)
    var verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|-doubleMargin-[logoImageView]-doubleMargin-|", options: [], metrics: metrics, views: views)
    contentView.addConstraints(horizontalConstraints)
    contentView.addConstraints(verticalConstraints)
    
    verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|-doubleMargin-[label]-doubleMargin-|", options: [], metrics: metrics, views: views)
    contentView.addConstraints(verticalConstraints)
    
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[contentView]|", options: [], metrics: nil, views: ["contentView": contentView])
    addConstraints(horizontalConstraints)
    verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[contentView]|", options: [], metrics: nil, views: ["contentView": contentView])
    addConstraints(verticalConstraints)
  }
  
  func configureWithGuideHomeCellViewModel(viewModel: GuideHomeCellViewModel) {
    self.viewModel = viewModel
  }
  
  #if DEBUG
  deinit {
    print("deinit", self)
  }
  #endif
}

extension GuideHomeCell: ReuseIdentifier {
  static func reuseIdentifier() -> String {
    return "GuideHomeCell"
  }
}
