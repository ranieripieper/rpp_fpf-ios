//
//  GuideDescriptionCell.swift
//  FPF
//
//  Created by Gilson Gil on 2/29/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class GuideDescriptionCell: UITableViewCell {
  private let card = UIView()
  private let descriptionLabel = UILabel()
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setUp()
  }
  
  override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    setUp()
  }
  
  private func setUp() {
    contentView.backgroundColor = UIColor.fpfLightestGreyColor()
    selectionStyle = .None
    
    let connector = UIView()
    connector.translatesAutoresizingMaskIntoConstraints = false
    connector.backgroundColor = UIColor.fpfStripeGreyColor()
    contentView.addSubview(connector)
    
    card.translatesAutoresizingMaskIntoConstraints = false
    card.backgroundColor = UIColor.whiteColor()
    contentView.addSubview(card)
    
    descriptionLabel.translatesAutoresizingMaskIntoConstraints = false
    descriptionLabel.font = UIFont.fpfRegular(14)
    descriptionLabel.textColor = UIColor.fpfCementColor()
    descriptionLabel.numberOfLines = 0
    card.addSubview(descriptionLabel)
    
    let metrics = ["cardMargin": 14, "margin": 12]
    let views = ["contentView": contentView, "connector": connector, "card": card, "descriptionLabel": descriptionLabel]
    var horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-cardMargin-[card]-cardMargin-|", options: [], metrics: metrics, views: views)
    contentView.addConstraints(horizontalConstraints)
    var verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[connector(cardMargin)][card]|", options: [], metrics: metrics, views: views)
    contentView.addConstraints(verticalConstraints)
    
    horizontalConstraints = [
      NSLayoutConstraint(item: connector, attribute: .Width, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1, constant: 4),
      NSLayoutConstraint(item: connector, attribute: .CenterX, relatedBy: .Equal, toItem: contentView, attribute: .CenterX, multiplier: 1, constant: 0)
    ]
    contentView.addConstraints(horizontalConstraints)
    
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-margin-[descriptionLabel]-margin-|", options: [], metrics: metrics, views: views)
    card.addConstraints(horizontalConstraints)
    verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|-margin-[descriptionLabel]-margin-|", options: [], metrics: metrics, views: views)
    card.addConstraints(verticalConstraints)
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    card.layer.masksToBounds = false
    card.layer.shadowColor = UIColor(white: 0.8, alpha: 1).CGColor
    card.layer.shadowOpacity = 0.75
    card.layer.shadowRadius = 1
    card.layer.shadowPath = UIBezierPath(rect: CGRect(x: 0, y: 4, width: card.bounds.width, height: card.bounds.height - 3)).CGPath
  }
  
  func update(viewModel: GuideDescriptionCellViewModel) {
    descriptionLabel.text = viewModel.description
  }
}

extension GuideDescriptionCell: Updatable {
  typealias ViewModel = GuideDescriptionCellViewModel
}
