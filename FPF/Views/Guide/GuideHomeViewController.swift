//
//  GuideHomeViewController.swift
//  FPF
//
//  Created by Gilson Gil on 2/25/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class GuideHomeViewController: UIViewController {
  private let guideHomeView = GuideHomeView()
  
  override func loadView() {
    view = guideHomeView
    guideHomeView.delegate = self
    navigationItem.title = Strings.GuideTitle
    
    let menuBarButton = UIBarButtonItem(image: UIImage.icnMenu(), style: .Plain, target: navigationController, action: #selector(MainViewController.presentMenu))
    navigationItem.rightBarButtonItems = [menuBarButton]
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    ProgressHUDHelper.show()
    GuideHomeViewModel.create { [weak self] viewModel in
      self?.guideHomeView.guideHomeViewModel = viewModel
      ProgressHUDHelper.dismiss()
    }
  }
  
  override func viewWillAppear(animated: Bool) {
    super.viewWillAppear(animated)
    
    NSNotificationCenter.defaultCenter().addObserver(guideHomeView, selector: #selector(guideHomeView.keyboardWillShow(_:)), name: UIKeyboardWillShowNotification, object: nil)
    NSNotificationCenter.defaultCenter().addObserver(guideHomeView, selector: #selector(guideHomeView.keyboardWillHide(_:)), name: UIKeyboardWillHideNotification, object: nil)
  }
  
  override func viewWillDisappear(animated: Bool) {
    super.viewWillDisappear(animated)
    
    NSNotificationCenter.defaultCenter().removeObserver(guideHomeView, name: UIKeyboardWillShowNotification, object: nil)
    NSNotificationCenter.defaultCenter().removeObserver(guideHomeView, name: UIKeyboardWillHideNotification, object: nil)
  }
  
  #if DEBUG
  deinit {
    print("deinit", self)
  }
  #endif
}

extension GuideHomeViewController: GuideHomeViewDelegate {
  func guideHomeView(guideHomeView: GuideHomeView, didSelectGuide guide: Guide) {
    let guideViewController = GuideViewController(guide: guide)
    navigationController?.pushViewController(guideViewController, animated: true)
    navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
  }
}
