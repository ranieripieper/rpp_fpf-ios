//
//  GuideViewController.swift
//  FPF
//
//  Created by Gilson Gil on 2/26/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class GuideViewController: UIViewController {
  private let guideView = GuideView()
  private let guide: Guide!
  
  required init?(coder aDecoder: NSCoder) {
    guide = nil
    super.init(coder: aDecoder)
  }
  
  init(guide: Guide) {
    self.guide = guide
    super.init(nibName: nil, bundle: nil)
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    guideView.guideViewModel = GuideViewModel(guide: guide)
  }
  
  override func loadView() {
    view = guideView
    guideView.delegate = self
    
    let menuBarButton = UIBarButtonItem(image: UIImage.icnMenu(), style: .Plain, target: navigationController, action: #selector(MainViewController.presentMenu))
    navigationItem.rightBarButtonItems = [menuBarButton]
  }
  
  #if DEBUG
  deinit {
    print("deinit", self)
  }
  #endif
}

extension GuideViewController: GuideViewDelegate {
  func guideViewTappedStaff(guideView: GuideView, guideStaffViewModel: GuideStaffViewModel) {
    guard guideStaffViewModel.staff.count > 1 else {
      return
    }
    let staffViewController = StaffViewController(staffViewModel: StaffViewModel(staff: guideStaffViewModel.staff))
    navigationController?.pushViewController(staffViewController, animated: true)
    navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .Plain, target: nil, action: nil)
  }
  
  func guideViewTappedMaps(guideView: GuideView, guideCityViewModel: GuideCityViewModel) {
    MapsHandler.open(guideCityViewModel.coordinate.coordinate, name: guideCityViewModel.cityStadiumName, fromViewController: self)
  }
}

