//
//  GuideMascotCell.swift
//  FPF
//
//  Created by Gilson Gil on 2/29/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit
import Kingfisher

final class GuideMascotCell: UITableViewCell {
  private let topCard = UIView()
  private let card = UIView()
  private let bottomCard = UIView()
  private let mascotLabel = UILabel()
  private let mascotImageView = UIImageView()
  private var aspectRatioConstraint: NSLayoutConstraint?
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setUp()
  }
  
  override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    setUp()
  }
  
  private func setUp() {
    contentView.translatesAutoresizingMaskIntoConstraints = false
    contentView.backgroundColor = UIColor.fpfLightestGreyColor()
    selectionStyle = .None
    
    topCard.translatesAutoresizingMaskIntoConstraints = false
    topCard.backgroundColor = UIColor.whiteColor()
    contentView.addSubview(topCard)
    
    let connector1 = UIView()
    connector1.translatesAutoresizingMaskIntoConstraints = false
    connector1.backgroundColor = UIColor.fpfStripeGreyColor()
    contentView.addSubview(connector1)
    
    let connector2 = UIView()
    connector2.translatesAutoresizingMaskIntoConstraints = false
    connector2.backgroundColor = UIColor.fpfStripeGreyColor()
    contentView.addSubview(connector2)
    
    card.translatesAutoresizingMaskIntoConstraints = false
    card.backgroundColor = UIColor.whiteColor()
    contentView.addSubview(card)
    
    let connector3 = UIView()
    connector3.translatesAutoresizingMaskIntoConstraints = false
    connector3.backgroundColor = UIColor.fpfStripeGreyColor()
    contentView.insertSubview(connector3, belowSubview: card)
    
    let connector4 = UIView()
    connector4.translatesAutoresizingMaskIntoConstraints = false
    connector4.backgroundColor = UIColor.fpfStripeGreyColor()
    contentView.insertSubview(connector4, belowSubview: card)
    
    bottomCard.translatesAutoresizingMaskIntoConstraints = false
    bottomCard.backgroundColor = UIColor.whiteColor()
    contentView.insertSubview(bottomCard, belowSubview: card)
    
    let mascotDescriptionLabel = UILabel()
    mascotDescriptionLabel.translatesAutoresizingMaskIntoConstraints = false
    mascotDescriptionLabel.font = UIFont.fpfBold(18)
    mascotDescriptionLabel.text = Strings.GuideMascot
    mascotDescriptionLabel.textColor = UIColor.fpfGreyishColor()
    card.addSubview(mascotDescriptionLabel)
    
    mascotLabel.translatesAutoresizingMaskIntoConstraints = false
    mascotLabel.font = UIFont.fpfMedium(14)
    mascotLabel.textColor = UIColor.fpfGreyishColor()
    mascotLabel.numberOfLines = 0
    card.addSubview(mascotLabel)
    
    mascotImageView.translatesAutoresizingMaskIntoConstraints = false
    mascotImageView.contentMode = .ScaleAspectFit
    card.addSubview(mascotImageView)
    
    let metrics = ["cardMargin": 14, "margin": 22, "topMargin": 32, "bottomMargin": 36, "extraCardHeight": 20, "imageSize": 160]
    let views = ["contentView": contentView, "connector1": connector1, "connector2": connector2, "connector3": connector3, "connector4": connector4, "topCard": topCard, "card": card, "bottomCard": bottomCard, "mascotDescriptionLabel": mascotDescriptionLabel, "mascotLabel": mascotLabel, "mascotImageView": mascotImageView]
    var horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[contentView]|", options: [], metrics: metrics, views: views)
    addConstraints(horizontalConstraints)
    var verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[contentView]|", options: [], metrics: metrics, views: views)
    addConstraints(verticalConstraints)
    
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-cardMargin-[topCard]-cardMargin-|", options: [], metrics: metrics, views: views)
    contentView.addConstraints(horizontalConstraints)
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-cardMargin-[card]-cardMargin-|", options: [], metrics: metrics, views: views)
    contentView.addConstraints(horizontalConstraints)
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-cardMargin-[bottomCard]-cardMargin-|", options: [], metrics: metrics, views: views)
    contentView.addConstraints(horizontalConstraints)
    horizontalConstraints = [
      NSLayoutConstraint(item: connector1, attribute: .CenterX, relatedBy: .Equal, toItem: contentView, attribute: .CenterX, multiplier: 0.5, constant: 0),
      NSLayoutConstraint(item: connector1, attribute: .Width, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1, constant: 4)
    ]
    contentView.addConstraints(horizontalConstraints)
    horizontalConstraints = [
      NSLayoutConstraint(item: connector2, attribute: .CenterX, relatedBy: .Equal, toItem: contentView, attribute: .CenterX, multiplier: 1.5, constant: 0),
      NSLayoutConstraint(item: connector2, attribute: .Width, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1, constant: 4)
    ]
    contentView.addConstraints(horizontalConstraints)
    verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[topCard(extraCardHeight)][connector1(cardMargin)][card][connector3(cardMargin)][bottomCard(extraCardHeight)]|", options: [], metrics: metrics, views: views)
    contentView.addConstraints(verticalConstraints)
    verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:[topCard][connector2(cardMargin)][card][connector4(cardMargin)][bottomCard]", options: [], metrics: metrics, views: views)
    contentView.addConstraints(verticalConstraints)
    horizontalConstraints = [
      NSLayoutConstraint(item: connector3, attribute: .CenterX, relatedBy: .Equal, toItem: contentView, attribute: .CenterX, multiplier: 0.5, constant: 0),
      NSLayoutConstraint(item: connector3, attribute: .Width, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1, constant: 4)
    ]
    contentView.addConstraints(horizontalConstraints)
    horizontalConstraints = [
      NSLayoutConstraint(item: connector4, attribute: .CenterX, relatedBy: .Equal, toItem: contentView, attribute: .CenterX, multiplier: 1.5, constant: 0),
      NSLayoutConstraint(item: connector4, attribute: .Width, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1, constant: 4)
    ]
    contentView.addConstraints(horizontalConstraints)
    
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-margin-[mascotDescriptionLabel]", options: [], metrics: metrics, views: views)
    card.addConstraints(horizontalConstraints)
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-margin-[mascotLabel]", options: [], metrics: metrics, views: views)
    card.addConstraints(horizontalConstraints)
    verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|-topMargin-[mascotDescriptionLabel][mascotLabel]-bottomMargin-|", options: [], metrics: metrics, views: views)
    card.addConstraints(verticalConstraints)
    horizontalConstraints = [NSLayoutConstraint(item: mascotImageView, attribute: .Width, relatedBy: .Equal, toItem: card, attribute: .Width, multiplier: 0.6, constant: 0)]
    card.addConstraints(horizontalConstraints)
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:[mascotImageView]|", options: [], metrics: metrics, views: views)
    card.addConstraints(horizontalConstraints)
    verticalConstraints = [
      NSLayoutConstraint(item: mascotImageView, attribute: .Height, relatedBy: .Equal, toItem: card, attribute: .Height, multiplier: 1.5, constant: 0),
      NSLayoutConstraint(item: mascotImageView, attribute: .CenterY, relatedBy: .Equal, toItem: card, attribute: .CenterY, multiplier: 1, constant: 0)
    ]
    card.addConstraints(verticalConstraints)
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    topCard.applyShadow()
    card.applyShadow()
    
    bottomCard.layer.masksToBounds = false
    bottomCard.layer.shadowColor = UIColor(white: 0.8, alpha: 1).CGColor
    bottomCard.layer.shadowOpacity = 0.75
    bottomCard.layer.shadowRadius = 1
    bottomCard.layer.shadowPath = UIBezierPath(rect: CGRect(x: 0, y: 4, width: bottomCard.bounds.width, height: bottomCard.bounds.height - 3)).CGPath
    
    mascotImageView.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, mascotImageView.bounds.width, 0)
  }
  
  func update(viewModel: GuideMascotViewModel) {
    mascotLabel.text = viewModel.mascotName
    if let mascotImageURL = viewModel.mascotImageURL {
      mascotImageView.kf_setImageWithURL(NSURL(string: mascotImageURL)!)
    }
    if let aspectRatioConstraint = aspectRatioConstraint {
      mascotImageView.removeConstraint(aspectRatioConstraint)
    }
    if let image = mascotImageView.image {
      aspectRatioConstraint = NSLayoutConstraint(item: mascotImageView, attribute: .Width, relatedBy: .Equal, toItem: mascotImageView, attribute: .Height, multiplier: image.size.width / image.size.height, constant: 0)
      mascotImageView.addConstraint(aspectRatioConstraint!)
      mascotImageView.layoutIfNeeded()
    }
  }
}

extension GuideMascotCell: Updatable {
  typealias ViewModel = GuideMascotViewModel
}

extension GuideMascotCell: RealTimeUpdatable {
  func update(percentage: CGFloat) {
    let factor = max(0, min(1, (percentage - 0.5) / 0.5))
    mascotImageView.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, factor * mascotImageView.bounds.width, 0)
  }
}
