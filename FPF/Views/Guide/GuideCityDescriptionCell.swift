//
//  GuideCityDescriptionCell.swift
//  FPF
//
//  Created by Gilson Gil on 3/1/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

protocol GuideCityDescriptionCellDelegate: class {
  func didLoadImage(cell: GuideCityDescriptionCell)
}

final class GuideCityDescriptionCell: UITableViewCell {
  private let card = UIView()
  private let cityImageView = UIImageView()
  private let cityDescriptionLabel = UILabel()
  
  var delegate: GuideCityDescriptionCellDelegate?
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setUp()
  }
  
  override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    setUp()
  }
  
  private func setUp() {
    contentView.backgroundColor = UIColor.fpfLightestGreyColor()
    selectionStyle = .None
    
    let connector1 = UIView()
    connector1.translatesAutoresizingMaskIntoConstraints = false
    connector1.backgroundColor = UIColor.fpfStripeGreyColor()
    contentView.addSubview(connector1)
    
    let connector2 = UIView()
    connector2.translatesAutoresizingMaskIntoConstraints = false
    connector2.backgroundColor = UIColor.fpfStripeGreyColor()
    contentView.addSubview(connector2)
    
    card.translatesAutoresizingMaskIntoConstraints = false
    card.backgroundColor = UIColor.whiteColor()
    contentView.addSubview(card)
    
    cityImageView.translatesAutoresizingMaskIntoConstraints = false
    cityImageView.contentMode = .ScaleAspectFill
    cityImageView.clipsToBounds = true
    cityImageView.setContentHuggingPriority(UILayoutPriorityRequired, forAxis: .Horizontal)
    cityImageView.setContentHuggingPriority(UILayoutPriorityRequired, forAxis: .Vertical)
    cityImageView.setContentCompressionResistancePriority(UILayoutPriorityRequired, forAxis: .Horizontal)
    cityImageView.setContentCompressionResistancePriority(UILayoutPriorityRequired, forAxis: .Vertical)
    card.addSubview(cityImageView)
    
    cityDescriptionLabel.translatesAutoresizingMaskIntoConstraints = false
    cityDescriptionLabel.font = UIFont.fpfRegular(14)
    cityDescriptionLabel.textColor = UIColor.fpfCementColor()
    cityDescriptionLabel.numberOfLines = 0
    card.addSubview(cityDescriptionLabel)
    
    let metrics = ["cardMargin": 14, "margin": 8, "margin2": 16]
    let views = ["connector1": connector1, "connector2": connector2, "card": card, "cityImageView": cityImageView, "cityDescriptionLabel": cityDescriptionLabel]
    var horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-cardMargin-[card]-cardMargin-|", options: [], metrics: metrics, views: views)
    contentView.addConstraints(horizontalConstraints)
    var verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[connector1(cardMargin)][card]|", options: [], metrics: metrics, views: views)
    contentView.addConstraints(verticalConstraints)
    verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[connector2(cardMargin)][card]", options: [], metrics: metrics, views: views)
    contentView.addConstraints(verticalConstraints)
    horizontalConstraints = [
      NSLayoutConstraint(item: connector1, attribute: .CenterX, relatedBy: .Equal, toItem: contentView, attribute: .CenterX, multiplier: 0.5, constant: 0),
      NSLayoutConstraint(item: connector1, attribute: .Width, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1, constant: 4)
    ]
    contentView.addConstraints(horizontalConstraints)
    horizontalConstraints = [
      NSLayoutConstraint(item: connector2, attribute: .CenterX, relatedBy: .Equal, toItem: contentView, attribute: .CenterX, multiplier: 1.5, constant: 0),
      NSLayoutConstraint(item: connector2, attribute: .Width, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1, constant: 4)
    ]
    contentView.addConstraints(horizontalConstraints)
    
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-margin-[cityImageView]-margin-|", options: [], metrics: metrics, views: views)
    card.addConstraints(horizontalConstraints)
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-margin-[cityDescriptionLabel]-margin-|", options: [], metrics: metrics, views: views)
    card.addConstraints(horizontalConstraints)
    verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|-margin-[cityImageView]-margin-[cityDescriptionLabel]-margin2-|", options: [], metrics: metrics, views: views)
    card.addConstraints(verticalConstraints)
    verticalConstraints = [NSLayoutConstraint(item: cityImageView, attribute: .Height, relatedBy: .LessThanOrEqual, toItem: nil, attribute: .NotAnAttribute, multiplier: 1, constant: 140)]
    cityImageView.addConstraints(verticalConstraints)
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    card.applyShadow()
  }
  
  func update(viewModel: GuideCityDescriptionViewModel) {
    if let cityURL = viewModel.cityURL, URL = NSURL(string: cityURL) {
      cityImageView.kf_setImageWithURL(URL, placeholderImage: nil, optionsInfo: nil) { [weak self] (image, error, cacheType, imageURL) -> () in
        if let weakSelf = self {
          weakSelf.delegate?.didLoadImage(weakSelf)
        }
      }
    }
    cityDescriptionLabel.text = viewModel.cityDescription
  }
}

extension GuideCityDescriptionCell: Updatable {
  typealias ViewModel = GuideCityDescriptionViewModel
}
