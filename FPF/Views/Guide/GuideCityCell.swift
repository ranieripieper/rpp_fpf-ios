//
//  GuideCityCell.swift
//  FPF
//
//  Created by Gilson Gil on 3/1/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit
import MapKit

protocol GuideCityCellDelegate {
  func mapsTappedInGuideCityCell(guideCityCell: GuideCityCell)
}

final class GuideCityCell: UITableViewCell {
  private let card = UIView()
  private let cityStadiumLabel = UILabel()
  private let mapView = MKMapView()
  
  var delegate: GuideCityCellDelegate?
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setUp()
  }
  
  override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    setUp()
  }
  
  private func setUp() {
    contentView.backgroundColor = UIColor.fpfLightestGreyColor()
    selectionStyle = .None
    
    let connector = UIView()
    connector.translatesAutoresizingMaskIntoConstraints = false
    connector.backgroundColor = UIColor.fpfStripeGreyColor()
    contentView.addSubview(connector)
    
    card.translatesAutoresizingMaskIntoConstraints = false
    card.backgroundColor = UIColor.whiteColor()
    contentView.addSubview(card)
    
    let cityDescriptionLabel = UILabel()
    cityDescriptionLabel.translatesAutoresizingMaskIntoConstraints = false
    cityDescriptionLabel.font = UIFont.fpfBold(18)
    cityDescriptionLabel.text = Strings.GuideCity
    cityDescriptionLabel.textColor = UIColor.fpfGreyishColor()
    card.addSubview(cityDescriptionLabel)
    
    cityStadiumLabel.translatesAutoresizingMaskIntoConstraints = false
    cityStadiumLabel.font = UIFont.fpfMedium(14)
    cityStadiumLabel.textColor = UIColor.fpfGreyishColor()
    cityStadiumLabel.minimumScaleFactor = 0.5
    cityStadiumLabel.adjustsFontSizeToFitWidth = true
    card.addSubview(cityStadiumLabel)
    
    mapView.translatesAutoresizingMaskIntoConstraints = false
    mapView.userInteractionEnabled = false
    card.addSubview(mapView)
    
    let button = UIButton(type: .Custom)
    button.translatesAutoresizingMaskIntoConstraints = false
    button.addTarget(self, action: #selector(GuideCityCell.mapTapped), forControlEvents: .TouchUpInside)
    card.addSubview(button)
    
    let metrics = ["cardMargin": 14, "margin": 22, "margin2": 8, "mapSize": 100]
    let views = ["connector": connector, "card": card, "cityDescriptionLabel": cityDescriptionLabel, "cityStadiumLabel": cityStadiumLabel, "mapView": mapView, "button": button]
    var horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-cardMargin-[card]-cardMargin-|", options: [], metrics: metrics, views: views)
    contentView.addConstraints(horizontalConstraints)
    var verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[connector(cardMargin)][card]|", options: [], metrics: metrics, views: views)
    contentView.addConstraints(verticalConstraints)
    horizontalConstraints = [
      NSLayoutConstraint(item: connector, attribute: .CenterX, relatedBy: .Equal, toItem: contentView, attribute: .CenterX, multiplier: 1, constant: 0),
      NSLayoutConstraint(item: connector, attribute: .Width, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1, constant: 4)
    ]
    contentView.addConstraints(horizontalConstraints)
    
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-margin-[cityDescriptionLabel]-margin-[mapView(mapSize)]-margin2-|", options: [], metrics: metrics, views: views)
    card.addConstraints(horizontalConstraints)
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-margin-[cityStadiumLabel]-margin2-[mapView]", options: [], metrics: metrics, views: views)
    card.addConstraints(horizontalConstraints)
    verticalConstraints = [
      NSLayoutConstraint(item: cityDescriptionLabel, attribute: .Bottom, relatedBy: .Equal, toItem: card, attribute: .CenterY, multiplier: 1, constant: 0),
      NSLayoutConstraint(item: cityStadiumLabel, attribute: .Top, relatedBy: .Equal, toItem: card, attribute: .CenterY, multiplier: 1, constant: 0)
    ]
    card.addConstraints(verticalConstraints)
    verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|-margin2-[mapView(mapSize)]-margin2-|", options: [], metrics: metrics, views: views)
    card.addConstraints(verticalConstraints)
    
    horizontalConstraints = [
      NSLayoutConstraint(item: button, attribute: .Left, relatedBy: .Equal, toItem: mapView, attribute: .Left, multiplier: 1, constant: 0),
      NSLayoutConstraint(item: button, attribute: .Right, relatedBy: .Equal, toItem: mapView, attribute: .Right, multiplier: 1, constant: 0)
    ]
    card.addConstraints(horizontalConstraints)
    verticalConstraints = [
      NSLayoutConstraint(item: button, attribute: .Top, relatedBy: .Equal, toItem: mapView, attribute: .Top, multiplier: 1, constant: 0),
      NSLayoutConstraint(item: button, attribute: .Bottom, relatedBy: .Equal, toItem: mapView, attribute: .Bottom, multiplier: 1, constant: 0)
    ]
    card.addConstraints(verticalConstraints)
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    card.applyShadow()
  }
  
  func update(viewModel: GuideCityViewModel) {
    cityStadiumLabel.text = viewModel.cityStadiumName
    let region = MKCoordinateRegionMakeWithDistance(viewModel.coordinate.coordinate, 1000, 1000)
    mapView.region = region
  }
  
  func mapTapped() {
    delegate?.mapsTappedInGuideCityCell(self)
  }
}

extension GuideCityCell: Updatable {
  typealias ViewModel = GuideCityViewModel
}
