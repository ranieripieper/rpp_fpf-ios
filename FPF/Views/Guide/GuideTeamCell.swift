//
//  GuideTeamCell.swift
//  FPF
//
//  Created by Gilson Gil on 2/29/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit
import Kingfisher

final class GuideTeamCell: UITableViewCell {
  private let logoImageView = UIImageView()
  private let nameLabel = UILabel()
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setUp()
  }
  
  override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    setUp()
  }
  
  private func setUp() {
    contentView.backgroundColor = UIColor.fpfLightestGreyColor()
    selectionStyle = .None
    
    let card = UIView()
    card.translatesAutoresizingMaskIntoConstraints = false
    card.backgroundColor = UIColor.whiteColor()
    contentView.addSubview(card)
    
    logoImageView.translatesAutoresizingMaskIntoConstraints = false
    logoImageView.contentMode = .ScaleAspectFill
    logoImageView.setContentCompressionResistancePriority(UILayoutPriorityRequired, forAxis: .Horizontal)
    logoImageView.setContentCompressionResistancePriority(UILayoutPriorityRequired, forAxis: .Vertical)
    card.addSubview(logoImageView)
    
    nameLabel.translatesAutoresizingMaskIntoConstraints = false
    nameLabel.font = UIFont.fpfBold(18)
    nameLabel.textColor = UIColor.fpfBlackColor()
    nameLabel.numberOfLines = 0
    nameLabel.textAlignment = .Right
    nameLabel.lineBreakMode = .ByWordWrapping
    nameLabel.adjustsFontSizeToFitWidth = true
    nameLabel.minimumScaleFactor = 0.8
    card.addSubview(nameLabel)
    
    let separatorLine = UIView()
    separatorLine.translatesAutoresizingMaskIntoConstraints = false
    separatorLine.backgroundColor = UIColor.fpfMediumGreyColor()
    card.addSubview(separatorLine)
    
    let metrics = ["cardMargin": 0, "margin": 12, "margin2": 24]
    let views = ["contentView": contentView, "card": card, "logoImageView": logoImageView, "nameLabel": nameLabel, "separatorLine": separatorLine]
    var horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-cardMargin-[card]-cardMargin-|", options: [], metrics: metrics, views: views)
    contentView.addConstraints(horizontalConstraints)
    var verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|-cardMargin-[card]-cardMargin-|", options: [], metrics: metrics, views: views)
    contentView.addConstraints(verticalConstraints)
    
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:[nameLabel]-margin2-|", options: [], metrics: metrics, views: views)
    card.addConstraints(horizontalConstraints)
    horizontalConstraints = [NSLayoutConstraint(item: logoImageView, attribute: .Right, relatedBy: .Equal, toItem: nameLabel, attribute: .Left, multiplier: 1, constant: 10)]
    card.addConstraints(horizontalConstraints)
    horizontalConstraints = [NSLayoutConstraint(item: nameLabel, attribute: .Left, relatedBy: .Equal, toItem: card, attribute: .CenterX, multiplier: 0.65, constant: 0)]
    card.addConstraints(horizontalConstraints)
    horizontalConstraints = [NSLayoutConstraint(item: logoImageView, attribute: .Width, relatedBy: .Equal, toItem: card, attribute: .Width, multiplier: 0.5, constant: 0)]
    card.addConstraints(horizontalConstraints)
    verticalConstraints = [NSLayoutConstraint(item: logoImageView, attribute: .Height, relatedBy: .Equal, toItem: logoImageView, attribute: .Width, multiplier: 1, constant: 0)]
    logoImageView.addConstraints(verticalConstraints)
    verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:[logoImageView]-margin-|", options: [], metrics: metrics, views: views)
    card.addConstraints(verticalConstraints)
    verticalConstraints = [NSLayoutConstraint(item: logoImageView, attribute: .Top, relatedBy: .Equal, toItem: card, attribute: .Top, multiplier: 1, constant: -12)]
    card.addConstraints(verticalConstraints)
    verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:[nameLabel]-margin2-|", options: [], metrics: metrics, views: views)
    card.addConstraints(verticalConstraints)
    
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[separatorLine]|", options: [], metrics: metrics, views: views)
    card.addConstraints(horizontalConstraints)
    verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:[separatorLine(2)]|", options: [], metrics: metrics, views: views)
    card.addConstraints(verticalConstraints)
  }
  
  func update(viewModel: GuideTeamCellViewModel) {
    logoImageView.kf_setImageWithURL(NSURL(string: viewModel.logoURL)!)
    nameLabel.text = viewModel.name.uppercaseString
  }
}

extension GuideTeamCell: Updatable {
  typealias ViewModel = GuideTeamCellViewModel
}
