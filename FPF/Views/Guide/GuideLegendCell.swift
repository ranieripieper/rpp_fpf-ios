//
//  GuideLegendCell.swift
//  FPF
//
//  Created by Gilson Gil on 2/27/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class GuideLegendCell: UITableViewCell {
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setUp()
  }
  
  override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    setUp()
  }
  
  private func setUp() {
    selectionStyle = .None
    
    let containerView = UIView()
    containerView.translatesAutoresizingMaskIntoConstraints = false
    containerView.backgroundColor = UIColor.fpfLightestGreyColor()
    addSubview(containerView)
    
    let sourceLabel = UILabel()
    sourceLabel.translatesAutoresizingMaskIntoConstraints = false
    sourceLabel.text = Strings.GuideSource
    sourceLabel.textColor = UIColor.fpfPinkishGreyColor()
    sourceLabel.font = UIFont.fpfRegular(14)
    containerView.addSubview(sourceLabel)
    
    let idhmLabel = UILabel()
    idhmLabel.translatesAutoresizingMaskIntoConstraints = false
    idhmLabel.text = Strings.GuideIDHMSubscript
    idhmLabel.textColor = UIColor.fpfPinkishGreyColor()
    idhmLabel.font = UIFont.fpfRegular(14)
    containerView.addSubview(idhmLabel)
    
    let metrics = ["hMargin": 20, "vMargin": 10, "vMargin2": 5]
    let views = ["containerView": containerView, "sourceLabel": sourceLabel, "idhmLabel": idhmLabel]
    var horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[containerView]|", options: [], metrics: metrics, views: views)
    addConstraints(horizontalConstraints)
    var verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[containerView]|", options: [], metrics: metrics, views: views)
    addConstraints(verticalConstraints)
    
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-hMargin-[sourceLabel]-hMargin-|", options: [], metrics: metrics, views: views)
    containerView.addConstraints(horizontalConstraints)
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-hMargin-[idhmLabel]-hMargin-|", options: [], metrics: metrics, views: views)
    containerView.addConstraints(horizontalConstraints)
    verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|-vMargin-[sourceLabel]-vMargin2-[idhmLabel]-vMargin-|", options: [], metrics: metrics, views: views)
    containerView.addConstraints(verticalConstraints)
  }
  
  func update(viewModel: Any?) {}
}

extension GuideLegendCell: Updatable {
  typealias ViewModel = Any?
}
