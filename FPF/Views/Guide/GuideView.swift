//
//  GuideView.swift
//  FPF
//
//  Created by Gilson Gil on 2/26/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

protocol GuideViewDelegate {
  func guideViewTappedStaff(guideView: GuideView, guideStaffViewModel: GuideStaffViewModel)
  func guideViewTappedMaps(guideView: GuideView, guideCityViewModel: GuideCityViewModel)
}

class GuideView: UIView {
  private let tableView = UITableView(frame: .zero, style: .Plain)
  private let backgroundView = UIView()
  
  var guideViewModel: GuideViewModel? {
    didSet {
      registerCells()
      dispatch_async(dispatch_get_main_queue()) { [weak self] in
        self?.tableView.reloadData()
      }
    }
  }
  var delegate: GuideViewDelegate?
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    setUp()
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setUp()
  }
  
  convenience init() {
    self.init(frame: .zero)
  }
  
  private func setUp() {
    backgroundColor = UIColor.fpfLightestGreyColor()
    
    backgroundView.backgroundColor = UIColor.whiteColor()
    backgroundView.frame = CGRect(x: 0, y: 64, width: UIScreen.mainScreen().bounds.width, height: 0)
    addSubview(backgroundView)
    
    tableView.translatesAutoresizingMaskIntoConstraints = false
    tableView.backgroundColor = UIColor.clearColor()
    tableView.dataSource = self
    tableView.delegate = self
    tableView.separatorStyle = .None
    addSubview(tableView)
    
    let views = ["tableView": tableView]
    let horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[tableView]|", options: [], metrics: nil, views: views)
    addConstraints(horizontalConstraints)
    let verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|-64-[tableView]|", options: [], metrics: nil, views: views)
    addConstraints(verticalConstraints)
  }
  
  func registerCells() {
    for cellConfigurator in guideViewModel?.items ?? [] {
      tableView.registerClass(cellConfigurator.cellClass, forCellReuseIdentifier: cellConfigurator.reuseIdentifier)
      if cellConfigurator.cellClass == GuideCityCell.classForCoder() {
        tableView.dequeueReusableCellWithIdentifier(cellConfigurator.reuseIdentifier)
      }
    }
  }
}

extension GuideView: UITableViewDataSource {
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return guideViewModel?.items.count ?? 0
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let configurator = guideViewModel?.items[indexPath.row]
    let cell = tableView.dequeueReusableCellWithIdentifier(configurator!.reuseIdentifier, forIndexPath: indexPath)
    configurator?.update(cell)
    if let cell = cell as? GuideCityCell {
      cell.delegate = self
    } else if let cell = cell as? GuideCityDescriptionCell {
      cell.delegate = self
    }
    return cell
  }
}

extension GuideView: UITableViewDelegate {
  func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    switch indexPath.row {
    case 0:
      return 290
    case 1:
      return 350
    case 2:
      return 130
    case 3:
      return 200
    case 4:
      return 80
    case 5:
      return 200
    case 6:
      return 100
    case 7:
      return 400
    case 8:
      return 140
    case 9:
      return 60
    default:
      return 0
    }
  }
  
  func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    return UITableViewAutomaticDimension
  }
  
  func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    if let guideStaffViewModel = guideViewModel?.items[indexPath.row].currentViewModel() as? GuideStaffViewModel {
      delegate?.guideViewTappedStaff(self, guideStaffViewModel: guideStaffViewModel)
    }
  }
}

extension GuideView: UIScrollViewDelegate {
  func scrollViewDidScroll(scrollView: UIScrollView) {
    backgroundView.frame = CGRect(x: 0, y: 64, width: UIScreen.mainScreen().bounds.width, height: -scrollView.contentOffset.y)
    tableView.visibleCells.forEach {
      if let cell = $0 as? RealTimeUpdatable, window = UIApplication.sharedApplication().delegate!.window!, cellY = $0.superview?.convertPoint($0.frame.origin, toView: window).y {
        let percentage = cellY/window.bounds.height
        cell.update(percentage)
      }
    }
  }
}

extension GuideView: GuideCityCellDelegate {
  func mapsTappedInGuideCityCell(guideCityCell: GuideCityCell) {
    guard let guideCityViewModel = guideViewModel?.guideCityViewModel else {
      return
    }
    delegate?.guideViewTappedMaps(self, guideCityViewModel: guideCityViewModel)
  }
}

extension GuideView: GuideCityDescriptionCellDelegate {
  func didLoadImage(cell: GuideCityDescriptionCell) {
    guard let indexPath = tableView.indexPathForCell(cell) else {
      return
    }
    tableView.reloadRowsAtIndexPaths([indexPath], withRowAnimation: .None)
//    tableView.beginUpdates()
//    tableView.endUpdates()
  }
}
