//
//  GuideStadiumCell.swift
//  FPF
//
//  Created by Gilson Gil on 3/1/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit
import Kingfisher

final class GuideStadiumCell: UITableViewCell {
  private let card = UIView()
  private let stadiumImageView = UIImageView()
  private let nameLabel = UILabel()
  private let capacityLabel = UILabel()
  private let establishmentLabel = UILabel()
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setUp()
  }
  
  override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    setUp()
  }
  
  private func setUp() {
    contentView.backgroundColor = UIColor.fpfLightestGreyColor()
    selectionStyle = .None
    
    let connector1 = UIView()
    connector1.translatesAutoresizingMaskIntoConstraints = false
    connector1.backgroundColor = UIColor.fpfStripeGreyColor()
    contentView.addSubview(connector1)
    
    let connector2 = UIView()
    connector2.translatesAutoresizingMaskIntoConstraints = false
    connector2.backgroundColor = UIColor.fpfStripeGreyColor()
    contentView.addSubview(connector2)
    
    card.translatesAutoresizingMaskIntoConstraints = false
    card.backgroundColor = UIColor.whiteColor()
    contentView.addSubview(card)
    
    stadiumImageView.translatesAutoresizingMaskIntoConstraints = false
    stadiumImageView.contentMode = .ScaleAspectFill
    stadiumImageView.clipsToBounds = true
    stadiumImageView.setContentCompressionResistancePriority(UILayoutPriorityRequired, forAxis: .Horizontal)
    stadiumImageView.setContentCompressionResistancePriority(UILayoutPriorityDefaultHigh, forAxis: .Vertical)
    card.addSubview(stadiumImageView)
    
    let overlayView = UIView()
    overlayView.translatesAutoresizingMaskIntoConstraints = false
    overlayView.backgroundColor = UIColor.blackColor()
    overlayView.alpha = 0.8
    card.addSubview(overlayView)
    
    let container = UIView()
    container.translatesAutoresizingMaskIntoConstraints = false
    container.backgroundColor = UIColor.clearColor()
    overlayView.addSubview(container)
    
    let stadiumDescriptionLabel = UILabel()
    stadiumDescriptionLabel.translatesAutoresizingMaskIntoConstraints = false
    stadiumDescriptionLabel.text = Strings.GuideStadium.uppercaseString
    stadiumDescriptionLabel.textColor = UIColor.fpfSunflowerColor()
    stadiumDescriptionLabel.textAlignment = .Right
    stadiumDescriptionLabel.font = UIFont.fpfBold(18)
    stadiumDescriptionLabel.setContentHuggingPriority(UILayoutPriorityRequired, forAxis: .Vertical)
    container.addSubview(stadiumDescriptionLabel)
    
    let nameDescriptionLabel = UILabel()
    nameDescriptionLabel.translatesAutoresizingMaskIntoConstraints = false
    nameDescriptionLabel.font = UIFont.fpfBold(14)
    nameDescriptionLabel.textColor = UIColor.whiteColor()
    nameDescriptionLabel.text = Strings.GuideStadiumName.uppercaseString
    nameDescriptionLabel.textAlignment = .Right
    nameDescriptionLabel.setContentHuggingPriority(UILayoutPriorityRequired, forAxis: .Vertical)
    container.addSubview(nameDescriptionLabel)
    
    nameLabel.translatesAutoresizingMaskIntoConstraints = false
    nameLabel.font = UIFont.fpfRegular(14)
    nameLabel.textColor = UIColor.whiteColor()
    nameLabel.textAlignment = .Right
    nameLabel.numberOfLines = 0
    nameLabel.setContentHuggingPriority(UILayoutPriorityRequired, forAxis: .Vertical)
    container.addSubview(nameLabel)
    
    let capacityDescriptionLabel = UILabel()
    capacityDescriptionLabel.translatesAutoresizingMaskIntoConstraints = false
    capacityDescriptionLabel.font = UIFont.fpfBold(14)
    capacityDescriptionLabel.textColor = UIColor.whiteColor()
    capacityDescriptionLabel.textAlignment = .Right
    capacityDescriptionLabel.text = Strings.GuideStadiumCapacity.uppercaseString
    capacityDescriptionLabel.setContentHuggingPriority(UILayoutPriorityRequired, forAxis: .Vertical)
    container.addSubview(capacityDescriptionLabel)
    
    capacityLabel.translatesAutoresizingMaskIntoConstraints = false
    capacityLabel.font = UIFont.fpfRegular(14)
    capacityLabel.textColor = UIColor.whiteColor()
    capacityLabel.textAlignment = .Right
    capacityLabel.numberOfLines = 0
    capacityLabel.setContentHuggingPriority(UILayoutPriorityRequired, forAxis: .Vertical)
    container.addSubview(capacityLabel)
    
    let establishmentDescriptionLabel = UILabel()
    establishmentDescriptionLabel.translatesAutoresizingMaskIntoConstraints = false
    establishmentDescriptionLabel.font = UIFont.fpfBold(14)
    establishmentDescriptionLabel.textAlignment = .Right
    establishmentDescriptionLabel.textColor = UIColor.whiteColor()
    establishmentDescriptionLabel.text = Strings.GuideStadiumEstablishment.uppercaseString
    establishmentDescriptionLabel.setContentHuggingPriority(UILayoutPriorityRequired, forAxis: .Vertical)
    container.addSubview(establishmentDescriptionLabel)
    
    establishmentLabel.translatesAutoresizingMaskIntoConstraints = false
    establishmentLabel.font = UIFont.fpfRegular(14)
    establishmentLabel.textColor = UIColor.whiteColor()
    establishmentLabel.textAlignment = .Right
    establishmentLabel.numberOfLines = 0
    establishmentLabel.setContentHuggingPriority(UILayoutPriorityRequired, forAxis: .Vertical)
    container.addSubview(establishmentLabel)
    
    let metrics = ["cardMargin": 14, "margin": 12, "margin2": 20]
    let views = ["contentView": contentView, "connector1": connector1, "connector2": connector2, "card": card, "stadiumImageView": stadiumImageView, "overlayView": overlayView, "container": container, "stadiumDescriptionLabel": stadiumDescriptionLabel, "nameDescriptionLabel": nameDescriptionLabel, "nameLabel": nameLabel, "capacityDescriptionLabel": capacityDescriptionLabel, "capacityLabel": capacityLabel, "establishmentDescriptionLabel": establishmentDescriptionLabel, "establishmentLabel": establishmentLabel]
    var horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-cardMargin-[card]-cardMargin-|", options: [], metrics: metrics, views: views)
    contentView.addConstraints(horizontalConstraints)
    var verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[connector1(cardMargin)][card]|", options: [], metrics: metrics, views: views)
    contentView.addConstraints(verticalConstraints)
    verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[connector2(cardMargin)][card]", options: [], metrics: metrics, views: views)
    contentView.addConstraints(verticalConstraints)
    horizontalConstraints = [
      NSLayoutConstraint(item: connector1, attribute: .CenterX, relatedBy: .Equal, toItem: contentView, attribute: .CenterX, multiplier: 0.5, constant: 0),
      NSLayoutConstraint(item: connector1, attribute: .Width, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1, constant: 4)
    ]
    contentView.addConstraints(horizontalConstraints)
    horizontalConstraints = [
      NSLayoutConstraint(item: connector2, attribute: .CenterX, relatedBy: .Equal, toItem: contentView, attribute: .CenterX, multiplier: 1.5, constant: 0),
      NSLayoutConstraint(item: connector2, attribute: .Width, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1, constant: 4)
    ]
    contentView.addConstraints(horizontalConstraints)
    
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[stadiumImageView]|", options: [], metrics: metrics, views: views)
    card.addConstraints(horizontalConstraints)
    verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[stadiumImageView]|", options: [], metrics: metrics, views: views)
    card.addConstraints(verticalConstraints)
    
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:[overlayView]|", options: [], metrics: metrics, views: views)
    card.addConstraints(horizontalConstraints)
    horizontalConstraints = [NSLayoutConstraint(item: overlayView, attribute: .Width, relatedBy: .Equal, toItem: card, attribute: .Width, multiplier: 0.4, constant: 0)]
    card.addConstraints(horizontalConstraints)
    verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[overlayView]|", options: [], metrics: metrics, views: views)
    card.addConstraints(verticalConstraints)
    
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[container]|", options: [], metrics: metrics, views: views)
    overlayView.addConstraints(horizontalConstraints)
    verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[container]|", options: [], metrics: metrics, views: views)
    overlayView.addConstraints(verticalConstraints)
    
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-margin-[stadiumDescriptionLabel]-margin-|", options: [], metrics: metrics, views: views)
    container.addConstraints(horizontalConstraints)
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-margin-[nameDescriptionLabel]-margin-|", options: [], metrics: metrics, views: views)
    container.addConstraints(horizontalConstraints)
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-margin-[nameLabel]-margin-|", options: [], metrics: metrics, views: views)
    container.addConstraints(horizontalConstraints)
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-margin-[capacityDescriptionLabel]-margin-|", options: [], metrics: metrics, views: views)
    container.addConstraints(horizontalConstraints)
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-margin-[capacityLabel]-margin-|", options: [], metrics: metrics, views: views)
    container.addConstraints(horizontalConstraints)
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-margin-[establishmentDescriptionLabel]-margin-|", options: [], metrics: metrics, views: views)
    container.addConstraints(horizontalConstraints)
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-margin-[establishmentLabel]-margin-|", options: [], metrics: metrics, views: views)
    container.addConstraints(horizontalConstraints)
    verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|-margin2-[stadiumDescriptionLabel]-margin2-[nameDescriptionLabel][nameLabel]-margin-[capacityDescriptionLabel][capacityLabel]-margin-[establishmentDescriptionLabel][establishmentLabel]-margin2-|", options: [], metrics: metrics, views: views)
    container.addConstraints(verticalConstraints)
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    card.applyShadow()
  }
  
  func update(viewModel: GuideStadiumViewModel) {
    stadiumImageView.kf_setImageWithURL(NSURL(string: viewModel.stadiumURL)!)
    nameLabel.text = viewModel.stadiumName
    capacityLabel.text = viewModel.capacity
    establishmentLabel.text = viewModel.established
  }
}

extension GuideStadiumCell: Updatable {
  typealias ViewModel = GuideStadiumViewModel
}
