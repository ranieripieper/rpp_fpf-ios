//
//  MenuAnimator.swift
//  FPF
//
//  Created by Gilson Gil on 1/12/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

enum Status {
  case Presenting, Dismissing
}

final class MenuAnimator: UIPercentDrivenInteractiveTransition {
  private let menuWidthRatio: CGFloat = 0.8
  private let dimAlpha: CGFloat = 0.6
  
  private let navigationController: UINavigationController
  private var status: Status?
  private var dimView: UIView?
  private var formerSuperview: UIView?
  private var interactive = false
  
  init(navigationController: UINavigationController) {
    self.navigationController = navigationController
    super.init()
  }
  
  #if DEBUG
  deinit {
    print("deinit", self)
  }
  #endif
}

extension MenuAnimator: UIViewControllerAnimatedTransitioning {
  func transitionDuration(transitionContext: UIViewControllerContextTransitioning?) -> NSTimeInterval {
    return 0.3
  }
  
  func animateTransition(transitionContext: UIViewControllerContextTransitioning) {
    guard let status = status else {
      return
    }
    let fromViewController = transitionContext.viewControllerForKey(UITransitionContextFromViewControllerKey)!
    let toViewController = transitionContext.viewControllerForKey(UITransitionContextToViewControllerKey)!
    let fromView = fromViewController.view
    let toView = toViewController.view
    let containerView = transitionContext.containerView()!
    
    switch status {
    case .Presenting:
      dimView = UIView(frame: containerView.bounds)
      dimView?.backgroundColor = UIColor.blackColor()
      dimView?.alpha = 0
      formerSuperview = fromView.superview
      containerView.addSubview(fromView)
      containerView.addSubview(dimView!)
      containerView.addSubview(toView)
      let horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-left-[menu]|", options: [], metrics: ["left": (1 - menuWidthRatio) * containerView.bounds.width], views: ["menu": toView])
      let verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[menu]|", options: [], metrics: nil, views: ["menu": toView])
      containerView.addConstraints(horizontalConstraints)
      containerView.addConstraints(verticalConstraints)
      toView.frame = CGRect(x: containerView.bounds.width * (1 - menuWidthRatio), y: toView.frame.origin.y, width: menuWidthRatio * containerView.bounds.width, height: containerView.bounds.height)
      toView.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, toView.bounds.width, 0)
      UIView.animateWithDuration(transitionDuration(transitionContext), animations: {
        toView.transform = CGAffineTransformIdentity
        self.dimView?.alpha = self.dimAlpha
        }, completion: { finished in
          if !transitionContext.transitionWasCancelled() {
            let tap = UITapGestureRecognizer(target: self, action: #selector(MenuAnimator.didTap(_:)))
            self.dimView?.addGestureRecognizer(tap)
            let pan = UIPanGestureRecognizer(target: self, action: #selector(MenuAnimator.didPan(_:)))
            containerView.addGestureRecognizer(pan)
          } else {
            self.formerSuperview?.addSubview(fromView)
          }
          transitionContext.completeTransition(!transitionContext.transitionWasCancelled())
      })
    case .Dismissing:
      UIView.animateWithDuration(transitionDuration(transitionContext), animations: {
        self.dimView?.alpha = 0
        fromView.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, fromView.bounds.width, 0)
        }, completion: { finished in
          if !transitionContext.transitionWasCancelled() {
            self.dimView?.removeFromSuperview()
            self.formerSuperview?.addSubview(toView)
          }
          transitionContext.completeTransition(!transitionContext.transitionWasCancelled())
      })
    }
  }
  
  func openMenuWithPan(pan: UIPanGestureRecognizer) {
    guard let view = navigationController.view where navigationController.viewControllers.count == 1 else {
      return
    }
    
    switch pan.state {
    case .Began:
      let location = pan.locationInView(view)
      if location.x > view.bounds.width * 0.9 {
        interactive = true
      }
    case .Changed:
      let translation = pan.translationInView(view)
      let d = fabs(translation.x / view.bounds.width)
      updateInteractiveTransition(d)
    case .Ended:
      if pan.velocityInView(view).x < 0 {
        finishInteractiveTransition()
      } else {
        cancelInteractiveTransition()
      }
      interactive = false
    default:
      break
    }
  }
  
  func didPan(pan: UIPanGestureRecognizer) {
    guard let view = navigationController.view else {
      return
    }
    
    interactive = true
    switch pan.state {
    case .Began:
      navigationController.dismissViewControllerAnimated(true, completion: nil)
    case .Changed:
      let translation = pan.translationInView(view)
      let d = max(translation.x / (view.bounds.width * menuWidthRatio), 0)
      updateInteractiveTransition(d)
    case .Ended:
      if pan.velocityInView(view).x > 0 {
        finishInteractiveTransition()
      } else {
        cancelInteractiveTransition()
      }
      interactive = false
    default:
      break
    }
  }
  
  func didTap(tap: UITapGestureRecognizer) {
    dismiss()
  }
  
  func dismiss() {
    interactive = false
    navigationController.dismissViewControllerAnimated(true) {
      if let window = UIApplication.sharedApplication().delegate?.window, subviews = window?.subviews {
        for subview in subviews {
          if let subview = subview as? UIControl {
            window?.bringSubviewToFront(subview)
            continue
          }
        }
      }
    }
  }
}

extension MenuAnimator: UIViewControllerTransitioningDelegate {
  func animationControllerForPresentedController(presented: UIViewController, presentingController presenting: UIViewController, sourceController source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
    status = .Presenting
    
    return self
  }
  
  func animationControllerForDismissedController(dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
    status = .Dismissing
    
    return self
  }
  
  func interactionControllerForPresentation(animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
    status = .Presenting
    
    return interactive ? self : nil
  }
  
  func interactionControllerForDismissal(animator: UIViewControllerAnimatedTransitioning) -> UIViewControllerInteractiveTransitioning? {
    status = .Dismissing
    
    return interactive ? self : nil
  }
}
