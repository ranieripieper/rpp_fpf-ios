//
//  MenuButton.swift
//  FPF
//
//  Created by Gilson Gil on 1/15/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class MenuButton: UIButton {
  private let label: UILabel = {
    let label = UILabel()
    label.translatesAutoresizingMaskIntoConstraints = false
    label.textAlignment = .Center
    label.textColor = UIColor.whiteColor()
    label.font = UIFont.fpfMedium(12)
    return label
  }()
  
  override var selected: Bool {
    willSet(newValue) {
      if newValue {
        label.textColor = UIColor.fpfSunflowerColor()
        tintColor = UIColor.fpfSunflowerColor()
      } else {
        label.textColor = UIColor.whiteColor()
        tintColor = UIColor.whiteColor()
      }
    }
  }
  
  override var highlighted: Bool {
    willSet(newValue) {
      if selected {
        if newValue {
          label.textColor = UIColor.fpfSunflowerColor().colorWithAlphaComponent(0.5)
        } else {
          label.textColor = UIColor.fpfSunflowerColor()
        }
      } else {
        if newValue {
          label.textColor = UIColor(white: 1, alpha: 0.5)
        } else {
          label.textColor = UIColor.whiteColor()
        }
      }
    }
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }
  
  init(title: String, image: UIImage?) {
    super.init(frame: .zero)
    setUp()
    label.text = title
    setImage(image?.imageWithRenderingMode(.AlwaysTemplate), forState: .Normal)
    tintColor = UIColor.whiteColor()
  }
  
  private func setUp() {
    translatesAutoresizingMaskIntoConstraints = false
    backgroundColor = UIColor.clearColor()
    contentVerticalAlignment = .Top
    
    addSubview(label)
    
    let views = ["label": label]
    let horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[label]|", options: [], metrics: nil, views: views)
    addConstraints(horizontalConstraints)
    let verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:[label]|", options: [], metrics: nil, views: views)
    addConstraints(verticalConstraints)
    let heightConstraint = NSLayoutConstraint(item: self, attribute: .Height, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1, constant: 60)
    addConstraint(heightConstraint)
  }
  
  #if DEBUG
  deinit {
    print("deinit", self)
  }
  #endif
}
