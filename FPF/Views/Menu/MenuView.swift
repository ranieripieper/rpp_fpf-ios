//
//  MenuView.swift
//  FPF
//
//  Created by Gilson Gil on 1/8/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

protocol MenuViewDelegate: class {
  func pressedLeague(league: League)
  func guidePressed()
  func notificationsPressed()
  func aboutPressed()
}

final class MenuView: UIView {
  private let notificationsButton = MenuButton(title: Strings.MenuManageNotifications, image: UIImage.icnAlertsMenu())
  private let guideButton = MenuButton(title: Strings.MenuGuideName, image: UIImage.icnGuideMenu())
  private let aboutButton = MenuButton(title: Strings.MenuAbout, image: UIImage.icnAboutMenu())
  private let tableView = UITableView(frame: .zero, style: .Plain)
  
  weak var delegate: MenuViewDelegate?
  var menuViewModel: MenuViewModel {
    didSet {
      menuViewModel.configurators.forEach {
        $0.register(tableView)
      }
      tableView.reloadData()
      switch menuViewModel.selectedViewType {
      case .League(let id):
        let index = menuViewModel.leagues.indexOf { league in
          return league.id == id
        }
        if let index = index {
          tableView.selectRowAtIndexPath(NSIndexPath(forRow: index, inSection: 0), animated: true, scrollPosition: .None)
        }
      default:
        break
      }
    }
  }
  
  required init?(coder aDecoder: NSCoder) {
    return nil
  }
  
  init(menuViewModel: MenuViewModel) {
    self.menuViewModel = menuViewModel
    super.init(frame: .zero)
    setUp()
    menuViewModel.configurators.forEach {
      $0.register(tableView)
    }
    defer {
      NSNotificationCenter.defaultCenter().addObserver(self, selector: #selector(MenuView.updateMenuViewModel), name: Constants.updatedLeaguesNotificationString, object: nil)
      switch menuViewModel.selectedViewType {
      case .League(let id):
        let index = menuViewModel.leagues.indexOf { league in
          return league.id == id
        }
        if let index = index {
          tableView.selectRowAtIndexPath(NSIndexPath(forRow: index, inSection: 0), animated: false, scrollPosition: .None)
        }
      default:
        break
      }
    }
  }
  
  private func setUp() {
    translatesAutoresizingMaskIntoConstraints = false
    clipsToBounds = true
    backgroundColor = UIColor.fpfLightGreyColor()
    
    let bgImage = UIImage.imgBgMenu()
    let bgImageView = UIImageView(image: bgImage)
    bgImageView.translatesAutoresizingMaskIntoConstraints = false
    bgImageView.contentMode = .ScaleAspectFill
    bgImageView.userInteractionEnabled = true
    bgImageView.clipsToBounds = true
    addSubview(bgImageView)
    
    let logoImage = UIImage.imgLogoMenu()
    let logoImageView = UIImageView(image: logoImage)
    logoImageView.translatesAutoresizingMaskIntoConstraints = false
    logoImageView.contentMode = .Center
    bgImageView.addSubview(logoImageView)
    
    notificationsButton.addTarget(self, action: #selector(MenuView.notificationsPressed), forControlEvents: .TouchUpInside)
    notificationsButton.selected = menuViewModel.selectedViewType == .Notifications
    bgImageView.addSubview(notificationsButton)
    
    guideButton.addTarget(self, action: #selector(MenuView.guidePressed), forControlEvents: .TouchUpInside)
    guideButton.selected = menuViewModel.selectedViewType == .Guide
    bgImageView.addSubview(guideButton)
    
    aboutButton.addTarget(self, action: #selector(MenuView.aboutPressed), forControlEvents: .TouchUpInside)
    aboutButton.selected = menuViewModel.selectedViewType == .About
    bgImageView.addSubview(aboutButton)
    
    tableView.translatesAutoresizingMaskIntoConstraints = false
    tableView.backgroundColor = UIColor.fpfCharcoalColor()
    tableView.separatorStyle = .None
    tableView.dataSource = self
    tableView.delegate = self
    addSubview(tableView)
    
    let metrics = ["margin": Constants.defaultMargin]
    let views = ["bgImageView": bgImageView, "logoImageView": logoImageView, "notificationsButton": notificationsButton, "guideButton": guideButton, "aboutButton": aboutButton, "tableView": tableView]
    
    var horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[bgImageView]|", options: [], metrics: metrics, views: views)
    addConstraints(horizontalConstraints)
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[tableView]|", options: [], metrics: metrics, views: views)
    addConstraints(horizontalConstraints)
    var verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[bgImageView][tableView]|", options: [], metrics: metrics, views: views)
    addConstraints(verticalConstraints)
    
    horizontalConstraints = [
      NSLayoutConstraint(item: logoImageView, attribute: .CenterX, relatedBy: .Equal, toItem: bgImageView, attribute: .CenterX, multiplier: 1, constant: 0)
    ]
    bgImageView.addConstraints(horizontalConstraints)
    verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|-50-[logoImageView]", options: [], metrics: metrics, views: views)
    bgImageView.addConstraints(verticalConstraints)
    
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[notificationsButton][guideButton][aboutButton]|", options: [], metrics: metrics, views: views)
    bgImageView.addConstraints(horizontalConstraints)
    horizontalConstraints = [
      NSLayoutConstraint(item: notificationsButton, attribute: .Width, relatedBy: .Equal, toItem: guideButton, attribute: .Width, multiplier: 1, constant: 0),
      NSLayoutConstraint(item: guideButton, attribute: .Width, relatedBy: .Equal, toItem: aboutButton, attribute: .Width, multiplier: 1, constant: 0)
    ]
    bgImageView.addConstraints(horizontalConstraints)
    verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:[notificationsButton]-20-|", options: [], metrics: metrics, views: views)
    bgImageView.addConstraints(verticalConstraints)
    verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:[guideButton]-20-|", options: [], metrics: metrics, views: views)
    bgImageView.addConstraints(verticalConstraints)
    verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:[aboutButton]-20-|", options: [], metrics: metrics, views: views)
    bgImageView.addConstraints(verticalConstraints)
  }
  
  func updateMenuViewModel(notification: NSNotification) {
    guard let userInfo = notification.userInfo, leagues = userInfo["leagues"] as? [League] else {
      return
    }
    let menuViewModel = MenuViewModel(leagues: leagues, selectedViewType: self.menuViewModel.selectedViewType)
    self.menuViewModel = menuViewModel
  }
  
  #if DEBUG
  deinit {
    print("deinit", self)
  }
  #endif
}

extension MenuView {
  func notificationsPressed() {
    notificationsButton.selected = !notificationsButton.selected
    delegate?.notificationsPressed()
  }
  
  func guidePressed() {
    guideButton.selected = !guideButton.selected
    delegate?.guidePressed()
  }
  
  func aboutPressed() {
    aboutButton.selected = !aboutButton.selected
    delegate?.aboutPressed()
  }
}

extension MenuView: UITableViewDataSource {
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return menuViewModel.leagues.count ?? 0
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let configurator = menuViewModel.configurators[indexPath.row]
    let cell = tableView.dequeueReusableCellWithIdentifier(configurator.reuseIdentifier, forIndexPath: indexPath)
    configurator.update(cell)
    return cell
  }
}

extension MenuView: UITableViewDelegate {
  func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    if tableView.indexPathForSelectedRow == indexPath {
      return 60
    }
    return 50
  }
  
  func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
    guard indexPath.row < menuViewModel.leagues.count else {
      return
    }
    let league = menuViewModel.leagues[indexPath.row]
    delegate?.pressedLeague(league)
  }
}
