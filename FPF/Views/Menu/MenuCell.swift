//
//  MenuCell.swift
//  FPF
//
//  Created by Gilson Gil on 5/4/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class MenuCell: UITableViewCell {
  private let nameLabel = UILabel()
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setUp()
  }
  
  override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    setUp()
  }
  
  private func setUp() {
    backgroundColor = UIColor.fpfCharcoalColor()
    selectionStyle = .None
    
    nameLabel.translatesAutoresizingMaskIntoConstraints = false
    nameLabel.font = UIFont.fpfMedium(18)
    nameLabel.textAlignment = .Center
    nameLabel.textColor = UIColor.fpfMediumGreyColor()
    contentView.addSubview(nameLabel)
    
    let metrics = ["margin": 14]
    let views = ["nameLabel": nameLabel]
    let horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-[nameLabel]-|", options: [], metrics: nil, views: views)
    contentView.addConstraints(horizontalConstraints)
    let verticalConstraint = NSLayoutConstraint.constraintsWithVisualFormat("V:|-margin-[nameLabel]-margin-|", options: [], metrics: metrics, views: views)
    contentView.addConstraints(verticalConstraint)
  }
  
  func update(viewModel: MenuCellViewModel) {
    nameLabel.text = viewModel.name
  }
  
  override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
    super.touchesBegan(touches, withEvent: event)
    UIView.transitionWithView(nameLabel, duration: 0.3, options: .TransitionCrossDissolve, animations: { [weak self] in
      self?.nameLabel.textColor = UIColor.fpfSunflowerColor()
      }, completion: nil)
  }
  
  override func touchesCancelled(touches: Set<UITouch>?, withEvent event: UIEvent?) {
    super.touchesCancelled(touches, withEvent: event)
    nameLabel.textColor = UIColor.fpfMediumGreyColor()
  }
  
  override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {
    super.touchesEnded(touches, withEvent: event)
    nameLabel.textColor = UIColor.fpfMediumGreyColor()
  }
  
  override func setSelected(selected: Bool, animated: Bool) {
    if selected {
      backgroundColor = UIColor.fpfCharcoalTwoColor()
      nameLabel.font = UIFont.fpfMedium(22)
      nameLabel.textColor = UIColor.fpfSunflowerColor()
    } else {
      backgroundColor = UIColor.fpfCharcoalColor()
      nameLabel.font = UIFont.fpfMedium(18)
      nameLabel.textColor = UIColor.fpfMediumGreyColor()
    }
  }
}

extension MenuCell: Updatable {
  typealias ViewModel = MenuCellViewModel
}
