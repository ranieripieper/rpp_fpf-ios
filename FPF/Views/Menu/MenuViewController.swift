//
//  MenuViewController.swift
//  FPF
//
//  Created by Gilson Gil on 1/8/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

protocol MenuViewControllerDelegate: class {
  func showLeague(league: League)
  func showGuide()
  func showNotifications()
  func showAbout()
}

final class MenuViewController: UIViewController {
  private let menuView: MenuView
  weak var delegate: MenuViewControllerDelegate?
  
  required init?(coder aDecoder: NSCoder) {
    return nil
  }
  
  init(menuViewModel: MenuViewModel) {
    menuView = MenuView(menuViewModel: menuViewModel)
    super.init(nibName: nil, bundle: nil)
  }
  
  override func loadView() {
    view = menuView
    menuView.delegate = self
  }
  
  override func viewDidAppear(animated: Bool) {
    super.viewDidAppear(animated)
    UIApplication.sharedApplication().statusBarHidden = true
  }
  
  override func viewWillDisappear(animated: Bool) {
    super.viewWillDisappear(animated)
    UIApplication.sharedApplication().statusBarHidden = false
  }
  
  #if DEBUG
  deinit {
    print("deinit", self)
  }
  #endif
}

extension MenuViewController: MenuViewDelegate {
  func pressedLeague(league: League) {
    delegate?.showLeague(league)
  }
  
  func guidePressed() {
    delegate?.showGuide()
  }
  
  func notificationsPressed() {
    delegate?.showNotifications()
  }
  
  func aboutPressed() {
    delegate?.showAbout()
  }
}
