//
//  ScorersViewController.swift
//  FPF
//
//  Created by Gilson Gil on 1/8/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class ScorersViewController: UIViewController, LeagueViewControllerProtocol {
  private let scorersView = ScorersView()
  
  var scorersViewModel: ScorersViewModel? {
    didSet {
      scorersView.scorersViewModel = scorersViewModel
    }
  }
  
  weak var leagueViewControllerDelegate: LeagueViewControllerDelegate?
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }
  
  init() {
    super.init(nibName: nil, bundle: nil)
  }
  
  override func loadView() {
    view = scorersView
  }
  
  #if DEBUG
  deinit {
    print("deinit", self)
  }
  #endif
}
