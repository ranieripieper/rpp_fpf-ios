//
//  ScorersCell.swift
//  FPF
//
//  Created by Gilson Gil on 1/14/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit
import Kingfisher

final class ScorersCell: UITableViewCell {
  private let positionLabel = UILabel()
  private let logoImageView = UIImageView()
  private let nameLabel = UILabel()
  private let teamNameLabel = UILabel()
  private let goalsLabel = UILabel()
  
  var viewModel: ScorersCellViewModel? {
    didSet {
      if let viewModel = viewModel {
        positionLabel.text = viewModel.position
      }
      if let URL = NSURL(string: viewModel?.teamLogoURL ?? "") {
        logoImageView.kf_setImageWithURL(URL, placeholderImage: Image.icnPlaceholder())
      }
      nameLabel.text = viewModel?.playerName
      teamNameLabel.text = viewModel?.teamName
      goalsLabel.text = viewModel?.goals
    }
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setUp()
  }
  
  override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    setUp()
  }
  
  private func setUp() {
    contentView.translatesAutoresizingMaskIntoConstraints = false
    selectionStyle = .None
    
    positionLabel.translatesAutoresizingMaskIntoConstraints = false
    positionLabel.font = UIFont.fpfCondensedBold(14)
    positionLabel.textColor = UIColor.fpfBlackColor()
    positionLabel.textAlignment = .Center
    contentView.addSubview(positionLabel)
    
    logoImageView.translatesAutoresizingMaskIntoConstraints = false
    logoImageView.contentMode = .ScaleAspectFit
    contentView.addSubview(logoImageView)
    
    nameLabel.translatesAutoresizingMaskIntoConstraints = false
    nameLabel.font = UIFont.fpfMedium(16)
    nameLabel.textColor = UIColor.fpfBlackColor()
    contentView.addSubview(nameLabel)
    
    teamNameLabel.translatesAutoresizingMaskIntoConstraints = false
    teamNameLabel.font = UIFont.fpfMedium(12)
    teamNameLabel.textColor = UIColor.fpfPinkishGreyColor()
    contentView.addSubview(teamNameLabel)
    
    goalsLabel.translatesAutoresizingMaskIntoConstraints = false
    goalsLabel.font = UIFont.fpfMedium(16)
    goalsLabel.textColor = UIColor.fpfPinkishGreyColor()
    goalsLabel.textAlignment = .Center
    contentView.addSubview(goalsLabel)
    
    let ballImage = UIImage.icnSoccerBall()
    let ballImageView = UIImageView(image: ballImage)
    ballImageView.translatesAutoresizingMaskIntoConstraints = false
    ballImageView.contentMode = .Center
    contentView.addSubview(ballImageView)
    
    let metrics = ["margin": Constants.defaultMargin, "doubleMargin": Constants.defaultMargin * 2, "halfMargin": Constants.defaultMargin / 2, "logoWidth": Constants.smallLogoWidth]
    let views = ["positionLabel": positionLabel, "logoImageView": logoImageView, "nameLabel": nameLabel, "teamNameLabel": teamNameLabel, "goalsLabel": goalsLabel, "ballImageView": ballImageView]
    var horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-margin-[positionLabel]-doubleMargin-[logoImageView(logoWidth)]-doubleMargin-[nameLabel]->=margin-[goalsLabel]-margin-[ballImageView]-margin-|", options: [], metrics: metrics, views: views)
    contentView.addConstraints(horizontalConstraints)
    
    horizontalConstraints = [NSLayoutConstraint(item: logoImageView, attribute: .Width, relatedBy: .Equal, toItem: logoImageView, attribute: .Height, multiplier: 1, constant: 0)]
    logoImageView.addConstraints(horizontalConstraints)
    
    horizontalConstraints = [
      NSLayoutConstraint(item: nameLabel, attribute: .Left, relatedBy: .Equal, toItem: teamNameLabel, attribute: .Left, multiplier: 1, constant: 0),
      NSLayoutConstraint(item: nameLabel, attribute: .Right, relatedBy: .Equal, toItem: teamNameLabel, attribute: .Right, multiplier: 1, constant: 0)
    ]
    contentView.addConstraints(horizontalConstraints)
    
    [positionLabel, goalsLabel, ballImageView].forEach { view in
      let verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|-doubleMargin-[view]-doubleMargin-|", options: [], metrics: metrics, views: ["view": view])
      contentView.addConstraints(verticalConstraints)
    }
    
    var verticalConstraints = [NSLayoutConstraint(item: logoImageView, attribute: .CenterY, relatedBy: .Equal, toItem: contentView, attribute: .CenterY, multiplier: 1, constant: 0)]
    contentView.addConstraints(verticalConstraints)
    
    verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|-doubleMargin-[nameLabel]-margin-[teamNameLabel]-doubleMargin-|", options: [], metrics: metrics, views: views)
    contentView.addConstraints(verticalConstraints)
    
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[contentView]|", options: [], metrics: nil, views: ["contentView": contentView])
    addConstraints(horizontalConstraints)
    verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[contentView]|", options: [], metrics: nil, views: ["contentView": contentView])
    addConstraints(verticalConstraints)
  }
  
  func configureWithScorersCellViewModel(viewModel: ScorersCellViewModel, index: Int) {
    self.viewModel = viewModel
    if index % 2 == 0 {
      contentView.backgroundColor = UIColor.whiteColor()
    } else {
      contentView.backgroundColor = UIColor.fpfLightGreyColor()
    }
  }
  
  #if DEBUG
  deinit {
    print("deinit", self)
  }
  #endif
}

extension ScorersCell: ReuseIdentifier {
  static func reuseIdentifier() -> String {
    return "ScorersCell"
  }
}
