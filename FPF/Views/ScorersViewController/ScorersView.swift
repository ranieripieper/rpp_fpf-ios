//
//  ScorersView.swift
//  FPF
//
//  Created by Gilson Gil on 1/8/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class ScorersView: UIView {
  private let tableView = UITableView()
  
  var scorersViewModel: ScorersViewModel? {
    didSet {
      dispatch_async(dispatch_get_main_queue()) { [weak self] in
        self?.tableView.reloadData()
      }
    }
  }
  
  weak var scrollDelegate: LeagueScrollDelegate?
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setUp()
  }
  
  init() {
    super.init(frame: .zero)
    setUp()
  }
  
  private func setUp() {
    translatesAutoresizingMaskIntoConstraints = false
    backgroundColor = UIColor.clearColor()
    
    tableView.translatesAutoresizingMaskIntoConstraints = false
    tableView.backgroundColor = UIColor.clearColor()
    tableView.dataSource = self
    tableView.delegate = self
    tableView.separatorStyle = .None
    tableView.allowsSelection = false
    tableView.estimatedRowHeight = 80
    tableView.registerClass(ScorersCell.classForCoder(), forCellReuseIdentifier: ScorersCell.reuseIdentifier())
    addSubview(tableView)
    
    let views = ["tableView": tableView]
    let horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[tableView]|", options: [], metrics: nil, views: views)
    let verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[tableView]|", options: [], metrics: nil, views: views)
    addConstraints(horizontalConstraints)
    addConstraints(verticalConstraints)
  }
  
  #if DEBUG
  deinit {
    print("deinit", self)
  }
  #endif
}

extension ScorersView: UITableViewDataSource {
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return scorersViewModel?.scorers.count ?? 0
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCellWithIdentifier(ScorersCell.reuseIdentifier(), forIndexPath: indexPath) as! ScorersCell
    if let scorersViewModel = scorersViewModel where indexPath.row < scorersViewModel.scorers.count {
      cell.configureWithScorersCellViewModel(scorersViewModel.scorers[indexPath.row], index: indexPath.row)
    }
    return cell
  }
}

extension ScorersView: UITableViewDelegate {
  func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    return UITableViewAutomaticDimension
  }
}

extension ScorersView: UIScrollViewDelegate {
  func scrollViewDidScroll(scroll: UIScrollView) {
    scrollDelegate?.scrollViewDidScroll(scroll)
  }
  
  func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
    scrollDelegate?.scrollViewDidStop(scrollView)
  }
  
  func scrollViewDidEndDragging(scrollView: UIScrollView, willDecelerate decelerate: Bool) {
    if !decelerate {
      scrollDelegate?.scrollViewDidStop(scrollView)
    }
  }
}

extension ScorersView: LeagueScrollProtocol {
  
}
