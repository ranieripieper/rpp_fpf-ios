//
//  AboutViewController.swift
//  FPF
//
//  Created by Gilson Gil on 1/12/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class AboutViewController: UIViewController {
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }
  
  init() {
    super.init(nibName: nil, bundle: nil)
    automaticallyAdjustsScrollViewInsets = false
  }
  
  override func loadView() {
    let aboutView = AboutView()
    view = aboutView
    navigationItem.title = Strings.AboutTitle
    
    let menuBarButton = UIBarButtonItem(image: UIImage.icnMenu(), style: .Plain, target: navigationController, action: #selector(MainViewController.presentMenu))
    navigationItem.rightBarButtonItems = [menuBarButton]
  }
  
  #if DEBUG
  deinit {
    print("deinit", self)
  }
  #endif
}
