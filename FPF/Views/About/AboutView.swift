//
//  AboutView.swift
//  FPF
//
//  Created by Gilson Gil on 1/12/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class AboutView: UIView {
  private let topInset: CGFloat = 50
  private let sideInset: CGFloat = 40
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    setUp()
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setUp()
  }
  
  convenience init() {
    self.init(frame: .zero)
  }
  
  private func setUp() {
    translatesAutoresizingMaskIntoConstraints = false
    backgroundColor = UIColor.fpfBlackColor()
    
    let horizontalConstraint = NSLayoutConstraint(item: self, attribute: .Width, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1, constant: UIScreen.mainScreen().bounds.width)
    let verticalConstraint = NSLayoutConstraint(item: self, attribute: .Height, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1, constant: UIScreen.mainScreen().bounds.height)
    addConstraint(horizontalConstraint)
    addConstraint(verticalConstraint)
    
    let scrollView = UIScrollView()
    scrollView.translatesAutoresizingMaskIntoConstraints = false
    scrollView.backgroundColor = UIColor.whiteColor()
    addSubview(scrollView)
    
    let scrollViewContentView = UIView()
    scrollViewContentView.translatesAutoresizingMaskIntoConstraints = false
    scrollViewContentView.setContentHuggingPriority(UILayoutPriorityRequired, forAxis: .Vertical)
    scrollView.addSubview(scrollViewContentView)
    
    let logoImage = UIImage.imgAboutLogo()
    let logoImageView = UIImageView(image: logoImage)
    logoImageView.translatesAutoresizingMaskIntoConstraints = false
    logoImageView.contentMode = .Center
    scrollViewContentView.addSubview(logoImageView)
    
    let label = UILabel()
    label.translatesAutoresizingMaskIntoConstraints = false
    let text1 = Strings.AboutText1
    let text2 = Strings.AboutText2
    let paragraphStyle = NSMutableParagraphStyle()
    paragraphStyle.alignment = .Center
    let attributedText = NSMutableAttributedString(string: text1 + text2, attributes: [NSForegroundColorAttributeName: UIColor.fpfCementColor(), NSFontAttributeName: UIFont.fpfRegular(14)!, NSParagraphStyleAttributeName: paragraphStyle])
    attributedText.addAttributes([NSFontAttributeName: UIFont.fpfBold(14)!], range: (attributedText.string as NSString).rangeOfString(text2))
    label.attributedText = attributedText
    label.numberOfLines = 0
    label.setContentHuggingPriority(UILayoutPriorityDefaultLow, forAxis: .Vertical)
    scrollViewContentView.addSubview(label)
    
    let footLabel = UILabel()
    footLabel.translatesAutoresizingMaskIntoConstraints = false
    footLabel.numberOfLines = 0
    footLabel.textAlignment = .Center
    footLabel.textColor = UIColor.fpfGreyishColor()
    footLabel.text = Strings.AboutText3
    footLabel.font = UIFont.fpfRegular(12)
    scrollViewContentView.addSubview(footLabel)
    
    let metrics = ["topInset": topInset, "margin": Constants.defaultMargin, "sideInset": sideInset]
    let views = ["scrollView": scrollView, "scrollViewContentView": scrollViewContentView, "logoImageView": logoImageView, "label": label, "footLabel": footLabel]
    var horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[scrollView]|", options: [], metrics: nil, views: views)
    var verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|-64-[scrollView]|", options: [], metrics: nil, views: views)
    addConstraints(horizontalConstraints)
    addConstraints(verticalConstraints)
    
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[scrollViewContentView]|", options: [], metrics: nil, views: views)
    verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[scrollViewContentView]|", options: [], metrics: nil, views: views)
    scrollView.addConstraints(horizontalConstraints)
    scrollView.addConstraints(verticalConstraints)
    
    horizontalConstraints = [NSLayoutConstraint(item: scrollViewContentView, attribute: .Width, relatedBy: .Equal, toItem: self, attribute: .Width, multiplier: 1, constant: 0)]
    addConstraints(horizontalConstraints)
    
    verticalConstraints = [NSLayoutConstraint(item: scrollViewContentView, attribute: .Height, relatedBy: .GreaterThanOrEqual, toItem: self, attribute: .Height, multiplier: 1, constant: -64)]
    addConstraints(verticalConstraints)
    
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[logoImageView]|", options: [], metrics: nil, views: views)
    scrollViewContentView.addConstraints(horizontalConstraints)
    
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-sideInset-[label]-sideInset-|", options: [], metrics: metrics, views: views)
    scrollViewContentView.addConstraints(horizontalConstraints)
    
    verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|-topInset-[logoImageView]-topInset-[label]->=sideInset-[footLabel]-20-|", options: [], metrics: metrics, views: views)
    scrollViewContentView.addConstraints(verticalConstraints)
    
    verticalConstraints = [NSLayoutConstraint(item: logoImageView, attribute: .Height, relatedBy: .Equal, toItem: label, attribute: .Height, multiplier: 0.667, constant: 0)]
    scrollViewContentView.addConstraints(verticalConstraints)
    
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-sideInset-[footLabel]-sideInset-|", options: [], metrics: metrics, views: views)
    scrollViewContentView.addConstraints(horizontalConstraints)
  }
  
  #if DEBUG
  deinit {
    print("deinit", self)
  }
  #endif
}
