//
//  NotificationsView.swift
//  FPF
//
//  Created by Gilson Gil on 1/12/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class NotificationsView: UIView {
  private let tableView = UITableView()
  
  var notificationsViewModel: NotificationsViewModel? {
    didSet {
      notificationsViewModel?.delegate = self
    }
  }
  
  override init(frame: CGRect) {
    super.init(frame: frame)
    setUp()
  }
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setUp()
  }
  
  convenience init() {
    self.init(frame: .zero)
  }
  
  private func setUp() {
    translatesAutoresizingMaskIntoConstraints = false
    backgroundColor = UIColor.fpfBlackColor()
    
    let widthConstraints = NSLayoutConstraint(item: self, attribute: .Width, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1, constant: UIScreen.mainScreen().bounds.width)
    let heightConstraints = NSLayoutConstraint(item: self, attribute: .Height, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1, constant: UIScreen.mainScreen().bounds.height)
    addConstraints([widthConstraints, heightConstraints])
    
    tableView.translatesAutoresizingMaskIntoConstraints = false
    tableView.dataSource = self
    tableView.delegate = self
    tableView.rowHeight = UITableViewAutomaticDimension
    tableView.allowsSelection = false
    tableView.separatorStyle = .None
    tableView.registerClass(NotificationsHeader.classForCoder(), forHeaderFooterViewReuseIdentifier: NotificationsHeader.reuseIdentifier())
    tableView.registerClass(NotificationsHeaderCell.classForCoder(), forCellReuseIdentifier: NotificationsHeaderCell.reuseIdentifier())
    tableView.registerClass(NotificationsCell.classForCoder(), forCellReuseIdentifier: NotificationsCell.reuseIdentifier())
    addSubview(tableView)

    let views = ["tableView": tableView]
    let horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[tableView]|", options: [], metrics: nil, views: views)
    let verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|-64-[tableView]|", options: [], metrics: nil, views: views)
    addConstraints(horizontalConstraints)
    addConstraints(verticalConstraints)
  }
  
  #if DEBUG
  deinit {
    print("deinit", self)
  }
  #endif
}

extension NotificationsView: UITableViewDataSource {
  func numberOfSectionsInTableView(tableView: UITableView) -> Int {
    return 3
  }
  
  func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    if section == 1 {
      let notificationsHeader = tableView.dequeueReusableHeaderFooterViewWithIdentifier(NotificationsHeader.reuseIdentifier()) as! NotificationsHeader
      notificationsHeader.configureWithTeamsCount(notificationsViewModel?.onNotifications.count ?? 0)
      return notificationsHeader
    }
    return nil
  }
  
  func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    switch section {
    case 0:
      return 1
    case 1:
      return notificationsViewModel?.onNotifications.count ?? 0
    case 2:
      return notificationsViewModel?.offNotifications.count ?? 0
    default:
      return 0
    }
  }
  
  func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    switch (indexPath.section, indexPath.row) {
    case (0, _):
      let cell = tableView.dequeueReusableCellWithIdentifier(NotificationsHeaderCell.reuseIdentifier(), forIndexPath: indexPath) as! NotificationsHeaderCell
      if notificationsViewModel?.onNotifications.count > 0 {
        cell.button?.setState(.Save, animated: false)
      } else {
        cell.button?.setState(.Later, animated: false)
      }
      return cell
    case let (section, row):
      guard let cell = tableView.dequeueReusableCellWithIdentifier(NotificationsCell.reuseIdentifier(), forIndexPath: indexPath) as? NotificationsCell else {
        return UITableViewCell()
      }
      if section == 1 {
        cell.configureWithNotificationCellViewModel(notificationsViewModel!.onNotifications[row])
        cell.enable(true)
      } else {
        cell.configureWithNotificationCellViewModel(notificationsViewModel!.offNotifications[row])
        cell.enable(notificationsViewModel?.onNotifications.count < Constants.maxTeamSelection)
      }
      cell.delegate = self
      return cell
    }
  }
}

extension NotificationsView: UITableViewDelegate {
  func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
    switch section {
    case 1:
      if notificationsViewModel?.onNotifications.count == 0 {
        return 0
      } else {
        return 30
      }
    case 2:
      if notificationsViewModel?.offNotifications.count == 0 {
        return 0
      } else {
        return 30
      }
    default:
      return 0
    }
  }

  func tableView(tableView: UITableView, estimatedHeightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
    if indexPath.section == 0 {
      return 200
    } else {
      return 50
    }
  }
}

extension NotificationsView: NotificationsCellDelegate {
  func didSetCell(cell: NotificationsCell, on: Bool) {
    guard let indexPath = tableView.indexPathForCell(cell) else {
      return
    }
    
    if indexPath.section == 1 && !on {
      if let newIndex = notificationsViewModel?.newIndexAfterOffAtIndex(indexPath.row) {
        tableView.moveRowAtIndexPath(indexPath, toIndexPath: NSIndexPath(forRow: newIndex, inSection: 2))
      }
    } else if indexPath.section == 2 && on {
      if let newIndex = notificationsViewModel?.newIndexAfterOnAtIndex(indexPath.row) {
        tableView.moveRowAtIndexPath(indexPath, toIndexPath: NSIndexPath(forRow: newIndex, inSection: 1))
      }
    }
    let newCount = notificationsViewModel?.onNotifications.count ?? 0
    if let header = tableView.headerViewForSection(1) as? NotificationsHeader {
      header.configureWithTeamsCount(newCount)
    }
    if let headerCell = tableView.cellForRowAtIndexPath(NSIndexPath(forRow: 0, inSection: 0)) as? NotificationsHeaderCell {
      if newCount == 0 {
        headerCell.changeButtonToLater()
      } else if newCount == 1 {
        headerCell.changeButtonToSave()
      }
    }
    tableView.visibleCells.forEach {
      if let cell = $0 as? NotificationsCell, indexPath = tableView.indexPathForCell(cell) where indexPath.section == 2 {
        cell.enable(newCount < Constants.maxTeamSelection)
      }
    }
  }
}

extension NotificationsView: NotificationsViewModelDelegate {
  func didChange() {
    dispatch_async(dispatch_get_main_queue()) { [weak self] in
      self?.tableView.reloadData()
    }
  }
}
