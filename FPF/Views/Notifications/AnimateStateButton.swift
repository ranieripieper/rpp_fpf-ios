//
//  AnimateStateButton.swift
//  FPF
//
//  Created by Gilson Gil on 1/19/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

enum AnimateState {
  case Later, Save
}

final class AnimateStateButton: UIButton {
  private let animationDuration: NSTimeInterval = 0.5
  
  var animateState: AnimateState = .Later
  var laterCallback: (() -> ())?
  var saveCallback: (() -> ())?
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }
  
  init() {
    super.init(frame: .zero)
    translatesAutoresizingMaskIntoConstraints = false
    backgroundColor = UIColor.fpfDarkRedColor()
    setTitle(Strings.NotificationsChooseLater, forState: .Normal)
    setTitleColor(UIColor.whiteColor(), forState: .Normal)
    titleLabel?.font = UIFont.fpfMedium(14)
    addTarget(self, action: #selector(AnimateStateButton.touchUpInside), forControlEvents: .TouchUpInside)
    titleEdgeInsets = UIEdgeInsets(top: 8, left: 22, bottom: 8, right: 22)
  }
  
  func touchUpInside() {
    switch animateState {
    case .Later:
      laterCallback?()
    case .Save:
      saveCallback?()
    }
  }
  
  func setState(state: AnimateState, animated: Bool) {
    dispatch_async(dispatch_get_main_queue()) { [weak self] in
      self?.animateState = state
      switch state {
      case .Later:
        let closure = {
          self?.setTitle(Strings.NotificationsChooseLater, forState: .Normal)
        }
        if animated {
          if let weakSelf = self {
            UIView.animateWithDuration(weakSelf.animationDuration, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .CurveEaseInOut, animations: {
              weakSelf.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, weakSelf.superview!.bounds.width, 0)
              }, completion: { _ in
                closure()
                weakSelf.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, -weakSelf.superview!.bounds.width, 0)
                UIView.animateWithDuration(weakSelf.animationDuration, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .CurveEaseInOut, animations: {
                  weakSelf.transform = CGAffineTransformIdentity
                  }, completion: nil)
            })
          }
        } else {
          closure()
        }
      case .Save:
        let closure = {
          self?.setTitle(Strings.NotificationsSaveTeams, forState: .Normal)
        }
        if animated {
          if let weakSelf = self {
            UIView.animateWithDuration(weakSelf.animationDuration, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .CurveEaseInOut, animations: {
              weakSelf.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, -weakSelf.superview!.bounds.width, 0)
              }, completion: { _ in
                closure()
                weakSelf.transform = CGAffineTransformTranslate(CGAffineTransformIdentity, weakSelf.superview!.bounds.width, 0)
                UIView.animateWithDuration(weakSelf.animationDuration, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .CurveEaseInOut, animations: {
                  weakSelf.transform = CGAffineTransformIdentity
                  }, completion: nil)
            })
          }
        } else {
          closure()
        }
      }
    }
  }

  override func intrinsicContentSize() -> CGSize {
    var size = super.intrinsicContentSize()
    size.width += titleEdgeInsets.left + titleEdgeInsets.right
    size.height += titleEdgeInsets.top + titleEdgeInsets.bottom
    return size
  }
  
  #if DEBUG
  deinit {
    print("deinit", self)
  }
  #endif
}
