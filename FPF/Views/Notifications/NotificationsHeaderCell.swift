//
//  NotificationsHeaderCell.swift
//  FPF
//
//  Created by Gilson Gil on 1/13/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

enum ButtonState {
  case Later, Save
}

final class NotificationsHeaderCell: UITableViewCell {
  private let topMargin: CGFloat = 30
  private let midMargin: CGFloat = 30
  private let bottomMargin: CGFloat = 40
  private let labelMargin: CGFloat = 40
  
  var button: AnimateStateButton?
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setUp()
  }
  
  override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    setUp()
  }
  
  private func setUp() {
    contentView.translatesAutoresizingMaskIntoConstraints = false
    
    let iconImage = UIImage.icnAlerts()
    let imageView = UIImageView(image: iconImage)
    imageView.translatesAutoresizingMaskIntoConstraints = false
    imageView.contentMode = .Center
    contentView.addSubview(imageView)
    
    let label = UILabel()
    label.translatesAutoresizingMaskIntoConstraints = false
    label.text = Strings.NotificationsHeader
    label.textColor = UIColor.fpfCementColor()
    label.textAlignment = .Center
    label.numberOfLines = 0
    label.font = UIFont.fpfRegular(14)
    contentView.addSubview(label)
    
    if !NSUserDefaults.standardUserDefaults().boolForKey(Constants.hasPresentedNotificationsSettingsString) {
      button = AnimateStateButton()
      button?.laterCallback = laterTapped
      button?.saveCallback = saveTapped
      contentView.addSubview(button!)
      
      let centerConstraint = NSLayoutConstraint(item: button!, attribute: .CenterX, relatedBy: .Equal, toItem: contentView, attribute: .CenterX, multiplier: 1, constant: 0)
      contentView.addConstraint(centerConstraint)
      let widthConstraint = NSLayoutConstraint(item: button!, attribute: .Width, relatedBy: .Equal, toItem: nil, attribute: .NotAnAttribute, multiplier: 1, constant: 150)
      button!.addConstraint(widthConstraint)
    }
    
    let metrics = ["topMargin": topMargin, "midMargin": midMargin, "bottomMargin": bottomMargin, "labelMargin": labelMargin]
    let views = ["imageView": imageView, "label": label]
    var horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[imageView]|", options: [], metrics: metrics, views: views)
    contentView.addConstraints(horizontalConstraints)
    
    if let button = button {
      let verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|-topMargin-[imageView]-midMargin-[label]-midMargin-[button]-bottomMargin-|", options: [], metrics: metrics, views: ["imageView": imageView, "label": label, "button": button])
      contentView.addConstraints(verticalConstraints)
    } else {
      let verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|-topMargin-[imageView]-midMargin-[label]-bottomMargin-|", options: [], metrics: metrics, views: views)
      contentView.addConstraints(verticalConstraints)
    }
    
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-labelMargin-[label]-labelMargin-|", options: [], metrics: metrics, views: views)
    contentView.addConstraints(horizontalConstraints)
    
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[contentView]|", options: [], metrics: nil, views: ["contentView": contentView])
    addConstraints(horizontalConstraints)
    let verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[contentView]|", options: [], metrics: nil, views: ["contentView": contentView])
    addConstraints(verticalConstraints)
  }
  
  func laterTapped() {
    if let appDelegate = UIApplication.sharedApplication().delegate as? AppDelegate {
      appDelegate.dismissNotificationsViewController()
    }
  }
  
  func saveTapped() {
    if let appDelegate = UIApplication.sharedApplication().delegate as? AppDelegate {
      appDelegate.dismissNotificationsViewController()
    }
  }
  
  func changeButtonToLater() {
    guard button?.animateState != .Later else {
      return
    }
    button?.setState(.Later, animated: true)
  }
  
  func changeButtonToSave() {
    guard button?.animateState != .Save else {
      return
    }
    button?.setState(.Save, animated: true)
  }
  
  #if DEBUG
  deinit {
    print("deinit", self)
  }
  #endif
}

extension NotificationsHeaderCell: ReuseIdentifier {
  static func reuseIdentifier() -> String {
    return "NotificationsHeaderCell"
  }
}
