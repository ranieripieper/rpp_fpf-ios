//
//  NotificationsViewController.swift
//  FPF
//
//  Created by Gilson Gil on 1/12/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class NotificationsViewController: UIViewController {
  private let notificationsViewModel = NotificationsViewModel()
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
  }
  
  init() {
    super.init(nibName: nil, bundle: nil)
    automaticallyAdjustsScrollViewInsets = false
    
    if NSUserDefaults.standardUserDefaults().boolForKey(Constants.hasPresentedNotificationsSettingsString) {
      let menuBarButton = UIBarButtonItem(image: UIImage.icnMenu(), style: .Plain, target: navigationController, action: #selector(MainViewController.presentMenu))
      navigationItem.rightBarButtonItems = [menuBarButton]
    }
  }
  
  override func loadView() {
    let notificationsView = NotificationsView()
    notificationsView.notificationsViewModel = notificationsViewModel
    view = notificationsView
    navigationItem.title = Strings.NotificationsTitle
  }
  
  #if DEBUG
  deinit {
    print("deinit", self)
  }
  #endif
}
