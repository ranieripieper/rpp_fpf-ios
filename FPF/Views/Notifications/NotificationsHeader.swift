//
//  NotificationsHeader.swift
//  FPF
//
//  Created by Gilson Gil on 1/13/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final class NotificationsHeader: UITableViewHeaderFooterView {
  private let leftLabel = UILabel()
  private let rightLabel = UILabel()
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setUp()
  }
  
  override init(reuseIdentifier: String?) {
    super.init(reuseIdentifier: reuseIdentifier)
    setUp()
  }
  
  private func setUp() {
    contentView.translatesAutoresizingMaskIntoConstraints = false
    contentView.backgroundColor = UIColor.fpfLightGreyColor()
    
    leftLabel.translatesAutoresizingMaskIntoConstraints = false
    leftLabel.textColor = UIColor.fpfCementColor()
    leftLabel.font = UIFont.fpfBold(14)
    contentView.addSubview(leftLabel)
    
    rightLabel.translatesAutoresizingMaskIntoConstraints = false
    rightLabel.textColor = UIColor.fpfCementColor()
    rightLabel.font = UIFont.fpfBold(14)
    contentView.addSubview(rightLabel)
    
    let metrics = ["margin": Constants.defaultMargin]
    let views = ["leftLabel": leftLabel, "rightLabel": rightLabel]
    var horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-margin-[leftLabel]", options: [], metrics: metrics, views: views)
    var verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[leftLabel]|", options: [], metrics: metrics, views: views)
    contentView.addConstraints(horizontalConstraints)
    contentView.addConstraints(verticalConstraints)
    
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:[rightLabel]-margin-|", options: [], metrics: metrics, views: views)
    contentView.addConstraints(horizontalConstraints)
    
    verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[rightLabel]|", options: [], metrics: metrics, views: views)
    contentView.addConstraints(verticalConstraints)
    
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[contentView]|", options: [], metrics: nil, views: ["contentView": contentView])
    addConstraints(horizontalConstraints)
    verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[contentView]|", options: [], metrics: nil, views: ["contentView": contentView])
    addConstraints(verticalConstraints)
  }
  
  func configureWithTeamsCount(count: Int) {
    if count == 0 {
      leftLabel.text = ""
    } else {
      leftLabel.text = Strings.NotificationsTeamHeader
    }
    rightLabel.text = "\(count) de \(Constants.maxTeamSelection)"
  }
  
  #if DEBUG
  deinit {
    print("deinit", self)
  }
  #endif
}

extension NotificationsHeader: ReuseIdentifier {
  static func reuseIdentifier() -> String {
    return "NotificationsHeader"
  }
}
