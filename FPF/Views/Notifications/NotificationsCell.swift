//
//  NotificationsCell.swift
//  FPF
//
//  Created by Gilson Gil on 1/13/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit
import Kingfisher

protocol NotificationsCellDelegate: class {
  func didSetCell(cell: NotificationsCell, on: Bool)
}

final class NotificationsCell: UITableViewCell {
  private let logoImageView = UIImageView()
  private let label = UILabel()
  private let aSwitch = UISwitch()
  var viewModel: NotificationsCellViewModel? {
    didSet {
      dispatch_async(dispatch_get_main_queue()) { [weak self] in
        if let URL = NSURL(string: self?.viewModel?.logoURLString ?? "") {
          self?.logoImageView.kf_setImageWithURL(URL, placeholderImage: UIImage.icnPlaceholder())
        }
        self?.label.text = self?.viewModel?.name
        self?.aSwitch.on = self?.viewModel?.switchOn ?? false
      }
    }
  }
  weak var delegate: NotificationsCellDelegate?
  
  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setUp()
  }
  
  override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    setUp()
  }
  
  private func setUp() {
    contentView.translatesAutoresizingMaskIntoConstraints = false
    selectionStyle = .None
    
    logoImageView.translatesAutoresizingMaskIntoConstraints = false
    logoImageView.contentMode = .ScaleAspectFit
    contentView.addSubview(logoImageView)
    
    label.translatesAutoresizingMaskIntoConstraints = false
    label.font = UIFont.fpfRegular(16)
    label.textColor = UIColor.fpfBlackColor()
    contentView.addSubview(label)
    
    aSwitch.translatesAutoresizingMaskIntoConstraints = false
    aSwitch.onTintColor = UIColor.fpfMustardColor()
    aSwitch.addTarget(self, action: #selector(NotificationsCell.switchChangedValue(_:)), forControlEvents: .ValueChanged)
    contentView.addSubview(aSwitch)
    
    let metrics = ["margin": Constants.defaultMargin, "doubleMargin": Constants.defaultMargin * 2, "logoWidth": Constants.mediumLogoWidth]
    let views = ["logoImageView": logoImageView, "label": label, "aSwitch": aSwitch]
    var horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|-margin-[logoImageView(logoWidth)]-margin-[label]->=margin-[aSwitch]-margin-|", options: [], metrics: metrics, views: views)
    var verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|-doubleMargin-[logoImageView]-doubleMargin-|", options: [], metrics: metrics, views: views)
    contentView.addConstraints(horizontalConstraints)
    contentView.addConstraints(verticalConstraints)
    
    verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|-doubleMargin-[label]-doubleMargin-|", options: [], metrics: metrics, views: views)
    contentView.addConstraints(verticalConstraints)
    
    verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|-doubleMargin-[aSwitch]-doubleMargin-|", options: [], metrics: metrics, views: views)
    contentView.addConstraints(verticalConstraints)
    
    horizontalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("H:|[contentView]|", options: [], metrics: nil, views: ["contentView": contentView])
    addConstraints(horizontalConstraints)
    verticalConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|[contentView]|", options: [], metrics: nil, views: ["contentView": contentView])
    addConstraints(verticalConstraints)
  }
  
  func configureWithNotificationCellViewModel(viewModel: NotificationsCellViewModel) {
    self.viewModel = viewModel
  }
  
  func switchChangedValue(aSwitch: UISwitch) {
    delegate?.didSetCell(self, on: aSwitch.on)
  }
  
  func enable(enable: Bool) {
    logoImageView.alpha = enable ? 1 : 0.7
    label.enabled = enable
    aSwitch.enabled = enable
  }
  
  #if DEBUG
  deinit {
    print("deinit", self)
  }
  #endif
}

extension NotificationsCell: ReuseIdentifier {
  static func reuseIdentifier() -> String {
    return "NotificationsCell"
  }
}
