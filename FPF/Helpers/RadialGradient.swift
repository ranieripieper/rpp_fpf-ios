//
//  RadialGradient.swift
//  FPF
//
//  Created by Gilson Gil on 1/19/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

final internal class RadialGradientLayer: CALayer {
  var innerColor: UIColor? {
    didSet {
      self.setNeedsDisplay()
    }
  }
  
  var outerColor: UIColor? {
    didSet {
      self.setNeedsDisplay()
    }
  }
  
  var point: CGPoint? {
    didSet {
      self.setNeedsDisplay()
    }
  }
  
  override func drawInContext(context: CGContext) {
    guard let innerColor = self.innerColor else {
      return
    }
    guard let outerColor = self.outerColor else {
      return
    }
    
    var point = CGPointZero
    point.x = bounds.width * (self.point?.x ?? 0.5)
    point.y = bounds.height * (self.point?.y ?? 0.5)
    
    let colorSpace = CGColorSpaceCreateDeviceRGB()
    let colors: CFArray = [innerColor.CGColor, outerColor.CGColor]
    let gradient = CGGradientCreateWithColors(colorSpace, colors, [0, 1])
    let radius = min(bounds.width / 2, bounds.height / 2)
    CGContextDrawRadialGradient(context, gradient, point, 0, point, radius, .DrawsAfterEndLocation)
  }
}

/// A view with a radial gradient.
final class RadialGradientView: UIView {
  /// The radial gradient to display.
  var gradient: Gradient? {
    didSet {
      let layer = self.layer as! RadialGradientLayer
      layer.innerColor = self.gradient?.startColor
      layer.outerColor = self.gradient?.endColor
    }
  }
  
  /// The origin point of the radial gradient.
  /// Range from 0 to 1, defaults to 0.5(center).
  var point: CGPoint? {
    didSet {
      let layer = self.layer as! RadialGradientLayer
      layer.point = point
    }
  }
  
  override class func layerClass() -> AnyClass {
    return RadialGradientLayer.self
  }
}

/// A gradient struct
struct Gradient {
  /// The start color of the gradient
  var startColor: UIColor?
  // The end color of the gradient
  var endColor: UIColor?
  
  /// Initialize a gradient struct without a start or end color
  init () {
    // Nothing to initialize
  }
  
  /// Initialize a gradient struct with a start and end color
  init (startColor: UIColor?, endColor: UIColor?) {
    self.startColor = startColor
    self.endColor = endColor
  }
  
  /// Returns a gradient with the start and end colors reversed.
  func reverse() -> Gradient {
    return Gradient(startColor: self.endColor, endColor: self.startColor)
  }
}
