//
//  Dynamic.swift
//  FPF
//
//  Created by Gilson Gil on 1/13/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation

final class Dynamic<T> {
  typealias Listener = T -> Void
  var listener: Listener?
  
  func bind(listener: Listener?) {
    self.listener = listener
    listener?(value)
  }
  
  var value: T {
    didSet {
      listener?(value)
    }
  }
  
  init(_ v: T) {
    value = v
  }
}
