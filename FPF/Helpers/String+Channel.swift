//
//  String+Channel.swift
//  FPF
//
//  Created by Gilson Gil on 1/29/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation

extension String {
  func parseChannelFormat() -> String {
    return stringByReplacingOccurrencesOfString(" ", withString: "-").stringByFoldingWithOptions(.DiacriticInsensitiveSearch, locale: NSLocale.currentLocale())
  }
}
