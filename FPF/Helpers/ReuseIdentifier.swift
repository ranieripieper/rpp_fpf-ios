//
//  ReuseIdentifier.swift
//  FPF
//
//  Created by Gilson Gil on 1/11/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

protocol RealTimeUpdatable: class {
  func update(percentage: CGFloat)
}

protocol Updatable: class {
  associatedtype ViewModel

  func update(viewModel: ViewModel)
}

struct CellConfigurator<Cell where Cell: Updatable> {
  let viewModel: Cell.ViewModel
  let reuseIdentifier: String = NSStringFromClass(Cell)
  let cellClass: AnyClass = Cell.self
  
  func update(cell: UITableViewCell) {
    if let cell = cell as? Cell {
      cell.update(viewModel)
    }
  }
  
  func update(cell: UICollectionViewCell) {
    if let cell = cell as? Cell {
      cell.update(viewModel)
    }
  }
  
  func register(tableView: UITableView) {
    tableView.registerClass(cellClass, forCellReuseIdentifier: reuseIdentifier)
  }
  
  func register(collectionView: UICollectionView) {
    collectionView.registerClass(cellClass, forCellWithReuseIdentifier: reuseIdentifier)
  }
  
  func currentViewModel() -> Any {
    return viewModel
  }
}

protocol CellConfiguratorType {
  var reuseIdentifier: String { get }
  var cellClass: AnyClass { get }
  
  func update(cell: UITableViewCell)
  func update(cell: UICollectionViewCell)
  func register(tableView: UITableView)
  func register(collectionView: UICollectionView)
  func currentViewModel() -> Any
}

extension CellConfigurator: CellConfiguratorType {}

protocol ReuseIdentifier {
  static func reuseIdentifier() -> String
}
