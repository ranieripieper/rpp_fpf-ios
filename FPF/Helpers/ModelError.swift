//
//  ModelError.swift
//  FPF
//
//  Created by Gilson Gil on 1/29/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation

enum ModelError: ErrorType {
  case Unknown, NoConnection
}
