//
//  UIImage+FPF.swift
//  FPF
//
//  Created by Gilson Gil on 1/15/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

extension UIImage {
  static func icnMenu() -> UIImage? {
    return UIImage(named: "icn_menu")
  }
  
  static func icnAboutMenu() -> UIImage? {
    return UIImage(named: "icnAboutMenu")
  }
  
  static func icnAlertsMenu() -> UIImage? {
    return UIImage(named: "icnAlertsMenu")
  }
  
  static func icnGuideMenu() -> UIImage? {
    return UIImage(named: "icnGuideMenu")
  }
  
  static func icnAlerts() -> UIImage? {
    return UIImage(named: "icn_alerts")
  }
  
  static func icnArrow() -> UIImage? {
    return UIImage(named: "icn_arrow")
  }
  
  static func imgBgMenu() -> UIImage? {
    return UIImage(named: "menuBg")
  }
  
  static func imgLogoMenu() -> UIImage? {
    return UIImage(named: "img_logo_menu")
  }
  
  static func icnSoccerBall() -> UIImage? {
    return UIImage(named: "icnSoccerBall")
  }
  
  static func imgSplash() -> UIImage? {
    return UIImage(named: "img_splash")
  }
  
  static func icnYellowCard() -> UIImage? {
    return UIImage(named: "icn_cartao_amarelo")
  }
  
  static func icnRedCard() -> UIImage? {
    return UIImage(named: "icn_cartao_vermelho")
  }
  
  static func icnGoal() -> UIImage? {
    return UIImage(named: "icn_gol")
  }
  
  static func icnSubs() -> UIImage? {
    return UIImage(named: "icn_substituicao")
  }
  
  static func icnLineupYellowCard() -> UIImage? {
    return UIImage(named: "icn_escala_cartao_amarelo")
  }
  
  static func icnLineupRedCard() -> UIImage? {
    return UIImage(named: "icn_escala_cartao_vermelho")
  }
  
  static func icnLineupGoal() -> UIImage? {
    return UIImage(named: "icn_escala_gol")
  }
  
  static func icnLineupSubsIn() -> UIImage? {
    return UIImage(named: "icnEntra")
  }
  
  static func icnLineupSubsOut() -> UIImage? {
    return UIImage(named: "icnSai")
  }
  
  static func icnPlaceholder() -> UIImage? {
    return UIImage(named: "icn_placeholder")
  }
  
  static func imgAboutLogo() -> UIImage? {
    return UIImage(named: "imgLogoSobre")
  }
  
  static func imgGuide() -> UIImage? {
    return UIImage(named: "guia")
  }
  
  static func icnFouls() -> UIImage? {
    return UIImage(named: "icnFaltas")
  }
  
  static func icnPenalties() -> UIImage? {
    return UIImage(named: "icnPenalty")
  }
  
  static func icnCorners() -> UIImage? {
    return UIImage(named: "icnEscanteio")
  }
  
  static func icnOval() -> UIImage? {
    return UIImage(named: "oval188")
  }
  
  static func icnLeftArrow() -> UIImage? {
    return UIImage(named: "triangleLeft")
  }
  
  static func icnRightArrow() -> UIImage? {
    return UIImage(named: "triangleRight")
  }
}
