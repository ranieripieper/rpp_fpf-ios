//
//  UIView+Shadow.swift
//  FPF
//
//  Created by Gilson Gil on 3/1/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

extension UIView {
  func applyShadow() {
    layer.masksToBounds = false
    layer.shadowColor = UIColor(white: 0.8, alpha: 1).CGColor
    layer.shadowOpacity = 0.75
    layer.shadowRadius = 1
    layer.shadowOffset = CGSize(width: 0, height: 2)
    layer.shadowPath = UIBezierPath(rect: bounds).CGPath
  }
  
  func updateShadow() {
    layer.shadowPath = UIBezierPath(rect: bounds).CGPath
  }
}
