//
//  UIFont+FPF.swift
//  FPF
//
//  Created by Gilson Gil on 1/15/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

extension UIFont {
  static func fpfRegular(size: CGFloat) -> UIFont? {
    return UIFont(name: "HelveticaNeue", size: size)
  }
  
  static func fpfBold(size: CGFloat) -> UIFont? {
    return UIFont(name: "HelveticaNeue-Bold", size: size)
  }
  
  static func fpfMedium(size: CGFloat) -> UIFont? {
    return UIFont(name: "HelveticaNeue-Medium", size: size)
  }
  
  static func fpfCondensedBold(size: CGFloat) -> UIFont? {
    return UIFont(name: "HelveticaNeue-CondensedBold", size: size)
  }
  
  static func fpfItalic(size: CGFloat) -> UIFont? {
    return UIFont(name: "HelveticaNeue-Italic", size: size)
  }
}
