//
//  MapsHandler.swift
//  FPF
//
//  Created by Gilson Gil on 3/9/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import MapKit

struct MapsHandler {
  static func open(coordinate: CLLocationCoordinate2D, name: String, fromViewController viewController: UIViewController) {
    let hasWaze = canOpenAppWithURL("waze://")
    let hasGoogleMaps = canOpenAppWithURL("comgooglemaps://")
    if hasWaze || hasGoogleMaps {
      let alertView = UIAlertController(title: "Escolha um aplicativo", message: nil, preferredStyle: .ActionSheet)
      if hasWaze {
        let wazeAction = UIAlertAction(title: "Waze", style: .Default, handler: { _ in
          self.openWaze(coordinate)
        })
        alertView.addAction(wazeAction)
      }
      if hasGoogleMaps {
        let googleAction = UIAlertAction(title: "Google Maps", style: .Default, handler: { _ in
          self.openGoogleMaps(coordinate)
        })
        alertView.addAction(googleAction)
      }
      let appleAction = UIAlertAction(title: "Apple Maps", style: .Default, handler: { _ in
        self.openMaps(coordinate, name: name)
      })
      alertView.addAction(appleAction)
      let cancelAction = UIAlertAction(title: "Cancelar", style: .Cancel, handler: nil)
      alertView.addAction(cancelAction)
      viewController.presentViewController(alertView, animated: true, completion: nil)
    } else {
      openMaps(coordinate, name: name)
    }
  }
  
  private static func canOpenAppWithURL(urlString: String) -> Bool {
    guard let url = NSURL(string: urlString) else {
      return false
    }
    return UIApplication.sharedApplication().canOpenURL(url)
  }
  
  private static func openWaze(coordinate: CLLocationCoordinate2D) {
    let urlString = "waze://?ll=\(coordinate.latitude),\(coordinate.longitude)&navigate=yes"
    guard let url = NSURL(string: urlString) else {
      return
    }
    UIApplication.sharedApplication().openURL(url)
  }
  
  private static func openGoogleMaps(coordinate: CLLocationCoordinate2D) {
    let urlString = "comgooglemaps://?center=\(coordinate.latitude),\(coordinate.longitude)&zoom=18&views=traffic&q=\(coordinate.latitude),\(coordinate.longitude)"
    if let url = NSURL(string: urlString.stringByAddingPercentEncodingWithAllowedCharacters(NSCharacterSet.URLQueryAllowedCharacterSet())!) {
      UIApplication.sharedApplication().openURL(url)
    }
  }
  
  private static func openMaps(coordinate: CLLocationCoordinate2D, name: String) {
    let place = MKPlacemark(coordinate: coordinate, addressDictionary: nil)
    let mapItem = MKMapItem(placemark: place)
    mapItem.name = name
    mapItem.openInMapsWithLaunchOptions(nil)
  }
}
