//
//  DateFormatter.swift
//  FPF
//
//  Created by Gilson Gil on 1/26/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation

final class DateFormatter: NSDateFormatter {
  static let displayFormatter: DateFormatter = {
    let formatter = DateFormatter()
    formatter.dateFormat = "dd/MM/yyyy - HH'h'mm"
    return formatter
  }()
}
