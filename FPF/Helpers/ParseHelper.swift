//
//  ParseHelper.swift
//  FPF
//
//  Created by Gilson Gil on 1/8/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation
import Parse

struct ParseHelper {
  static func setApplication() {
    Standing.registerSubclass()
    Live.registerSubclass()
    Lineup.registerSubclass()
    Scorer.registerSubclass()
    Match.registerSubclass()
    Team.registerSubclass()
    League.registerSubclass()
    Guide.registerSubclass()
    MatchStats.registerSubclass()
    TeamStats.registerSubclass()
    Parse.enableLocalDatastore()
    guard let applicationId = NSBundle.mainBundle().infoDictionary?["Parse_Application_Id"] as? String, clientKey = NSBundle.mainBundle().infoDictionary?["Parse_Client_Key"] as? String else {
      return
    }
    Parse.setApplicationId(applicationId, clientKey: clientKey)
  }
  
  static func trackPushAppOpen(launchOptions: [NSObject: AnyObject]?) {
    let oldPushHandlerOnly = !UIApplication.sharedApplication().respondsToSelector(#selector(UIApplicationDelegate.application(_:didReceiveRemoteNotification:fetchCompletionHandler:)))
    let noPushPayload: AnyObject? = launchOptions?[UIApplicationLaunchOptionsRemoteNotificationKey]
    if oldPushHandlerOnly || noPushPayload != nil {
      PFAnalytics.trackAppOpenedWithLaunchOptions(launchOptions)
    }
  }
  
  static func registerForRemoteNotifications() {
    let settings = UIUserNotificationSettings(forTypes: [.Alert, .Badge, .Sound], categories: nil)
    UIApplication.sharedApplication().registerUserNotificationSettings(settings)
    UIApplication.sharedApplication().registerForRemoteNotifications()
  }
  
  static func didFailToRegisterForRemoteNotifications(error: NSError) {
    
  }
  
  static func didReceiveRemoteNotification(userInfo: [NSObject: AnyObject]) {
    PFAnalytics.trackAppOpenedWithRemoteNotificationPayload(userInfo)
  }
  
  static func didRegisterForRemoteNotifications(deviceToken: NSData) {
    guard let currentInstalation = PFInstallation.currentInstallation() else {
      return
    }
    currentInstalation.setDeviceTokenFromData(deviceToken)
    currentInstalation.saveInBackground()
    if let pinnedChannel = NSUserDefaults.standardUserDefaults().stringForKey(Constants.channelString) {
      registerPushForChannel(pinnedChannel)
    }
  }
  
  static func currentChannels() -> [String] {
    let currentInstallation = PFInstallation.currentInstallation()
    return currentInstallation?.channels ?? []
  }
  
  static func registerPushForChannel(channel: String) {
    if !NSUserDefaults.standardUserDefaults().boolForKey(Constants.hasRegisteredForRemoteNotificationsString) {
      NSUserDefaults.standardUserDefaults().setBool(true, forKey: Constants.hasRegisteredForRemoteNotificationsString)
      NSUserDefaults.standardUserDefaults().setObject(channel, forKey: Constants.channelString)
      NSUserDefaults.standardUserDefaults().synchronize()
      registerForRemoteNotifications()
    }
    let newChannel = channel.parseChannelFormat()
    let currentInstallation = PFInstallation.currentInstallation()
    var channels = currentInstallation?.channels ?? []
    if !channels.contains(newChannel) {
      channels.append(newChannel)
    }
    currentInstallation?.channels = channels
    currentInstallation?.saveInBackground()
  }
  
  static func deregisterPushForChannel(channel: String) {
    let newChannel = channel.parseChannelFormat()
    let currentInstallation = PFInstallation.currentInstallation()
    if var channels = currentInstallation?.channels {
      channels = channels.filter {
        $0 != newChannel
      }
      currentInstallation?.channels = channels
      currentInstallation?.saveInBackground()
    }
  }
}
