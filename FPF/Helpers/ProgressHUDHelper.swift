//
//  ProgressHUDHelper.swift
//  FPF
//
//  Created by Gilson Gil on 1/20/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import SVProgressHUD

struct ProgressHUDHelper {
  static var counter = 0
  
  static func setUp() {
    SVProgressHUD.setDefaultMaskType(.Gradient)
    SVProgressHUD.setBackgroundColor(UIColor.fpfBlackColor())
    SVProgressHUD.setForegroundColor(UIColor.fpfSunflowerColor())
    SVProgressHUD.setFont(UIFont.fpfRegular(20))
  }
  
  static func show() {
    counter = counter + 1
    guard counter == 1 else {
      return
    }
    dispatch_async(dispatch_get_main_queue()) {
      UIApplication.sharedApplication().networkActivityIndicatorVisible = true
      SVProgressHUD.show()
    }
  }
  
  static func dismiss() {
    counter = counter - 1
    guard counter == 0 else {
      return
    }
    dispatch_async(dispatch_get_main_queue()) {
      UIApplication.sharedApplication().networkActivityIndicatorVisible = false
      SVProgressHUD.dismiss()
    }
  }
}
