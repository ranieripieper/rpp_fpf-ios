//
//  GroupBy.swift
//  FPF
//
//  Created by Gilson Gil on 1/27/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation

extension CollectionType {
  public typealias ItemType = Self.Generator.Element
  public typealias Grouper = (ItemType, ItemType) -> Bool
  
  public func groupBy(grouper: Grouper) -> [[ItemType]] {
    var result : Array<Array<ItemType>> = []
    
    var previousItem: ItemType?
    var group = [ItemType]()
    
    for item in self {
      defer {previousItem = item}
      guard let previous = previousItem else {
        group.append(item)
        continue
      }
      if grouper(previous, item) {
        // Item in the same group
        group.append(item)
      } else {
        // New group
        result.append(group)
        group = [ItemType]()
        group.append(item)
      }
    }
    result.append(group)
    
    return result
  }
}

public protocol Groupable {
  func sameGroupAs(otherPerson: Self) -> Bool
}

extension CollectionType where Self.Generator.Element: Groupable {
  public func group() -> [[Self.Generator.Element]] {
    return groupBy { $0.sameGroupAs($1) }
  }
}

extension CollectionType where Self.Generator.Element: Comparable {
  public func uniquelyGroupBy(grouper: (Self.Generator.Element, Self.Generator.Element) -> Bool) -> [[Self.Generator.Element]] {
    let sorted = sort().reverse()
    return sorted.groupBy(grouper)
  }
}
