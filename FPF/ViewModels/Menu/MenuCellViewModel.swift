//
//  MenuCellViewModel.swift
//  FPF
//
//  Created by Gilson Gil on 5/4/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation

struct MenuCellViewModel {
  let name: String
}
