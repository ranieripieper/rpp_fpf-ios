//
//  MenuViewModel.swift
//  FPF
//
//  Created by Gilson Gil on 5/4/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation

enum ViewType: Equatable {
  case League(Int?), Notifications, Guide, About
}

func ==(lhs: ViewType, rhs: ViewType) -> Bool {
  switch (lhs, rhs) {
  case (.League(let lId), .League(let rId)):
    return lId == rId
  case (.Notifications, .Notifications):
    return true
  case (.Guide, .Guide):
    return true
  case (.About, .About):
    return true
  default:
    return false
  }
}

struct MenuViewModel {
  let leagues: [League]
  let configurators: [CellConfiguratorType]
  let selectedViewType: ViewType
  
  init(leagues: [League], selectedViewType: ViewType) {
    self.leagues = leagues
    self.configurators = leagues.flatMap { league -> CellConfiguratorType? in
      guard let name = league.leagueName ?? league.name else {
        return nil
      }
      return CellConfigurator<MenuCell>(viewModel: MenuCellViewModel(name: name))
    }
    switch selectedViewType {
    case .League(let id):
      if id == nil {
        self.selectedViewType = .League(leagues.first?.id ?? 0)
      } else {
        self.selectedViewType = selectedViewType
      }
    default:
      self.selectedViewType = selectedViewType
    }
  }
  
  static func viewModel(leagues: [League]?, viewType: ViewType, completion: (inner: () throws -> MenuViewModel) -> ()) {
    if let leagues = leagues {
      let viewModel = MenuViewModel(leagues: leagues, selectedViewType: viewType)
      completion { return viewModel }
      return
    }
    League.get { inner in
      do {
        let leagues = try inner()
        let viewModel = MenuViewModel(leagues: leagues, selectedViewType: viewType)
        completion { return viewModel }
      } catch {
        completion { throw error }
      }
    }
  }
}
