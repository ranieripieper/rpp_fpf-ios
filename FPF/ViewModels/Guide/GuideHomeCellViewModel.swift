//
//  GuideHomeCellViewModel.swift
//  FPF
//
//  Created by Gilson Gil on 2/26/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation

struct GuideHomeCellViewModel {
  let guide: Guide
  let logoURLString: String
  let name: String
  
  init?(guide: Guide?) {
    guard let guide = guide else {
      return nil
    }
    self.guide = guide
    self.logoURLString = guide.team?.logoURLString ?? ""
    self.name = guide.team?.name ?? ""
  }
}
