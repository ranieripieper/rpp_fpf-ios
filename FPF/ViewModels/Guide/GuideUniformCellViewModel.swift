//
//  GuideUniformCellViewModel.swift
//  FPF
//
//  Created by Gilson Gil on 2/29/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation

struct GuideUniformCellViewModel {
  let uniformURL: String
  
  init?(uniformURL: String?) {
    guard let uniformURL = uniformURL else {
      return nil
    }
    self.uniformURL = uniformURL
  }
}
