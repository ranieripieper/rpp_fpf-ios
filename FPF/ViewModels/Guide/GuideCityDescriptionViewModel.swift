//
//  GuideCityDescriptionViewModel.swift
//  FPF
//
//  Created by Gilson Gil on 2/27/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation

struct GuideCityDescriptionViewModel {
  let cityURL: String?
  let cityDescription: String
  
  init?(cityURL: String?, cityDescription: String?) {
    guard let cityDescription = cityDescription else {
      return nil
    }
    self.cityURL = cityURL
    self.cityDescription = cityDescription
  }
}
