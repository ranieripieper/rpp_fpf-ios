//
//  GuideMascotViewModel.swift
//  FPF
//
//  Created by Gilson Gil on 2/27/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation

struct GuideMascotViewModel {
  let mascotName: String?
  let mascotImageURL: String?
  
  init?(mascotName: String?, mascotImageURL: String?) {
    guard mascotName != nil || mascotImageURL != nil else {
      return nil
    }
    self.mascotName = mascotName
    self.mascotImageURL = mascotImageURL
  }
}
