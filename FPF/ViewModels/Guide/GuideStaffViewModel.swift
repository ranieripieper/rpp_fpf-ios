//
//  GuideStaffViewModel.swift
//  FPF
//
//  Created by Gilson Gil on 2/27/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation

struct GuideStaffViewModel {
  let staff: [Staff]
  
  init?(staff: [Staff]?) {
    guard let staff = staff where staff.count > 0 else {
      return nil
    }
    self.staff = staff
  }
}
