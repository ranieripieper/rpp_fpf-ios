//
//  GuideCityViewModel.swift
//  FPF
//
//  Created by Gilson Gil on 2/27/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation
import CoreLocation

struct GuideCityViewModel {
  let cityStadiumName: String
  let coordinate: CLLocation
  
  init?(cityStadiumName: String?, coordinate: CLLocation?) {
    guard let cityStadiumName = cityStadiumName, let coordinate = coordinate else {
      return nil
    }
    self.cityStadiumName = cityStadiumName
    self.coordinate = coordinate
  }
}
