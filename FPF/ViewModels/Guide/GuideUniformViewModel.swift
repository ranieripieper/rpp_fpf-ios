//
//  GuideUniformViewModel.swift
//  FPF
//
//  Created by Gilson Gil on 2/27/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation

struct GuideUniformViewModel {
  let uniformsURLs: [String]
  let uniformMaker: String?
  
  init?(uniformsURLs: [String]?, uniformMaker: String?) {
    guard let uniformsURLs = uniformsURLs where uniformsURLs.count > 0 else {
      return nil
    }
    self.uniformsURLs = uniformsURLs
    self.uniformMaker = uniformMaker
  }
}
