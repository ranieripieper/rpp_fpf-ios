//
//  GuideDescriptionCellViewModel.swift
//  FPF
//
//  Created by Gilson Gil on 2/27/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation

struct GuideDescriptionCellViewModel {
  let description: String
  
  init?(description: String?) {
    guard let description = description else {
      return nil
    }
    self.description = description
  }
}
