//
//  GuideHomeViewModel.swift
//  FPF
//
//  Created by Gilson Gil on 2/25/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation

struct GuideHomeViewModel {
  let teams: [(String, [GuideHomeCellViewModel])]
  var filteredTeams: [(String, [GuideHomeCellViewModel])] = []
  
  init(teams: [(String, [GuideHomeCellViewModel])]) {
    self.teams = teams
    filteredTeams = teams
  }
  
  static func create(completion: GuideHomeViewModel -> ()) {
    Guide.guides {
      let guides = try? $0()
      let viewModels = guides?.flatMap { ($0.0, $0.1.flatMap {GuideHomeCellViewModel(guide: $0)}) }
      completion(GuideHomeViewModel(teams: viewModels ?? []))
    }
  }
}
