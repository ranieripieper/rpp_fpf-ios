//
//  StaffViewModel.swift
//  FPF
//
//  Created by Gilson Gil on 3/2/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation

struct StaffViewModel {
  let staff: [CellConfiguratorType]
  
  init(staff: [Staff]) {
    self.staff = staff.map { CellConfigurator<StaffCell>(viewModel: StaffCellViewModel(staff: $0)) }
  }
}
