//
//  StaffCellViewModel.swift
//  FPF
//
//  Created by Gilson Gil on 3/2/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation

struct StaffCellViewModel {
  let name: String
  let birthDate: String
  let position: String
  
  init(staff: Staff) {
    name = staff.name
    birthDate = staff.birthDate
    position = staff.position
  }
}
