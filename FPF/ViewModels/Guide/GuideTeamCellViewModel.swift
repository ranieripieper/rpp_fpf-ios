//
//  GuideTeamCellViewModel.swift
//  FPF
//
//  Created by Gilson Gil on 2/29/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation

struct GuideTeamCellViewModel {
  let logoURL: String
  let name: String
  
  init?(logoURL: String?, name: String?) {
    guard let logoURL = logoURL, let name = name else {
      return nil
    }
    self.logoURL = logoURL
    self.name = name
  }
}
