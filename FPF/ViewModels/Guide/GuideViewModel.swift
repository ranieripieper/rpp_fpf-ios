//
//  GuideViewModel.swift
//  FPF
//
//  Created by Gilson Gil on 2/26/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation
import CoreLocation

struct GuideViewModel {
  let guide: Guide
  let guideTeamCellViewModel: GuideTeamCellViewModel?
  let guideDescriptionCellViewModel: GuideDescriptionCellViewModel?
  let guideMascotViewModel: GuideMascotViewModel?
  let guideUniformViewModel: GuideUniformViewModel?
  let guideStaffViewModel: GuideStaffViewModel?
  let guideStadiumViewModel: GuideStadiumViewModel?
  let guideCityViewModel: GuideCityViewModel?
  let guideCityDescriptionViewModel: GuideCityDescriptionViewModel?
  let guideDataViewModel: GuideDataViewModel?
  
  var items: [CellConfiguratorType] {
    var items = [CellConfiguratorType]()
    if let guideTeamCellViewModel = guideTeamCellViewModel {
      items.append(CellConfigurator<GuideTeamCell>(viewModel: guideTeamCellViewModel))
    }
    if let guideDescriptionCellViewModel = guideDescriptionCellViewModel {
      items.append(CellConfigurator<GuideDescriptionCell>(viewModel: guideDescriptionCellViewModel))
    }
    if let guideMascotViewModel = guideMascotViewModel {
      items.append(CellConfigurator<GuideMascotCell>(viewModel: guideMascotViewModel))
    }
    if let guideUniformViewModel = guideUniformViewModel {
      items.append(CellConfigurator<GuideUniformsCell>(viewModel: guideUniformViewModel))
    }
    if let guideStaffViewModel = guideStaffViewModel {
      items.append(CellConfigurator<GuideStaffCell>(viewModel: guideStaffViewModel))
    }
    if let guideStadiumViewModel = guideStadiumViewModel {
      items.append(CellConfigurator<GuideStadiumCell>(viewModel: guideStadiumViewModel))
    }
    if let guideCityViewModel = guideCityViewModel {
      items.append(CellConfigurator<GuideCityCell>(viewModel: guideCityViewModel))
    }
    if let guideCityDescriptionViewModel = guideCityDescriptionViewModel {
      items.append(CellConfigurator<GuideCityDescriptionCell>(viewModel: guideCityDescriptionViewModel))
    }
    if let guideDataViewModel = guideDataViewModel {
      items.append(CellConfigurator<GuideDataCell>(viewModel: guideDataViewModel))
    }
    items.append(CellConfigurator<GuideLegendCell>(viewModel: nil))
    return items
  }
  
  init(guide: Guide) {
    self.guide = guide
    guideTeamCellViewModel = GuideTeamCellViewModel(logoURL: guide.badgeURL, name: guide.name)
    guideDescriptionCellViewModel = GuideDescriptionCellViewModel(description: guide.teamDescription)
    guideMascotViewModel = GuideMascotViewModel(mascotName: guide.mascot, mascotImageURL: guide.mascotURL)
    guideUniformViewModel = GuideUniformViewModel(uniformsURLs: guide.uniformURLs, uniformMaker: guide.uniformMaker)
    guideStaffViewModel = GuideStaffViewModel(staff: guide.staff)
    guideStadiumViewModel = GuideStadiumViewModel(stadiumURL: guide.stadiumURL, stadiumName: guide.stadiumName, capacity: GuideViewModel.capacity(guide.stadiumCapacity), established: guide.stadiumEstablishment)
    guideCityViewModel = GuideCityViewModel(cityStadiumName: guide.cityStadium, coordinate: guide.coordinate)
    guideCityDescriptionViewModel = GuideCityDescriptionViewModel(cityURL: guide.cityURL, cityDescription: guide.cityDescription)
    guideDataViewModel = GuideDataViewModel(area: guide.cityArea, population: guide.cityPopulation, density: guide.demographicDensity, idhm: guide.idhm)
  }
  
  static func create(teamId: Int, completion: GuideViewModel? -> ()) {
    Guide.guideWithTeamId(teamId) {
      do {
        let guide = try $0()
        let guideViewModel = GuideViewModel(guide: guide)
        completion(guideViewModel)
      } catch {
        completion(nil)
      }
    }
  }
  
  private static func capacity(capacity: Int?) -> String {
    if let capacity = capacity {
      let numberFormatter = NSNumberFormatter()
      numberFormatter.usesGroupingSeparator = true
      return numberFormatter.stringFromNumber(NSNumber(int: Int32(capacity))) ?? "-"
    } else {
      return "-"
    }
  }
}
