//
//  GuideDataViewModel.swift
//  FPF
//
//  Created by Gilson Gil on 2/27/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation

struct GuideDataViewModel {
  let area: String?
  let population: String?
  let density: String?
  let idhm: String?
  
  init?(area: String?, population: String?, density: String?, idhm: String?) {
    guard area != nil || population != nil || density != nil || idhm != nil else {
      return nil
    }
    self.area = area
    self.population = population
    self.density = density
    self.idhm = idhm
  }
}
