//
//  GuideStadiumViewModel.swift
//  FPF
//
//  Created by Gilson Gil on 2/27/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation

struct GuideStadiumViewModel {
  let stadiumURL: String
  let stadiumName: String?
  let capacity: String?
  let established: String?
  
  init?(stadiumURL: String?, stadiumName: String?, capacity: String?, established: String?) {
    guard let stadiumURL = stadiumURL else {
      return nil
    }
    self.stadiumURL = stadiumURL
    self.stadiumName = stadiumName
    self.capacity = capacity
    self.established = established
  }
}
