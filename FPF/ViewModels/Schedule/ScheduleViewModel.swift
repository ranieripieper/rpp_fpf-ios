//
//  ScheduleViewModel.swift
//  FPF
//
//  Created by Gilson Gil on 1/8/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation

struct ScheduleViewModel {
  let rounds: [RoundCellViewModel]
  let currentRound: Int
  
  init(rounds: [(String, [Match])], currentRound: Int) {
    let viewModelRounds = rounds.map { round in
      RoundCellViewModel(roundName: round.0, matches: round.1)
    }
    self.rounds = viewModelRounds
    self.currentRound = currentRound
  }
  
  init(rounds: [RoundCellViewModel]) {
    self.rounds = rounds
    currentRound = 1
  }
  
  static func matchesForLeagueId(leagueId: Int, currentRound: Int, forced: Bool, completion: (scheduleViewModel: ScheduleViewModel?) -> ()) {
    Match.matchesForLeagueId(leagueId, forced: forced) { inner in
      do {
        let scheduleViewModel = ScheduleViewModel(rounds: try inner(), currentRound: currentRound)
        completion(scheduleViewModel: scheduleViewModel)
      } catch {
        completion(scheduleViewModel: nil)
      }
    }
  }
}
