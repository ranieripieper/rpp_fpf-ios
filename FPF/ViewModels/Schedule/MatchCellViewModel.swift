//
//  MatchCellViewModel.swift
//  FPF
//
//  Created by Gilson Gil on 1/12/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation

struct MatchCellViewModel {
  var team1LogoURLString: String
  let team1Name: String
  let team1Score: String
  var team2LogoURLString: String
  let team2Name: String
  let team2Score: String
  let date: String
  let pastDate: Bool
  let location: String
  let match: Match
  
  init(match: Match) {
    self.match = match
    team1LogoURLString = match.homeURLString
    team1Name = match.homeTeamName ?? ""
    if let homeScore = match.homeScore {
      team1Score = String(homeScore)
    } else {
      team1Score = ""
    }
    team2LogoURLString = match.awayURLString
    team2Name = match.awayTeamName ?? ""
    if let awayScore = match.awayScore {
      team2Score = String(awayScore)
    } else {
      team2Score = ""
    }
    if let date = match.date {
      self.date = DateFormatter.displayFormatter.stringFromDate(date)
      pastDate = date.compare(NSDate()) == .OrderedAscending
    } else {
      self.date = "A definir"
      pastDate = false
    }
    location = match.stadiumName ?? ""
  }
}
