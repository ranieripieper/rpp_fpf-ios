//
//  MatchStatsPercentageViewModel.swift
//  FPF
//
//  Created by Gilson Gil on 3/2/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation

struct MatchStatsPercentageViewModel {
  let home: String
  let away: String
  let title: String
  
  init?(home: Float?, away: Float?, title: String?) {
    guard let title = title where (home != nil || away != nil)  else {
      return nil
    }
    self.title = title
    let numberFormatter = NSNumberFormatter()
    numberFormatter.alwaysShowsDecimalSeparator = true
    numberFormatter.numberStyle = .PercentStyle
    numberFormatter.maximumFractionDigits = 2
    if let home = home, homeString = numberFormatter.stringFromNumber(NSNumber(float: home/100)) {
      self.home = homeString
    } else {
      return nil
    }
    if let away = away, awayString = numberFormatter.stringFromNumber(NSNumber(float: away/100)) {
      self.away = awayString
    } else {
      return nil
    }
  }
}
