//
//  LineupCellViewModel.swift
//  FPF
//
//  Created by Gilson Gil on 1/27/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation

struct LineupCellViewModel {
  let hasHomePlayer: Bool
  let homeName: String?
  let homePosition: String?
  let homeGoals: Int?
  let homeSubstitute: Bool?
  let homeSubstitutee: Bool?
  let homeRedCard: Bool?
  let homeYellowCard: Bool?
  let hasAwayPlayer: Bool
  let awayName: String?
  let awayPosition: String?
  let awayGoals: Int?
  let awaySubstitute: Bool?
  let awaySubstitutee: Bool?
  let awayRedCard: Bool?
  let awayYellowCard: Bool?
  
  init(homePlayer: LineupPlayer?, awayPlayer: LineupPlayer?) {
    if let homePlayer = homePlayer {
      hasHomePlayer = true
      homeName = homePlayer.name
      homePosition = homePlayer.position
      homeGoals = homePlayer.goals
      homeSubstitute = homePlayer.substituted
      homeSubstitutee = homePlayer.substitutee
      homeRedCard = homePlayer.redCard
      homeYellowCard = homePlayer.yellowCard
    } else {
      hasHomePlayer = false
      homeName = nil
      homePosition = nil
      homeGoals = nil
      homeSubstitute = nil
      homeSubstitutee = nil
      homeRedCard = nil
      homeYellowCard = nil
    }
    if let awayPlayer = awayPlayer {
      hasAwayPlayer = true
      awayName = awayPlayer.name
      awayPosition = awayPlayer.position
      awayGoals = awayPlayer.goals
      awaySubstitute = awayPlayer.substituted
      awaySubstitutee = awayPlayer.substitutee
      awayRedCard = awayPlayer.redCard
      awayYellowCard = awayPlayer.yellowCard
    } else {
      hasAwayPlayer = false
      awayName = nil
      awayPosition = nil
      awayGoals = nil
      awaySubstitute = nil
      awaySubstitutee = nil
      awayRedCard = nil
      awayYellowCard = nil
    }
  }
}
