//
//  LiveCellViewModel.swift
//  FPF
//
//  Created by Gilson Gil on 1/27/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation

enum GraphicType {
  case Goal, RedCard, Substitute, YellowCard
  
  init?(_ string: String?) {
    switch string {
    case .None:
      return nil
    case .Some("Gol"):
      self = .Goal
    case .Some("Cartão vermelho"):
      self = .RedCard
    case .Some("Substituição"):
      self = .Substitute
    case .Some("Cartão amarelo"):
      self = .YellowCard
    default:
      return nil
    }
  }
}

struct LiveCellViewModel {
  let graphicType: GraphicType?
  let content: String
  let time: String
  
  init?(live: Live) {
    guard let liveDescription = live.liveDescription else {
      return nil
    }
    guard let time = live.time, let minute = time.componentsSeparatedByString(":").first, let minuteInt = Int(minute) else {
      return nil
    }
    graphicType = GraphicType(live.importantAction)
    content = liveDescription
    self.time = String(minuteInt) + "'"
  }
}
