//
//  MatchViewModel.swift
//  FPF
//
//  Created by Gilson Gil on 1/26/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation

struct MatchViewModel {
  let match: Match
  var team1LogoURLString: String
  let team1Name: String
  let team1Score: Dynamic<Int>
  var team2LogoURLString: String
  let team2Name: String
  let team2Score: Dynamic<Int>
  let date: String
  let location: String
  let referee: String
  let city: String
  let matchId: Int
  let hasStats: Bool
  
  init(match: Match) {
    self.match = match
    matchId = match.id ?? 0
    team1LogoURLString = match.homeURLString
    team1Name = match.homeTeamName ?? ""
    team1Score = Dynamic(match.homeScore ?? 0)
    team2LogoURLString = match.awayURLString
    team2Name = match.awayTeamName ?? ""
    team2Score = Dynamic(match.awayScore ?? 0)
    hasStats = match.hasStats ?? false
    if let date = match.date {
      self.date = DateFormatter.displayFormatter.stringFromDate(date)
    } else {
      self.date = "-"
    }
    if let stadium = match.stadiumName where stadium.characters.count > 0 {
      location = stadium
    } else {
      location = "-"
    }
    if let referee = match.referee where referee.characters.count > 0 {
      self.referee = referee
    } else {
      self.referee = "-"
    }
    if let city = match.city where city.characters.count > 0 {
      self.city = city
    } else {
      self.city = "-"
    }
  }
}

extension MatchViewModel {
  func lives(completion: (liveViewModel: LiveViewModel?) -> ()) {
    Live.livesForMatchId(matchId, forceCached: match.liveCompleted) { inner in
      do {
        let lives = try inner()
        let liveViewModel = LiveViewModel(lives: lives)
        self.match.cards = lives.flatMap { live in
          if let action = live.importantAction {
            if action == Strings.MatchYellowCard {
              return (teamName: live.teamName!, playerName: live.playerName!, type: .YellowCard)
            } else if action == Strings.MatchRedCard {
              return (teamName: live.teamName!, playerName: live.playerName!, type: .RedCard)
            } else {
              return nil
            }
          } else {
            return nil
          }
        }
        if self.hasFinished() {
          self.match.liveCompleted = true
          self.match.pinInBackground()
        }
        completion(liveViewModel: liveViewModel)
      } catch {
        completion(liveViewModel: nil)
      }
    }
  }
  
  func hasStarted() -> Bool {
    return self.match.currentPeriod != Strings.MatchNotStarted
  }
  
  func hasFinished() -> Bool {
    return self.match.currentPeriod == Strings.MatchFinished
  }
  
  func lineups(completion: (lineupViewModel: LineupViewModel?) -> ()) {
    Lineup.lineupsForMatchId(match.id ?? 0, forcedCache: match.lineupCompleted) { inner in
      do {
        let lineup = try inner()
        let lineupViewModel = LineupViewModel(lineup: lineup, match: self.match)
        if self.hasFinished() {
          self.match.lineupCompleted = true
          self.match.pinInBackground()
        }
        completion(lineupViewModel: lineupViewModel)
      } catch {
        completion(lineupViewModel: nil)
      }
    }
  }
  
  func stats(completion: (matchStatsViewModel: MatchStatsViewModel?) -> ()) {
    MatchStats.statsForMatchId(match.id ?? 0) { inner in
      do {
        let matchStats = try inner()
        let matchStatsViewModel = MatchStatsViewModel(matchStats: matchStats)
        if self.hasFinished() {
          self.match.matchStatsCompleted = true
          self.match.pinInBackground()
        }
        completion(matchStatsViewModel: matchStatsViewModel)
      } catch {
        completion(matchStatsViewModel: nil)
      }
    }
  }
}
