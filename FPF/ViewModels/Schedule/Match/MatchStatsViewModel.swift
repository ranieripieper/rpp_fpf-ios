//
//  MatchStatsViewModel.swift
//  FPF
//
//  Created by Gilson Gil on 3/2/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation

struct MatchStatsViewModel {
  let possessionViewModel: MatchStatsPercentageViewModel?
  let shotsViewModel: MatchStatsCompletedViewModel?
  let foulsViewModel: MatchStatsAbsoluteViewModel?
  let passesViewModel: MatchStatsCompletedViewModel?
  let cornersViewModel: MatchStatsAbsoluteViewModel?
  let tacklesViewModel: MatchStatsCompletedViewModel?
  let yellowCardsViewModel: MatchStatsAbsoluteViewModel?
  let redCardsViewModel: MatchStatsAbsoluteViewModel?
  let crossesViewModel: MatchStatsCompletedViewModel?
  let throughBallsViewModel: MatchStatsCompletedViewModel?
  let penaltiesViewModel: MatchStatsAbsoluteViewModel?
  
  var items: [CellConfiguratorType] {
    var items = [CellConfiguratorType]()
    if let possessionViewModel = possessionViewModel {
      items.append(CellConfigurator<MatchStatsPercentageCell>(viewModel: possessionViewModel))
    }
    if let shotsViewModel = shotsViewModel {
      items.append(CellConfigurator<MatchStatsCompletedCell>(viewModel: shotsViewModel))
    }
    if let foulsViewModel = foulsViewModel {
      items.append(CellConfigurator<MatchStatsAbsoluteCell>(viewModel: foulsViewModel))
    }
    if let passesViewModel = passesViewModel {
      items.append(CellConfigurator<MatchStatsCompletedCell>(viewModel: passesViewModel))
    }
    if let cornersViewModel = cornersViewModel {
      items.append(CellConfigurator<MatchStatsAbsoluteCell>(viewModel: cornersViewModel))
    }
    if let tacklesViewModel = tacklesViewModel {
      items.append(CellConfigurator<MatchStatsCompletedCell>(viewModel: tacklesViewModel))
    }
    if let yellowCardsViewModel = yellowCardsViewModel {
      items.append(CellConfigurator<MatchStatsAbsoluteCell>(viewModel: yellowCardsViewModel))
    }
    if let redCardsViewModel = redCardsViewModel {
      items.append(CellConfigurator<MatchStatsAbsoluteCell>(viewModel: redCardsViewModel))
    }
    if let crossesViewModel = crossesViewModel {
      items.append(CellConfigurator<MatchStatsCompletedCell>(viewModel: crossesViewModel))
    }
    if let throughBallsViewModel = throughBallsViewModel {
      items.append(CellConfigurator<MatchStatsCompletedCell>(viewModel: throughBallsViewModel))
    }
    if let penaltiesViewModel = penaltiesViewModel {
      items.append(CellConfigurator<MatchStatsAbsoluteCell>(viewModel: penaltiesViewModel))
    }
    return items
  }
  
  init(matchStats: MatchStats) {
    possessionViewModel = MatchStatsPercentageViewModel(home: matchStats.homePossession, away: matchStats.awayPossession, title: Strings.MatchStatsPossession)
    shotsViewModel = MatchStatsCompletedViewModel(homeCompleted: matchStats.homeShotsOnTarget, homeWrong: matchStats.homeShotsOffTarget, awayCompleted: matchStats.awayShotsOnTarget, awayWrong: matchStats.awayShotsOffTarget, title: Strings.MatchStatsShots)
    foulsViewModel = MatchStatsAbsoluteViewModel(home: matchStats.homeFouls, away: matchStats.awayFouls, type: .Foul)
    passesViewModel = MatchStatsCompletedViewModel(homeCompleted: matchStats.homeCompletedPasses, homeWrong: matchStats.homeWrongPasses, awayCompleted: matchStats.awayCompletedPasses, awayWrong: matchStats.awayWrongPasses, title: Strings.MatchStatsPasses)
    cornersViewModel = MatchStatsAbsoluteViewModel(home: matchStats.homeCorners, away: matchStats.awayCorners, type: .Corner)
    tacklesViewModel = MatchStatsCompletedViewModel(homeCompleted: matchStats.homeCompletedTackles, homeWrong: matchStats.homeWrongTackles, awayCompleted: matchStats.awayCompletedTackles, awayWrong: matchStats.awayWrongTackles, title: Strings.MatchStatsTackles)
    yellowCardsViewModel = MatchStatsAbsoluteViewModel(home: matchStats.homeYellowCards, away: matchStats.awayYellowCards, type: .YellowCard)
    redCardsViewModel = MatchStatsAbsoluteViewModel(home: matchStats.homeRedCards, away: matchStats.awayRedCards, type: .RedCard)
    crossesViewModel = MatchStatsCompletedViewModel(homeCompleted: matchStats.homeCompletedCrosses, homeWrong: matchStats.homeWrongCrosses, awayCompleted: matchStats.awayCompletedCrosses, awayWrong: matchStats.awayWrongCrosses, title: Strings.MatchStatsCrosses)
    throughBallsViewModel = MatchStatsCompletedViewModel(homeCompleted: matchStats.homeCompletedThroughBalls, homeWrong: matchStats.homeWrongThroughBalls, awayCompleted: matchStats.awayCompletedThroughBalls, awayWrong: matchStats.awayWrongThroughBalls, title: Strings.MatchStatsThroughBalls)
    penaltiesViewModel = MatchStatsAbsoluteViewModel(home: matchStats.homePenalties, away: matchStats.awayPenalties, type: .Penalty)
  }
}
