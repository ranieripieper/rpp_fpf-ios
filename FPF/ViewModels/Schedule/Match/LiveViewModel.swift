//
//  LiveViewModel.swift
//  FPF
//
//  Created by Gilson Gil on 1/26/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation

struct LiveViewModel {
  let lives: [[LiveCellViewModel]]
  
  init(lives: [Live]) {
    let grouped = lives.uniquelyGroupBy { $0.sameGroupAs($1) }
    let groups = grouped.map { $0.sort {$0.id > $1.id}.flatMap { LiveCellViewModel(live: $0) } }
    
    self.lives = groups
  }
}
