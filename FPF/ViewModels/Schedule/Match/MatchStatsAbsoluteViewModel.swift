//
//  MatchStatsAbsoluteViewModel.swift
//  FPF
//
//  Created by Gilson Gil on 3/2/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import UIKit

enum MatchStatsAbsoluteType {
  case Foul, Corner, YellowCard, RedCard, Penalty
  
  func title() -> String {
    switch self {
    case .Foul:
      return Strings.MatchStatsFouls
    case .Corner:
      return Strings.MatchStatsCorners
    case .YellowCard:
      return Strings.MatchStatsYellowCards
    case .RedCard:
      return Strings.MatchStatsRedCards
    case .Penalty:
      return Strings.MatchStatsPenalties
    }
  }
  
  func image() -> UIImage? {
    switch self {
    case .Foul:
      return UIImage.icnFouls()
    case .Corner:
      return UIImage.icnCorners()
    case .YellowCard:
      return UIImage.icnYellowCard()
    case .RedCard:
      return UIImage.icnRedCard()
    case .Penalty:
      return UIImage.icnPenalties()
    }
  }
}

struct MatchStatsAbsoluteViewModel {
  let home: String
  let away: String
  private let type: MatchStatsAbsoluteType
  var title: String {
    return type.title()
  }
  var image: UIImage? {
    return type.image()
  }
  
  init(home: Int?, away: Int?, type: MatchStatsAbsoluteType) {
    if let home = home {
      self.home = String(home)
    } else {
      self.home = "-"
    }
    if let away = away {
      self.away = String(away)
    } else {
      self.away = "-"
    }
    self.type = type
  }
}
