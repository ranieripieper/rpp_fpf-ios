//
//  LineupViewModel.swift
//  FPF
//
//  Created by Gilson Gil on 1/26/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation

struct LineupViewModel {
  let lineups: [LineupCellViewModel]
  
  init(lineup: Lineup, match: Match) {
    let homePlayers = lineup.lineupPlayers.0
    let awayPlayers = lineup.lineupPlayers.1
    let mostPlayers: [LineupPlayer]
    if homePlayers.count >= awayPlayers.count {
      mostPlayers = homePlayers
    } else {
      mostPlayers = awayPlayers
    }
    
    var lineups = [LineupCellViewModel]()
    for i in 0..<mostPlayers.count + 1 {
      var homePlayer: LineupPlayer? = nil
      var awayPlayer: LineupPlayer? = nil
      if i < homePlayers.count {
        homePlayer = homePlayers[i]
        let goals = match.goals?.filter {
          $0.teamName == match.homeTeamName && $0.playerName == homePlayer?.name
        }
        let yellowCard = match.cards.filter {
          let sameTeamName = $0.teamName == match.homeTeamName
          let sameName = $0.playerName == homePlayer!.name
          let yellowCard = $0.type == .YellowCard
          return sameTeamName && sameName && yellowCard
        }
        let redCard = match.cards.filter {
          let sameTeamName = $0.teamName == match.homeTeamName
          let sameName = $0.playerName == homePlayer!.name
          let redCard = $0.type == .RedCard
          return sameTeamName && sameName && redCard
        }
        homePlayer = LineupPlayer(lineupPlayer: homePlayer!, goals: goals?.count ?? 0, yellowCard: yellowCard.count > 0, redCard: redCard.count > 0)
      } else if let homeManager = match.homeManager where i == homePlayers.count {
        homePlayer = LineupPlayer(managerName: homeManager)
      }
      if i < awayPlayers.count {
        awayPlayer = awayPlayers[i]
        let goals = match.goals?.filter {
          $0.teamName == match.awayTeamName && $0.playerName == awayPlayer?.name
        }
        let yellowCard = match.cards.filter {
          let sameTeamName = $0.teamName == match.awayTeamName
          let sameName = $0.playerName == awayPlayer?.name
          let yellowCard = $0.type == .YellowCard
          return sameTeamName && sameName && yellowCard
        }
        let redCard = match.cards.filter {
          let sameTeamName = $0.teamName == match.awayTeamName
          let sameName = $0.playerName == awayPlayer?.name
          let redCard = $0.type == .RedCard
          return sameTeamName && sameName && redCard
        }
        awayPlayer = LineupPlayer(lineupPlayer: awayPlayer!, goals: goals?.count ?? 0, yellowCard: yellowCard.count > 0, redCard: redCard.count > 0)
      } else if let awayManager = match.awayManager where i == awayPlayers.count {
        awayPlayer = LineupPlayer(managerName: awayManager)
      }
      lineups.append(LineupCellViewModel(homePlayer: homePlayer, awayPlayer: awayPlayer))
    }
    self.lineups = lineups
  }
}
