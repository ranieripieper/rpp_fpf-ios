//
//  MatchStatsCompletedViewModel.swift
//  FPF
//
//  Created by Gilson Gil on 3/2/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation

struct MatchStatsCompletedViewModel {
  let homeCompleted: String
  let homeWrong: String
  let awayCompleted: String
  let awayWrong: String
  let title: String
  
  init?(homeCompleted: Int?, homeWrong: Int?, awayCompleted: Int?, awayWrong: Int?, title: String?) {
    guard let title = title else {
      return nil
    }
    if let homeCompleted = homeCompleted {
      self.homeCompleted = String(homeCompleted)
    } else {
      self.homeCompleted = "-"
    }
    if let homeWrong = homeWrong {
      self.homeWrong = String(homeWrong)
    } else {
      self.homeWrong = "-"
    }
    if let awayCompleted = awayCompleted {
      self.awayCompleted = String(awayCompleted)
    } else {
      self.awayCompleted = "-"
    }
    if let awayWrong = awayWrong {
      self.awayWrong = String(awayWrong)
    } else {
      self.awayWrong = "-"
    }
    self.title = title
  }
}
