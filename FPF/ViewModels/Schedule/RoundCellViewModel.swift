//
//  RoundCellViewModel.swift
//  FPF
//
//  Created by Gilson Gil on 1/12/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation

struct RoundCellViewModel {
  let roundName: String
  let matches: [Match]
  
  init(roundName: String, matches: [Match]) {
    self.roundName = roundName
    self.matches = matches
  }
}
