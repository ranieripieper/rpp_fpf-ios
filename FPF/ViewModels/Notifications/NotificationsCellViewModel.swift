//
//  NotificationsCellViewModel.swift
//  FPF
//
//  Created by Gilson Gil on 1/13/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation

struct NotificationsCellViewModel {
  let logoURLString: String
  let name: String
  var switchOn: Bool {
    didSet {
      if switchOn {
        ParseHelper.registerPushForChannel(name)
      } else {
        ParseHelper.deregisterPushForChannel(name)
      }
    }
  }
  
  init(logoURLString: String, name: String, switchOn: Bool) {
    self.logoURLString = logoURLString
    self.name = name
    self.switchOn = switchOn
  }
  
  init(logoURLString: String?, teamId: Int?, name: String, switchOn: Bool) {
    self.name = name
    self.switchOn = switchOn
    self.logoURLString = logoURLString ?? ""
  }
}
