//
//  NotificationsViewModel.swift
//  FPF
//
//  Created by Gilson Gil on 1/13/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation

protocol NotificationsViewModelDelegate: class {
  func didChange()
}

final class NotificationsViewModel {
  var onNotifications = [NotificationsCellViewModel]()
  var offNotifications = [NotificationsCellViewModel]()
  weak var delegate: NotificationsViewModelDelegate?
  
  init() {
    if NSUserDefaults.standardUserDefaults().boolForKey(Constants.hasPresentedNotificationsSettingsString) {
      fetchTeams()
    } else {
      Team.fetchTeams { [weak self] in
        self?.fetchTeams()
      }
    }
  }
  
  func fetchTeams() {
    Team.pushTeams {
      let currentChannels = ParseHelper.currentChannels()
      let teams = $0.sort { $0.name! < $1.name! }
      self.offNotifications = teams.filter { !currentChannels.contains($0.name!.parseChannelFormat()) }.map {
        NotificationsCellViewModel(logoURLString: $0.logoURLString!, name: $0.name!, switchOn: false)
      }
      self.onNotifications = teams.filter { currentChannels.contains($0.name!.parseChannelFormat()) }.map {
        NotificationsCellViewModel(logoURLString: $0.logoURLString!, name: $0.name!, switchOn: true)
      }
      self.delegate?.didChange()
    }
  }
  
  func newIndexAfterOnAtIndex(index: Int) -> Int? {
    guard index < offNotifications.count else {
      return nil
    }
    
    var cellViewModel = offNotifications[index]
    offNotifications.removeAtIndex(index)
    cellViewModel.switchOn = true
    
    onNotifications.append(cellViewModel)
    onNotifications.sortInPlace { $0.name < $1.name }
    let newIndex = onNotifications.indexOf { $0.name == cellViewModel.name }
    
    return newIndex
  }
  
  func newIndexAfterOffAtIndex(index: Int) -> Int? {
    guard index < onNotifications.count else {
      return nil
    }
    
    var cellViewModel = onNotifications[index]
    onNotifications.removeAtIndex(index)
    cellViewModel.switchOn = false
    
    offNotifications.append(cellViewModel)
    offNotifications.sortInPlace { $0.name < $1.name }
    let newIndex = offNotifications.indexOf { $0.name == cellViewModel.name }
    
    return newIndex
  }
  
  #if DEBUG
  deinit {
    print("deinit", self)
  }
  #endif
}
