//
//  LeagueViewModel.swift
//  FPF
//
//  Created by Gilson Gil on 1/7/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation

struct LeagueViewModel {
  var leagueId: Int
  var leagueName: String
  var leagueNewsId: Int
  let currentRound: Int
  var needsUpdateLeague = false
  var hasStandings = false
  
  let tabbarTitles = [Strings.LeagueStandings, Strings.LeagueSchedule, Strings.LeagueScorers, Strings.LeagueNews]
  
  static func viewModel(leagueId: Int?, completion: (inner: () throws -> LeagueViewModel) -> ()) {
    League.leagueWithId(leagueId) { inner in
      do {
        let league = try inner()
        let leagueViewModel = LeagueViewModel(league: league)
        completion { return leagueViewModel }
      } catch {
        completion { throw error }
      }
    }
  }
  
  init(league: League) {
    leagueId = league.id ?? 0
    leagueName = league.leagueName ?? league.name ?? ""
    leagueNewsId = league.newsLeagueId ?? 0
    currentRound = league.currentRound ?? 1
    hasStandings = league.hasStandings ?? false
  }
  
  init(leagueId: Int, leagueName: String) {
    self.leagueId = leagueId
    self.leagueName = leagueName
    self.leagueNewsId = leagueId
    currentRound = 1
    needsUpdateLeague = true
    hasStandings = false
  }
  
  func refreshTeams(completion: () -> ()) {
    Team.shouldRefreshTeams {
      if $0 {
        Team.teams {
          completion()
        }
      } else {
        completion()
      }
    }
  }
  
  static func setLastLeagueOpened(leagueId: Int, name: String) {
    NSUserDefaults.standardUserDefaults().setInteger(leagueId, forKey: "LastLeagueOpenedId")
    NSUserDefaults.standardUserDefaults().setObject(name, forKey: "LastLeagueOpenedName")
    NSUserDefaults.standardUserDefaults().synchronize()
  }
  
  static func lastLeagueOpened() -> Int? {
    guard let leagueId = NSUserDefaults.standardUserDefaults().objectForKey("LastLeagueOpenedId") as? Int else {
      return nil
    }
    return leagueId
  }
}
