//
//  ScorersCellViewModel.swift
//  FPF
//
//  Created by Gilson Gil on 1/14/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation

struct ScorersCellViewModel {
  let position: String
  let teamLogoURL: String
  let playerName: String
  let teamName: String
  let goals: String
  
  init(scorer: Scorer) {
    if scorer.position > 0 {
      position = String(scorer.position) + "º"
    } else {
      position = "-"
    }
    teamLogoURL = scorer.teamURLString
    playerName = scorer.playerName ?? ""
    teamName = scorer.teamName ?? ""
    goals = String(scorer.total ?? 0)
  }
}
