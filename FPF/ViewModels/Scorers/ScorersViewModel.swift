//
//  ScorersViewModel.swift
//  FPF
//
//  Created by Gilson Gil on 1/20/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation

struct ScorersViewModel {
  let scorers: [ScorersCellViewModel]
  
  init(scorers: [Scorer]) {
    let viewModelScorers = scorers.map { ScorersCellViewModel(scorer: $0) }
    self.scorers = viewModelScorers
  }
  
  static func scorersForLeagueId(leagueId: Int, completion: (scorersViewModel: ScorersViewModel?) -> ()) {
    Scorer.scorersForLeagueId(leagueId) { inner in
      do {
        let scorersViewModel = ScorersViewModel(scorers: try inner())
        completion(scorersViewModel: scorersViewModel)
      } catch {
        completion(scorersViewModel: nil)
      }
    }
  }
}
