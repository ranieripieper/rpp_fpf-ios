//
//  NewsListViewModel.swift
//  FPF
//
//  Created by Gilson Gil on 5/5/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation

struct NewsListViewModel {
  let networking = DDSNetworking(baseURL: "http://www.fpf.org.br/apinoticias/")
  let leagueId: Int
  let news: [News]
  let configurators: [CellConfigurator<NewsCell>]
  
  init(leagueId: Int, news: [News]) {
    self.leagueId = leagueId
    self.news = news
    configurators = news.flatMap {
      CellConfigurator<NewsCell>(viewModel: NewsCellViewModel(title: $0.title, date: $0.date, imageURL: $0.imageURL))
    }
  }
  
  static func viewModel(leagueId: Int, completion: (inner: () throws -> NewsListViewModel) -> ()) -> NewsListViewModel {
    let viewModel = NewsListViewModel(leagueId: leagueId, news: [])
    let request = viewModel.networking.clientURLRequest("campeonato/\(leagueId)")
    viewModel.networking.get(request) { success, object in
      guard let jsons = object as? [[String: AnyObject]] else {
        return
      }
      let news = jsons.flatMap { News(json: $0) }
      let newViewModel = NewsListViewModel(leagueId: leagueId, news: news)
      completion { return newViewModel }
    }
    return viewModel
  }
}
