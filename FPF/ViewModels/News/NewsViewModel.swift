//
//  NewsViewModel.swift
//  FPF
//
//  Created by Gilson Gil on 5/5/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation

struct NewsViewModel {
  let title: String
  let date: String
  let imageURL: String?
  let url: String
  let content: String?
  
  let networking = DDSNetworking(baseURL: "")
  
  init(title: String, date: String, imageURL: String?, url: String, content: String?) {
    self.title = title
    self.date = date
    self.imageURL = imageURL
    self.url = url
    self.content = content
  }
  
  func getContent(completion: (inner: () throws -> NewsViewModel) -> ()) {
    let request = networking.clientURLRequest(url)
    networking.get(request) { success, object in
      guard let json = object as? [String: AnyObject] else {
        return
      }
      let newViewModel = NewsViewModel(title: self.title, date: self.date, imageURL: self.imageURL, url: self.url, content: json["texto"] as? String)
      completion { return newViewModel }
    }
  }
}
