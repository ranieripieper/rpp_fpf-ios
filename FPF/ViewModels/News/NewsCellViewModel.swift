//
//  NewsCellViewModel.swift
//  FPF
//
//  Created by Gilson Gil on 5/5/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation

struct NewsCellViewModel {
  let title: String
  let date: String
  let imageURL: String?
}
