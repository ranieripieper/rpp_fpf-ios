//
//  StandingsViewModel.swift
//  FPF
//
//  Created by Gilson Gil on 1/8/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation

struct StandingsViewModel {
  let phases: [StandingPhaseCellViewModel]
  
  init(phases: [Phase]) {
    self.phases = phases.map { StandingPhaseCellViewModel(name: $0.name, groups: $0.groups) }
  }
  
  static func standingsForLeagueId(leagueId: Int, completion: (standingsViewModel: StandingsViewModel?) -> ()) {
    Standing.standingsForLeagueId(leagueId) { inner in
      do {
        let phases = try inner()
        let standingsViewModel = StandingsViewModel(phases: phases)
        completion(standingsViewModel: standingsViewModel)
      } catch {
        completion(standingsViewModel: nil)
      }
    }
  }
}
