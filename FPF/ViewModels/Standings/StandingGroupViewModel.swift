//
//  StandingGroupViewModel.swift
//  FPF
//
//  Created by Gilson Gil on 1/28/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation

struct StandingGroupViewModel {
  let name: String
  let standings: [StandingCellViewModel]
  
  init(name: String, standings: [Standing]) {
    self.name = name
    self.standings = standings.map { StandingCellViewModel(standing: $0) }
  }
}
