//
//  StandingPhaseCellViewModel.swift
//  FPF
//
//  Created by Gilson Gil on 1/28/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation

struct StandingPhaseCellViewModel {
  let name: String
  let groups: [StandingGroupViewModel]
  
  init(name: String, groups: [Group]) {
    self.name = name
    self.groups = groups.map { StandingGroupViewModel(name: $0.name, standings: $0.standings) }
  }
}

