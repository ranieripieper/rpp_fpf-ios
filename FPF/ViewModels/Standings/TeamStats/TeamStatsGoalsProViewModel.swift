//
//  TeamStatsGoalsProViewModel.swift
//  FPF
//
//  Created by Gilson Gil on 3/2/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation

struct Goal {
  let name: String
  let total: Int
  
  init?(json: [String: AnyObject]) {
    guard let goals = json["Gols"] as? Int, let name = json["Jogador"] as? String else {
      return nil
    }
    self.name = name
    self.total = goals
  }
}

struct TeamStatsGoalsProViewModel {
  let items: [CellConfiguratorType]
  let total: String
  
  init(goals: [Goal]) {
    self.items = goals.flatMap {
      if let viewModel = TeamStatsGoalsProCellViewModel(name: $0.name, goals: $0.total) {
        return CellConfigurator<TeamStatsGoalProCell>(viewModel: viewModel)
      } else {
        return nil
      }
    }
    var total = 0
    goals.forEach {
      total += $0.total
    }
    self.total = String(total)
  }
}
