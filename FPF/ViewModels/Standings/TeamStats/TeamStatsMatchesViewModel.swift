//
//  TeamStatsMatchesViewModel.swift
//  FPF
//
//  Created by Gilson Gil on 3/2/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation

struct TeamStatsMatchesViewModel {
  let homeMatchesCount: String
  let awayMatchesCount: String
  let homeMatches: [CellConfiguratorType]
  let awayMatches: [CellConfiguratorType]
  
  init?(home: Int?, homeMatches: [[String: AnyObject]]?, away: Int?, awayMatches: [[String: AnyObject]]?, teamLogo: String, teamLogos: [Int: (String, Bool)]?) {
    if let home = home {
      homeMatchesCount = String(home)
    } else {
      homeMatchesCount = "-"
    }
    if let away = away {
      awayMatchesCount = String(away)
    } else {
      awayMatchesCount = "-"
    }
    if let homeMatches = homeMatches {
      self.homeMatches = homeMatches.flatMap {
        let url: String
        if let teamLogos = teamLogos, teamId = $0["IdEquipe"] as? Int, tuple = teamLogos[teamId] {
          url = tuple.0
        } else {
          url = ""
        }
        if let homeScore = $0["GolsPro"] as? Int, awayScore = $0["GolsContra"] as? Int, let viewModel = TeamStatsMatchViewModel(homeURL: teamLogo, homeScore: homeScore, awayURL: url, awayScore: awayScore) {
          return CellConfigurator<TeamStatsMatchCell>(viewModel: viewModel)
        } else {
          return nil
        }
      }
    } else {
      self.homeMatches = []
    }
    if let awayMatches = awayMatches {
      self.awayMatches = awayMatches.flatMap {
        let url: String
        if let teamLogos = teamLogos, teamId = $0["IdEquipe"] as? Int, tuple = teamLogos[teamId] {
          url = tuple.0
        } else {
          url = ""
        }
        if let homeScore = $0["GolsContra"] as? Int, awayScore = $0["GolsPro"] as? Int, let viewModel = TeamStatsMatchViewModel(homeURL: url, homeScore: homeScore, awayURL: teamLogo, awayScore: awayScore) {
          return CellConfigurator<TeamStatsMatchCell>(viewModel: viewModel)
        } else {
          return nil
        }
      }
    } else {
      self.awayMatches = []
    }
  }
}
