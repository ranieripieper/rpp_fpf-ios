//
//  TeamStatsViewModel.swift
//  FPF
//
//  Created by Gilson Gil on 3/2/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation

struct TeamStatsViewModel {
  let teamId: Int
  let logoURL: String
  let teamName: String
  let points: String
  let homePoints: String
  let awayPoints: String
  let teamStatsMatchesViewModel: TeamStatsMatchesViewModel?
  let teamStatsGoalsProViewModel: TeamStatsGoalsProViewModel?
  let teamStatsGoalsAgainstViewModel: TeamStatsGoalsAgainstViewModel?
  let teamStatsStaffViewModel: TeamStatsStaffViewModel?
  
  var items: [(String, CellConfiguratorType)] {
    var items = [(String, CellConfiguratorType)]()
    if let teamStatsMatchesViewModel = teamStatsMatchesViewModel {
      items.append((Strings.TeamStatsMatches, CellConfigurator<TeamStatsMatchesCell>(viewModel: teamStatsMatchesViewModel)))
    }
    if let teamStatsGoalsProViewModel = teamStatsGoalsProViewModel {
      items.append((Strings.TeamStatsGoals, CellConfigurator<TeamStatsGoalsProCell>(viewModel: teamStatsGoalsProViewModel)))
    }
    if let teamStatsGoalsAgainstViewModel = teamStatsGoalsAgainstViewModel {
      items.append((Strings.TeamStatsGoalsAgainst, CellConfigurator<TeamStatsGoalsAgainstCell>(viewModel: teamStatsGoalsAgainstViewModel)))
    }
    if let teamStatsStaffViewModel = teamStatsStaffViewModel {
      items.append((Strings.TeamStatsStaff, CellConfigurator<TeamStatsStaffCell>(viewModel: teamStatsStaffViewModel)))
    }
    return items
  }
  
  init(teamStats: TeamStats, teamId: Int, teamLogos: [Int: (String, Bool)]?) {
    self.teamId = teamId
    logoURL = teamStats.logoURL
    teamName = teamStats.teamName ?? ""
    var points = 0
    if let homePoints = teamStats.homePoints {
      self.homePoints = String(homePoints)
      points += homePoints
    } else {
      self.homePoints = "0"
    }
    if let awayPoints = teamStats.awayPoints {
      self.awayPoints = String(awayPoints)
      points += awayPoints
    } else {
      self.awayPoints = "0"
    }
    self.points = String(points)
    teamStatsMatchesViewModel = TeamStatsMatchesViewModel(home: teamStats.homeMatchesCount, homeMatches: teamStats.homeMatches, away: teamStats.awayMatchesCount, awayMatches: teamStats.awayMatches, teamLogo: teamStats.logoURL, teamLogos: teamLogos)
    if let goals = teamStats.goals {
      let goals = goals.flatMap { Goal(json: $0) }
      let sorted = goals.sort {
        switch ($0.total,$1.total) {
        case let (lhs,rhs) where lhs == rhs:
          return $0.name < $1.name
        case let (lhs, rhs):
          return lhs > rhs
        }
      }
      teamStatsGoalsProViewModel = TeamStatsGoalsProViewModel(goals: sorted)
    } else {
      teamStatsGoalsProViewModel = nil
    }
    if let goalsAgainst = teamStats.goalsAgainst {
      let goalsAgainst = goalsAgainst.flatMap { GoalAgainst(json: $0, teamLogos: teamLogos) }
      let sorted = goalsAgainst.sort {
        switch ($0.total,$1.total) {
        case let (lhs,rhs) where lhs == rhs:
          return $0.teamName < $1.teamName
        case let (lhs, rhs):
          return lhs > rhs
        }
      }
      teamStatsGoalsAgainstViewModel = TeamStatsGoalsAgainstViewModel(goals: sorted)
    } else {
      teamStatsGoalsAgainstViewModel = nil
    }
    teamStatsStaffViewModel = TeamStatsStaffViewModel(staff: teamStats.staff.flatMap {Staff($0)})
  }
  
  static func create(teamId: Int, completion: TeamStatsViewModel? -> ()) {
    TeamStats.statsForTeamId(teamId) {
      do {
        let teamStats = try $0()
        Team.allLogosAndHasStats(nil) { dict in
          completion(TeamStatsViewModel(teamStats: teamStats, teamId: teamId, teamLogos: dict))
        }
      } catch {
        completion(nil)
      }
    }
  }
  
  static func refresh(teamId: Int, completion: TeamStatsViewModel? -> ()) {
    TeamStats.fetchStatsForTeamId(teamId) {
      do {
        let teamStats = try $0()
        Team.allLogosAndHasStats(nil) { dict in
          completion(TeamStatsViewModel(teamStats: teamStats, teamId: teamId, teamLogos: dict))
        }
      } catch {
        completion(nil)
      }
    }
  }
}
