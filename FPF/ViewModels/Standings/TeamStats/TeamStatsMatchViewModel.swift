//
//  TeamStatsMatchViewModel.swift
//  FPF
//
//  Created by Gilson Gil on 3/4/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation

struct TeamStatsMatchViewModel {
  let homeURL: String?
  let homeScore: String
  let awayURL: String?
  let awayScore: String
  
  init?(homeURL: String?, homeScore: Int?, awayURL: String?, awayScore: Int?) {
    guard let homeScore = homeScore, let awayScore = awayScore else {
      return nil
    }
    self.homeURL = homeURL
    self.homeScore = String(homeScore)
    self.awayURL = awayURL
    self.awayScore = String(awayScore)
  }
}
