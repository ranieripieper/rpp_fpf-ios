//
//  TeamStatsGoalsAgainstCellViewModel.swift
//  FPF
//
//  Created by Gilson Gil on 3/2/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation

struct TeamStatsGoalsAgainstCellViewModel {
  let name: String
  let logoURL: String
  let goals: String
  
  init?(name: String?, goals: Int?, logoURL: String) {
    guard let name = name, let goals = goals else {
      return nil
    }
    self.name = name
    self.goals = String(goals)
    self.logoURL = logoURL
  }
}
