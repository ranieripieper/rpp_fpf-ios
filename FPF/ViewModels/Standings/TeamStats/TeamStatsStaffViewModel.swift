//
//  TeamStatsStaffViewModel.swift
//  FPF
//
//  Created by Gilson Gil on 3/2/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation

struct TeamStatsStaffViewModel {
  let items: [CellConfiguratorType]
  
  init(staff: [Staff]) {
    self.items = staff.flatMap {
      CellConfigurator<StaffCell>(viewModel: StaffCellViewModel(staff: $0))
    }
  }
}
