//
//  TeamStatsGoalsAgainstViewModel.swift
//  FPF
//
//  Created by Gilson Gil on 3/2/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation

struct GoalAgainst {
  let teamName: String
  private let teamId: Int
  let teamLogoURL: String
  let total: Int
  
  init?(json: [String: AnyObject], teamLogos: [Int: (String, Bool)]?) {
    guard let goals = json["Gols"] as? Int, let name = json["Equipe"] as? String, let teamId = json["EquipeId"] as? Int else {
      return nil
    }
    self.teamId = teamId
    self.teamName = name
    self.total = goals
    if let teamLogos = teamLogos, tuple = teamLogos[teamId] {
      self.teamLogoURL = tuple.0
    } else {
      self.teamLogoURL = ""
    }
  }
}

struct TeamStatsGoalsAgainstViewModel {
  let items: [CellConfiguratorType]
  let total: String
  
  init(goals: [GoalAgainst]) {
    self.items = goals.flatMap {
      if let viewModel = TeamStatsGoalsAgainstCellViewModel(name: $0.teamName, goals: $0.total, logoURL: $0.teamLogoURL) {
        return CellConfigurator<TeamStatsGoalAgainstCell>(viewModel: viewModel)
      } else {
        return nil
      }
    }
    var total = 0
    goals.forEach {
      total += $0.total
    }
    self.total = String(total)
  }
}
