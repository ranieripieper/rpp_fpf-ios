//
//  StandingCellViewModel.swift
//  FPF
//
//  Created by Gilson Gil on 1/8/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation

struct StandingCellViewModel {
  let position: String
  var imageURL: String
  let name: String
  let points: String
  let played: String
  let wins: String
  let draws: String
  let losses: String
  let goalDifference: String
  let hasTeamStats: Bool
  let teamId: Int
  
  init(standing: Standing) {
    position = String(standing.position ?? 0)
    imageURL = standing.logoURLString
    name = standing.teamName ?? ""
    points = String(standing.points ?? 0)
    played = String(standing.played ?? 0)
    wins = String(standing.wins ?? 0)
    draws = String(standing.draws ?? 0)
    losses = String(standing.losses ?? 0)
    goalDifference = String(standing.goalDifference ?? 0)
    hasTeamStats = standing.teamHasStats && standing.leagueId == Constants.a1Id
    teamId = standing.teamId ?? 0
  }
}
