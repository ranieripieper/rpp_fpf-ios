//
//  Strings.swift
//  FPF
//
//  Created by Gilson Gil on 1/19/16.
//  Copyright © 2016 doisdoissete. All rights reserved.
//

import Foundation

struct Strings {
  static let MenuManageNotifications = NSLocalizedString("MENU_MANAGE_NOTIFICATIONS", comment: "Manage Notifications button title")
  static let MenuAbout = NSLocalizedString("MENU_ABOUT", comment: "About button title")
  static let MenuAboutThis = NSLocalizedString("MENU_ABOUT_THIS", comment: "About this button title")
  static let MenuVersion = NSLocalizedString("MENU_VERSION", comment: "Version label")
  static let MenuLeagueA1Name = NSLocalizedString("MENU_LEAGUE_A1_NAME", comment: "A1 league name")
  static let MenuLeagueA2Name = NSLocalizedString("MENU_LEAGUE_A2_NAME", comment: "A2 league name")
  static let MenuLeagueA3Name = NSLocalizedString("MENU_LEAGUE_A3_NAME", comment: "A3 league name")
  static let MenuLeagueSegundaName = NSLocalizedString("MENU_LEAGUE_SEGUNDA_NAME", comment: "Paulista second division league name")
  static let MenuLeagueLibertadoresName = NSLocalizedString("MENU_LEAGUE_LIBERTADORES_NAME", comment: "Libertadores league name")
  static let MenuGuideName = NSLocalizedString("MENU_GUIDEBUTTON_NAME", comment: "Guide name")
  
  static let LeagueStandings = NSLocalizedString("LEAGUE_STANDINGS", comment: "Standings tabbar button title")
  static let LeagueSchedule = NSLocalizedString("LEAGUE_SCHEDULE", comment: "Schedule tabbar button title")
  static let LeagueScorers = NSLocalizedString("LEAGUE_SCORERS", comment: "Scorers tabbar button title")
  static let LeagueNews = NSLocalizedString("LEAGUE_NEWS", comment: "News tabbar button title")
  
  static let TeamStatsPoints = NSLocalizedString("TEAMSTATS_POINTS", comment: "Team stats points label")
  static let TeamStatsAway = NSLocalizedString("TEAMSTATS_AWAY", comment: "Team stats away label")
  static let TeamStatsHome = NSLocalizedString("TEAMSTATS_HOME", comment: "Team stats home label")
  static let TeamStatsMatches = NSLocalizedString("TEAMSTATS_MATCHES", comment: "Team stats matches label")
  static let TeamStatsGoals = NSLocalizedString("TEAMSTATS_GOALS", comment: "Team stats goals label")
  static let TeamStatsTotalGoals = NSLocalizedString("TEAMSTATS_TOTALGOALS", comment: "Team stats total goals label")
  static let TeamStatsGoalsAgainst = NSLocalizedString("TEAMSTATS_GOALSAGAINST", comment: "Team stats goals against label")
  static let TeamStatsStaff = NSLocalizedString("TEAMSTATS_STAFF", comment: "Team stats staff label")
  
  static let MatchDate = NSLocalizedString("MATCH_DATE", comment: "Match date and time label")
  static let MatchPlace = NSLocalizedString("MATCH_PLACE", comment: "Match place label")
  static let MatchReferee = NSLocalizedString("MATCH_REFEREE", comment: "Match referee label")
  static let MatchCity = NSLocalizedString("MATCH_CITY", comment: "Match city label")
  static let MatchLive = NSLocalizedString("MATCH_LIVE", comment: "Match live tabbar button title")
  static let MatchLineup = NSLocalizedString("MATCH_LINEUP", comment: "Match lineup tabbar button title")
  static let MatchStats = NSLocalizedString("MATCH_STATS", comment: "Match stats tabbar button title")
  static let MatchRedCard = NSLocalizedString("MATCH_REDCARD", comment: "Match red card")
  static let MatchYellowCard = NSLocalizedString("MATCH_YELLOWCARD", comment: "Match yellow card")
  static let MatchFinished = NSLocalizedString("MATCH_FINISHED", comment: "Match finished")
  static let MatchNotStarted = NSLocalizedString("MATCH_NOT_STARTED", comment: "Match not started")
  
  static let MatchStatsPossession = NSLocalizedString("MATCHSTATS_POSSESSION", comment: "Match stats possession title")
  static let MatchStatsShots = NSLocalizedString("MATCHSTATS_SHOTS", comment: "Match stats shots title")
  static let MatchStatsFouls = NSLocalizedString("MATCHSTATS_FOULS", comment: "Match stats fouls title")
  static let MatchStatsPasses = NSLocalizedString("MATCHSTATS_PASSES", comment: "Match stats passes title")
  static let MatchStatsCorners = NSLocalizedString("MATCHSTATS_CORNERS", comment: "Match stats corners title")
  static let MatchStatsTackles = NSLocalizedString("MATCHSTATS_TACKLES", comment: "Match stats tackles title")
  static let MatchStatsYellowCards = NSLocalizedString("MATCHSTATS_YELLOWCARDS", comment: "Match stats yellow cards title")
  static let MatchStatsRedCards = NSLocalizedString("MATCHSTATS_REDCARDS", comment: "Match stats red cards title")
  static let MatchStatsCrosses = NSLocalizedString("MATCHSTATS_CROSSES", comment: "Match stats crosses title")
  static let MatchStatsThroughBalls = NSLocalizedString("MATCHSTATS_THROUGHBALLS", comment: "Match stats through balls title")
  static let MatchStatsPenalties = NSLocalizedString("MATCHSTATS_PENALTIES", comment: "Match stats penalties title")
  
  static let NotificationsTitle = NSLocalizedString("NOTIFICATIONS_TITLE", comment: "Notifications nav bar title")
  static let NotificationsHeader = NSLocalizedString("NOTIFICATIONS_HEADER", comment: "Notifications header label")
  static let NotificationsTeamHeader = NSLocalizedString("NOTIFICATIONS_TEAMS_HEADER", comment: "Notifications teams header label")
  static let NotificationsSaveTeams = NSLocalizedString("NOTIFICATIONS_SAVETEAMS", comment: "Notifications save teams button title")
  static let NotificationsChooseLater = NSLocalizedString("NOTIFICATIONS_CHOOSELATER", comment: "Notifications choose later button title")
  
  static let GuideTitle = NSLocalizedString("GUIDE_TITLE", comment: "Guide nav bar title")
  static let GuideAbout = NSLocalizedString("GUIDE_ABOUT", comment: "Guide about text")
  static let GuideSearchBarPlaceholder = NSLocalizedString("GUIDE_SEARCHBARPLACEHOLDER", comment: "Guide about text")
  static let GuideMascot = NSLocalizedString("GUIDE_MASCOT", comment: "Guide mascot description label")
  static let GuideUniformMaker = NSLocalizedString("GUIDE_UNIFORMMAKER", comment: "Guide uniform maker description label")
  static let GuideStaff = NSLocalizedString("GUIDE_STAFF", comment: "Guide staff label")
  static let GuideManager = NSLocalizedString("GUIDE_MANAGER", comment: "Guide manager label")
  static let GuideStadium = NSLocalizedString("GUIDE_STADIUM", comment: "Guide stadium label")
  static let GuideStadiumName = NSLocalizedString("GUIDE_STADIUMNAME", comment: "Guide stadium name label")
  static let GuideStadiumCapacity = NSLocalizedString("GUIDE_STADIUMCAPACITY", comment: "Guide stadium capacity label")
  static let GuideStadiumEstablishment = NSLocalizedString("GUIDE_STADIUMESTABLISHMENT", comment: "Guide stadium establishment label")
  static let GuideCity = NSLocalizedString("GUIDE_CITY", comment: "Guide city label")
  static let GuideArea = NSLocalizedString("GUIDE_AREA", comment: "Guide area label")
  static let GuidePopulation = NSLocalizedString("GUIDE_POPULATION", comment: "Guide population label")
  static let GuideDensity = NSLocalizedString("GUIDE_DENSITY", comment: "Guide density label")
  static let GuideIDHM = NSLocalizedString("GUIDE_IDHM", comment: "Guide idhm label")
  static let GuideSource = NSLocalizedString("GUIDE_SOURCE", comment: "Guide source label")
  static let GuideIDHMSubscript = NSLocalizedString("GUIDE_IDHMSUBSCRIPT", comment: "Guide idhm subscript label")
  
  static let AboutTitle = NSLocalizedString("ABOUT_TITLE", comment: "About nav bar title")
  static let AboutText1 = NSLocalizedString("ABOUT_TEXT_1", comment: "About text 1 label")
  static let AboutText2 = NSLocalizedString("ABOUT_TEXT_2", comment: "About text 2 label")
  static let AboutText3 = NSLocalizedString("ABOUT_TEXT_3", comment: "About text 3 label")
  
  static let TutorialText = NSLocalizedString("TUTORIAL_TEXT", comment: "Tutorial text label")
}
